<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

$dir = FileExistsRecursive($APPLICATION->GetCurDir(), ".lang.php");
if(strlen($dir)>0) $APPLICATION->IncludeFile($dir);

if(!defined("LANG_ID_DEF")) define("LANG_ID_DEF", "ua");
if(!defined("LANG_ID")) define("LANG_ID", LANG_ID_DEF);
?>
<?$APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "add_review", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "3",
	"STATUS_NEW" => "ANY",
	"LIST_URL" => "",
	"USE_CAPTCHA" => "Y",
	"USER_MESSAGE_EDIT" => "�������, ��� ����� ������� ��������! ����� �������� �� ����� �����������.",
	"USER_MESSAGE_ADD" => "�������, ��� ����� ������� ��������! ����� �������� �� ����� �����������.",
	"DEFAULT_INPUT_SIZE" => "30",
	"RESIZE_IMAGES" => "N",
	"PROPERTY_CODES" => array(
		0 => "NAME",
		1 => "PREVIEW_TEXT",
		2 => "6",
	),
	"PROPERTY_CODES_REQUIRED" => array(
		0 => "NAME",
		1 => "PREVIEW_TEXT",
	),
	"GROUPS" => array(
		0 => "2",
	),
	"STATUS" => "INACTIVE",
	"ELEMENT_ASSOC" => "CREATED_BY",
	"MAX_USER_ENTRIES" => "100000",
	"MAX_LEVELS" => "100000",
	"LEVEL_LAST" => "Y",
	"MAX_FILE_SIZE" => "0",
	"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
	"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "/ru/",
	"CUSTOM_TITLE_NAME" => "�.�.�.",
	"CUSTOM_TITLE_TAGS" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
	"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
	"CUSTOM_TITLE_IBLOCK_SECTION" => "",
	"CUSTOM_TITLE_PREVIEW_TEXT" => "����� ������",
	"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
	"CUSTOM_TITLE_DETAIL_TEXT" => "",
	"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);?>