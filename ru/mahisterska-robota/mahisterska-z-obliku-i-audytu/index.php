<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ �� ����� � ������ - ��������� ��������");
$APPLICATION->SetPageProperty("description", "����� ������������ ������ �� ����� � ������ �������� ��� ���������� ����� ��� ���������� � ������ �� ������� ���� &#10003; 10 ��� ����� &#10003; 100% �������� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?><div class="wrap-bg">
	<div class="wrap">
		<h1>������������ ������ ��&nbsp; ����� � ������ �� �����</h1>
		<div>
 <br>
		</div>
		<p>
		</p>
		<p>
		</p>
		<div>
			<p>
 <span style="font-size: 14pt;"><img width="300" alt="������������ ������ �� ����� � ������ - �����������" src="/upload/medialibrary/267/oblik.jpg" height="206" align="left">������������� "���� � �����" �������� ����� �� ����� ���������� ����� ������������� ��������������. ����������, ���������� �������������� ����� � ������ ����� ��������� �� ������ ������� ����������, �� � ����������� ���������, ��������� �� ����� ������� ��������� ����� � ������ ���������� "������������" �����. �������������, ��� ������������ ������ �� ����� � ������ �� ����� �������� �������������� ������� �� ����� ��������� ������������ �����. ���� ��������� �������� ����� ���������, ��������� ��� ������ ��������. </span><span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span>
			</p>
 <span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span><span style="font-size: 14pt;">
			������� �������� ������������ ������ �� ����� � ������ �� ���������� ������� ������� � ��������, ��������������� ������ ��������� � �����. �� ������������ ���������� �� ����������� ������� ����������������, ������� ������� ������, ������� ������ �������� � �� ������. �� �����, �� ��� ����� ������������, ������� �������� ����������� �������� ������������ �� ����� � ������.</span>
			<p>
			</p>
		</div>
		<p>
		</p>
		<p>
 <br>
		</p>
		<p>
		</p>
		<h2>
		�������� � ��c������ ���� �� ��������� "�������"</h2>
		<p dir="ltr">
 <span style="font-size: 14pt;">��������� "�������" ������������� ����������� �������� ��������� ������������ �� ����� � ������&nbsp;�� �������� ����. ������� ��, �� ������ ������ ������������ ������ � ������� ������������ ����, �� ����� ��� �������� ������ �������:</span>
		</p>
		<p dir="ltr">
		</p>
		<ol>
			<li><span style="font-size: 14pt;">� ������������� ������;</span></li>
			<li><span style="font-size: 14pt;">� �������� ��������� ���������� �� �����������, �� ������� ���������� ��������;</span></li>
			<li><span style="font-size: 14pt;">� �������� ������ � ������;</span></li>
			<li><span style="font-size: 14pt;">� ������������� ������ ����������� ����������� � ��������� ������������.</span></li>
		</ol>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 14pt;">�������� ������� ��������� ����� ��������. ��� ���� �������� � ������ ������� � ����� ��������, �� � ������ � ����� "������". &nbsp;���� ��� 10 ��� ����� ����� ���� ����� ������� ������ � ����, ������������, � ��� ������� ����� ��, �� ������� ��� ����, ����� �������� ������ ��������, �������� ������ �����������.</span>
		</p>
		<h2>
		�������������� ����������� - � ����� �������</h2>
 <span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span>
		<p>
 <span style="font-size: 14pt;"> </span><span style="font-size: 14pt;">
			�������, ������� ����� ��������� ����� ������ �� �����, ����� ��� ������� ������ �������� � ������ �������. ��� ������� ��, ��� �������������, ������� �����, ��� ���������� �������� ������������� ��� ����, ����� ����� ������������. </span><span style="font-size: 14pt;"> </span>
		</p>
 <span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span>
		<p>
 <span style="font-size: 14pt;"> </span><span style="font-size: 14pt;">
			������� �������, ��� �� ����� ��� ���������� � ��� �������� ����, ��� � ����� ������� �� ������� ������ ���� �������, ��������� ���� ����������� � �������� ������� ���, � � �������, ���������� ����� �� �������������.</span><span style="font-size: 14pt;"> </span>
		</p>
 <span style="font-size: 14pt;"> </span>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p>
 <span style="font-size: 18px;"><b>�� ��������� ������ ��&nbsp;</b><a href="https://magistr.in.ua/ru/mahisterska-robota/">������������ ������</a><b>&nbsp;�� ����� �����������: <br>
 </b></span>
		</p>
		<ul>
			<li><span style="font-size: 14pt;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/">������������� ����</a></span></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 14pt;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 14pt;">������������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 14pt;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 14pt;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 14pt;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 14pt;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 14pt;">����������</span></a></li>
			<li><span style="font-size: 18px;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/"><span style="font-size: 14pt;">������������� ���������</span></a></span></li>
			<li><span style="font-size: 18px;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 14pt;">�������</span></a></span></li>
			<li><span style="font-size: 14pt;">���� � �����</span></li>
			<li><span style="font-size: 14pt;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-ekonomiky/">���������</a></span></li>
		</ul>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/ru/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>