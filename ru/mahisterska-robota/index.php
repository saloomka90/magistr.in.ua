<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ - ��������� �������� - �. ����");
$APPLICATION->SetPageProperty("description", "����� �������� ������������ ������ ������ � ��� �������� - ��� �������! &#10003; 10 ��� ����� &#10003; 100% �������� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?><div class="wrap-bg">
	<div class="wrap">
		<h1><span style="font-size: 32px;">������������ ������ �� �����</span></h1>
		<p dir="ltr">
 <br>
		</p>
		<p dir="ltr">
 <img width="300" alt="�������� ������������ ������ � ��������� &quot;�������&quot;" src="/upload/medialibrary/f15/mahisterski2_min.jpeg" height="198" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">������������ ������ - ��� ����� �������������� ����������� ����������������� ����, ������������ ���������� � �������� ������ �������� ��������� �������� ����� ���������� ����������� ������� ��������. �������� ���������� ����� ����� ���������� ��������� ������� ������� �������� � &#8203;&#8203;��������� ����� ������������, � ��������� �� �������� ��������� �� ��� �������������� ������� ������������ �����. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<h2><b><span style="font-size: 24px;">������ ������ �������� ������ �������� ������������ ������?</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">����� ���������������� ������� ���������� ���������� ����������� ����������, ���������� ��������� ��������, ����������������� � �������� ��������� ������ ����� ����� ����� ������������ ������. ����� ��� ������, ��� ������� �������������, ��� ������� ������������, �� ������� �� ���������� �� �������. ������������ ���� �������� ����������� ����� ������, � ������ ������� �� �������� ���������� ������ ������������ ������ � ��������������. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
		</p>
		<h2><b><span style="font-size: 24px;">������ �������� ��������� ������������ � ��������� �������� - ������ �������?</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<ol>
			<li><span style="font-size: 18px;">� ��� �������� ������� �������������� ��������� ������������, ������� ����� � ������ ���� ������������ ������� ����.</span></li>
			<li><span style="font-size: 18px;">������������, ���������� ������ ���������������, ����������� � ����������� ����, ����� ��������� � ����������������� ����������, � ���������� ���������, � ������� ������ �������.</span></li>
			<li><span style="font-size: 18px;">�� ������ �������� ������ �� ������������� �� ��������� ����.</span></li>
			<li><span style="font-size: 18px;">����������� ������������ ����� ������� ������� ������������.</span></li>
			<li><span style="font-size: 18px;">������������ ������ �� ����� ����� ���������� ���� �������������� ����� � ���������.</span></li>
			<li><span style="font-size: 18px;">��� ������������� �������� �������� ������ ��������� �������������� � �������� ����.</span></li>
			<li><span style="font-size: 18px;">������������ �������������� �� ������� �� ������.</span></li>
			<li><span style="font-size: 18px;">�� ����������� ������ ��������������� ������ �� 30%.&nbsp; <br>
 <br>
 </span></li>
		</ol>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<h2><b><span style="font-size: 24px;">��� �������� ������������ ������?</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">�� ����� ����� ��������� ����� ��� ������ ������. � ��� �� ������� ����� ����������, ������� ���������� ������ ���. ��� ���������� ����� ������ ������������, � ��� �������� �� �������� ��������������� �� ����������. ���� � ������ ����� ���������� ��� ���������, �� �������� �� ��������� ������������� ����������� ������� ��� ������.</span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">������� ������������ �������� ���� ����� � �����. ����� ��������� ������� ������ ��� ��������� ������ �������� ������� ��, ����� �������� ��������������� � �� ��������� � ����������.</span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">��� �� �� �� ������� - � �. ����, �����, ����� ��� ������ ������ ������� - ���� ����������� ������ ������ ������ ��� �������� ������������ ������ �� ����� �� ��������� ����. �����������! <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<h2><span style="font-size: 24px;">&nbsp;<b>������� ���������� ������:</b></span></h2>
		<p>
		</p>
		<p>
		</p>
		<ol>
			<li><span style="font-size: 18px;">�������� �����: ������� ���� ���������� ������ � ���������� � ������.</span></li>
			<li><span style="font-size: 18px;">� ������� ����� ���� ������ ������ ��� ����� � ��� �������� ��� ���������.</span></li>
			<li><span style="font-size: 18px;">�� ������� ���������� �� ���� � ������ ������ � ������� 30-50% ��������� ������.</span></li>
			<li><span style="font-size: 18px;">�� ������ ���� � �������� ��� ��� ��� ����������� �������������.</span></li>
			<li><span style="font-size: 18px;">���� � ���� ������������ ���� ����������, �� �� ����� ��������� ������ �� ������������������ �����.</span></li>
			<li><span style="font-size: 18px;">�� ��������� ������ ������ � ���������� ��� ��� �������� ����� �������������.</span></li>
			<li><span style="font-size: 18px;">��� ������������� �� ���������� ��� ��������� ������������ ��� �����������.</span></li>
			<li><span style="font-size: 18px;">���������� �� ����������� ��������� ��������� ��������, � �� ��������� ��.</span></li>
			<li><span style="font-size: 18px;">��� ������������� �� 1,5 ������ ��������� ������ ������ � ������ �� ���������� ������������.&nbsp; <br>
			 &nbsp; &nbsp;</span></li>
		</ol>
		<p>
		</p>
		<p>
		</p>
		<h2><b><span style="font-size: 24px;">������� ����� �������� ������������ ������</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<div dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
			<table width="100%" height="30%" border="1" cellpadding="10" cellspacing="0" align="left">
			<tbody>
			<tr>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">��� ������</span></b>
				</td>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">���� ����������</span></b>
				</td>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">���� (���)</span></b>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">������������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">20 - 30 ����</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 2500</span>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">������ ������������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">7 - 15 ����</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 1000</span>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">��������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">1 - 2 ���</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 100</span>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b> <br>
 <span style="font-size: 24px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;">�� ��������� ������ �� ������������ ������ �� ����� �����������:</span></b>
		</p>
		<p>
		</p>
		<ul>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/"><span style="font-size: 18px; color: #0000ff;">������������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 18px; color: #0000ff;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 18px; color: #0000ff;">������������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 18px; color: #0000ff;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 18px; color: #0000ff;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 18px; color: #0000ff;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 18px; color: #0000ff;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 18px; color: #0000ff;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/"><span style="font-size: 18px; color: #0000ff;">������������� ���������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px; color: #0000ff;">�������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px; color: #0000ff;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px; color: #0000ff;">���������</span></a></li>
		</ul>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<p>
		</p>
		<div>
 <br>
		</div>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/ru/order/" class="gbt">�������� ������</a>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>