<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ �� ��������� - ��������� ��������");
$APPLICATION->SetPageProperty("description", "�������� ����� ������������ ������ �� ��������� � ��������� ����� ����������� �� ���-�� �������� � �������� &#10003; 10 ��� ����� &#10003; 100% �������� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?><div class="wrap-bg">
	<div class="wrap">
		<h1>������������ ������ �� ��������� �� �����<br>
 <br>
 </h1>
		<div>
 <img width="300" alt="����������� � �������� - ����������" src="/upload/medialibrary/034/ekonomika6_min.jpg" height="199" align="left">
			<p>
 <span style="font-size: 14pt;">������������� "���������" ���������� ��� �����-, ��� � ��������������. �� ����, ���������� ����� ���� ������������ � 2-� ������. ��� ���������� ������, ������� �������� �������� ��� ���� ����� �������, � � ���������� ���������� ������������� ������������� ��� ���������������� �������.<br>
 <br>
 </span>
			</p>
			<h2>
			������������ �� ��������� � ������� ������� </h2>
			<p>
 <span style="font-size: 14pt;">������������ ������ �� ��������� �� ����� ������������ ����� �������� �� 120 ������, ������� �������� �������������, ������������� � ���������������� ����� � ��������� ��� �����-, ��� � ������������������ ����������. � ������������� ������ ������������ ������ ������ ����� ������, ������ ��� ���������� ������� ������� ���������� � ���������� ��� ������ �������. ����� ��� ���� �� �������.<br>
 </span>&nbsp; &nbsp; &nbsp;
			</p>
			<h2>
			������� �������� ������ </h2>
			<p>
 <span style="font-size: 14pt;">�� ����� ������� ������ ���� �� ������ �����������, �� � ����������������� ������������, ��������� ������������ ���������� �����. �� � �� ������� ����� ������ �������, ��� �������, �����. ������ ����� ������ ������������ ������ �� ���������, ������ ������������� � ����� �� ������� ���, ��� ���������� ���� �� ���, ���������� � ��������� ��� � ����������� ����������. �� ������ �������� ��������� ������������ ������ �� ��������� �� ����� "�������".<br>
 <br>
 </span>
			</p>
			<h2>
			��������� ���� </h2>
			<p>
 <span style="font-size: 14pt;">���� �� ��������� ������� ����� ������������, ����� ����� �������� ������������ ������ �� ���������. �� ��� ������ ������� � ��� �� �����, � �� ����������� ������ ����� � �����, � �������, ������ �������, �� ����������� ��� ������� 50%. ������� ����� �������� � ����� ���������, �� ������ ����������� ��������� ���� � �����, �� ����� ����� ��������� ���� ������, ����� ������� � ������������, � ���� ��� ���� � ���������, �� �� � ����� ��������.<br>
 <br>
 </span>
			</p>
			<h2>
			������� � ���� �� �������� </h2>
			<p>
 <span style="font-size: 14pt;">�� ����� ����� �������� �������� � ������ ������������ ������ �� ������ ������� ������������. ����� ���������������� �� ������� ��������� �������������, �� ������ ��������� ���� ������, � ������ ����� ����� ���������� � ���������� ������. ��� ��������� � ������ �� ��������� �� ���� ������������ ������. �� �� �����������, ��� ��� ����� �������� ����� �������� ������������.<br>
 <br>
 </span>
			</p>
			<h2>
			���� ������������� � ������ ������������������� ����� </h2>
			<p>
 <span style="font-size: 14pt;">�������� ������������ ������ �� ��������� �� ����� ����� ������ �������� ���������. ��� �������, ��� ������������� ����� ��� ����������� � ������� �������. �� ��������� ��������� ���� � ��������� ������������ ����� ���, ��� ������ �� ������� �� ��������� ������������ �����. </span>
			</p>
 <span style="font-size: 14pt;"> </span>
			<p>
 <span style="font-size: 14pt;">
				�� ����� ����� �������, ��� ��� �� ����� ������, ������� �� �������� ������������ ������, ����������� ���������� "�������".&nbsp; ��� ������ ��������� ������ ������� ��������������� ���� �������� � �������� ����������.<br>
 <br>
 </span>
			</p>
			<p>
 <span style="font-size: 14pt;">�� ��������� ������ ��&nbsp;</span><a href="https://magistr.in.ua/ru/mahisterska-robota/"><span style="font-size: 14pt;">������������ ������</span></a><span style="font-size: 14pt;">&nbsp;�� ����� �����������:&nbsp; </span>
			</p>
 <span style="font-size: 14pt;"> </span>
			<ul type="disc">
				<li><span style="font-size: 14pt;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/">������������� ����</a></span></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 14pt;">���������� ����</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 14pt;">������������</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 14pt;">�����</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 14pt;">����������</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 14pt;">����������</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 14pt;">���������� ����</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 14pt;">����������</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/"><span style="font-size: 14pt;">������������� ���������</span></a></li>
				<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 14pt;">�������</span></a></li>
				<li><span style="font-size: 14pt;"><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-obliku-i-audytu/">���� � �����</a></span></li>
				<li><span style="font-size: 14pt;">���������</span></li>
			</ul>
		</div>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/ru/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>