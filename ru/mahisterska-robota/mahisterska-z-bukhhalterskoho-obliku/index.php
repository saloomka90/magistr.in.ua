<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ �� �������������� ����� - �������");
$APPLICATION->SetPageProperty("description", "������� ��������� ������������ ������ �� �������������� ����� � ��� �� ��������� ����� � �������� ���������� � ������ &#10003; 10 ��� ����� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?><div class="wrap-bg">
	<div class="wrap">
		<h1>������������ ������ �� �������������� ����� �� �����</h1>
		<div>
 <br>
		</div>
		<p>
		</p>
		<p dir="ltr">
 <img width="300" alt="�������� ������������ ������ �� �������������� ����� � ��������� &quot;�������&quot;" src="/upload/medialibrary/1e2/bukhoblik4_min.jpg" height="200" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">������������� ���� - ��� ����� ������ � ������ �� ������ ������. �� ����� ����� � ����� �������� ���������� ����� �����������. � ������� ��������� ������������ ������ �� �������������� ����� ���� �������������.&nbsp;</span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <h2><b><span style="font-size: 24px;">����� ��������� ��������� ��� ��������� ������������ �� �������������� �����?</span></b></h2>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">������� ��������, ��������, ��������, ������� ����� �������, ������ � ����� ��� ����, ����� ��������� ��� ����������� ����������, ��������� ���������������� ��, ����������������� � � ���������� ���� ������ � ����� ������. ����� ����� ������ ��������, ����� �������� ���� ��������, ���������� ��� �������, ��������� ��� � ��������. � ��� ����������� ��� ������� �� ��, ����� �������� � ����� ������������ �������. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
 <h2><b><span style="font-size: 24px;">��� ��������� ����� �� ��������������� ��������?</span></b></h2>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">�������, ��� ���� ����� � ���, ��� ���������� �������� ���� ��������. �� ���� ����������� �� ������������� ������������� ��� ������ �������. ������� ����������� ��������� ���������� ������ ������������ ������ �� �������������� ����� � ������������ ������������. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
 <h2><b><span style="font-size: 24px;">����� ������������ � ������ ������������ ����� ��������� ��������?</span></b></h2>
		</p>
		<p dir="ltr">
		</p>
		<ol>
			<li><span style="font-size: 18px;">�� ��������� ������������, ������� ������������� ����������� ����������� � ����������.</span></li>
			<li><span style="font-size: 18px;">����� ����������� ���������� ������������, ��� ����������� ��� ������� ������������.</span></li>
			<li><span style="font-size: 18px;">� ��� �������� ����� ��� ������������ � ����������� ������ � ������������� � �� ������.</span></li>
			<li><span style="font-size: 18px;">�� ���������� ���� ���� � ����� ��� ������� ������ �������� ��������.</span></li>
			<li><span style="font-size: 18px;">�� �������, ��� ������ ������� ��� ��������, � ����� ���������� ����� ������� �������������.</span></li>
			<li><span style="font-size: 18px;">�����������, �� ����� �������� � ������ ��������. <br>
 <br>
 </span></li>
		</ol>
		<p>
		</p>
		<p dir="ltr">
 <h2><b><span style="font-size: 24px;">��� �������� ������������ ������ �� �������������� �����?</span></b></h2>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">�� ����� ����� ���� ����� ��� ������. � ��� ����� ����������</span>
		</p>
		<p dir="ltr">
		</p>
		<ul>
			<li><span style="font-size: 18px;">����� ����������</span></li>
			<li><span style="font-size: 18px;">����������, ���������� ������ ����� ������.</span></li>
		</ul>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">��� ���������� ����� ��� ��������� ������ ������ ������������. � ��� �������� ��������������� �� ����������. ��� ����������� � ������ �������� ���� ���������� ���������� �������� �������� � ���� ��� ��������� �������������.</span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">� ����� �� ��������� ������� ����������� �����, ������ ��� ����� ������ ������� �� �� �������, ������� ����� ������������ ������ ������ ��� ������ ������� �������� ������������ �� �������������� ����� �� ��������� ����. �����������! <br>
 <br>
 </span>
		</p>
		<p>
 <span style="font-size: 18px;"><b>�� ��������� ������ ��&nbsp;</b><a href="https://magistr.in.ua/ru/mahisterska-robota/">������������ ������</a><b>&nbsp;�� ����� �����������: <br>
 </b></span>
		</p>
		<ul>
			<li><b><span style="font-size: 18px;">������������� ����</span></b></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 18px;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 18px;">������������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 18px;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 18px;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/"><span style="font-size: 18px;">������������� ���������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px;">�������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px;">���������</span></a></li>
		</ul>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/ru/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>