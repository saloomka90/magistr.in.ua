<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ �� ������������� ��������� - �������");
$APPLICATION->SetPageProperty("description", "����� ������������ ������ �� ������������� ��������� - ��� ���������� ������� ��� ��������, ������� �� ����� �� ����������� �������� ��� �� ����� � ������!");
$APPLICATION->SetTitle("������������");
?><div class="wrap-bg">
	<div class="wrap">
		<p dir="ltr">
		</p>
		<h1>
		<p dir="ltr">
			 ������������ ������ �� ������������� ��������� �� �����
		</p>
		<div>
 <br>
		</div>
 </h1>
		<p>
 <img width="300" alt="�������� ������������ ������ �� ������������� ��������� � ��������� &quot;�������&quot; - ��������" src="/upload/medialibrary/c19/ekonomika4_min.jpg" height="226" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">�����������, ������� ����� �������� � ����� �������� ������������� ����������, ������� �� ��� ������. ������� �� ������������ ����� ������ ������, ��������� ������������� ��������, ��� ������� ������� ������ � ������� ���������, �����, �����������, ����������� ������. ������������ ������ �� ������������� ��������� � ��������� ���� �������� ��������� ������. �� ����, ��������� ���� ������ ������� ������ ���� ������������, ����� �������� �� ������ ������ � �������, �� � ���������� �������� �������.</span>
		</p>
		<p>
 <span style="font-size: 18px;">���������� � ������������ ������� �� ������������� ���������� ����� �� ��� ��������. <br>
 </span>
		</p>
		<ol>
			<li><span style="font-size: 18px;">��� ������� � ���, ��� ���������� ����� ���� ������ �������������� ���������� �� ������ �������. ��� �� ������ ��������� ������ ������� ����������� ������.&nbsp;</span></li>
			<li><span style="font-size: 18px;">����� ������ ������� ������� ������������� � ���������������� �����, � ������� ������� ������� ������ ������ ������������������ ������������� ���������, ���������� ����� ���� ��� ������� � ��������� � �����.&nbsp;</span></li>
			<li><span style="font-size: 18px;">�� ���� ���������� � �� ��������� ���������� ��������� �� ����� 2-� �������. � �� �� ����� ���� ���������� � � ��������� ���������, � �������� ���� ��� �������� ���������� ������. <br>
 <br>
 </span></li>
		</ol>
		<p>
		</p>
		<p>
 <h2><b><span style="font-size: 24px;">� ���������� "�������" �� ������� ��������� ����� ��������</span></b></h2>
		</p>
		<p>
 <span style="font-size: 18px;">�������� "�������" ������������� ������ �� ����� ���������� ��� �������� ��� ����, ����� ��� ����� ���������� ������� �� �������� ���� � ����� �������� ���������� �� ������� ������������� ������. ����� ������ � ������ ������� �������� ��� ��������� ������������ ���� � �� ���������� �� ����������� ����� �����������.</span>
		</p>
		<p>
 <span style="font-size: 18px;">��� ��������� ������� �������� ��� ������������ ������ ������ ������������ ������ �� ������������� ���������. ����� ����� ��������� � ���� �������������, �� ������ ����� ������� ����, ���������� � ��������� ������� �����������, � ����� ������ ����� ��������, �� �������� �������� ������. <br>
 <br>
 </span>
		</p>
		<p>
 <h2><b><span style="font-size: 24px;">������ ����� ���������� � ��������� "�������"</span></b></h2>
		</p>
		<p>
 <span style="font-size: 18px;">�� ������ �������� ������������ ������ �� ������������� ��������� � �������� �� ������� �� ���� ���������. ��� �������� ��� ������������ ���������� ��������� ������� ���������� ������� � ���������� ������� ��������� � ������ ������������, ������� ��������� �� �������� �������.</span>
		</p>
		<p>
 <span style="font-size: 18px;">��� �������������� ������������ ��������� ������ ������ ���� ������������ � ������. �� ��������� � ��� � ���������. ���� ������������ �������������� � ���, ��� �� �������� ��������� �������, � ��������� ���������� �� ��������� � ������� - �� ������� ����� ����������. ���� ����������� ������� ��� ���������, ����� ��� ������������ ������� ���������. ��� ������������� ���� �������� ����� ������������ ������� ��������� ������������ � ������� ���������� ����, ��� ����� �� ���������� ���������� ������������. ���� �� ������� ����� ������������ ������ ���������� ������� � �������������� �������.</span>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">��� ������������ ���� � ��������� ����� ��������� �������� ������������ �� ������������� ��������� �� ����� ������ � �����������. ������ �� ���������� �� ������������ ���������� ������������, � ������������ ����� ������ ����������� � ����������. ��� ��������� ��� ������������� ������� �������� ������ �������, ������� �� �������� �������� ��������� ��������. � ����� �� ���� �� 50 - 100% ���� �����.&nbsp; <br>
 <br>
 </span>
		</p>
		<p>
 <h2><b><span style="font-size: 24px;">������� ����� �������� ������������ ������</span></b></h2>
		</p>
		<p dir="ltr">
		</p>
		<div dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
			<table width="100%" height="30%" border="1" cellpadding="5" cellspacing="0" align="left">
			<tbody>
			<tr>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">��� ������</span></b>
				</td>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">���� ����������</span></b>
				</td>
				<td style="border-image: initial;">
 <b><span style="font-size: 18px;">���� (���)</span></b>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">������������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">20 - 30 ����</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 2500</span>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">������ ������������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">7 - 15 ����</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 1000</span>
				</td>
			</tr>
			<tr>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">��������</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">1 - 2 ���</span>
				</td>
				<td style="border-image: initial;">
 <span style="font-size: 18px;">�� 100</span>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 </span></b>
		</p>
		<p>
 <b><span style="font-size: 18px;"> <br>
 <br>
 </span></b>
		</p>
		<p>
 <span style="font-size: 18px;"><b>�� ��������� ������ ��&nbsp;</b><a href="https://magistr.in.ua/ru/mahisterska-robota/">������������ ������</a><b>&nbsp;�� ����� �����������:</b></span>
		</p>
		<p>
		</p>
		<ul>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/"><span style="font-size: 18px;">������������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 18px;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 18px;">������������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 18px;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 18px;">���������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 18px;">����������</span></a></li>
			<li><span style="font-size: 18px;">������������� ���������</span></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px;">�������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px;">���������</span></a></li>
		</ul>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/ru/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>