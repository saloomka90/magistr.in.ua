<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ �� ����������� ���� � ����� -�������");
$APPLICATION->SetPageProperty("description", "����� ������������ ������ �� ����������� ���� � ��� - ����� ��� ��������, ������� �� ����� �� ����������� �������� � �������� &#10003; 10 ��� ����� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?> 		 
<div class="wrap-bg"> 			 	 
  <div class="wrap"> 					 	 	 
    <h1>������������ ������ �� ����������� ���� �� ����� </h1>
   
    <p dir="ltr"> 
      <br />
     </p>
   
    <p dir="ltr"><img src="/upload/medialibrary/2ce/bankivska_sprava_min.jpg" hspace="30" vspace="10" border="1" align="left" alt="�������� ������������ ������ �� ����������� ���� � ��������� &quot;�������&quot;" width="300" height="250"  /><font size="4">��������� ������������ ����� - ��� ������� �� �� ������, � � ��� ����������� ������ �� ������ �������. ���������� �������� ��� ������� ����� ��� ����, ����� ������� ��� ������, � ��� ������ ������� �� ������� �����������. �������� ��, ��� ��� ���������� ����� ����������� �� ������������, � ��� ����� � ������, �� �� ������������� ������� ������ ������� ������ �� ���� ����.</font></p>
   
    <p dir="ltr"><font size="4">�� ������ �������� ����� ����������� ���������� � ��������� ��������� � ������ ����������� ����� ������� ������������� � ������������ ������������. ������� ���������� ��������, ��� � ���������� ����������: ������ �� �������� ��� �������� ������.</font></p>
   
    <p dir="ltr"><font size="4">�� ������ �������� ����� ������ ���������� ������, ��� ����� ����� �������� ������������ ������ �� ����������� ���� �� ����� �����. 
        <br />
       
        <br />
       </font></p>
   
    <p dir="ltr"><b><font size="5">������� ������</font></b></p>
   
    <p dir="ltr"> </p>
   
    <ol> 
      <li><font size="4">������������ ������ �� ����������� ���� �� ����� ����������� ��������� &quot;�������&quot; ��� ������� ������������� �����. ���� ���� �� ���������������, �� ����� ���� ��� ���������, �� ���������� � ��������� ���� ����������� ����� ������ ����� ��� �����������.</font></li>
     
      <li><font size="4">�� ��������� ������ �� ����������� ���� ����� ��������� � ������������� ������������ ������� �� ���� �������������� � �������� ������� ����������� ����� ��� ������, ��������������� ���������. �� ����� ����� ����������� ��������� ���������� �������������� ����������, ������� ���� ������ ����� ������������ �� ������� ������. 
          <br />
         
          <br />
         </font></li>
     </ol>
   
    <p></p>
   
    <p dir="ltr"><b><font size="5">������������ ������ � ��������� &quot;�������&quot;</font></b></p>
   
    <p dir="ltr"> </p>
   
    <ol> 
      <li><font size="4">�� ����� �������� ������������ ������ �� ����������� ���� �� ����� � ������� ������������ ����������� ������������, ����� ��� ����������� ����������� � ��� ��� ����� ����.</font></li>
     
      <li><font size="4">���� �� ������������ �������� �� ������ ��� �������� ���� �����, �� � ��� ������������ �����.</font></li>
     
      <li><font size="4">�������� ������������ ����� � ������� �������, � ����� ������� �� �������� � ��� �� ����������������. �� ��������� ������������ ������� ��������� ����, ��� � ������� �� ��������� ������������ ��������� ������������.</font></li>
     </ol>
   
    <p></p>
   
    <p dir="ltr"><font size="4">����� �������� ������ ��������, ������ ������������ ������������. ��� ����� ���������� ��� �������, ��������� �� ������������ � ������������ � ������ ������� �������� ���� ��������. ��� ������������� ���� ����������� ����������� ��������������� ������ �� ���������� ���������.</font></p>
   
    <p dir="ltr"><font size="4">����� �������� ��������� ������������ ������ �� ����������� ����, ����������� �� ���������, ��������� �� �����. �� ������ ������������ ��� �� ���� � ��������� ������ ���������� ������. � ��� ����� ����� ���������� ����� ������������.</font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p><font size="4"><b>�� ��������� ������ ��</b><a href="https://magistr.in.ua/ru/mahisterska-robota/" >������������ ������</a><b>��� ����� �����������: 
          <br />
         </b></font></p>
   
    <ul> 
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/" ><font size="4">������������� ����</font></a></li>
     
      <li><b><font size="4">���������� ����</font></b></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/" ><font size="4">������������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/" ><font size="4">�����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/" ><font size="4">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/" ><font size="4">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/" ><font size="4">���������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/" ><font size="4">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/" ><font size="4">������������� ���������</font></a></li>
     
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px;">�������</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px;">���������</span></a></li>

     </ul>
   
   </div>

  <div class="global" style="text-align: center;"><a href="https://magistr.in.ua/ru/order/" class="gbt" >�������� ������</a></div>

 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>