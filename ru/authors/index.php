<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "����� �� ��������� �����, ������� ���������");
$APPLICATION->SetPageProperty("title", "����� �� ��������� �����. ������� ��������� ��� ��������");
$APPLICATION->SetPageProperty("description", "����� ������ �� ��������� �����. ������� ��������� � ��������");
$APPLICATION->SetTitle("� �������");

CModule::IncludeModule("iblock");
$rsWRC = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>6,"PROPERTY_state"=>22,"ACTIVE"=>"Y"));

switch(substr((string)$rsWRC->SelectedRowsCount(),-1,1))
{
    case 2:
    case 3:
    case 4:
        if(strlen($rsWRC->SelectedRowsCount())>1 && substr($rsWRC->SelectedRowsCount(),-2,1)==1) $s = "��";
        else $s = "���";
        break;
    case 1:
        if(strlen($rsWRC->SelectedRowsCount())>1 && substr($rsWRC->SelectedRowsCount(),-2,1)==1) $s = "��";
        else $s = "���";
        break;
    default:
        $s = "��";
        break;
}
?> 
<p> </p>

<p>���������� ������������ ������������ ����� � ��������������.</p>

<?include("include_stat.php");?>
 
<h2>1. ������� ����� ������� �����.</h2>
 
<p>� ��� ���� ����� ������� ������������ �����? - ���������� �� �� ����� Magistr.in.ua � ���������� ��������� �������������� �������. ��� ������ ����������� ����� - ��� ������ �����.</p>
 
<p>��������� ������ �� ����� ������ ����� �������� �� ����� ������:</p>
 
<p> </p>

<ol> 
  <li>�������� �������� � ���. � ����� ������ ��� �� ����� ������ ������ � ����� ������, ������� � ��� ������ � ���������� ������ �������. ����� �� �������������� ��������� � �������� � ���������� ������� ������ � �������� ������.</li>
 
  <li>�������� ������ � ��������������� ���������. � ����� ������ � ���� ����������� ������������� ���������. ������ ������������ ������� ��� ��� �������� (����������, WebMoney, �� ���������). �������� �� �������������� ������� �� ��, � ������.�</li>
 </ol>
 
<p></p>

<div class="block float_r" style="width:200px;text-align:left;margin:0px 30px 0px 20px;">
  <div class="ctl">
    <div class="ctr">
      <div class="cbl">
        <div class="cbr"> 	
          <div class="in"> 
            <ul>
               <li>������ ���� �������� ����� ������ ��������� � ����, �� ������� �������� ��������</li>
               <li>��������� ������� ������</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
 
<h2>2. ���������� ����� �� �����.</h2>

<p>���� � ��� ���� ������� ���� ��������� �����, ������� ��������, � ����� ��������� ����� - ���������� ��� �������������� � ����� ��������� ������������ ����� �� �����. �������� �� ������� ���������� �������, �� ��������� �� <b><?=date("d.m.Y")?></b> � ��������� ���� �������� <b><?=$rsWRC->SelectedRowsCount()?></b> ���<?=$s?> �� ����� �� ������: �� ������� ����������� ������ ��� ���� �� ������� �������. ������� ���� �� ���������� ������ ����������� � �� ��������� ����� - � ��� ����� ���������� ������ � ���������� �������. ������� �������������� ���������� ��������� �������:</p>

<p> </p>

<ol> 
  <li>��� �� ����� ����� ��������� ������ � ������������ � ����� ������.</li>
 
  <li>�� ����� � ������� ������ ����� ����� �� ���������������� � ������������ � ��������� ������ � ���������� ����, �� ������� ������ �� ���������.</li>
 
  <li>���� ��� ������� ������������ ������ ������, � ���� ����������� �������� ������������� ��������� �� ��������.</li>
 </ol>
 
<p></p>
 
<p></p>
 
<div class="col2_container" style="text-align: center; "> <a href="/ru/authors/registration/" ><font size="5">�����������</font></a></div>
 
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>