<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("����� �������� � ��������");
?>
<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><b style="mso-bidi-font-weight: normal"><span lang="UK" style="mso-ansi-language: UK">1. ����&rsquo;���� ���������
      <p></p>
    </span></b></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">
    <p>�</p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">1.1. ��������� ������������<span style="mso-spacerun: yes">� </span>�������� ��������� ������� ���������� ���� �� �������� ��� ������, ���� �� ���������� ��� �������� ���������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">1.2. ��������� ������������ ������ ��������� ��� ���������� ���� ������ � �����/��� ��������� �� ��������� ������. 
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">1.3. ��������� ������������ �������� �������� ������ �������� 2-� ���� � �������, ���� ������ ������ ������� � ���������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">1.4. ���� ��������� �� ������ ������ ������� �볺��� ��������� ������ �� ���� ���������, �볺��� ������������ ������� ������� ������ ��� ������������ ����� ��������. � ������ ������� ����������� ����� ������������� �� �������� ���������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="EN-US" style="mso-ansi-language: EN-US">
    <p>�</p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="EN-US" style="mso-ansi-language: EN-US">
    <p>�</p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><b style="mso-bidi-font-weight: normal"><span lang="UK" style="mso-ansi-language: UK">2. �������� ���������
      <p></p>
    </span></b></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">
    <p>�</p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">2.1. ���������� ������������ ������ �������� � ������� � ��������� ��� ��� ��������� ������� ������ � ������� ���������� ������, � ���� �� 21.00 ������ ���� ���������, ���� �� ������� ���� ������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">2.2. ���� ���������� �� ����� �������� �������� � ���������� �����, �� ������� ��������� � ���������� � ��������� ��� ��������. ���� ���������� �� ������� � ��������� �������� ������ �� �������� ������ � �� ������ �� ������, � ����� �� ������� �� ������ � </span><span lang="EN-US" style="mso-ansi-language: EN-US">e</span><span lang="UK" style="mso-ansi-language: UK">-</span><span lang="EN-US" style="mso-ansi-language: EN-US">mail</span><span lang="EN-US" style="mso-ansi-language: UK"> </span><span lang="UK" style="mso-ansi-language: UK">�����������, �� � ������ ��� �� ����� �� ������ ������������ �볺��� �� ������� �� ����, ��� ������� ������ ��� ��������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">2.3. ���������� ������������ �������� ��������� ������� (���� ���� �������) �������� 3-� ���� � ������� ������� �����糿 �� ������.
    <p></p>
  </span></p>

<p class="MsoNormal" style="MARGIN: 0cm 0cm 0pt"><span lang="UK" style="mso-ansi-language: UK">2.4. ������ ������ ������� ���� � ������ </span>.<span lang="EN-US" style="mso-ansi-language: EN-US">doc</span><span lang="EN-US"> </span><span lang="UK" style="mso-ansi-language: UK">��� </span>.<span lang="EN-US" style="mso-ansi-language: EN-US">rtf</span>.</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>