<?
define("STOP_STATISTICS", true);die();
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");


$arSelect = Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID");
$arFilter = Array("IBLOCK_ID"=>IntVal(10), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $l = 1; 
    switch($arFields["IBLOCK_SECTION_ID"]){
        case 110:
            $l = 1.1; 
            break;
        case 111:
            $l = 0.95; 
            break;
        case 112:
            $l = 1.15; 
            break;
        case 113:
            $l = 1.05; 
            break;
        case 114:
            $l = 1; 
            break;
        case 138:
            $l = 1; 
            break;
    }
    
    CIBlockElement::SetPropertyValues($arFields["ID"], $arFields["IBLOCK_ID"], $l, "KOEF");
    
}

?>