<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("���� ������");
global $USER;


if ( $_REQUEST['ajax'] == "Y" ) {    
    
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE')) {
        define('PUBLIC_AJAX_MODE', true);
    }
    header('Content-type: application/json');
    
    
    
    if(check_bitrix_sessid() && $_REQUEST['action'] == 'change_pass'){
        if($_REQUEST['password'] == $_REQUEST['passwordagain']){
            
            
            (new CUser())->Update($USER->GetID(), ["PASSWORD"=>$_REQUEST['password'],"CONFIRM_PASSWORD"=>$_REQUEST['passwordagain']]);
            
            echo json_encode(array('error' => false));
        }else{
            echo json_encode(array('error' => true));
        }
           
    }
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}  




?>

<form method="POST" class="change-password" action="/client/password/">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name='action' value="change_pass">
    <div class="contact-information">
        <div class="title-18 title-18_padlock">
            <span class="icon-padlock"></span>
            ������ ������
        </div>
        <div class="form-item">
            <div class="form-item__label">������ ����� ������:</div>
            <label class="password-label">
                <span class="show-hide-pass"></span>
                <input type="password" name="password" id="password">
            </label>
        </div>
        <div class="form-item">
            <div class="form-item__label">����� ������ �� ���:</div>
            <label class="password-label">
                <span class="show-hide-pass"></span>
                <input type="password" name="passwordagain">
            </label>
        </div>  
    </div>
    <div class="form-item-bottom form-item-bottom_320">
        <div class="form-item-bottom__save">
            <button type="submit" class="button">��������</button>
        </div>
        <div class="form-item-bottom__back">
            <a href="/client/profile/" class="button button_gray">�����</a>
        </div>
    </div>
</form>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>