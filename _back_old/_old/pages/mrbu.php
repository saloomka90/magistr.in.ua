<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ������ � ����� - ��������� ��������");
$APPLICATION->SetPageProperty("description", "����� �������� ������������ ������ ������ � ��� �������� - ��� �������! &#10003; 10 ��� ����� &#10003; 100% �������� &#10003; �����������!");
$APPLICATION->SetTitle("������������");
?> 		 
<div class="wrap-bg"> 			 	 
  <div class="wrap"> 					 	 	 
    <h1><font size="6">������������ ������ �� �����</font></h1>
   
    <p dir="ltr"> 
      <br />
     </p>
   
    <p dir="ltr"><img src="/upload/medialibrary/f15/mahisterski2_min.jpeg" hspace="30" vspace="10" border="1" align="left" alt="�������� ������������ ������ � ��������� &quot;�������&quot;" width="300" height="198"  /><font size="4">������������ ������ - ��� ����� �������������� ����������� ����������������� ����, ������������ ���������� � �������� ������ �������� ��������� �������� ����� ���������� ����������� ������� ��������. �������� ���������� ����� ����� ���������� ��������� ������� ������� �������� � &#8203;&#8203;��������� ����� ������������, � ��������� �� �������� ��������� �� ��� �������������� ������� ������������ �����. 
        <br />
       
        <br />
       </font></p>
   
    <p dir="ltr"><b><font size="5">������ ������ �������� ������ �������� ������������ ������?</font></b></p>
   
    <p dir="ltr"><font size="4">����� ���������������� ������� ���������� ���������� ����������� ����������, ���������� ��������� ��������, ����������������� � �������� ��������� ������ ����� ����� ����� ������������ ������. ����� ��� ������, ��� ������� �������������, ��� ������� ������������, �� ������� �� ���������� �� �������. ������������ ���� �������� ����������� ����� ������, � ������ ������� �� �������� ���������� ������ ������������ ������ � ��������������. 
        <br />
       
        <br />
       </font></p>
   
    <p dir="ltr"> </p>
   
    <p dir="ltr"><b><font size="5">������ �������� ��������� ������������ � ��������� &laquo;�������&raquo; - ������ �������?</font></b></p>
   
    <p dir="ltr"> </p>
   
    <ol> 
      <li><font size="4">� ��� �������� ������� �������������� ��������� ������������, ������� ����� � ������ ���� ������������ ������� ����.</font></li>
     
      <li><font size="4">������������, ���������� ������ ���������������, ����������� � ����������� ����, ����� ��������� � ����������������� ����������, � ���������� ���������, � ������� ������ �������.</font></li>
     
      <li><font size="4">�� ������ �������� ������ �� ������������� �� ��������� ����.</font></li>
     
      <li><font size="4">����������� ������������ ����� ������� ������� ������������.</font></li>
     
      <li><font size="4">������������ ������ �� ����� ����� ���������� ���� �������������� ����� � ���������.</font></li>
     
      <li><font size="4">��� ������������� �������� �������� ������ ��������� �������������� � �������� ����.</font></li>
     
      <li><font size="4">������������ �������������� �� ������� �� ������.</font></li>
     
      <li><font size="4">�� ����������� ������ ��������������� ������ �� 30%.� 
          <br />
         
          <br />
         </font></li>
     </ol>
   
    <p></p>
   
    <p dir="ltr"><b><font size="5">��� �������� ������������ ������?</font></b></p>
   
    <p dir="ltr"><font size="4">�� ����� ����� ��������� ����� ��� ������ ������. � ��� �� ������� ����� ����������, ������� ���������� ������ ���. ��� ���������� ����� ������ ������������, � ��� �������� �� �������� ��������������� �� ����������. ���� � ������ ����� ���������� ��� ���������, �� �������� �� ��������� ������������� ����������� ������� ��� ������.</font></p>
   
    <p dir="ltr"><font size="4">������� ������������ �������� ���� ����� � �����. ����� ��������� ������� ������ ��� ��������� ������ �������� ������� ��, ����� �������� ��������������� � �� ��������� � ����������.</font></p>
   
    <p dir="ltr"><font size="4">��� �� �� �� ������� - � �����, ������ ��� ������ ������ ������� - ���� ����������� ������ ������ ������ ��� �������� ������������ ������ �� ����� �� ��������� ����. �����������! 
        <br />
       
        <br />
       </font></p>
   
    <p dir="ltr"><font size="5">�<b>������� ���������� ������:</b></font></p>
   
    <p> </p>
   
    <ol> 
      <li><font size="4">�������� �����: ������� ���� ���������� ������ � ���������� � ������.</font></li>
     
      <li><font size="4">� ������� ����� ���� ������ ������ ��� ����� � ��� �������� ��� ���������.</font></li>
     
      <li><font size="4">�� ������� ���������� �� ���� � ������ ������ � ������� 30-50% ��������� ������.</font></li>
     
      <li><font size="4">�� ������ ���� � �������� ��� ��� ��� ����������� �������������.</font></li>
     
      <li><font size="4">���� � ���� ������������ ���� ����������, �� �� ����� ��������� ������ �� ������������������ �����.</font></li>
     
      <li><font size="4">�� ��������� ������ ������ � ���������� ��� ��� �������� ����� �������������.</font></li>
     
      <li><font size="4">��� ������������� �� ���������� ��� ��������� ������������ ��� �����������.</font></li>
     
      <li><font size="4">���������� �� ����������� ��������� ��������� ��������, � �� ��������� ��.</font></li>
     
      <li><font size="4">��� ������������� �� 1,5 ������ ��������� ������ ������ � ������ �� ���������� ������������.� 
          <br />
         � �</font></li>
     </ol>
   
    <p></p>
   
    <p><b><font size="5">������� ����� �������� ������������ ������</font></b></p>
   
    <p dir="ltr"> </p>
   
    <div dir="ltr"> <font size="4"> 
        <br />
       </font> 
      <table width="100%" height="30%" border="1" cellpadding="10" cellspacing="0" align="left"> 
        <tbody> 
          <tr><td style="border-image: initial;"><b><font size="4">��� ������</font></b></td><td style="border-image: initial;"><b><font size="4">���� ����������</font></b></td><td style="border-image: initial;"><b><font size="4">���� (���)</font></b></td></tr>
         
          <tr><td style="border-image: initial;"><font size="4">������������</font></td><td style="border-image: initial;"><font size="4">20 - 30 ����</font></td><td style="border-image: initial;"><font size="4">�� 2500</font></td></tr>
         
          <tr><td style="border-image: initial;"><font size="4">������ ������������</font></td><td style="border-image: initial;"><font size="4">7 - 15 ����</font></td><td style="border-image: initial;"><font size="4">�� 1000</font></td></tr>
         
          <tr><td style="border-image: initial;"><font size="4">��������</font></td><td style="border-image: initial;"><font size="4">1 - 2 ���</font></td><td style="border-image: initial;"><font size="4">�� 100</font></td></tr>
         </tbody>
       </table>
     </div>
   
    <p><b><font size="4"> 
          <br />
         </font></b></p>
   
    <p><b><font size="4"> 
          <br />
         </font></b></p>
   
    <p><b><font size="4"> 
          <br />
         </font></b></p>
   
    <p><b><font size="4"> 
          <br />
         </font></b></p>
   
    <p><b> 
        <br />
       <font size="5"> 
          <br />
         </font></b></p>
   
    <p><b><font size="4">�� ��������� ������ �� ������������ ������ �� ����� �����������:</font></b></p>
   
    <p> </p>
   
    <ul> 
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/" ><font size="4" color="#0000ff">������������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-bankivskoi-spravy/" ><font size="4" color="#0000ff">���������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-zhurnalistyky/" ><font size="4" color="#0000ff">������������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-prava/" ><font size="4" color="#0000ff">�����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-menedzhmentu/" ><font size="4" color="#0000ff">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-pedahohiky/" ><font size="4" color="#0000ff">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/" ><font size="4" color="#0000ff">���������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-psykhologii/" ><font size="4" color="#0000ff">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/" ><font size="4" color="#0000ff">������������� ���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/ru/mahisterska-robota/mahisterska-z-finansiv/" ><font size="4" color="#0000ff">�������</font></a></li>
     </ul>
   
    <p></p>
   
    <p dir="ltr"> </p>
   
    <p> </p>
   
    <div> 
      <br />
     </div>
   	 	</div>
   <div class="global" style="text-align: center;"><a href="https://magistr.in.ua/ru/order/" class="gbt" >�������� ������</a></div>

 </div>
 	 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>