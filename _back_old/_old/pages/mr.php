<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ����������� ������ � ��� - ��������� �������");
$APPLICATION->SetPageProperty("description", "������� ������ ����������� ������ �������� � ��� ������� - ��� �������! &#10003; 10 ���� ������ &#10003; 100 % ������� &#10003; ����������!");
$APPLICATION->SetTitle("����������");
?> 		 
<div class="wrap-bg"> 			 	 
  <div class="wrap"> 					 	 	 
    <p dir="ltr"></p>
   
    <h1><font size="6">����������� ������ �� ����������</font></h1>
   
    <div> 
      <br />
     </div>
   
    <p></p>
   
    <p dir="ltr"><img src="/upload/medialibrary/e2d/mahisterski_min.jpg" hspace="30" vspace="10" border="1" align="left" alt="�������� ����������� ������ � �������� &quot;������&quot;" width="300" height="315"  /><font size="4">����������� ������ &ndash; �� ���� ��������� �������-������� ������, ����� ��������� �� ������ ���������� ���, ������� ������ ������ �������. ����� �� ��������� ������ �������� ������� ������� ����� �� ��������� � ������ ���� ��������, �� �������� �� ������� ��������� �������-����������� ���������.</font></p>
   
    <p dir="ltr"> 
      <br />
     <font size="5"><b> ���� ������ �������� �������� �������� ����������� ������</b></font></p>
   
    <p dir="ltr"><font size="4">��� ������� ����� ������ ������� ������ ����������, ���������� ���������� �������, ���������������� �� �������� ������ ���� ������� ������ ����� ���������� �����. ����� ��� �������, �� ������� ���, �� ������� �����������, ��� ���� �� ��������� �� �������. ����������� ����� �������� ����������, � ����������� ������� �� �������� ��� ������ ����������� ������ � �����������.</font></p>
   
    <p dir="ltr"> </p>
   
    <p dir="ltr"><b> 
        <br />
       <font size="5"> ���� �������� ��������� ����������� ������ � �������� &laquo;������&raquo; - �������� ������</font></b></p>
   
    <p dir="ltr"> </p>
   
    <ol> 
      <li><font size="4">� ��� ������ ������� ����������� ����� �����������, �� �� � ������ ��� �������� ������� �����.</font></li>
     
      <li><font size="4">����������� ������, �� �������� ������ �����������, ���������� � ����������� ����, ���� ����������� ��������������� ������ �� ������ �������, � ����� ��������� �������.</font></li>
     
      <li><font size="4">�� ���� ��������� �������� ������: �� ��������� �� ��������� ����.</font></li>
     
      <li><font size="4">�������� ���������� ����� ������� ����� �����������.</font></li>
     
      <li><font size="4">����������� ������ �� ���������� �� ��������� ���� �������� �� ������� �� �� ���������.</font></li>
     
      <li><font size="4">��� ����������� �������� �������� ������ ����������� ����������� � �������� �����.</font></li>
     
      <li><font size="4">����������� �������������� �� ������� �� �������.</font></li>
     
      <li><font size="4">��� �������� �볺��� ��������� ������ �� 30%.</font></li>
     </ol>
   
    <p></p>
   
    <p dir="ltr"><b> 
        <br />
       <font size="5"> �� �������� �����������</font></b></p>
   
    <p dir="ltr"><font size="4">�� ���� ������������ ����� ����������. � �� �� ������ ���� ����������� ����� ������, �� ��������� ���� ���. �� ������ ������ ��������� ����������, � ��� �������� � ����� ������������ �� ���������. ���� � ���������� ������ ��������� ��� ��������, �� �������� ��� ��������� ����������� ����&rsquo;������ �������� �� ������ ������.</font></p>
   
    <p dir="ltr"><font size="4">������� ����������� ������� ���� ����� � ���. ϳ��� ��������� ������ ������ ��� ���������� ���� �������� ������� ��, ��� �������� ����������� � �� �������� �� ����.</font></p>
   
    <p dir="ltr"> 
      <br />
     <b><font size="5">������� ��������� ������ </font> 
        <br />
       
        <br />
       </b></p>
   
    <div dir="ltr"> 
      <div dir="ltr"> 
        <table width="100%" height="10%" border="1" cellpadding="5" cellspacing="1" align="left"><colgroup><col></col></colgroup> 
          <tbody> 
            <tr><td style="border-image: initial;"><font size="4">�1.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">������� ����������: ������ ��� ��������� ���� �� ���������� ��� ����������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�2.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">��������� ���� ���� ������ ������� ���� ���������� � ��� ������ ���� �������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�3.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">��� ������� ����������� �� ���� � ������ ����� � ����� 30-50% ������� ������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�4.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">��� ������ ����, � ��������� ��� ��� ������������ ���������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�5.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">����� � ���� ������� ���� ���������, �� �� ������ ���������� ������ �� �������������� �����.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�6.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">��� �������� ������ ����� � ����������� ��� ��� �������� ����� ���������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�7.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">���� ����������� �� ��������� ��� ���������� �������� ��� �����������.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�8.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">���������� �� �������� ������� ��������� ������ � �� �������� ��.</font></p>
               </td></tr>
           
            <tr><td style="border-image: initial;"><font size="4">�9.</font></td><td style="border-image: initial;"> 
                <p dir="ltr"><font size="4">���� ����������� �� 1,5 ����� ����������� ������� ������ � ������ �� ����������� ��������.</font></p>
               </td></tr>
           </tbody>
         </table>
       </div>
     <font size="4"> 
        <br />
       </font></div>
   
    <p dir="ltr"> <b><font size="4">�</font></b></p>
   
    <p dir="ltr"> </p>
   
    <p dir="ltr"><b> 
        <br />
       <font size="5"> ������ ����� �������� ����������� ������ </font></b></p>
   
    <div dir="ltr"> <font size="4"> 
        <br />
       </font> 
      <table width="100%" height="30%" border="1" cellpadding="10" cellspacing="0" align="left"> 
        <tbody> 
          <tr><td><b><font size="4">��� ������</font></b></td><td><b><font size="4">����� ���������</font></b></td><td><b><font size="4">ֳ�� (���)</font></b></td></tr>
         
          <tr><td><font size="4">����������� ������</font></td><td><font size="4">20 - 30 ����</font></td><td><font size="4">�� 2500</font></td></tr>
         
          <tr><td><font size="4">����� ����������� ������</font></td><td><font size="4">7 - 15 ����</font></td><td><font size="4">�� 1000</font></td></tr>
         
          <tr><td><font size="4">�������</font></td><td><font size="4">1 - 2 ���</font></td><td><font size="4">�� 100</font></td></tr>
         </tbody>
       </table>
     </div>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       
        <br />
       </font></p>
   
    <p><b><font size="5"> 
          <br />
         </font></b></p>
   
    <p><b><font size="4">�������� ���������� ������������ ������� ����� ��������:</font></b></p>
   
    <p> </p>
   
    <ul> 
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/" ><font color="#0000ff" size="4">�������������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bankivskoi-spravy/" ><font color="#0000ff" size="4">��������� ������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-zhurnalistyky/" ><font color="#0000ff" size="4">�����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-prava/" ><font color="#0000ff" size="4">�����</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-menedzhmentu/" ><font color="#0000ff" size="4">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-pedahohiky/" ><font color="#0000ff" size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/" ><font color="#0000ff" size="4">��������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-psykhologii/" ><font color="#0000ff" size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/" ><font color="#0000ff" size="4">̳�������� ��������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-finansiv/" ><font color="#0000ff" size="4">Գ�����</font></a></li>
     </ul>
   <font size="4"> 
      <br />
     �� ������ ����� �������� ����������� ������ �� ���������� �� ��� ��������, �� ���������� � ��� ���, ��� � � ����� ��� ������. ����������!</font> 
    <br />
   
    <br />
   </div>
 						 
  <div style="text-align: center;"><font size="5" color="#00ff00"><span style="background-color: rgb(0, 255, 0);">�<a href="/order/" class="price-bt" ><b>�������� ������</b></a>�</span></font></div>
 
  <div style="text-align: center;"> 
    <br />
   </div>
 
  <div style="text-align: center;"> 
    <br />
   </div>
 </div>
 	 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>