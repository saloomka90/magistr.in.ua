window.workFileRequired = false;

function checkType(selectVal){
    var allowedIds = [50928,50929,50930,50932];
    if($(".zakaz-form-l > label > span.required").length == 0){
        $(".zakaz-form-l > label").eq(0).append("<span class='required' />");
    }
    
    var el = $(".zakaz-form-l .required");
    var matched = allowedIds.join("||").match(selectVal);
    
    if( matched != null){
        el.html("Обов’язково прикріпіть методичку");
        window.workFileRequired = true;
    } else{
        el.html(" ");
        window.workFileRequired = false;
    }
}

function plagiatus(val){
    if(val == "yes"){
        $("#plagiatus").parents(".z-val").eq(0).addClass("plagiatus_yes");
        $("#plagiatus_procent").next(".sbHolder").show();
    } else{
        $("#plagiatus").parents(".z-val").eq(0).removeClass("plagiatus_yes");
        $("#plagiatus_procent").next(".sbHolder").hide();
    }
}

$(document).ready(function(){
    $("#plagiatus_procent").next(".sbHolder").hide();
});