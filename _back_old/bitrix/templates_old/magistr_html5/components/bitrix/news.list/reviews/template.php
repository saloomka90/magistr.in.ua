<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="rev-page">
				<a href="javascript:void(0)" onclick="getReview();" class="add-rev"><?=GetMessage("ADD_REVIEW")?></a>
					<?if($arParams["DISPLAY_TOP_PAGER"]):?>
						<?=$arResult["NAV_STRING"]?>
					<?endif;?>
				<div class="clr"></div>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="rev-bl">
						<div class="rev-left">
							
								<div class="stars">
									<?for($i=1; $i<6; $i++):?>
										<span class="rite<?=($i<= intval($arItem["PROPERTIES"]["BALL"]["VALUE"]))? ' act':'';?>"></span>
									<?endfor;?>
								</div>
							
						</div>
						<div class="rev-right">
							<div class="point"></div>
							<p><?=$arItem["~PREVIEW_TEXT"]?></p>
							<div class="rev-name"><strong><?=$arItem["NAME"]?></strong><?if($arItem["PROPERTIES"]["ORDER_ID"]["VALUE"]):?>, (<?=GetMessage("ORDER")?> �<?=$arItem["PROPERTIES"]["ORDER_ID"]["VALUE"]?>)<?endif;?><? if($arItem["ACTIVE_FROM"]): echo ", "; echo FormatDate("j F Y", MakeTimeStamp($arItem["ACTIVE_FROM"]))." ".GetMessage("RIK"); endif;?></div>
						</div>
						<div class="clr"></div>
					</div>
				<?endforeach;?>
				
				<div class="clr"></div>
				<a href="javascript:void(0)" onclick="getReview();" class="add-rev"><?=GetMessage("ADD_REVIEW")?></a>
					<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
						<?=$arResult["NAV_STRING"]?>
					<?endif;?>
				<div class="clr"></div>
			</div>

<script>
function getReview(){
          $('#overlay').fadeIn('fast',function(){
	             $('#nonebox').animate({'top':'40px'},500);
	             $("html, body").scrollTop(0);
	             
	              $.ajax({
					url: '<?=LANG_ROOT_DIR?>/reviews/add_review.php',
					type: 'post',
					success: function(results){
						$('#nonebox .rev-form').html(results);
					}
				});
             }); 
              return false;
};		
</script>		