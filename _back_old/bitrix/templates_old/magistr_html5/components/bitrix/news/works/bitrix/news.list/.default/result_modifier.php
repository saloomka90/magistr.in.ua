<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$cols_cnt = 0;
if($arParams["DISPLAY_PICTURE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_DATE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_NAME"]!="N") $cols_cnt++;
if($arParams["DISPLAY_PREVIEW_TEXT"]!="N") $cols_cnt++;
if(count($arParams["FIELD_CODE"])>0) $cols_cnt += count($arParams["FIELD_CODE"]);
if(count($arParams["PROPERTY_CODE"])>0) $cols_cnt += count($arParams["PROPERTY_CODE"]);

$arResult["COLS_COUNT"] = $cols_cnt;

if($arResult["ITEMS"]){
	foreach ($arResult["ITEMS"] as $arItem) {
		$arSectionFilter[]=$arItem["IBLOCK_SECTION_ID"];
	}
	

	if($arSectionFilter){

		$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y',"ID"=> $arSectionFilter);
  		$db_list = CIBlockSection::GetList(Array(), $arFilter, false, array("ID", "NAME"));
		 
		while($ar_result = $db_list->GetNext())
	  {
	    $arResult["SECTION"]["NAMES"][$ar_result["ID"]] = $ar_result["NAME"];
	  }

	}


}
?>