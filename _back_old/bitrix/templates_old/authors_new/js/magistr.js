$(document).ready(function(){
    $("#hamburger").click(function(){
        $(this).toggleClass("active");
        $("#sidebar").toggleClass("active");
        $("#hamburger2, #topmenu").removeClass("active");
        if(parseInt($(window).outerWidth()) < 767){
            $("body").toggleClass("overlay");
        }
    });

    $("#hamburger2").click(function(){
        $(this).toggleClass("active");
        $("#hamburger, #sidebar").removeClass("active");
        $("#topmenu").toggleClass("active");
        $("body").removeClass("overlay");
    });
    
    $(".my_profile .formField > input[type=text], .my_profile textarea").on("focus", function(){
        $(this).parents(".formRow").addClass("focus");
    });

    $(".my_profile .formField > input[type=text], .my_profile textarea").on("blur", function(){
        $(this).parents(".formRow").removeClass("focus");
    });
    
    //tabs
    if($(".tabsContainer").length > 0){
        $(".tabsContainer").each(function(){
            $(this).children(".tabsMenu").children("span").eq(0).addClass("active");
            $(this).find(".tabContent").hide();
            $(this).children(".tabsContent").children("div").eq(0).children(".tabContent").show(0);
            $(this).children(".tabsContent").find("h6.responsiveTabTitle").eq(0).addClass("active")
        });
        $(".tabsMenu span").click(function(){
            if(!$(this).is(".active")){
                var parent = $(this).parents(".tabsContainer");
                var ind = $(this).index();
                parent.children(".tabsMenu").children("span").removeClass("active").eq(ind).addClass("active");
                parent.children(".tabsContent").children("div").find(".tabContent").hide();
                parent.children(".tabsContent").children("div").eq(ind).find(".tabContent").fadeIn(250);
            }
        });
        $("h6.responsiveTabTitle").click(function(){
            if(!$(this).is(".active")){
                var parent = $(this).parents(".tabsContainer");
                var ind = $(this).parent().index();
                parent.find("h6.responsiveTabTitle").removeClass("active");
                $(this).addClass("active")
                parent.children(".tabsContent").children("div").find(".tabContent").hide();
                parent.children(".tabsContent").children("div").eq(ind).find(".tabContent").fadeIn(250);
            }
        });
    }
    //end tabs
    
    //show optional phone
    $(".addPhone").click(function(){
        $(this).parents(".formRow").addClass("optionalVisible");
        $(this).parents(".formRow").find(".optionalPhone").slideDown(150);
    });
    
    $(".closeOptional").click(function(){
        $(this).parents(".formRow").removeClass("optionalVisible");
        $(this).parents(".formRow").find(".optionalPhone").slideUp(150);        
    });
    //end show optional phone
    
    /*
    if($(".goCalendar").length > 0){
        $('.goCalendar').bootstrapMaterialDatePicker({
            time: false,
    		clearButton: true
        });
    }*/
    
    $(".checkboxW").parents("table").addClass("withCheckbox");
    $(".checkboxW").parents("td").addClass("td_checkbox");
    $(".contentDiv > div > table").parents(".contentDiv").addClass("noborder");
    
    $(".toggleBlocks > div > span").click(function(){
        $(this).toggleClass("active").next().slideToggle(150);
        return false;
    });
    
    $("select").each(function(){
        $(this).before("<span class='selectSpan' />");
        $(this).prev(".selectSpan").html($(this).find("option:selected").text());
        if($(this).val() == "") $(this).prev(".selectSpan").addClass("default");
        else $(this).prev(".selectSpan").removeClass("default");
        $(this).on("change",function(){
            $(this).prev(".selectSpan").html($(this).find("option:selected").text());
            if($(this).val() == "") $(this).prev(".selectSpan").addClass("default");
            else $(this).prev(".selectSpan").removeClass("default");
        });
    });
    
    $(".faqDiv > span").click(function(){
        $(this).toggleClass("active").next("p").slideToggle(150);
    });
    
    $(".focusOn").find("input").eq(0).focus();
    
    $(".showHidePass").mousedown(function(){
        $(this).addClass("active");
        $(this).prev().prop("type","text");
    });

    $(".showHidePass").mouseup(function(){
        $(this).removeClass("active");
        $(this).prev().prop("type","password");
    });
    
    $(".thead input[type=checkbox]").on("change",function(){
        if($(this).is(":checked")){
            $(".contentDiv").find("input[type=checkbox]").prop("checked","checked");
        } else{
            $(".contentDiv").find("input[type=checkbox]").removeProp("checked");
        }
    });
    
    if($(".workSelected").length > 0){
        var workcontentDiv = $(".contentDiv");
        
        workcontentDiv.find("input[type=checkbox]").on("change",function(){
            var workCheck = 1;
            workcontentDiv.find("input[type=checkbox]").each(function(){
                if($(this).is(":checked")){
                    workCheck+=1;
                }
            });
            if($(this).parents(".thead")){
                workCheck -= 1;
            }
            
            if(workCheck < 1){
                $(".workSelected").removeClass("active");
                $(".workSelected span").html("");
                $(".thead input[type=checkbox]").removeProp("checked");
            }
            
            if(workCheck == 1){
                $(".workSelected").addClass("active");
                $(".workSelected span").html(workCheck + " " + $(".workSelected span").data("one"));
            }
            if(workCheck > 1 && workCheck < 5){
                $(".workSelected").addClass("active");
                $(".workSelected span").html(workCheck + " " + $(".workSelected span").data("four"));
            }
            if(workCheck > 5){
                $(".workSelected").addClass("active");
                $(".workSelected span").html(workCheck + " " + $(".workSelected span").data("multi"));
            }
        });
    }
});