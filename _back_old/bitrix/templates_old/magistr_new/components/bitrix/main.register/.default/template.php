<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo "<pre>"; print_r($arParams); print_r($arResult); echo "</pre>";

$ar_auth2_fields = array("UF_LIVING_PLACE", "UF_BIRTH_YEAR", "UF_OCCUPATION", "UF_OPIT", "UF_WORK_TYPES", "UF_SPHERE", "UF_LANGS", "UF_OTHER_DATA", "UF_SUBJECTS");

if (count($arResult["ERRORS"]) > 0)
{
	foreach ($arResult["ERRORS"] as $key => $error)
	{
		if (intval($key) <= 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
	}

	ShowError(implode("<br />", $arResult["ERRORS"]));
}
elseif($arResult["VALUES"]["USER_ID"]>0)
{
	if($_POST["auth_type"]==2)
	{
		$user_id = $arResult["VALUES"]["USER_ID"];
		$user = new CUser;
		$user->Update($user_id, array("GROUP_ID"=>array(5)));

		$rsUser = CUser::GetByID($user_id);
		if($arUser = $rsUser->Fetch())
		{
			foreach($arUser as $key=>$value) $arEventFields["USER_".$key] = $value;
			CEvent::Send("NEW_AUTHOR", "s1", $arEventFields);
			CEvent::CheckEvents();
		}
	}

	if($arResult["USE_EMAIL_CONFIRMATION"] === "Y")
	{
		?><p><?ShowNote(GetMessage("REGISTER_EMAIL_WILL_BE_SENT"))?></p><?
	}
	else
	{
		?><p><?ShowNote(GetMessage("REGISTER_OK"))?></p><?
	}
}
?>
<script type="text/javascript">
	function regSubmit(caller, pref)
	{
		$(caller).append('<input type="hidden" name="REGISTER[EMAIL]" value="'+$("#reg_LOGIN").attr("value")+'" />');
		if($("#rules").attr("checked")==false)
		{
			alert('<?=GetMessage("REG_RULES_NOT_ACCEPTED")?>');
			return false;
		}
		return submForm(pref);
	}
</script>
<form onsubmit="return regSubmit(this, 'reg');" method="POST" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
$js_titles = "";
?>
<h3><?=GetMessage("REG_I_WANT");?>:</h3>
<div>
	<div class="radio"><input<?if($_POST['auth_type']==1||!isset($_POST['auth_type'])):?> checked="checked"<?endif;?> onclick="sw('auth2_fields', !this.checked)" type="radio" class="radio" id="auth_type1" name="auth_type" value="1" /></div>
	<label for="auth_type1"><?=GetMessage("REG_SALE");?></label>
	<div class="clear">&nbsp;</div>
	<small><?=GetMessage("REG_SALE_DESCR");?></small>
</div><br />
<div>
	<div class="radio"><input<?if($_POST['auth_type']==2):?> checked="checked"<?endif;?> onclick="sw('auth2_fields', this.checked)" type="radio" class="radio" id="auth_type2" name="auth_type" value="2" /></div>
	<label for="auth_type2"><?=GetMessage("REG_SALE_ORDER");?></label>
	<div class="clear">&nbsp;</div>
	<small><?=GetMessage("REG_SALE_ORDER_DESCR");?></small>
</div><br />
<div class="reg_col">
<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):
	if($FIELD=="EMAIL") continue;

	$title = GetMessage("CUSTOM_TITLE_".$FIELD);
	if(strlen($title)<=0) $title = GetMessage("REGISTER_FIELD_".$FIELD);
	if($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y") $title .= "*";

	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$FIELD.": '".$title."'";

	switch ($FIELD)
	{
		case "PASSWORD":
		case "CONFIRM_PASSWORD":
			?><input title="<?=$title?>" class="inputtext wide" size="30" type="password" name="REGISTER[<?=$FIELD?>]" id="reg_<?=$FIELD?>" /><div class="wide_field_descr"><?=$title?></div><?
		break;

		case "PERSONAL_GENDER":
			?><select class="wide" title="<?=$title?>" name="REGISTER[<?=$FIELD?>]"><option value="">(<?=$title?>)</option>
				<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
				<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
				<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
			</select><?
		break;

		case "PERSONAL_COUNTRY":
			//echo "<pre>"; print_r($arResult["COUNTRIES"]); echo "</pre>";
			?><select title="<?=$title?>" name="REGISTER[<?=$FIELD?>]"><option value="">(<?=$title?>)</option><?
			foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
			{
				?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
			<?
			}
			?></select><?
		break;

		case "PERSONAL_PHOTO":
		case "WORK_LOGO":
			?><input title="<?=$title?>" size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
		break;

		case "PERSONAL_NOTES":
		case "WORK_NOTES":
			?><textarea title="<?=$title?>" cols="30" rows="5" name="REGISTER[<?=$FIELD?>]" id="reg_<?=$FIELD?>" onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')"><?=$arResult["VALUES"][$FIELD]?></textarea><?
		break;
		default:
			if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
			?><input title="<?=$title?>" class="inputtext wide" size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=(strlen($arResult["VALUES"][$FIELD])>0?$arResult["VALUES"][$FIELD]:$title)?>" id="reg_<?=$FIELD?>" onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" /><?
				if ($FIELD == "PERSONAL_BIRTHDAY")
					$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'N',
							'FORM_NAME' => 'regform',
							'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
							'SHOW_TIME' => 'N'
						),
						null,
						array("HIDE_ICONS"=>"Y")
					);
				?><?
	}?>
	<div class="clear">&nbsp;</div>
<?endforeach?>
<?// ********************* User properties ***************************************************?>
<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<?
	//$title = strLen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB");
	?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
	<?
		if(in_array($FIELD_NAME, $ar_auth2_fields))
		{
			ob_start();
		}
		$title = GetMessage("REGISTER_FIELD_".$FIELD_NAME);
		$descr = GetMessage("REGISTER_FIELD_".$FIELD_NAME."_DESCR");
		if(strlen($title)<=0) $title = $arUserField["EDIT_FORM_LABEL"].($arUserField["MANDATORY"]=="Y"?"*":"");
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$FIELD_NAME.": '".$title."'";
	?>
		<div id="register-field-<?=$FIELD_NAME?>">
			<?if($arUserField["SETTINGS"]["DISPLAY"]=="CHECKBOX"):?>
				<div><?=$title?></div>
			<?endif;?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform", "field_params"=>array("class"=>"inputtext wide", "id"=>"reg_".$FIELD_NAME, "title"=>$title, "onblur"=>"unsetFocusField(this, 'reg')", "onfocus"=>"setFocusField(this, 'reg')")), null, array("HIDE_ICONS"=>"Y"));?>
			<?if(strlen($descr)>0):?><div class="wide_field_descr"><?=$descr?></div><?endif;?>
			<div class="clear">&nbsp;</div>
		</div>
	<?
		if(in_array($FIELD_NAME, $ar_auth2_fields))
		{
			$content = ob_get_contents();
			ob_end_clean();
			$auth2_fields .= $content;
		}
	endforeach;
	?>
<?endif;?>
<div id="auth2_fields"<?if($_POST["auth_type"]!=2):?> style="display:none;"<?endif;?>>
<?=$auth2_fields?>
</div>
<?// ******************** /User properties ***************************************************?>
<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?><div class="captcha">
		<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
		<div class="pd"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="192" height="40" alt="CAPTCHA" /></div>
		<div class="clear">&nbsp;</div>
		<?
		$title = GetMessage("REGISTER_CAPTCHA_PROMT")."*";
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_captcha_word: '".$title."'";
		?>
		<input title="<?=$title?>" class="inputtext" type="text" name="captcha_word" maxlength="50" value="<?=$title?>" id="reg_captcha_word" onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" /><div class="clear">&nbsp;</div>
	</div><?
}
/* CAPTCHA */
?>		<div class="rules">
			<div class="check"><input type="checkbox" id="rules" name="rules" value="1" /></div>
			<?=str_replace("#RULES_LINK#",(LANG_ID!=LANG_ID_DEF?"/".LANG_ID:"")."/autors.php", GetMessage("REG_RULES"));?>
			<div class="clear">&nbsp;</div>
		</div>

		<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
		<input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" /><br />

<?/*<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>*/?>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>
</div>
</form>
<?
$script = '
<script type="text/javascript">
	var names_reg = new Object;
	names_reg = {'.$js_titles.'};
</script>
';
$APPLICATION->AddHeadString($script);
?>
<script type="text/javascript">
	var allways_disable = false;
	function disableChecks(cp) {
		if(allways_disable) {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else if(jQuery("input:checkbox:checked",cp).size()>=3) {
			jQuery("input:checkbox:not(:checked)",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","");});
		}
	}
	jQuery(document).ready(function(){
		jQuery("#register-field-UF_SPHERE input:checkbox").each(function(){
			jQuery(this).bind("click",function(){
				disableChecks(jQuery(this).parent().parent());
			});
		});
		disableChecks(jQuery("#register-field-UF_SPHERE"));
	});
</script>