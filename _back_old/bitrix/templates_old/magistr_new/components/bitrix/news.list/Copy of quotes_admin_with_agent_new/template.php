<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
// echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
$prt_ids = array();
foreach($arResult["ITEMS"] as $arItem)
{
	$ids[] = $arItem["ID"];
	if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["partner_id"]["VALUE"];
	
	if($arItem["PROPERTIES"]["agent_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["agent_id"]["VALUE"];
	
}

$user_responses = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","@PROPERTY_order_id"=>$ids,"PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_responses[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$user_refused = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","@PROPERTY_order_id"=>$ids,"!PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_refused[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$partners = array();
$rs_ur = CUser::GetList($sort="", $order="",array("ID"=>implode("|",$prt_ids)));
while($ar_ur=$rs_ur->GetNext()) $partners[$ar_ur["ID"]] = $ar_ur;

//echo "<pre>".print_r($user_responses,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id, back_to_perf_id,refused,other_params)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'+(!!back_to_perf_id ? '&back_to_perf_id='+back_to_perf_id : '')+(!!refused?'&refused=Y':'')+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
	}
	function showPerformers(order_id,user_id,detail,refused)
	{
		<?/* ?>
		$.nyroModalManual({
			url: '<?=$cur_dir?>/performers.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':'')
		});
		return false;
		<? */?>
		$.ajax({
 		   type: "GET",
 		   url: "<?=$cur_dir?>/performers.php",
 		   data: 'o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':''),
 		   success: function(msg){
 		     alert( msg );
 		   }
 		 });
 	
 	
 	
 	
      $('#overlay_ws').fadeIn('fast',function(){
			$('#nonebox_ws').animate({'top':'40px'},500);
      });


      
	}
	function showPartners(order_id,user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partners.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editPartners(user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partner_edit.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function changeState(order_id,comment,state,performer,partner_sum,author_sum_paid,author_sum_total,sum_paid,sum_total,q_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&comment='+escape(comment)+'&state='+state+'&performer='+performer+'&partner_sum='+partner_sum+'&author_sum_paid='+author_sum_paid+'&author_sum_total='+author_sum_total+'&sum_paid='+sum_paid+'&sum_total='+sum_total+'&Q_ID='+q_id+'&lang=<?=LANG_ID?>&save=Y'
		});
		return false;
	}
	function selectAuthor(order_id,user_id)
	{
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&performer='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editOrder(order_id)
	{
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_edit.php?CODE='+order_id+'&lang=<?=LANG_ID?>',
			endShowContent: function(elts, settings) {
				setFormOptions();
				setTimeout('$("#nyroModalContent").scrollTo("p:eq(0)",200);',400);
			}
		});
		return false;
	}
	//]]>



	//Pop-up window autor list
    $(function() {
      
        $('#overlay_ws, #close_ws').live('click',function(){
             $('#nonebox_ws').animate({'top':'-2000px'},1000,function(){
				$('#overlay_ws').fadeOut('fast');
             });
			 return false;
        });	
    });
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<div class="table_box work_list_box">
					<div class="send_box_hide">
						<div class="send_box">
							<span class="send_box_arr"><span></span></span>
							<div class="send_box_title">��������� ��������:</div>
							<div class="send_box_val">
								<input type="radio" name="send_type" id="send_type" checked="checked" value="0" />
								<label for="send_type">�� ������</label>
								<div class="clr"></div>
							</div>
							<div class="send_box_val">
								<input type="radio" name="send_type" id="send_type2" value="1" />
								<label for="send_type2">�� i���� ����</label>
								<input type="text" id="send_date" value="22.11.13" />
								<div class="clr"></div>
							</div>
							<a class="cancel" href="#" onclick="$(this).parent().remove(); return false;">���������</a>
							<input type="submit" value="OK" />
							<div class="clr"></div>						
						</div>
					</div>
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
						<thead>
							<tr>
								<th class="today" colspan="5">�������i, 22.12.2013</th>
							</tr>
						</thead>
						<tbody>
<?foreach($arResult["ITEMS"] as $arItem):?>
<? 

	//var_dump($arItem);
	$style = "am1";
	
	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{	
		case 23:
			$style = 'am1';
			break;
		case 24:
			$style = 'am3';
			break;
		case 21:
			$style = 'am4';
			break;		
		case 22:
			$style = 'am2';
			break;
		
	}
	?> 

<tr class="<?=$style;?>">
								<td class="nomer"><?=$arItem["ID"]?></td>
								
								
									
									<td class="sub">
										<a href="#" onclick="showOrderDetails(<?=$arItem["ID"]?>);"><?=$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"]?></a> 
										<br>
											<span><?=(int)$arItem["PROPERTIES"]["sum_paid"]["VALUE"]?> i� <?=(int)$arItem["PROPERTIES"]["sum_total"]["VALUE"]?></b> ���.
										
										<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
											<?echo $arItem["~PREVIEW_TEXT"];?>
										<?endif;?>
									</td>
									
								<td class="tip"><?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?></td>
								<td style="text-align: center;">									
									<a class="f_la"href="javascript:void(0)" onclick="showPerformers(<?=$arItem["ID"]?>,'');"><?=(sizeof($user_responses[$arItem["ID"]]))?></a>
								</td>
								<td class="status2"><?=$arItem["PROPERTIES"]["state"]["VALUE"];?></td>
								
							</tr>
	

<?endforeach;?>
</tbody>
					</table>
</div>
<?/*if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;*/?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>