<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
//echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
foreach($arResult["ITEMS"] as $arItem)
	if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
		$ids[] = $arItem["PROPERTIES"]["partner_id"]["VALUE"];

$arUsers = array();
$rsUser = CUser::GetList($by="",$order="",array("ID"=>implode("|",$ids)));
while($arUser=$rsUser->Fetch())
	$arUsers[$arUser["ID"]] = $arUser;

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id, other_params)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
	}
	function showPartners(user_id)
	{
		$.nyroModalManual({
			url: '/bitrix/templates/magistr/components/custom/users.list/partners_admin/partners.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editPartners(user_id)
	{
		$.nyroModalManual({
			url: '/bitrix/templates/magistr/components/custom/users.list/partners_admin/partner_edit.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function changeState(order_id,state)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&state='+state+'&lang=<?=LANG_ID?>&save=Y'
		});
		return false;
	}
	//]]>
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<td style="width:100% !important;"><?=GetMessage("IBLOCK_PROP_topic")?></td>
		<td><?=GetMessage("IBLOCK_PROP_type")?></td>
		<?foreach($arParams["FIELD_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_FIELD_".$key));?>
			</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $key):if(strlen($key)>0):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_PROP_".$key));?>
				<?if($key=="state"):?><img src="/images/1.gif" width="200" height="1" alt="" /><?endif;?>
			</td>
		<?endif;endforeach;?>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
//echo "<pre>".print_r($arItem,true)."</pre>";
	//if((!is_array($user_responses[$arItem["ID"]]) || !sizeof($user_responses[$arItem["ID"]])) && $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21):
		$link_o = '<a href="javascript:void(0)" onclick="showOrderDetails('.$arItem["ID"].');">';
		$link_c = '</a>';
	//else:
	//	$link_o = '';
	//	$link_c = '';
	//endif;
//echo "<pre>".print_r($arItem["PROPERTIES"]["state"],true)."</pre>";
	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{
		case 22:
			$style = ' style="background:#fa832b;"';
			break;
		case 23:
			$style = ' style="background:#99CCFF;"';
			break;
		case 24:
			if(is_array($arItem["PROPERTIES"]["partner_paid"]["VALUE_ENUM_ID"]) && reset($arItem["PROPERTIES"]["partner_paid"]["VALUE_ENUM_ID"])>0)
			{
				$arItem["DISPLAY_PROPERTIES"]["state"]["VALUE"] = $arItem["PROPERTIES"]["partner_paid"]["VALUE"];
				$style = ' style="background:#cccccc;"';
			}
			else
				$style = ' style="background:#b4e828;"';
			break;
		default:
			//if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0) $style = ' style="background:#eeff61;"';
			//else
			$style = '';
			break;
	}
	?>
	<tr<?=$style?>>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<td><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></td>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0):?><div class="referal-order">Referal</div><?endif;?>
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
			<div><b><?=GetMessage("IBLOCK_FIELD_ID")?></b> <?=sprintf("%07d",$arItem["ID"])?></div>
			<?if(strlen($arItem["PROPERTIES"]["discount"]["VALUE"])>0):?>
				<span style="color:#ff0000;">%</span>
			<?endif;?>
		</td>
		<?endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["topic"]):?>
		<td>
			<?=$link_o?><?=$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"]?><?=$link_c?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["topic"]);?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<br />[<?echo $arItem["~PREVIEW_TEXT"];?>]
			<?endif;?>
		</td>
		<?//endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["type"]):?>
		<td>
			<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["type"]);?>
		</td>
		<?//endif?>
		
		<?/*if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<td>
			<?echo $arItem["NAME"]?>
		</td>
		<?endif;*/?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
		<td align="center">
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
		</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
		<td align="center">
			<?
				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;
				
				/*if($pid=="state"&&($arItem["PROPERTIES"]["performer"]["VALUE"]>0||$arItem["PROPERTIES"]["performer"]["VALUE"]==-1))
				{
					$value_text .= '<br />';
					if($arItem["PROPERTIES"]["performer"]["VALUE"]>0)
					{
						$rsUser = CUser::GetByID($arItem["PROPERTIES"]["performer"]["VALUE"]);
						if($arUser=$rsUser->Fetch())
						{
							$rsQuote = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"PROPERTY_order_id"=>$arItem["ID"],"ACTIVE"=>"Y","PROPERTY_author_id"=>$arUser["ID"]),false,array("nTopCount"=>1),array("ID","PROPERTY_SUMM"));
							$arQuote = $rsQuote->Fetch();
							$value_text .= '['.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].']<br />'.$arQuote["PROPERTY_SUMM_VALUE"].' ���.';
						}
					}
					else
					{
						$value_text .= '['.GetMessage('PERFORMER_OTHER').']';
					}
				}
				else*/if($pid=="partner_id" && $arProperty["VALUE"]>0)
				{
					$rsUser = CUser::GetByID($arProperty["VALUE"]);
					if($arUser=$rsUser->Fetch())
					{
						$value_text = '<a href="javascript:void(0)" onclick="showPartners('.$arUser["ID"].')">'.str_replace(' ','<br />',$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"]).'</a>';
					}
				}
				elseif($pid=="partner_sum")
				{
					if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
					{
						$value_text = floatval($value_text)*$arUsers[$arItem["PROPERTIES"]["partner_id"]["VALUE"]]["PERSONAL_NOTES"]/100;
						if($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==23)
							$value_text = '&asymp;'.$value_text;
						elseif($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]!=24)
							$value_text = '-';
						
						if(in_array($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"], array(23, 24)))
							$value_text .= ' ���.';
					}
					else
					{
						$value_text = '-';
					}
				}
				
				echo $value_text;
			?>
		</td>
		<?endforeach;?>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>