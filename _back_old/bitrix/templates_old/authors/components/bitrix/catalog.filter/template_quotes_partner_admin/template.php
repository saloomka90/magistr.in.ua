<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
//echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
$ar_new_values = array("(���)"=>GetMessage("CUSTOM_TITLE_#0"),"������������"=>GetMessage("CUSTOM_TITLE_#1"),"��������� ����������"=>GetMessage("CUSTOM_TITLE_#2"),"���������� ���������"=>GetMessage("CUSTOM_TITLE_#3"),"��������"=>GetMessage("CUSTOM_TITLE_#4"),"����"=>GetMessage("CUSTOM_TITLE_#5"),"³������ ���������� ������"=>GetMessage("CUSTOM_TITLE_#6"),"������� ������"=>GetMessage("CUSTOM_TITLE_#7"),"������"=>GetMessage("CUSTOM_TITLE_#8"));

$ar_replacements = array(
	'#<input type=\"text\" name=\"'.$arParams["FILTER_NAME"].'_ff\[ID\]\[LEFT\]\" size="5" value="[^\"]*" />&nbsp;��&nbsp;<input type="text" name="'.$arParams["FILTER_NAME"].'_ff\[ID\]\[RIGHT\]" size="5" value="[^\"]*" />#Usi' => '<input type="text" name="'.$arParams["FILTER_NAME"].'_ff[ID]" value="'.($_REQUEST[$arParams["FILTER_NAME"]."_ff"]["ID"]["LEFT"]).'" />',
);
?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?//echo $arResult["FORM_ACTION"]?>" method="get">
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
	<table class="data-table" cellspacing="0" cellpadding="2">
	<thead>
		<tr>
			<td colspan="2" align="center"><?=GetMessage("IBLOCK_FILTER_TITLE")?></td>
		</tr>
	</thead>
	<tbody>
		<?foreach($arResult["ITEMS"] as $cnt => $arItem):?>
			<?
			if(!array_key_exists("HIDDEN", $arItem)):
				$code = "";
				$cnt_in = 0;
				foreach($arResult["arrProp"] as $vals):
					if($cnt_in<$cnt):
						$cnt_in++;
						continue;
					else:
						$code = $vals["CODE"];
						break;
					endif;
				endforeach;
				$title = GetMessage("CUSTOM_TITLE_".$code);
				if(strlen($title)<=0) $title = GetMessage("CUSTOM_TITLE_".$arItem["NAME"]);
				if(strlen($title)<=0) $title = $arItem["NAME"];
				foreach($ar_new_values as $key=>$value):
					if(strlen($value)>0) $arItem["INPUT"] = str_replace($key,$value,$arItem["INPUT"]);
				endforeach;

				foreach($ar_replacements as $ar_rk => $ar_rv):
					$arItem["INPUT"] = preg_replace($ar_rk,$ar_rv,$arItem["INPUT"]);
				endforeach;
				?>
				<tr>
					<td valign="top"><?=$title?>:</td>
					<td valign="top"><?=$arItem["INPUT"]?></td>
				</tr>
			<?endif?>
		<?endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
		</tr>
	</tfoot>
	</table>
</form>
