<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
?>
<div class="add"><a href="javascript:sw('work_add_form')" class="add_review"><?=GetMessage("ADD_NEW_WORK")?></a></div>
<div class="clear">&nbsp;</div>
<?$APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "", $arParams, $component);?>
<?
$APPLICATION->IncludeComponent("bitrix:iblock.element.add.list", "", $arParams, $component);
?>