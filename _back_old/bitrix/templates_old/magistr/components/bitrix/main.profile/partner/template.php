<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo "<pre>"; print_r($arResult); echo "</pre>";
//exit();
//echo "<pre>"; print_r($_SESSION); echo "</pre>";
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
//if($USER->IsAdmin()) echo "<pre>".print_r($_SESSION,true)."</pre>";
if ($arResult['DATA_SAVED'] == 'Y')
{
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}
?>
<script type="text/javascript">
var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<form onsubmit="return submForm('reg');" method="POST" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<?
$js_titles = "";
?>
<div class="reg_col">
	<?
	/*if($arResult["ID"]>0)
	{
	?>
		<?
		if (strlen($arResult["arUser"]["TIMESTAMP_X"])>0)
		{
		?>
		<tr>
			<td><?=GetMessage('LAST_UPDATE')?></td>
			<td><?=$arResult["arUser"]["TIMESTAMP_X"]?></td>
		</tr>
		<?
		}
		?>
		<?
		if (strlen($arResult["arUser"]["LAST_LOGIN"])>0)
		{
		?>
		<tr>
			<td><?=GetMessage('LAST_LOGIN')?></td>
			<td><?=$arResult["arUser"]["LAST_LOGIN"]?></td>
		</tr>
		<?
		}
		?>
	<?
	}*/
	?>
	<?
	$title = GetMessage("LOGIN")."*";
	$name = "LOGIN";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	?>
	<input type="text" class="inputtext wide" id="<?=$id?>" title="<?=$title?>" value="<?=$value?>" maxlength="50" disabled="dissabled" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NEW_PASSWORD");
	$name = "NEW_PASSWORD";
	$id = "reg_".$name;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" autocomplete="off" maxlength="50" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NEW_PASSWORD_CONFIRM");
	$name = "NEW_PASSWORD_CONFIRM";
	$id = "reg_".$name;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" maxlength="50" autocomplete="off" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NAME")."*";
	$name = "NAME";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("USER_PHONE")."*";
	$name = "PERSONAL_PHONE";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("USER_WWW");
	$name = "WORK_WWW";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<? // ********************* User properties ***************************************************?>
	<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
		<?$first = true;?>
		<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
			<?
//if($USER->IsAdmin()) echo "<pre>".print_r($arUserField,true)."</pre>";
			if(in_array($FIELD_NAME, $ar_auth2_fields))
			{
				ob_start();
			}
			$title = GetMessage($FIELD_NAME);
			$descr = GetMessage("REGISTER_FIELD_".$FIELD_NAME."_DESCR");
			if(strlen($title)<=0) $title = $arUserField["EDIT_FORM_LABEL"];
			$title .= ($arUserField["MANDATORY"]=="Y"?"*":"");
			$name = $FIELD_NAME;
			$id = "reg_".$name;
			$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
			$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
			?>
			<div id="register-field-<?=$FIELD_NAME?>">
				<?if($arUserField["SETTINGS"]["DISPLAY"]=="CHECKBOX"):?>
					<div><?=$title?></div>
				<?endif;?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "field_params"=>array("class"=>"inputtext wide", "id"=>"reg_".$FIELD_NAME, "title"=>$title, "onblur"=>"unsetFocusField(this, 'reg')", "onfocus"=>"setFocusField(this, 'reg')")), null, array("HIDE_ICONS"=>"Y"));?>
				<?if(strlen($descr)>0):?><div class="wide_field_descr"><?=$descr?></div><?endif;?>
				<div class="clear">&nbsp;</div>
			</div>
			<?
			if(in_array($FIELD_NAME, $ar_auth2_fields))
			{
				$content = ob_get_contents();
				ob_end_clean();
				$auth2_fields .= $content;
			}
		endforeach;
		?>
	<?endif;?>
<div id="auth2_fields"<?if(($_POST["auth_type"]!=2||!isset($_POST["auth_type"]))&&!$group_ok):?> style="display:none;"<?endif;?>>
<?=$auth2_fields?>
</div>
	<div><?=GetMessage("USER_REFERAL_LINK");?></div>
	<div><b>http://<?=$_SERVER["HTTP_HOST"]?>/ref_<?=$arResult["arUser"]["ID"]?></b></div>
	<?// ******************** /User properties ***************************************************?>
	<div class="clear">&nbsp;</div>
	<?/*<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>*/?>
	<p><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("SAVE") : GetMessage("ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('RESET');?>"></p>
</form>
</div>
<?
$script = '
<script type="text/javascript">
	var names_reg = new Object;
	names_reg = {'.$js_titles.'};
</script>
';
$APPLICATION->AddHeadString($script);
?>