<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
// echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
$prt_ids = array();
foreach($arResult["ITEMS"] as $arItem)
{
	$ids[] = $arItem["ID"];
	if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["partner_id"]["VALUE"];
	
	if($arItem["PROPERTIES"]["agent_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["agent_id"]["VALUE"];
	
}

$user_responses = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","@PROPERTY_order_id"=>$ids,"PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_responses[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$user_refused = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","@PROPERTY_order_id"=>$ids,"!PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_refused[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$partners = array();
$rs_ur = CUser::GetList($sort="", $order="",array("ID"=>implode("|",$prt_ids)));
while($ar_ur=$rs_ur->GetNext()) $partners[$ar_ur["ID"]] = $ar_ur;

//echo "<pre>".print_r($user_responses,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id, back_to_perf_id,refused,other_params)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'+(!!back_to_perf_id ? '&back_to_perf_id='+back_to_perf_id : '')+(!!refused?'&refused=Y':'')+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
	}
	function showPerformers(order_id,user_id,detail,refused)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/performers.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':'')
		});
		return false;
	}
	function showPartners(order_id,user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partners.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editPartners(user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partner_edit.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function changeState(order_id,comment,state,performer,partner_sum,author_sum_paid,author_sum_total,sum_paid,sum_total,q_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&comment='+escape(comment)+'&state='+state+'&performer='+performer+'&partner_sum='+partner_sum+'&author_sum_paid='+author_sum_paid+'&author_sum_total='+author_sum_total+'&sum_paid='+sum_paid+'&sum_total='+sum_total+'&Q_ID='+q_id+'&lang=<?=LANG_ID?>&save=Y'
		});
		return false;
	}
	function selectAuthor(order_id,user_id)
	{
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&performer='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editOrder(order_id)
	{
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_edit.php?CODE='+order_id+'&lang=<?=LANG_ID?>',
			endShowContent: function(elts, settings) {
				setFormOptions();
				setTimeout('$("#nyroModalContent").scrollTo("p:eq(0)",200);',400);
			}
		});
		return false;
	}
	//]]>
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<td style="width:100% !important;"><?=GetMessage("IBLOCK_PROP_topic")?></td>
		<td><?=GetMessage("IBLOCK_PROP_type")?></td>
		<td><?=GetMessage("IBLOCK_PROP_performers")?></td>
		<?foreach($arParams["FIELD_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_FIELD_".$key));?>
			</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $key):if(strlen($key)>0):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_PROP_".$key));?>
				<?if($key=="state"):?><img src="/images/1.gif" width="200" height="1" alt="" /><?endif;?>
			</td>
		<?endif;endforeach;?>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
//echo "<pre>".print_r($arItem,true)."</pre>";
	//if((!is_array($user_responses[$arItem["ID"]]) || !sizeof($user_responses[$arItem["ID"]])) && $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21):
		$link_o = '<a href="javascript:void(0)" onclick="showOrderDetails('.$arItem["ID"].');">';
		$link_c = '</a>';
	//else:
	//	$link_o = '';
	//	$link_c = '';
	//endif;
//echo "<pre>".print_r($arItem,true)."</pre>";
	
	if($arItem["ACTIVE"] != "N"){
	
			switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
			{
				case 22:
					$style = ' style="background:#fa832b;"';
					break;
				case 23:
					$style = ' style="background:#b4e828;"';
					break;
				case 24:
					$style = ' style="background:#cccccc;"';
					break;
				default:
					//if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0) $style = ' style="background:#eeff61;"';
					//else
					$style = '';
					break;
			}
	}else{
		$style = ' style="background:#865306;"'; 
	}
	?>
	<tr<?=$style?>>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<td><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></td>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0):?><div class="referal-order" title="[ID: <?=$arItem["PROPERTIES"]["partner_id"]["VALUE"]?>] <?=($partners[$arItem["PROPERTIES"]["partner_id"]["VALUE"]]["NAME"]." ".$partners[$arItem["PROPERTIES"]["partner_id"]["VALUE"]]["LAST_NAME"])?>">Referal</div><?endif;?>
			
			<?if($arItem["PROPERTIES"]["agent_id"]["VALUE"]>0):?><div class="referal-order" style="color: rgb(218, 17, 226);" title="[ID: <?=$arItem["PROPERTIES"]["agent_id"]["VALUE"]?>] <?=($partners[$arItem["PROPERTIES"]["agent_id"]["VALUE"]]["NAME"]." ".$partners[$arItem["PROPERTIES"]["agent_id"]["VALUE"]]["LAST_NAME"])?>">Agent</div><?endif;?>
			
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
			<div><b><?=GetMessage("IBLOCK_FIELD_ID")?></b> <?=$arItem["ID"]?><?//=sprintf("%07d",$arItem["ID"])?></div>
			<?if(strlen($arItem["PROPERTIES"]["discount"]["VALUE"])>0):?>
				<span style="color:#ff0000;">%</span>
			<?endif;?>
		</td>
		<?endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["topic"]):?>
		<td>
			<?=$link_o?><?=$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"]?><?=$link_c?> (<small><a href="javascript:;" onclick="editOrder(<?=$arItem["ID"]?>);"><?=GetMessage("EDIT")?></a></small>)
			<?unset($arItem["DISPLAY_PROPERTIES"]["topic"],$arItem["DISPLAY_PROPERTIES"]["pref_price"],$arItem["DISPLAY_PROPERTIES"]["sum_paid"]);?>
			<?//if(($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]) || in_array($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"],array(23,24))):?>
				<br />
			<?//endif;?>
			<?//if(in_array($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"],array(23,24))):?>
				<b><?=(int)$arItem["PROPERTIES"]["sum_paid"]["VALUE"]?></b> � <b><?=(int)$arItem["PROPERTIES"]["sum_total"]["VALUE"]?></b> ���.
			<?//endif;?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				[<?echo $arItem["~PREVIEW_TEXT"];?>]
			<?endif;?>
		</td>
		<?//endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["type"]):?>
		<td>
			<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["type"]);?>
		</td>
		<?//endif?>
		
		<td align="center">
			<a href="javascript:void(0)" onclick="showPerformers(<?=$arItem["ID"]?>,'');"><?=(sizeof($user_responses[$arItem["ID"]]))?></a>
			&nbsp;/&nbsp;
			
			<a style="color:#ff0000;" href="javascript:void(0)" onclick="showPerformers(<?=$arItem["ID"]?>,'',false,true);"><?=(sizeof($user_refused[$arItem["ID"]]))?></a>
		</td>
		
		<?/*if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<td>
			<?echo $arItem["NAME"]?>
		</td>
		<?endif;*/?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
		<td align="center">
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
		</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
		<td align="center">
			<?
				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;
				
				if($pid=="state" && ($arItem["PROPERTIES"]["performer"]["VALUE"]>0||$arItem["PROPERTIES"]["performer"]["VALUE"]==-1))
				{  
					$value_text .= '<br />';
					if($arItem["PROPERTIES"]["performer"]["VALUE"]>0)
					{
						$rsUser = CUser::GetByID($arItem["PROPERTIES"]["performer"]["VALUE"]);
						if($arUser=$rsUser->Fetch())
						{
							$rsQuote = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"PROPERTY_order_id"=>$arItem["ID"],"ACTIVE"=>"Y","PROPERTY_author_id"=>$arUser["ID"]),false,array("nTopCount"=>1),array("ID","PROPERTY_SUMM"));
							$arQuote = $rsQuote->Fetch();
							$value_text .= '[<a href="javascript:void(0)" onclick="showPerformers('.$arItem["ID"].','.$arUser["ID"].')">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</a>]<br /><b>'.(int)$arItem["PROPERTIES"]["author_sum_paid"]["VALUE"].'</b> � <b>'.(int)$arQuote["PROPERTY_SUMM_VALUE"].'</b> ���.';
						}
					}
					else
					{
						$value_text .= '['.GetMessage('PERFORMER_OTHER').']<br /><b>'.(int)$arItem["PROPERTIES"]["author_sum_paid"]["VALUE"].'</b> � <b>'.(int)$arItem["PROPERTIES"]["author_sum_total"]["VALUE"].'</b> ���.';
					}
				}
				elseif( ($pid=="partner_id" && $arProperty["VALUE"]>0) || ($pid=="agent_id" && $arProperty["VALUE"]>0)  )
				{    echo $arProperty["VALUE"];
					$rsUser = CUser::GetByID($arProperty["VALUE"]);
					if($arUser=$rsUser->Fetch())
					{
						$value_text = '<a href="javascript:void(0)" onclick="showPartners('.$arItem["ID"].','.$arUser["ID"].')">'.str_replace(' ','<br />',$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"]).'</a>';
					   
					}
				}
				elseif($pid=="partner_sum")
				{
					if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
					{
						$value_text = floatval($value_text);
						if($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==23)
							$value_text = '?'.$value_text;
						elseif($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]!=24)
							$value_text = '-';
						
						if(in_array($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"], array(23, 24)))
							$value_text .= ' ���.';
					}
					else
					{
						$value_text = '-';
					}
				}
				
				echo $value_text;
			?>
		</td>
		<?endforeach;?>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>