<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$link = DeleteParam(array("delete"));
$link = strpos($link,"?")===false ? "?" : "&";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

//echo "<pre>".print_r($arResult,true)."</pre>";
?>
<script type="text/javascript">
//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {

			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});

	function showPartners(user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partners.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editPartners(user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partner_edit.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
//]]>
</script>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<p align="right"><a href="javascript:void(0)" onclick="editPartners(0);">������ ������</a></p>
<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="4"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tbody>
	<tr class="head">
		<td style="width:0%;">���.</td>
		<td style="width:0%;">ID</td>
		<td style="width:0%;">���� ���������</td>
		<td style="width:100% !important;">�.�.�.</td>
		<td>Email</td>
		<td>&nbsp;</td>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$td_class = '';
	if($arItem["ACTIVE"]!="Y") $td_class = ' class="yellow"';
	?>
	<tr <?=$td_class?>>
		<td style="width:0%;"<?=$td_class?>><?=($arItem["ACTIVE"]!="Y" ? "ͳ" : "���")?></td>
		<td style="width:0%;"><?=$arItem["ID"]?></td>
		<td style="width:0%;"><?=$arItem["DATE_REGISTER"]?></td>
		<td><a href="javascript:void(0)" onclick="showPartners(<?=$arItem["ID"]?>);"><?//=$arItem["LAST_NAME"]?><?=$arItem["NAME"]?><?//=$arItem["SECOND_NAME"]?></a></td>
		<td><?=$arItem["EMAIL"]?></td>
		<td>
			<a href="javascript:void(0)" onclick="if(confirm('�� ������ �������� ��������?')) location.href='<?=$link?>delete=<?=$arItem["ID"]?>'" title="��������">��������</a>
		</td>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
