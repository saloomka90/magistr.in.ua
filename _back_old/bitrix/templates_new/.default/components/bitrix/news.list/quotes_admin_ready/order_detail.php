<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
//echo "<pre>".print_r($_GET,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 5;
	
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
                $arOrder = $obOrder->GetFields();
                $arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($_GET["save"]=="Y")
		{
			$el = new CIBlockElement;
			
			$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(),
			  "IBLOCK_ID"      => $BID,
			  "PREVIEW_TEXT"   => $_GET["comment"],
			  "PREVIEW_TEXT_TYPE"   => "text"
			  );
			
			//echo "<pre>".print_r($arLoadProductArray,true)."</pre>";
			if($ID=$el->Update($arOrder["ID"],$arLoadProductArray))
			{
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "state", array("VALUE"=>$_GET["state"]));
				echo '<table width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("SAVED").'</td></tr></table><script type="text/javascript">var reload_page = 1;</script>';
			}
			else echo "Error: ".$el->LAST_ERROR;
		}
		else
		{
			$arWork = GetIBlockElement($arOrder["PROPERTIES"]["parent"]["VALUE"]);
			$rsUser = CUser::GetByID($arWork["CREATED_BY"]);
			$arUser =$rsUser->Fetch();

			$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td></td>
		<td><strong>'.GetMessage("CLIENT").'</strong></td>
		<td><strong>'.GetMessage("AUTHOR").'</strong></td>
	</tr>';
			$res .= '
	<tr>
		<td><strong>'.GetMessage("USER_NAME").':</strong></td>
		<td>'.$arOrder["NAME"].'</td>
		<td><a href="javascript:void(0)" onclick="showPerformers('.$arOrder["ID"].','.$arUser["ID"].')">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</a></td>
	</tr>
	<tr>
		<td><strong>'.GetMessage("USER_PHONE").':</strong></td>
		<td>'.$arOrder["PROPERTIES"]["phone"]["VALUE"].'</td>
		<td>'.(strlen($arUser["PERSONAL_PHONE"])>0?'<div>'.$arUser["PERSONAL_PHONE"].'</div>':'').(strlen($arUser["PERSONAL_MOBILE"])>0?'<div>'.$arUser["PERSONAL_MOBILE"].'</div>':'').'</td>
	</tr>
	<tr>
		<td><strong>E-mail:</strong></td>
		<td>'.$arOrder["PROPERTIES"]["email"]["VALUE"].'</td>
		<td>'.$arUser["EMAIL"].'</td>
	</tr>
	<tr>
		<td><strong>'.GetMessage("USER_NOTES").':</strong></td>
		<td>'.$arOrder["PROPERTIES"]["note"]["VALUE"].'</td>
		<td>'.$arUser["ADMIN_NOTES"].'</td>
	</tr>';
			$res .= '
</tbody>
</table>';

			$db_enum_list = CIBlockProperty::GetPropertyEnum("state", Array("sort"=>"asc"), Array("IBLOCK_ID"=>$arOrder["IBLOCK_ID"]));
			while($ar_enum_list=$db_enum_list->GetNext())
			{
				//echo "<pre>".print_r($ar_enum_list,true)."</pre>";
				$states[$ar_enum_list["ID"]] = GetMessage("IBLOCK_PROP_VALUES_".$ar_enum_list["VALUE"]);
				if(strlen($states[$ar_enum_list["ID"]])<=0) $states[$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
			}
			$res .= '
<div class="float_l ff">
	<select id="state" name="state">';
			foreach($states as $state_id => $state)
			{
				$res .= '<option'.($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==$state_id ? ' selected="selected"' : '').' value="'.$state_id.'">'.$state.'</option>';
			}
			$res .= '
	</select>
</div>
<div class="clear">&nbsp;</div>
';
			$res .= '
<br >'.GetMessage('COMMENT').'<br />
<textarea id="comment" name="comment" rows="5" style="width:99%;" cols="55">'.$arOrder["~PREVIEW_TEXT"].'</textarea>
<div class="clear">&nbsp;</div><br />
<input type="button" onclick="validateStateChange('.$order_id.');" value="'.GetMessage("SAVE").'" />
<div class="clear">&nbsp;</div><br />';
			
			$ar_props = array("type","date_make","storinok","zmist","cina");
			//echo "<pre>".print_r($arWork,true)."</pre>";

			$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td colspan="2"><strong><a href="'.$arWork["DETAIL_PAGE_URL"].'" target="_blank">'.$arWork["NAME"].'</a></strong></td>
	</tr>';

			foreach($ar_props as $pid)
			{
				$arProperty = $arWork["PROPERTIES"][$pid];

				$name = GetMessage("IBLOCK_PROP_".$pid);
				if(strlen($name)<=0) $name = $arProperty["NAME"];

				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;

				$res .= '
			<tr>
				<td style="width:30% !important;">'.$name.'</td>
				<td style="width:70% !important;">'.TxtToHtml($value_text).'</td>
			</tr>';
			}

			$res .= '
</tbody>
</table>';

			$res .= '
<script type="text/javascript">
	function validateStateChange(order_id)
	{
		changeState(order_id,$(\'#comment\').val(),$(\'#state\').val(),\'\');
	}
</script>
';
			
			echo $res;
		}
	}
}
?>