<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?><?

global $MESS;
include (dirname(__FILE__) . "/lang/" . "ua" . "/" . basename(__FILE__));

if (substr($arResult["AUTH_REGISTER_URL"], 0, 10) == "/partners/")
	$arResult["AUTH_REGISTER_URL"] = "/partners/registration/";
?>
<?
if($arParams["~AUTH_RESULT"]):
?>
	<div class="alert alert-danger">
		
			<?=$arParams["~AUTH_RESULT"]["MESSAGE"];?>
	</div>
<?endif;?>
<?
?>

<div id="at_bitrix">

<form class="form-signin" name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"] ?>">
        <input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"] ?>" />
		<?endif ?>
		<?
		foreach ($arResult["POST"] as $key => $value)
		{
		?>
		<input type="hidden" name="<?=$key ?>" value="<?=$value ?>" />
		<?
		}
		?>
        <input type="text" class="form-control" placeholder="login" autofocus="" name="USER_LOGIN"  value="<?=$arResult["LAST_LOGIN"] ?>">
        <br/><input type="password" class="form-control" placeholder="Password" name="USER_PASSWORD">
        
        
		     <?
			if ($arResult["STORE_PASSWORD"] == "Y")
			{
			?>
			<label class="checkbox">
          <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y"> Remember me
        </label>

			<?
			}
			?>
        <button class="btn btn-lg btn-primary btn-block" name="Login" type="submit"><?=GetMessage("AUTH_AUTHORIZE") ?></button>
      </form>



<script type="text/javascript"><?
if (strlen($arResult["LAST_LOGIN"])>0)
{
?>
	try {
		document.form_auth.USER_PASSWORD.focus();
	} catch(e) {
	}
<?
}
else
{
?>
	try {
		document.form_auth.USER_LOGIN.focus();
	} catch(e) {
	}
<?
}
?></script>
</div>
<? $APPLICATION->SetTitle("�����������");
?>
