<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
//echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
	<table class="data-table" cellspacing="0" cellpadding="2">
	<thead>
		<tr>
			<td colspan="2" align="center"><?=GetMessage("IBLOCK_FILTER_TITLE")?></td>
		</tr>
	</thead>
	<tbody>
		<?foreach($arResult["ITEMS"] as $cnt => $arItem):?>
			<?
			if(!array_key_exists("HIDDEN", $arItem)):
				$code = "";
				$cnt_in = 0;
				foreach($arResult["arrProp"] as $vals):
					if($cnt_in<$cnt):
						$cnt_in++;
						continue;
					else:
						$code = $vals["CODE"];
						break;
					endif;
				endforeach;
				$title = GetMessage("CUSTOM_TITLE_".$code);
				if(strlen($title)<=0) $title = $arItem["NAME"];
				?>
				<tr>
					<td valign="top"><?=$title?>:</td>
					<td valign="top"><?=$arItem["INPUT"]?></td>
				</tr>
			<?endif?>
		<?endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
		</tr>
	</tfoot>
	</table>
</form>
