<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<? 



if( !empty($_POST) and check_bitrix_sessid() and $_POST['save']=='Y'){

	echo "<pre>";
	var_dump($_POST);
	echo "</pre>";
	
$el = new CIBlockElement;

$PROP = array();


$PROP['note'] = $_POST['note'];
$PROP['cina'] = $_POST['cina'];
$PROP['zmist'] = $_POST['zmist'];
$PROP['storinok'] = $_POST['storinok'];
$PROP['date_make'] = $_POST['date_make'];
$PROP['type'] = $_POST['type'];
$PROP['work_prev'] = $_POST['work_prev'];






$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
		"IBLOCK_SECTION" => intval($_POST['workCategory']),          // ������� ����� � ����� �������
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $_POST['workName'],
		"ACTIVE"         => "Y",            // �������		
);

$PRODUCT_ID = intval($_POST["ID"]);  // �������� ������� � ����� (ID) 2
$res = $el->Update($PRODUCT_ID, $arLoadProductArray);

LocalRedirect("/authors/my_works/",true);

}

?>


<!--# content #-->
                <div class="content">
                    <!--# content header #-->
                    <div class="contentHeader">
                        <h1>������ ���� ������</h1>
                    </div>
                    <!--# end content header #-->
                    
                    <div class="contentDiv my_profile add_work">
                    <div class="formDiv">
                        <form id="formAddWork" method="post">
                      		<input type="hidden" name="save" value="Y">
                        	<input type="hidden" name="ID" value="<?=$arResult["ID"]?>">
                        	<?=bitrix_sessid_post()?>
                            <!--# form row #-->
                            <div class="formRow field_workName focusOn">
                                <div class="fieldLabel">����� ������</div>
                                <div class="formField">
                                    <input type="text" name="workName" class="control-it" value="<?=$arResult["NAME"]?>"/>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workCategory">
                                <div class="fieldLabel">�������� ������</div>
                                <div class="formField">
                                    <select class="form-control control-it select-control" name="workCategory">
                                        <option selected="" value="">������� � ������</option>
                                        <? 
                                        $arFilter = array('IBLOCK_ID' => 4); // ������� �������� ��� ����� ����������
                                        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
                                        while ($arSect = $rsSect->GetNext())
                                        {
                                        	if($arResult["IBLOCK_SECTION_ID"] == $arSect["ID"]){
                                        		echo '<option selected value="'.$arSect["ID"].'">'.$arSect["NAME"].'</option>';
                                        	}else{
												echo '<option value="'.$arSect["ID"].'">'.$arSect["NAME"].'</option>';
											}
                                        	
                                        }
                                        
                                        ?>                                      
                                    </select>
                                </div>
                                <div class="fieldRemark_float">
                                    <p>���� ��������� �������� �������, �������� ����� ��</p>
                                    <b>magistr.in.ua@gmail.com</b>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workType">
                                <div class="fieldLabel">��� ������</div>
                                <div class="formField">
                                    <select name="type" class="control-it select-control">
                                        <option value="">������� � ������</option>
                                        
                                        <? 
                                        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>4, "CODE"=>"type"));
										while($enum_fields = $property_enums->GetNext())
										{
											if($arResult["PROPERTIES"]["type"]["VALUE_ENUM_ID"] == $enum_fields["ID"]){
												echo '<option selected value="'.$enum_fields["ID"].'">'.$enum_fields["VALUE"].'</option>';
											}else{
												echo '<option value="'.$enum_fields["ID"].'">'.$enum_fields["VALUE"].'</option>';
											}

										 
										}?>
                                        
                                    </select>
                                </div>
                                <!--
                                <div class="workAddInfo">
                                    <div>
                                        <div class="fieldLabel">г� ���������</div>
                                        <div class="formField">
                                            <input type="text" class="" size="4" maxlength="4" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="fieldLabel">ʳ������ �������</div>
                                        <div class="formField">
                                            <input type="text" />
                                        </div>                                        
                                    </div>
                                </div>-->
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow noBg">
                                <div class="formField workAddInfo">
                                    <div>
                                        <div class="fieldLabel">г� ���������</div>
                                        <div class="formField">
                                            <input type="text" size="4" maxlength="4" placeholder="XXXX" name="date_make" class="control-it digit4" value="<?=$arResult["PROPERTIES"]["date_make"]["VALUE"]?>"/>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="fieldLabel">ʳ������ �������</div>
                                        <div class="formField">
                                            <input type="text" name="storinok" class="control-it" value="<?=$arResult["PROPERTIES"]["storinok"]["VALUE"]?>"/>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workContent">
                                <div class="fieldLabel">����</div>
                                <div class="formField">
                                    <textarea name="zmist" placeholder="�������� ��� ���� (����) �� ���� ������" class="control-it"><?=$arResult["PROPERTIES"]["zmist"]["VALUE"]?></textarea>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workPrice">
                                <div class="fieldLabel">ֳ��, ���</div>
                                <div class="formField">
                                    <input type="text" name="cina" class="control-it" value="<?=$arResult["PROPERTIES"]["cina"]["VALUE"]?>"/>
                                </div>
                                <div class="toggleBlocks">
                                    <div>
                                        <span <? if($arResult["PROPERTIES"]["note"]["VALUE"]!='')echo 'class="active"'?>>������ �������� <span>(���� ����� ���� ���)</span></span>
                                        <div <? if($arResult["PROPERTIES"]["note"]["VALUE"]!='')echo 'style="display:block"'?>>
                                            <textarea name="note" placeholder="..."><?=$arResult["PROPERTIES"]["note"]["VALUE"]?></textarea>
                                        </div>
                                        <p>���������, ���� � ��� � ������ ������� ���� � ���� ������������� - ������ ��� ������� ����� ������</p>
                                    </div>
                                    <div>
                                        <span <? if($arResult["PROPERTIES"]["work_prev"]["VALUE"]!='')echo 'class="active"'?>>��������� ������� ������ ��� ������������</span>
                                        <div <? if($arResult["PROPERTIES"]["work_prev"]["VALUE"]!='')echo 'style="display:block"'?>>
                                            <textarea name="work_prev" placeholder="..."><?=$arResult["PROPERTIES"]["work_prev"]["VALUE"]?></textarea>
                                        </div>
                                        <p>��� ������ ������ ��������� ����-��� ������� ������. �� ������ ������ ���������� ������� ���� ���� ������. <br />��������� ���� �����. ������� �� ������� ������������ �� ������.</p>
                                    </div>
                                </div>
                            </div>
                            <!--# end form row #-->
                            
                            <div class="submitForm">
                                <input type="submit" value="��������" class="btn2 bg-orange" />
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                <!--# end content #-->  
                
                
                
                
                
                
                
                
                <script type="text/javascript">
$(document).ready(function() {
    /*
    $('#formAddWork').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            workName: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workCategory: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            workType: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workYear: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workPages: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workContent: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workPrice: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            }
        }
    });*/

    var validIt = function(form, target){
        var form = $(form);
        var errors = 0;
        var phonePat = /^\+[0-9]{12}$/;
        var messages = {
            empty: "��������� �� ����",
            phone: "������� ������ ��������",
            email: "������� ������ email",
            atLeast: "������ ����� ����",
            wrong: "������"
        };
        form.find(".control-it").each(function(){
            var parent = $(this).parents(".formField").eq(0);
            parent.removeClass("has-error");
            if($(this).hasClass("phone-control")){
                if($(this).val().length == 0){
                    errors +=1;
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                }
                else{
                    if(!phonePat.test($(this).val())){
                        errors +=1;
                        if(!parent.hasClass("has-error")){
                            if(parent.find(".error_sub").length == 0){
                                parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.phone+"</div>");
                            }
                        }
                    }
                }
            }
            if($(this).hasClass("email-control")){
                if($(this).val().length == 0){
                    errors +=1;
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                }
                else{
                    var emailPat = /^.{3,}\@.{1,}\..*$/
                    if(!emailPat.test($(this).val())){
                        errors +=1;
                        if(!parent.hasClass("has-error")){
                            if(parent.find(".error_sub").length == 0){
                                parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.email+"</div>");
                            }
                        }
                    }
                }
            }
            if($(this).hasClass("digit4")){
                if($(this).val().length != 4 || !/^[0-9]{4}$/.test($(this).val())){
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.wrong+"</div>");
                        }
                    }
                    errors += 1;
                    
                }
            }
            if($(this).attr("type") == "checkbox"){
                var fieldName = $(this).attr("name");
                if($("input[type=checkbox][name='"+fieldName+"']:checked").length < 1){
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.atLeast+"</div>");
                        }
                    }
                    errors += 1;
                }
            }
            else{
                if($(this).val() !== ""){
                    
                } else{
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                    errors += 1;
                }
            }
        });
        return errors == 0 ? true : false;
    };

    $('#formAddWork').submit(function(e) {
        if(!validIt('#formAddWork')){
            e.preventDefault();
            console.log("error");
        } else{
            
        }
    });
});

</script>









<? /*?><div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img class="detail_picture" border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			<br />
	<?endforeach;?>
	<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;?>
	<?
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
<? */?>