<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo "<pre>"; print_r($arResult); echo "</pre>";
//exit();
//echo "<pre>"; print_r($_SESSION); echo "</pre>";

$group_ok = in_array(5, $USER->GetUserGroupArray()) ? true : false;

//$ar_auth2_fields = array("UF_LIVING_PLACE", "UF_BIRTH_YEAR", "UF_OCCUPATION", "UF_OPIT", "UF_WORK_TYPES", "UF_SPHERE", "UF_LANGS", "UF_OTHER_DATA", "UF_SUBJECTS");
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
//if($USER->IsAdmin()) echo "<pre>".print_r($_SESSION,true)."</pre>";
if ($arResult['DATA_SAVED'] == 'Y')
{
	if($_POST["auth_type"]==2&&!$group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(5))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = true;
		}

		$rsUser = CUser::GetByID($user_id);
		if($arUser = $rsUser->Fetch())
		{
			$arEventFields = array(
				"USER_ID"=>$arUser["ID"],
				"LOGIN"=>$arUser["LOGIN"],
				"EMAIL"=>$arUser["EMAIL"],
				"NAME"=>$arUser["NAME"]
			);
			CEvent::Send("NEW_AUTHOR_FROM_OLD", "s1", $arEventFields);
			CEvent::CheckEvents();
		}
	}
	elseif($group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(3))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = false;
		}
	}

	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}
?>
<script type="text/javascript">
var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';

	function regSubmit(caller, pref)
	{
		$(caller).append('<input type="hidden" name="EMAIL" value="'+$("#"+pref+"_LOGIN").attr("value")+'" />');
		return submForm(pref);
	}
</script>
<form onsubmit="return regSubmit(this, 'reg');"
	  method="POST" name="form1"
	  action="<?=$arResult["FORM_TARGET"]?>?"
	  enctype="multipart/form-data">

	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
	<?
		$js_titles = "";
	?>

	<div class="reg_col">
		<?
		/*if($arResult["ID"]>0)
		{
		?>
			<?
			if (strlen($arResult["arUser"]["TIMESTAMP_X"])>0)
			{
			?>
			<tr>
				<td><?=GetMessage('LAST_UPDATE')?></td>
				<td><?=$arResult["arUser"]["TIMESTAMP_X"]?></td>
			</tr>
			<?
			}
			?>
			<?
			if (strlen($arResult["arUser"]["LAST_LOGIN"])>0)
			{
			?>
			<tr>
				<td><?=GetMessage('LAST_LOGIN')?></td>
				<td><?=$arResult["arUser"]["LAST_LOGIN"]?></td>
			</tr>
			<?
			}
			?>
		<?
		}*/
		?>
		<?
		$title = GetMessage("LOGIN")."*";
		$name = "LOGIN";
		$id = "reg_".$name;
		$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="50" /><br />
		<div class="clear">&nbsp;</div>

		<?
		$title = GetMessage("NEW_PASSWORD");
		$name = "NEW_PASSWORD";
		$id = "reg_".$name;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" autocomplete="off" maxlength="50" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
		<div class="clear">&nbsp;</div>

		<?
		$title = GetMessage("NEW_PASSWORD_CONFIRM");
		$name = "NEW_PASSWORD_CONFIRM";
		$id = "reg_".$name;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" maxlength="50" autocomplete="off" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
		<div class="clear">&nbsp;</div>

		<?
		$title = GetMessage("NAME")."*";
		$name = "NAME";
		$id = "reg_".$name;
		$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
		<div class="clear">&nbsp;</div>

		<?
		$title = GetMessage("USER_PHONE")."*";
		$name = "PERSONAL_PHONE";
		$id = "reg_".$name;
		$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
		<div class="clear">&nbsp;</div>

		<?
		$title = GetMessage("USER_MOBILE");
		$name = "PERSONAL_MOBILE";
		$id = "reg_".$name;
		$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
		$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
		?>
		<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
		<div class="clear">&nbsp;</div>

		<? // ********************* User properties ***************************************************?>
		<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
			<?$first = true;?>
			<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
				<?
	//if($USER->IsAdmin()) echo "<pre>".print_r($arUserField,true)."</pre>";
				if(in_array($FIELD_NAME, $ar_auth2_fields))
				{
					ob_start();
				}
				$title = GetMessage($FIELD_NAME);
				$descr = GetMessage("REGISTER_FIELD_".$FIELD_NAME."_DESCR");
				if(strlen($title)<=0) $title = $arUserField["EDIT_FORM_LABEL"];
				$title .= ($arUserField["MANDATORY"]=="Y"?"*":"");
				$name = $FIELD_NAME;
				$id = "reg_".$name;
				$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
				$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
				?>
				<div id="register-field-<?=$FIELD_NAME?>">
					<?if($arUserField["SETTINGS"]["DISPLAY"]=="CHECKBOX"):?>
						<div><?=$title?></div>
					<?endif;?>
					<?
//						var_dump($arUserField["USER_TYPE"]);
//						echo '<br>';
					?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:system.field.edit",
						$arUserField["USER_TYPE"]["USER_TYPE_ID"],
						array(
							"bVarsFromForm" => $arResult["bVarsFromForm"],
							"arUserField" => $arUserField,
							"field_params"=>array(
								"class"=>"inputtext wide",
								"id"=>"reg_".$FIELD_NAME,
								"title"=>$title,
								"onblur"=>"unsetFocusField(this, 'reg')",
								"onfocus"=>"setFocusField(this, 'reg')")
						),
						null,
						array("HIDE_ICONS"=>"Y")
					);?>
					<?if(strlen($descr)>0):?><div class="wide_field_descr"><?=$descr?></div><?endif;?>
					<div class="clear">&nbsp;</div>
				</div>
				<?
				if(in_array($FIELD_NAME, $ar_auth2_fields))
				{
					$content = ob_get_contents();
					ob_end_clean();
					$auth2_fields .= $content;
				}
			endforeach;
			?>
		<?endif;?>
	<div id="auth2_fields"<?if(($_POST["auth_type"]!=2||!isset($_POST["auth_type"]))&&!$group_ok):?> style="display:none;"<?endif;?>>
	<?=$auth2_fields?>
	</div>
	<?// ******************** /User properties ***************************************************?>
	<div class="clear">&nbsp;</div>
	<?/*<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>*/?>
	<p><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("SAVE") : GetMessage("ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('RESET');?>"></p>
</form>
</div>
<?
$script = '
<script type="text/javascript">
	var names_reg = new Object;
	names_reg = {'.$js_titles.'};
</script>
';
$APPLICATION->AddHeadString($script);
?>
<script type="text/javascript">
	var allways_disable = false;
	function disableChecks(cp) {
		if(allways_disable) {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else if(jQuery("input:checkbox:checked",cp).size()>=3) {
			jQuery("input:checkbox:not(:checked)",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","");});
		}
	}
	jQuery(document).ready(function(){
		jQuery("#register-field-UF_SPHERE input:checkbox").each(function(){
			jQuery(this).bind("click",function(){
				disableChecks(jQuery(this).parent().parent());
			});
		});
		disableChecks(jQuery("#register-field-UF_SPHERE"));
	});

</script>

<script>
	function GetByID(id)
	{
		return document.getElementById(id);
	}

	function activateTabE(id, num, t_id)
	{
		var tab_c = GetByID(id);
		var tab_t_p = GetByID(t_id);
		if(tab_c && tab_t_p)
		{
			var tabs = tab_c.getElementsByTagName('DIV');
			if(!num) num = 0;
			var cnt = 0;
			for(i in tabs)
			{
				var cls = tabs[i].className;
				if(cls && (cls=="tab" || cls.indexOf("tab ")>-1 || cls.indexOf(" tab")>-1))
				{
					if(cnt==num) tabs[i].style.display = "block";
					else tabs[i].style.display = "none";
					cnt++;
				}
			}
			var tabs_t = tab_t_p.getElementsByTagName('DIV');
			var cnt = 0;
			for(j=0;j<tabs_t.length;j++)
			{
				var cls = tabs_t[j].className;
				if(cls && (cls=="tab" || cls.indexOf("tab ")>-1 || cls.indexOf(" tab")>-1))
				{
					if(cnt==num) tabs_t[j].className += " selected";
					else tabs_t[j].className = tabs_t[j].className.replace(" selected", "");
					cnt++;
				}
			}
		}
	}

	function setContentHeight()
	{
		var id = "content";
		var cont = GetByID(id);
		var bd = document.body;
		if(cont)
		{
			cont.style.height = "auto";
			var min_h = bd.clientHeight - 149 - 156 - 42;
			if(min_h<120) min_h = 120;
			if(cont.clientHeight<min_h) cont.style.height = min_h+"px";
		}
	}

	function setFocusField(caller, pref)
	{
//alert(names);
		if(pref) var names_new = window['names_'+pref];
		else if(typeof(names)!=='undefined') var names_new = names;
		if(names_new[(caller.id)]!=undefined)
		{
			if(caller.value==names_new[(caller.id)])
			{
				caller.value = '';
				caller.style.color = '#000000';
			}
		}
	}

	function unsetFocusField(caller, pref)
	{
		if(pref) var names_new = window['names_'+pref];
		else if(typeof(names)!=='undefined') var names_new = names;
		if(names_new[(caller.id)]!=undefined)
		{
			if(caller.value=='')
			{
				caller.style.color = '';
				caller.value = names_new[(caller.id)];
			}
			else caller.style.color = '#000000';
		}
	}

	function checkFieldFilling(pref)
	{
		if(pref) var names_new = window['names_'+pref];
		else if(typeof(names)!=='undefined') var names_new = names;

		if(names_new)
		{
			for(i in names_new)
			{
				var f = GetByID(i);
				if(f)
				{
					if(f.value!=names_new[i]) f.style.color = '#000000';
					else f.style.color = '';
				}
			}
		}
	}

	function sw(id, tp)
	{
		var node = GetByID(id);
		if(node)
		{
			if(tp!=undefined) node.style.display = tp ? "block" : "none";
			else node.style.display = node.style.display=="none" ? "block" : "none";
		}
	}

	function submForm(pref)
	{
		if(pref) var names_new = window['names_'+pref];
		else if(typeof(names)!=='undefined') var names_new = names;

		for(i in names_new)
		{
			var field = GetByID(i);
			if(field)
			{
				if(field.value==names_new[i])
				{
					field.value = '';
				}
			}
		}
		return true;
	}

	function selectAll(pid, toggle)
	{
		$("#"+pid+" input[type='checkbox']").each(function(){$(this).attr("checked",toggle);});
		return true;
	}

	function checkSelection(pid, cid)
	{
		var checked = "checked";
		$("#"+pid+" input[type='checkbox']").not("#"+cid).each(function() {
			if($(this).attr("checked")==false) {
				$("#"+cid).attr("checked", false);
				checked = false;
				return false;
			}
		});
		$("#"+cid).attr("checked", checked);
		return true;
	}

	function submitReviews(caller, inc_file, pref) {


		var options = {
			url: /*caller.action+(caller.action.indexOf('?')<0?'?':'&')+'inc_file='+*/inc_file,
			beforeSubmit: function(formData, jqForm, options) {
				for (i in formData) {
					formData[i].value = /*encodeURIComponent*/escape(formData[i].value);
				}

				return true;
			},
			success: function(response) {
				$(caller).before(response);
				$(caller).remove();
			}
		};

		submForm(pref);
		$(caller).css("position","relative");
		$(caller).append('<div style="height:43px;width:43x;background:#ffffff;border:1px solid #cccccc;position:absolute;top:50%;left:50%;margin:-35px 0px 0px -35px;padding:14px 13px 13px 14px;"><img src="/images/ajaxLoader.gif" width="43" height="43" alt="" /></div>');
		$(caller).ajaxSubmit(options);
		return false;
	}

	$(document).ready(function(){
		checkFieldFilling();
		checkFieldFilling('n');
		checkFieldFilling('p');
		checkFieldFilling('reg');
		//setContentHeight();

		jQuery("input.datepicker").each(function(){
			jQuery(this).datepicker({
				changeMonth: true,
				changeYear: true,
				//yearRange: '1990:'+td.getFullYear(),
				//minDate: new Date(1990, 1 - 1, 1),
				minDate: new Date()
			});
		});
	});

	$(document).resize(function(){
		//setContentHeight();
	});

	function setFormOptions() {
		jQuery("#nyroModalContent form").each(function(){
			var options = {
				target: "#nyroModalContent",
				url: $(this).attr("action")+"&ajax=1",
				beforeSubmit: function(formData, jqForm, options) {

					for (i in formData) {
						formData[i].value = escape(formData[i].value);
					}
					return true;
				},
				success: function(data) {
					setFormOptions();
				}
			};
			jQuery(this).ajaxForm(options);
		});
	}
</script>