<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(empty($_REQUEST["edit"])):?>
<!DOCTYPE HTML>
<html>
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="windows-1251">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />

	<link rel="stylesheet" href="<?=$APPLICATION->GetTemplatePath("css/style.css");?>" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/popup.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/checkbox/checkbox.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/jquery-filestyle.css");?>" />
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-1.7.1.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/cufon-yui.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/tahoma_cufon.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox-0.2.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/popup.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/checkbox/jquery.checkbox.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-filestyle.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.validate.min.js");?>"></script>
	<script type="text/javascript" src="/bitrix/templates/magistr/js/jquery.nyroModal-1.6.2.pack.js"></script>


    <?$APPLICATION->ShowHead()?>
</head>

<body>

	<?$APPLICATION->ShowPanel();?>
	<div class="bg">
		<div class="overlay" id="overlay_order" style="display:none;"></div>
		<div class="header-bg">
			<div class="header">
				<div class="logo">
					<a href="#"><img src="<?=$APPLICATION->GetTemplatePath('images/logo.png');?>" /></a>
				</div>
				<div class="header-r">
					<? if($USER->IsAuthorized()):?>
					<div class="header_top">
						<div class="name"><?= $USER->GetFullName()?></div>
						<div class="panel_link">
							<a href="/authors/personal_data"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_account.png');?>" /> <span>����i��</span></a> |
							<a href="/?logout=yes"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_exit.png');?>" /> <span>�����</span></a>
						</div>
					</div>
					<div class="clr"></div>

					<?php
						CModule::IncludeModule('iblock');
						$arFilter = array(
							'IBLOCK_ID' => 4,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_works = $el['CNT'];
						}

						$arFilter = array(
							"IBLOCK_ID" => 5,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_quotes = $el['CNT'];
						}

                        $arFilter = array(
                            'IBLOCK_ID' => 6,//4
                            "PROPERTY" => array(
                                "performer"=> array(
                                    "VALUE" => $USER->GetID()
                                    ),
                                "state" => array(
                                	"VALUE_ENUM_ID" =>23
                                	)
                                )
                            // 'CREATED_USER_ID'=>$USER->GetID(),
                        );
                        $res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
                        if ($el = $res->Fetch()) {
                            $in_progress = $el['CNT'];
                        }

						$url_segments = explode('/',$APPLICATION->GetCurPage());
						$active_item = $url_segments[2];
					?>
					<!-- ��� ����! -->
					<div class="menu">
						<ul>
							<li class="parent <?= ($active_item == 'my_works' or  $active_item == 'add_work' or $active_item == 'my_quotes')?'active':''?>">
								<a href="/authors/my_works/?PAGEN_1=1">������ ������� ����</a>
								<ul>
									<li <?= ($active_item == 'my_works' )?'class="active"':''?>><a href="/authors/my_works/?PAGEN_1=1">�� ������<?= ($count_works)? '<span class="count">'.$count_works.'</span>': ''?></a></li>
									<li <?= ($active_item == 'add_work' )?'class="active"':''?>><a href="/authors/add_work/" id="add_work">+������ ������</a></li>
									<li <?= ($active_item == 'my_quotes' )?'class="active"':''?>><a href="/authors/my_quotes/?PAGEN_1=1">������ �� ����� ������<?//= ($count_quotes)? '<span class="count2">'.$count_quotes.'</span>': ''?></a></li>
								</ul>
							</li>
							<li class="parent <?= ($active_item == 'my_quotes_work' || $active_item == 'my_quotes_new' || $active_item == 'my_quotes_finish')?'active':''?>"">
								<a href="/authors/my_quotes_new/?PAGEN_1=1">��������� ���� �� ����������</a>
								<ul>
									<li><a <?= ($active_item == 'my_quotes_work' )?'style="text-decoration: none;"':''?> href="http://magistr.in.ua/authors/my_quotes_work/?PAGEN_1=1">��������� �� ����<span class="count"><?=$in_progress?></span></a></li>
									<li><a <?= ($active_item == 'my_quotes_new' )?'style="text-decoration: none;"':''?> href="http://magistr.in.ua/authors/my_quotes_new/?PAGEN_1=1">���i ������</a></li>
									<li><a <?= ($active_item == 'my_quotes_finish' )?'style="text-decoration: none;"':''?> href="http://magistr.in.ua/authors/my_quotes_finish/?PAGEN_1=1">�������i</a></li><!-- <img id="mig" src="<?=$APPLICATION->GetTemplatePath('images/c.png');?>" /> -->
								</ul>
							</li>
						</ul>
					</div>
					<div class="clr"></div>
					<? endif;?>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<div class="wrap-bg">
			<div class="wrap">
			<h1>
				<?$APPLICATION->ShowTitle()?>
				<?if($a):?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="profile-edit">��i���� ������</a>
				<?endif;?>
			</h1>
<?endif;?>
