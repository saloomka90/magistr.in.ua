<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";

if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;

?>


				<div class="content">
				<?if (strlen($arResult["MESSAGE"]) > 0):?>
		<?=ShowNote($arResult["MESSAGE"])?>
	<?endif?>
                    <!--# content header #-->
                    <div class="contentHeader">
                        <div class="headerToolbar">
                            <a href="/authors/add_work/" class="btn2 bg-orange">������ ������</a>
                        </div>
                        <h1>�� ������</h1>
                    </div>
                    <!--# end content header #-->
                    
                    <div class="contentDiv my_works">
                    <div>
                   
                    <table class="trHover">
                     <?if (count($arResult["ELEMENTS"]) > 0):$cnt=(intval($_GET["PAGEN_1"])>1?intval($_GET["PAGEN_1"])-1:0)*$arParams["NAV_ON_PAGE"];?>
                        <tr class="thead">
                            <th class="id_col">#</th>
                            <th>���� ������</th>
                        </tr>
                        
                        <?foreach ($arResult["ELEMENTS"] as $arElement):$cnt++;?>
					<?//echo "<pre>"; print_r($arElement['DATE_CREATE']); ?>
						<?
							$rsProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], "sort", "asc", Array("CODE"=>"note"));
							if($arProp = $rsProp->Fetch()) $note = $arProp["VALUE"];
							else $note = "";
						?>
                        <tr>
                            <td class="id_col" title="#"><?=$cnt?>.</td>
                            <td title="���� ������" class="text-left">
                            <div class="tdDiv">
                                <div class="itemToolbar">
                                <?if ($arElement["CAN_EDIT"] == "Y"):?>
									<a href="<?= $arElement["ID"]?>/" title="edit" id="entry_id_<?= $arElement["ID"]?>" class="mgIcon_edit mg_small"></a>
									
								<?endif?>
                                    
                                    <?if ($arElement["CAN_DELETE"] == "Y"):?>
                                    <a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" title="delete" onClick="return confirm('<?=str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))?>')" class="mgIcon_delete mg_small"></a>
                                    <?endif?>
                                </div>
                                <div class="itemShortInfo">
                                   <a href="
								<?=str_replace(
									array("#SITE_DIR#", "#SECTION_ID#", "#ID#"),
									array("", $arElement["IBLOCK_SECTION_ID"], $arElement["ID"]),
									$arElement["DETAIL_PAGE_URL"]);
								?>" target="_blank">
									<?=$arElement["NAME"] ?>
								</a>
								<?if(strlen($note)>0):?>
									<p><?=$note?></p>
								<?endif;?>
                                    
                                </div>
                            </div>
                            </td>
                        </tr>
                        <?endforeach?>
                        <?else:?>
					<tr>
						<td>
							<div class="alert"><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></div>
						</td>
					</tr>
					<?endif?>
                    </table>
                    </div>
                    </div>                    
                </div>
                <!--# end content #-->
                
                
                
                



<?if (strlen($arResult["NAV_STRING"]) > 0):?>

		<td><?=$arResult["NAV_STRING"]?> </td>

<?endif?>
