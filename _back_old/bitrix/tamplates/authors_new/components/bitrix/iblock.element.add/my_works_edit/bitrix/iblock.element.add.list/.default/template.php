<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";

if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;

?>


<div class="content">
                    <!--# content header #-->
                    <div class="contentHeader">
                        <div class="headerToolbar">
                            <a href="#" class="btn2 bg-orange">������ ������</a>
                        </div>
                        <h1>�� ������</h1>
                    </div>
                    <!--# end content header #-->
                    
                    <div class="contentDiv my_works">
                    <div>
                    <table class="trHover">
                        <tr class="thead">
                            <th class="id_col">#</th>
                            <th>���� ������</th>
                        </tr>
                        <tr>
                            <td class="id_col" title="#">1.</td>
                            <td title="���� ������" class="text-left">
                            <div class="tdDiv">
                                <div class="itemToolbar">
                                    <a href="#" title="edit" class="mgIcon_edit mg_small"></a>
                                    <a href="#" title="delete" class="mgIcon_delete mg_small"></a>
                                </div>
                                <div class="itemShortInfo">
                                    <a href="#">��������� ������� �� ������ ������������� ������� � ��������� ����������� ���������</a>
                                    <p>��������� ��������� ���������� ���� ����</p>
                                </div>
                            </div>
                            </td>
                        </tr>
                        
                    </table>
                    </div>
                    </div>                    
                </div>
                <!--# end content #-->
                
                
                

<div class="table_box">
	<?if (strlen($arResult["MESSAGE"]) > 0):?>
		<?=ShowNote($arResult["MESSAGE"])?>
	<?endif?>
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		<thead>
			<tr>

				<td colspan="2" class="theme-w">���� ������:</td>
				<td>����������:</td>
				<td>��������:</td>
			</tr>
		</thead>
		<tbody>
			<?if($arResult["NO_USER"] == "N"):?>
				<?if (count($arResult["ELEMENTS"]) > 0):$cnt=(intval($_GET["PAGEN_1"])>1?intval($_GET["PAGEN_1"])-1:0)*$arParams["NAV_ON_PAGE"];?>
					<?
					/*function cmp($a, $b) {
					    if ($a == $b) {
					        return 0;
					    }
					    return (strtotime($a['DATE_CREATE']) < strtotime($b['DATE_CREATE'])) ? 1 : -1;
					}
					usort($arResult["ELEMENTS"], 'cmp')  */
					?>
					<?foreach ($arResult["ELEMENTS"] as $arElement):$cnt++;?>
					<?//echo "<pre>"; print_r($arElement['DATE_CREATE']); ?>
						<?
							$rsProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], "sort", "asc", Array("CODE"=>"note"));
							if($arProp = $rsProp->Fetch()) $note = $arProp["VALUE"];
							else $note = "";
						?>
						<tr class="am1">
							<td><div class="num1"><?=$cnt?></div></td>
							<td class="sub2">

								<div><a href="
								<?=str_replace(
									array("#SITE_DIR#", "#SECTION_ID#", "#ID#"),
									array("", $arElement["IBLOCK_SECTION_ID"], $arElement["ID"]),
									$arElement["DETAIL_PAGE_URL"]);
								?>" target="_blank">
									<?=$arElement["NAME"] ?>
								</a></div>
								<?if(strlen($note)>0):?>
									<span>[<?=$note?>]</span>
								<?endif;?>
							</td>
							<td class="status3">
								<?/*<td><small><?=is_array($arResult["WF_STATUS"]) ? $arResult["WF_STATUS"][$arElement["WF_STATUS_ID"]] : $arResult["ACTIVE_STATUS"][$arElement["ACTIVE"]]?></small></td>*/?>
								<?if ($arElement["CAN_EDIT"] == "Y"):?>
									<div class="edit-img" id="entry_id_<?= $arElement["ID"]?>"></div>
								<?endif?>
							</td>
							<td class="rem">
								<?if ($arElement["CAN_DELETE"] == "Y"):?>
									<a class="del-img" href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="return confirm('<?=str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))?>')">
									</a>
								<?endif?>
							</td>
						</tr>
					<?endforeach?>
				<?else:?>
					<tr>
						<td>
							<div class="alert"><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></div>
						</td>
					</tr>
				<?endif?>
			<?endif?>
				<?/*if ($arParams["MAX_USER_ENTRIES"] > 0 && $arResult["ELEMENTS_COUNT"] < $arParams["MAX_USER_ENTRIES"]):?><a href="<?=$arParams["EDIT_URL"]?>?edit=Y"><?=GetMessage("IBLOCK_ADD_LINK_TITLE")?></a><?else:?><?=GetMessage("IBLOCK_LIST_CANT_ADD_MORE")?><?endif*/?>


		</tbody>
	</table>
</div>

<?if (strlen($arResult["NAV_STRING"]) > 0):?>

		<td><?=$arResult["NAV_STRING"]?> </td>

<?endif?>
