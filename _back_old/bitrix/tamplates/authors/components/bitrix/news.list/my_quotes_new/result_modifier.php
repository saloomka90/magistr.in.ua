<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die('die');
global $USER;
if($arParams['HIDE_CANCELED_ITEMS'] == 'Y' && $USER->IsAuthorized() )
			{

				$arTmpItems = $arResult["ITEMS"];
				$user_responses = array();
				$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","PROPERTY_author_id"=>$USER->GetID(),"@PROPERTY_order_id"=>$arResult["ELEMENTS"]),false,false,array("ID","PROPERTY_author_id","PROPERTY_refused","PROPERTY_order_id"));//"ID","NAME","PREVIEW_TEXT","PROPERTY_author_id","PROPERTY_summ","PROPERTY_refused","PROPERTY_order_id"

				while($ar_ur=$rs_ur->GetNext()) {
					$user_responses[$ar_ur["PROPERTY_ORDER_ID_VALUE"]] = $ar_ur;
				}
				$arItem = null;
				foreach($arTmpItems as $key=>$arItem){
					if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0){
						/*if($USER->IsAdmin()){
						echo $USER->GetID();
						echo "<pre>"; print_r($user_responses); echo "</pre>";
						}*/
						unset($arResult["ITEMS"][$key]);
						unset($arResult["ELEMENTS"][$key]);
					}
				}

			}

$cols_cnt = 1;
if($arParams["DISPLAY_PICTURE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_DATE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_NAME"]!="N") $cols_cnt++;
if($arParams["DISPLAY_PREVIEW_TEXT"]!="N") $cols_cnt++;
if(count($arParams["FIELD_CODE"])>0) $cols_cnt += count($arParams["FIELD_CODE"]);
if(count($arParams["PROPERTY_CODE"])>0) $cols_cnt += count($arParams["PROPERTY_CODE"]);

$arResult["COLS_COUNT"] = $cols_cnt;



?>