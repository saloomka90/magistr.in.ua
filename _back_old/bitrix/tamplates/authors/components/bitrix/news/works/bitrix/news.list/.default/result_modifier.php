<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cols_cnt = 0;
if($arParams["DISPLAY_PICTURE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_DATE"]!="N") $cols_cnt++;
if($arParams["DISPLAY_NAME"]!="N") $cols_cnt++;
if($arParams["DISPLAY_PREVIEW_TEXT"]!="N") $cols_cnt++;
if(count($arParams["FIELD_CODE"])>0) $cols_cnt += count($arParams["FIELD_CODE"]);
if(count($arParams["PROPERTY_CODE"])>0) $cols_cnt += count($arParams["PROPERTY_CODE"]);

$arResult["COLS_COUNT"] = $cols_cnt;
?>