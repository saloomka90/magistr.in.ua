<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";

if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>
<div class="my-works-list">
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<?if($arResult["NO_USER"] == "N"):?>
	<?if (count($arResult["ELEMENTS"]) > 0):$cnt=(intval($_GET["PAGEN_1"])>1?intval($_GET["PAGEN_1"])-1:0)*$arParams["NAV_ON_PAGE"];?>
		<?foreach ($arResult["ELEMENTS"] as $arElement):$cnt++;?>
		<?
			$rsProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], "sort", "asc", Array("CODE"=>"note"));
			if($arProp = $rsProp->Fetch()) $note = $arProp["VALUE"];
			else $note = "";
		?>
		<div class="item">
			<div class="number"><?=$cnt?>.</div>
			<div class="title"><a href="<?=str_replace(array("#SITE_DIR#", "#SECTION_ID#", "#ID#"), array(LANG_ID!=LANG_ID_DEF?"/".LANG_ID:"", $arElement["IBLOCK_SECTION_ID"], $arElement["ID"]), $arElement["DETAIL_PAGE_URL"])?>"><?=$arElement["NAME"]?></a><?if(strlen($note)>0):?><br /><span class="note">[<?=$note?>]</span><?endif;?></div>
			<?/*<td><small><?=is_array($arResult["WF_STATUS"]) ? $arResult["WF_STATUS"][$arElement["WF_STATUS_ID"]] : $arResult["ACTIVE_STATUS"][$arElement["ACTIVE"]]?></small></td>*/?>
			<?if ($arElement["CAN_EDIT"] == "Y"):?><a class="edit" href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?><?endif?></a>
			<?if ($arElement["CAN_DELETE"] == "Y"):?><a class="delete" href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="return confirm('<?=str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))?>')"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?></a><?endif?>
			<div class="clear">&nbsp;</div>
		</div>
		<?endforeach?>
	<?else:?>
		<div class="alert"><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></div>
	<?endif?>
<?endif?>
	<?/*if ($arParams["MAX_USER_ENTRIES"] > 0 && $arResult["ELEMENTS_COUNT"] < $arParams["MAX_USER_ENTRIES"]):?><a href="<?=$arParams["EDIT_URL"]?>?edit=Y"><?=GetMessage("IBLOCK_ADD_LINK_TITLE")?></a><?else:?><?=GetMessage("IBLOCK_LIST_CANT_ADD_MORE")?><?endif*/?>
<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>
</div>