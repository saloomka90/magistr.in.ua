<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="fields string" id="main_<?=$arParams["arUserField"]["FIELD_NAME"]?>"><?
foreach ($arResult["VALUE"] as $res):
?><div class="fields string"><?
	if($arParams["arUserField"]["SETTINGS"]["ROWS"] < 2):
?><input<?foreach($arParams["field_params"] as $key=>$val):?> <?=$key?>="<?=$val?>"<?endforeach;?> type="text" name="<?=$arParams["arUserField"]["FIELD_NAME"]?>" value="<?=(strlen($res)>0?$res:$arParams["field_params"]["title"])?>"<?
	if (intVal($arParams["arUserField"]["SETTINGS"]["SIZE"]) > 0):
		?> size="<?=$arParams["arUserField"]["SETTINGS"]["SIZE"]?>"<?
	endif;
	if (intVal($arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"]) > 0):
		?> maxlength="<?=$arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"]?>"<?
	endif;
	if ($arParams["arUserField"]["EDIT_IN_LIST"]!="Y"):
		?> disabled="disabled"<?
	endif;
?> class="fields string"><?
	else:
?><textarea class="fields string big"<?foreach($arParams["field_params"] as $key=>$val):if($key=="class") continue;?> <?=$key?>="<?=$val?>"<?endforeach;?> name="<?=$arParams["arUserField"]["FIELD_NAME"]?>"<?
	?> cols="<?=$arParams["arUserField"]["SETTINGS"]["SIZE"]?>"<?
	?> rows="<?=$arParams["arUserField"]["SETTINGS"]["ROWS"]?>" <?
	if (intVal($arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"]) > 0):
		?> maxlength="<?=$arParams["arUserField"]["SETTINGS"]["MAX_LENGTH"]?>"<?
	endif;
	if ($arParams["arUserField"]["EDIT_IN_LIST"]!="Y"):
		?> disabled="disabled"<?
	endif;
?>><?=(strlen($res)>0?$res:$arParams["field_params"]["title"])?></textarea><?	
	endif;
?></div><?
endforeach;
?></div>
<?if ($arParams["arUserField"]["MULTIPLE"] == "Y" && $arParams["SHOW_BUTTON"] != "N"):?>
<input type="button" value="<?=GetMessage("USER_TYPE_PROP_ADD")?>" onClick="addElement('<?=$arParams["arUserField"]["FIELD_NAME"]?>', this)">
<?endif;?>