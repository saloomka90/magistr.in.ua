<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

unset($arParams["PROPERTY_CODE"][array_search("parent", $arParams["PROPERTY_CODE"])]);
?>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<form class="zayavki" onsubmit="return confirm('<?=GetMessage("CONFIRM_DELETE")?>');" action="" method="POST">
<table class="rounded" id="my_quotes">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td style="width:0%;"><input type="checkbox" id="top_chck" onclick="selectAll('my_quotes', this.checked);" /></td>
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<td style="width:100% !important;"><?=GetMessage("IBLOCK_PROP_NAME")?></td>
		<?if($arParams["DISPLAY_NAME"]!="N"):?><td><?=GetMessage("IBLOCK_FIELD_NAME")?></td><?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N"):?><td><?=GetMessage("IBLOCK_FIELD_PREVIEW_TEXT")?></td><?endif;?>
		<?foreach($arParams["FIELD_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_FIELD_".$key));?>
			</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_PROP_".$key));?>
			</td>
		<?endforeach;?>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<tr>
		<td style="width:0%;"><input type="checkbox" onclick="checkSelection('my_quotes', 'top_chck')" name="delete_me[]" value="<?=$arItem["ID"]?>" /></td>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
		<td>
			<?/*if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
			<?else:*/?>
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
			<?//endif;?>
		</td>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
			<div><b><?=GetMessage("IBLOCK_FIELD_ID")?></b> <?=$arItem["ID"];?></div>
		</td>
		<?endif?>
		<?if($arItem["DISPLAY_PROPERTIES"]["parent"]):?>
		<td>
			<?
				$rs = CIBlockElement::GetList(array("id"=>"asc"), array("ID"=>$arItem["DISPLAY_PROPERTIES"]["parent"]["VALUE"]), false, array("nTopCount"=>1), array("NAME","DETAIL_PAGE_URL","PROPERTY_NOTE"));
				if($el=$rs->GetNext()) echo '<a href="'.$el["DETAIL_PAGE_URL"].'">'.$el["NAME"].'</a>';
			?>
			<div class="note">[<?=$el["PROPERTY_NOTE_VALUE"]?>]</div>
			<?unset($arItem["DISPLAY_PROPERTIES"]["parent"]);?>
		</td>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<td>
			<?/*if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
			<?else:*/?>
				<?echo $arItem["NAME"]?>
			<?//endif;?>
		</td>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<td>
			<?echo $arItem["PREVIEW_TEXT"];?>
		</td>
		<?endif;?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
		<td>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
		</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
		<td>
			<?//=$arProperty["NAME"]?>
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
		</td>
		<?endforeach;?>
	</tr>
<?endforeach;?>
</tbody>
</table>
<input type="submit" name="submit_delete_quotes" value="<?=GetMessage("IBLOCK_DELETE_QUOTES")?>" />
</form>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>
