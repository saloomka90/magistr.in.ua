<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $_is_home, $DB;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

$dir = FileExistsRecursive($APPLICATION->GetCurDir(), ".lang.php");
if(strlen($dir)>0) $APPLICATION->IncludeFile($dir);

if(!defined("LANG_ID_DEF")) define("LANG_ID_DEF", "ua");
if(!defined("LANG_ID")) define("LANG_ID", LANG_ID_DEF);

$_is_home = $APPLICATION->GetCurPage() == SITE_DIR."index.php" ? true : false;

?><!DOCTYPE HTML>
<html>
<head>

<meta name='yandex-verification' content='7859672a2454421a' />

<?//$APPLICATION->ShowMeta("keywords")?>
<?//$APPLICATION->ShowMeta("description")?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-1.3.2.min.js");?>"></script>
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.form-2.25.js");?>"></script>

<link type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/cupertino/jquery-ui-1.7.3.custom.css");?>" rel="stylesheet">	
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-ui-1.7.3.custom.min.js");?>"></script>

<link href="<?=$APPLICATION->GetTemplatePath("css/nyroModal.css");?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.nyroModal-1.6.2.pack.js");?>"></script>

<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/script.js");?>"></script>
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.scrollTo-1.4.2-min.js");?>"></script>

<?$APPLICATION->ShowHead();?>
<link href="<?=$APPLICATION->GetTemplatePath("css/opera.css");?>" rel="stylesheet" type="opera/css">
<!--[if IE]><link rel="stylesheet" type="text/css" media="all" href="<?=$APPLICATION->GetTemplatePath("css/ie.css");?>"><![endif]-->
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" media="all" href="<?=$APPLICATION->GetTemplatePath("css/ie6.css");?>">
<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/DD_belatedPNG.js");?>"></script>
<script type="text/javascript">DD_belatedPNG.fix('.iePNG');</script>
<![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="all" href="<?=$APPLICATION->GetTemplatePath("css/ie7.css");?>"><![endif]-->
<?//$APPLICATION->ShowHeadStrings()?>
<?//$APPLICATION->ShowHeadScripts()?>


<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

<script type="text/javascript">
  VK.init({apiId: 2720853, onlyWidgets: true});
</script>


<meta name='yandex-verification' content='7859672a2454421a'>

</head>
<body>
<div class="panel"><?$APPLICATION->ShowPanel();?></div>

	<div class="body"><div class="bcntr"><div class="bottom_bg">
		<div class="header"><div class="r">
			<div class="inner">
				<div class="ovh">
					<a href="<?=SITE_DIR?>" class="logo"><img src="<?=$APPLICATION->GetTemplatePath("images/logo.gif");?>" width="241" height="78" alt="Magistr.in.ua" /></a>
					<div class="cntr">
						<?$APPLICATION->IncludeFile("includes/".LANG_ID."/top.php");?>
					</div>
					<div class="rght">
						<div class="lang">
							<a<?if(LANG_ID=="ua"):?> class="sel"<?else:?> href="<?=str_replace("/".LANG_ID."/","/",$_SERVER["REQUEST_URI"])?>"<?endif;?>>���</a>
							<a<?if(LANG_ID=="ru"):?> class="sel"<?else:?> href="/ru<?=$_SERVER["REQUEST_URI"]?>"<?endif;?>>���</a>
						</div>
						<div class="contacts" style="margin-left:-85px;">
							<?$APPLICATION->IncludeFile("includes/".LANG_ID."/contacts.php");?>
						</div>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="menu-str">
					<div class="float_r">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "top2", array(
	"ROOT_MENU_TYPE" => "top2",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
					</div>
					<div class="float_l">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div></div>
		<div class="middle"><div class="bl"><div class="br"><div class="inner" id="content">
			<?if($USER->IsAdmin() || CSite::InGroup(array(6))):?>
				<div style="/*position:absolute;*/ float: right; right:10px;top:0px;<?if(isIE()):?>min-height:0;<?endif;?>">
					<a href="/quotes_admin/">������ �� ��� ������</a> |
					<a href="/quotes_partners_admin/">������ ��������</a> |
					<a href="/quotes_admin_ready/">������ �� ����� ������</a> |
					<a href="/authors_admin/">������</a> |
					<a href="/partners_admin/">��������</a>
<br/>
<a href="/quotes_agents_admin/">������ ������</a> | <a href="/agents_admin/">������</a>
				</div>
			<?endif;?>
			<?if(file_exists($_SERVER["DOCUMENT_ROOT"].$APPLICATION->GetCurDir()."right_inc.php")):?>
			<div class="lc">
			<?endif;?>
				<?if(strpos($APPLICATION->GetCurPage(),"/works/")!==false):?>
				<div class="float_r">
					<?$APPLICATION->IncludeComponent("bitrix:search.form", "search_form", Array(
	"PAGE" => (LANG_ID!="ua"?"/".LANG_ID:"")."/search/index.php",	// �������� ������ ����������� ������ (�������� ������ #SITE_DIR#)
	),
	false
);?>
				</div>
				<?endif;?>
				<h2><?$APPLICATION->ShowTitle();?></h2>
				