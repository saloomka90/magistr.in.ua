<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo "<pre>"; print_r($arResult); echo "</pre>";
//exit();
//echo "<pre>"; print_r($_SESSION); echo "</pre>";

$group_ok = in_array(5, $USER->GetUserGroupArray()) ? true : false;

$ar_auth2_fields = array("UF_LIVING_PLACE", "UF_BIRTH_YEAR", "UF_OCCUPATION", "UF_OPIT", "UF_WORK_TYPES", "UF_SPHERE", "UF_LANGS", "UF_OTHER_DATA", "UF_SUBJECTS");
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
//if($USER->IsAdmin()) echo "<pre>".print_r($_SESSION,true)."</pre>";
if ($arResult['DATA_SAVED'] == 'Y')
{
	if($_POST["auth_type"]==2&&!$group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(5))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = true;
		}

		$rsUser = CUser::GetByID($user_id);
		if($arUser = $rsUser->Fetch())
		{
			$arEventFields = array(
				"USER_ID"=>$arUser["ID"],
				"LOGIN"=>$arUser["LOGIN"],
				"EMAIL"=>$arUser["EMAIL"],
				"NAME"=>$arUser["NAME"]
			);
			CEvent::Send("NEW_AUTHOR_FROM_OLD", "s1", $arEventFields);
			CEvent::CheckEvents();
		}
	}
	elseif($group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(3))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = false;
		}
	}

	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}
?>
<script type="text/javascript">
var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';

	function regSubmit(caller, pref)
	{
		$(caller).append('<input type="hidden" name="EMAIL" value="'+$("#"+pref+"_LOGIN").attr("value")+'" />');
		return submForm(pref);
	}
</script>
<form onsubmit="return regSubmit(this, 'reg');" method="POST" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<?
$js_titles = "";
?>
<h3><?=GetMessage("REG_I_WANT");?>:</h3>
<div>
	<div class="radio"><input<?if($_POST['auth_type']==1||(!isset($_POST['auth_type']) && !$group_ok)):?> checked="checked"<?endif;?> onclick="sw('auth2_fields', !this.checked)" type="radio" class="radio" id="auth_type1" name="auth_type" value="1" /></div>
	<label for="auth_type1"><?=GetMessage("REG_SALE");?></label>
	<div class="clear">&nbsp;</div>
	<small><?=GetMessage("REG_SALE_DESCR");?></small>
</div><br />
<div>
	<div class="radio"><input<?if($_POST['auth_type']==2||$group_ok):?> checked="checked"<?endif;?> onclick="sw('auth2_fields', this.checked)" type="radio" class="radio" id="auth_type2" name="auth_type" value="2" /></div>
	<label for="auth_type2"><?=GetMessage("REG_SALE_ORDER");?></label>
	<div class="clear">&nbsp;</div>
	<small><?=GetMessage("REG_SALE_ORDER_DESCR");?></small>
</div><br />
<div class="reg_col">
	<?
	/*if($arResult["ID"]>0)
	{
	?>
		<?
		if (strlen($arResult["arUser"]["TIMESTAMP_X"])>0)
		{
		?>
		<tr>
			<td><?=GetMessage('LAST_UPDATE')?></td>
			<td><?=$arResult["arUser"]["TIMESTAMP_X"]?></td>
		</tr>
		<?
		}
		?>
		<?
		if (strlen($arResult["arUser"]["LAST_LOGIN"])>0)
		{
		?>
		<tr>
			<td><?=GetMessage('LAST_LOGIN')?></td>
			<td><?=$arResult["arUser"]["LAST_LOGIN"]?></td>
		</tr>
		<?
		}
		?>
	<?
	}*/
	?>
	<?
	$title = GetMessage("LOGIN")."*";
	$name = "LOGIN";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="50" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NEW_PASSWORD");
	$name = "NEW_PASSWORD";
	$id = "reg_".$name;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" autocomplete="off" maxlength="50" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NEW_PASSWORD_CONFIRM");
	$name = "NEW_PASSWORD_CONFIRM";
	$id = "reg_".$name;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input type="password" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="" maxlength="50" autocomplete="off" /><div class="wide_field_descr" style="width:250px;padding-top:3px;"><?=$title?></div><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("NAME")."*";
	$name = "NAME";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("USER_PHONE")."*";
	$name = "PERSONAL_PHONE";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<?
	$title = GetMessage("USER_MOBILE");
	$name = "PERSONAL_MOBILE";
	$id = "reg_".$name;
	$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
	$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
	?>
	<input onblur="unsetFocusField(this, 'reg')" onfocus="setFocusField(this, 'reg')" type="text" class="inputtext wide" id="<?=$id?>" name="<?=$name?>" title="<?=$title?>" value="<?=$value?>" maxlength="255" /><br />
	<div class="clear">&nbsp;</div>

	<? // ********************* User properties ***************************************************?>
	<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
		<?$first = true;?>
		<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
			<?
//if($USER->IsAdmin()) echo "<pre>".print_r($arUserField,true)."</pre>";
			if(in_array($FIELD_NAME, $ar_auth2_fields))
			{
				ob_start();
			}
			$title = GetMessage($FIELD_NAME);
			$descr = GetMessage("REGISTER_FIELD_".$FIELD_NAME."_DESCR");
			if(strlen($title)<=0) $title = $arUserField["EDIT_FORM_LABEL"];
			$title .= ($arUserField["MANDATORY"]=="Y"?"*":"");
			$name = $FIELD_NAME;
			$id = "reg_".$name;
			$value = strlen($arResult["arUser"][$name])>0 ? $arResult["arUser"][$name] : $title;
			$js_titles .= (strlen($js_titles)>0?", ":"")."reg_".$name.": '".$title."'";
			?>
			<div id="register-field-<?=$FIELD_NAME?>">
				<?if($arUserField["SETTINGS"]["DISPLAY"]=="CHECKBOX"):?>
					<div><?=$title?></div>
				<?endif;?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "field_params"=>array("class"=>"inputtext wide", "id"=>"reg_".$FIELD_NAME, "title"=>$title, "onblur"=>"unsetFocusField(this, 'reg')", "onfocus"=>"setFocusField(this, 'reg')")), null, array("HIDE_ICONS"=>"Y"));?>
				<?if(strlen($descr)>0):?><div class="wide_field_descr"><?=$descr?></div><?endif;?>
				<div class="clear">&nbsp;</div>
			</div>
			<?
			if(in_array($FIELD_NAME, $ar_auth2_fields))
			{
				$content = ob_get_contents();
				ob_end_clean();
				$auth2_fields .= $content;
			}
		endforeach;
		?>
	<?endif;?>
<div id="auth2_fields"<?if(($_POST["auth_type"]!=2||!isset($_POST["auth_type"]))&&!$group_ok):?> style="display:none;"<?endif;?>>
<?=$auth2_fields?>
</div>
	<?// ******************** /User properties ***************************************************?>
	<div class="clear">&nbsp;</div>
	<?/*<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>*/?>
	<p><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("SAVE") : GetMessage("ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('RESET');?>"></p>
</form>
</div>
<?
$script = '
<script type="text/javascript">
	var names_reg = new Object;
	names_reg = {'.$js_titles.'};
</script>
';
$APPLICATION->AddHeadString($script);
?>
<script type="text/javascript">
	var allways_disable = false;
	function disableChecks(cp) {
		if(allways_disable) {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else if(jQuery("input:checkbox:checked",cp).size()>=3) {
			jQuery("input:checkbox:not(:checked)",cp).each(function(){jQuery(this).attr("disabled","disabled");});
		} else {
			jQuery("input:checkbox",cp).each(function(){jQuery(this).attr("disabled","");});
		}
	}
	jQuery(document).ready(function(){
		jQuery("#register-field-UF_SPHERE input:checkbox").each(function(){
			jQuery(this).bind("click",function(){
				disableChecks(jQuery(this).parent().parent());
			});
		});
		disableChecks(jQuery("#register-field-UF_SPHERE"));
	});
</script>