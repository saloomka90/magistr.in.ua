<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;

//echo "<pre>".$_POST["comment"]."</pre>";
if(!defined("LANG_ID") && strlen($_REQUEST["lang"])>0) define("LANG_ID",$_REQUEST["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_REQUEST["o_id"]);
//echo "<pre>".print_r($_GET,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,5),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 7;

	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
		$arOrder = $obOrder->GetFields();
		$arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21 && (strlen($_REQUEST["summ"])>0 || $_REQUEST["refuse"]=="Y"))
		{
			$rs_resp = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$BID,"ACTIVE"=>"Y","PROPERTY_author_id"=>$USER->GetID(),"PROPERTY_order_id"=>$order_id),false,array("nTopCount"=>1),array("ID","PREVIEW_TEXT","PROPERTY_summ","PROPERTY_order_id","PROPERTY_author_id","PROPERTY_refused"));
			$ar_resp = $rs_resp->GetNext();
			//echo "<pre>".print_r($ar_resp,true)."</pre>";

			$el = new CIBlockElement;
			$PROP = array();
			$PROP["author_id"] = $USER->GetID();
			$PROP["order_id"] = $order_id;

			if(strlen($_REQUEST["summ"])>0) $PROP["summ"] = $_REQUEST["summ"];
			else
			{
				$db_enum_list = CIBlockProperty::GetPropertyEnum("refused", Array(), Array("IBLOCK_ID"=>$BID));
				if($ar_enum_list = $db_enum_list->GetNext())
				{
					//echo "<pre>".print_r($ar_enum_list,true)."</pre>";
					$PROP["refused"] = $ar_enum_list["ID"];
				}
			}



			$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(),
			  "IBLOCK_SECTION_ID" => false,
			  "IBLOCK_ID"      => $BID,
			  "PREVIEW_TEXT"=> iconv("utf-8", "windows-1251", $_REQUEST["comment"]),
			  "PREVIEW_TEXT_TYPE"=> "text",
			  "PROPERTY_VALUES"=> $PROP,
			  "NAME"           => "������ � ������ �".$order_id,
			  "ACTIVE"         => "Y",
			  );
			//echo "<pre>".print_r($arLoadProductArray,true)."</pre>";
			if($ar_resp)
			{
				if($ID=$el->Update($ar_resp["ID"],$arLoadProductArray)){
					$adasdas = 1;
					// echo '<script type="text/javascript">var reload_page = 0;</script>';
					echo '<div id="qqq"></div><table width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("RESPONSE_ACCEPTED").'</td></tr></table><script type="text/javascript">var reload_page = 1;location.reload()</script>';
				}else {
					$adasdas = 0;
					echo "Error: ".$el->LAST_ERROR;
				}
			}
			else
			{

				if($ID=$el->Add($arLoadProductArray)){
					$adasdas = 1;
					// echo '<script type="text/javascript">var reload_page = 0;</script>';
					echo '<div id="qqq"></div><table  width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("RESPONSE_ACCEPTED").'</td></tr></table><script type="text/javascript">var reload_page = 1;location.reload();</script>';
				}else{
					$adasdas = 0;
					//echo "Error: ".$el->LAST_ERROR;
				}
			}
		}
		else
		{
			$ar_props = array("type","subject","lang","pages","period","pract","organ","city_region","task");

			//echo "<pre>".print_r($arOrder,true)."</pre>";
			$res = '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td colspan="2"><strong>'.$arOrder["PROPERTIES"]["topic"]["VALUE"].'</strong></td>
	</tr>';

			foreach($ar_props as $pid)
			{
				$arProperty = $arOrder["PROPERTIES"][$pid];

				$name = GetMessage("IBLOCK_PROP_".$pid);
				if(strlen($name)<=0) $name = $arProperty["NAME"];

				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;

				$res .= '
			<tr>
				<td style="width:30% !important;">'.$name.'</td>
				<td style="width:70% !important;">'.TxtToHtml($value_text).'</td>
			</tr>';
			}
			if(!is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && $arOrder["PROPERTIES"]["file"]["VALUE"]>0)
				$arOrder["PROPERTIES"]["file"]["VALUE"] = array($arOrder["PROPERTIES"]["file"]["VALUE"]);
			//$file = CFile::GetFileArray($arOrder["PROPERTIES"]["file"]["VALUE"]);

			//echo "<pre>".print_r($arOrder["PROPERTIES"]["file"],true)."</pre>";
			//if(is_array($file) && sizeof($file))
			if(is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && sizeof($arOrder["PROPERTIES"]["file"]["VALUE"]))
			{
				$res .= '
			<tr>
				<td>'.GetMessage("IBLOCK_PROP_FILE").'</td>
				<td><a href="/zipfiles.php?oid='.$arOrder["ID"].'" class="zip">'.GetMessage("DOWNLOAD")/*.' ('.sprintf("%.1f",$file["FILE_SIZE"]/1024).' Kb)'*/.'</a></td>
			</tr>';
			}
			$res .= '
</tbody>
</table>';
			if($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21)
				$res .= '
<div><b>'.GetMessage("SUMM_DESCR").'</b></div><br />
<div class="float_r ff"><input type="button" onclick="refuse_order('.$order_id.',$(\'#comment\').val());" value="'.GetMessage("REFUSE").'" /></div>
<form id="sendReq" method="post" action="/bitrix/templates/.default/components/bitrix/news.list/my_quotes_new/order_detail.php" class="nyroModal">
<div class="float_l ff">
	<input name="summ" type="text" id="accept_summ" onkeypress="onlyDigits();" onkeyup="onlyDigits();" style="width:100px;" class="inputtext" value="'.$ar_resp["PROPERTY_SUMM_VALUE"].'" /> ���.
	<input type="button" onclick="checkForm();" value="'.GetMessage("ENTER_SUMM").'" />
</div>
<div class="clear">&nbsp;</div><br />
<b>'.GetMessage("COMMENT").'</b><br />
<textarea id="comment" name="comment" rows="5" style="width:99%;" cols="55">'.$ar_resp["~PREVIEW_TEXT"].'</textarea>
<input type="hidden" name="o_id" value="'.$order_id.'">
<input type="hidden" name="lang" value="'.$_REQUEST["lang"].'">


</form>
<script>
 function checkForm(){
 	if($("#accept_summ").val().length>0){
 		$("#sendReq").submit();
 	}else{
 		 alert("'.GetMessage("DIDNT_ENTER_SUMM").'");
 	}
  return false;
 }
</script>
<br />
';
			else
				$res .= '
<div><b>'.GetMessage("SUMM_DESCR").'</b></div><br />
<div>'.$ar_resp["PROPERTY_SUMM_VALUE"].' ���.</div>
<div class="clear">&nbsp;</div><br />
<b>'.GetMessage("COMMENT").'</b><br />
<div>'.TxtToHtml($ar_resp["~PREVIEW_TEXT"]).'</div>
<br/>
';

			 echo $res;
		}
	}
}
?>