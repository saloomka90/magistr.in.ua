<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
// echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
$prt_ids = array();
foreach($arResult["ITEMS"] as $arItem)
{
	$ids[] = $arItem["ID"];
	if($arItem["PROPERTIES"]["partner_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["partner_id"]["VALUE"];
	
	if($arItem["PROPERTIES"]["agent_id"]["VALUE"]>0)
		$prt_ids[] = (int)$arItem["PROPERTIES"]["agent_id"]["VALUE"];
	
}

$arrTypes = array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>9, );
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
	$okr =  $ob->GetFields();
	$arrTypes[$okr["ID"]] = $okr["NAME"];
		
		
}
$arrPredmsect = array();
$arrPredm = array();
$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID");
$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockSection::GetMixedList(Array(), $arFilter, false,$arSelect);
while($zob = $res->GetNext())
{
		
	$arrPredm[$zob["ID"]] = $zob["NAME"];
		



}

$user_responses = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","PROPERTY_order_id"=>$ids,"PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_responses[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$user_refused = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","PROPERTY_order_id"=>$ids,"!PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_refused[$ar_ur["PROPERTY_ORDER_ID_VALUE"]][] = $ar_ur;

$partners = array();
$rs_ur = CUser::GetList($sort="", $order="",array("ID"=>implode("|",$prt_ids)));
while($ar_ur=$rs_ur->GetNext()) $partners[$ar_ur["ID"]] = $ar_ur;

//echo "<pre>".print_r($user_responses,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {

		$('.new-pay-l .sbOptions li a').live('click',function(){

			$('input[name="autir_price"]').val($('.new-pay-l .niceSelect option[value="'+$(this).attr('rel')+'"]').attr('data'));

			});
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function set_termin(order_id){

		period = $('#send_new_date').val();
		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/propupdate.php",
	 		   data: 'o_id='+order_id+'&period='+period,
	 		   success: function(msg){
	 			 	 $("#set_here_new_termin").html(period);
	 				$('.send_box_w3').css({'opacity':'0'});
	 				$('.send_box_w3').css({'z-index':'-9999'});
	 		   }
		});

		
		
	}
	function validateStateChange(order_id){

		var PREVIEW_TEXT = $('.'+order_id+'_PREVIEW_TEXT').val();
		var state = $('.'+order_id+'_state').val();
		//var sum_total = $('.'+order_id+'_sum_total').val();
		var AUTHOR_ID = $('.'+order_id+'_AUTHOR_ID').val();
		var author_sum_total = $('.'+order_id+'_author_sum_total').val();

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/propupdate.php",
	 		   data: 'o_id='+order_id+'&PREVIEW_TEXT='+PREVIEW_TEXT+'&state='+state+'&AUTHOR_ID='+AUTHOR_ID+'&author_sum_total='+author_sum_total,
	 		   success: function(msg){
	 			  //alert(msg);
	 			   $('*').css({"cursor":"wait"});
	 			 location.reload();
	 		   }
		});
		
		
	}
	function no_autor_send(order_id){
		send_event = $('.send_box.send_box_w6 input[type="radio"]:checked').val();
		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/not_found.php",
	 		   data: 'o_id='+order_id+'&send_event='+send_event,
	 		   success: function(msg){		  			   
	 			 $('.send_box_w6').css({'opacity':'0'});
	 			 $('.send_box_w6').css({'z-index':'-9999'});
	 			 alert('�i���������!');			  
	 		   }
		});

		
	}
	function set_page(page,uid){
		

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/pager.php",
	 		   data: 'uid='+uid+'&page='+page,
	 		   success: function(msg){
		 		   //alert(msg); 
		 		   $('.r-pag-num').removeClass('active');
					$('.r-pag-num[npage="'+page+'"]').addClass('active');
						
		 		   $('.put_here').html(msg)	;		 	
		 	   }
		});

		
	}
	function add_our_sum_to(order_id){
		event = $('.send_box.send_box_w4 input[type="radio"]:checked').val();
		sum_total = $('#price_4').val();


		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/propupdate.php",
	 		   data: 'o_id='+order_id+'&sum_total='+sum_total+'&event='+event,
	 		   success: function(msg){
					$('.'+order_id+'_sum_total').html(sum_total);	 
					$('.send_box.send_box_w4').hide();	 			 
	 			 	//alert(msg);
	 			 	$('.something_set_this2').text('������');
	 			 	$("#price_4").val('');
	 			 	$('.send_box_val.dellthisafter').remove();
	 		   }
		});

		
		
	}
	function activator(order_id){
		//alert(order_id);
		if($('.activ_send_mail').prop("checked")){
			send = 'Y';
		}else{
			send = 'N';	
		}

		if($('.client_send_sms').prop("checked")){
			send_sms_client = 'Y';
		}else{
			send_sms_client = 'N';	
		}

		if($('.client_send_mail').prop("checked")){
			send_client = 'Y';
		}else{
			send_client = 'N';	
		}

		
		$.ajax({
	 		   type: "POST",
	 		   url: "<?=$cur_dir?>/order_activate.php",
	 		   data: 'order_id='+order_id+'&send='+send+'&send_sms_client='+send_sms_client+'&send_client='+send_client,
	 		   success: function(msg){
		 		   //	alert(msg);
		 		   	
	 			 	 $('*').css({"cursor":"wait"});
		 			 location.reload();
		 	   }
		});
	}
	function showOrderDetails(order_id, back_to_perf_id,refused,other_params)
	{

	
		
		<?/*?>
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'+(!!back_to_perf_id ? '&back_to_perf_id='+back_to_perf_id : '')+(!!refused?'&refused=Y':'')+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
		<?*/?>
		
		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/order_detail.php",
	 		   data: 'o_id='+order_id+'&lang=<?=LANG_ID?>'+(!!back_to_perf_id ? '&back_to_perf_id='+back_to_perf_id : '')+(!!refused?'&refused=Y':'')+(other_params&&typeof(other_params)!==undefined?other_params:''),
	 		   success: function(msg){
	 		   	 	$('#nonebox_w2').html(msg);

	 		   	 	
 	             $('#overlay_w2').fadeIn(0,function(){
 					$('#nonebox_w2').animate({'top':window.pageYOffset},0);
 					$("#ui-datepicker-div").hide();
 	             });

 	            
 	           

 	            var select = $('.niceSelect');
 	   		$(select).selectbox({
 	   			onOpen: function (select) {
 	   				$('.sbHolder').addClass('focus');
 	   				$('.sbGroup').parent().addClass('sbGroup');
 	   			},
 	   			onClose: function (inst) {
 	   				$('.sbHolder').removeClass('focus');
 	   			},
 	   			onChange: function (val, inst) {},
 	   			effect: "slide"				
 	   		});



 	   	$("#send_date_p").datepicker({
			showOn: "button",
			buttonImage: "/bitrix/templates/magistr_new/images/calendar.png",
			buttonImageOnly: true,
			dateFormat: "dd.mm.yy"
			
		});	

 	   $("#send_new_date").datepicker({
			showOn: "button",
			buttonImage: "/bitrix/templates/magistr_new/images/calendar.png",
			buttonImageOnly: true,
			dateFormat: "dd.mm.yy"
			
		});

 	   $.datepicker.regional['ua'] = {
				closeText: '�������',
				prevText: '&#x3c;�����',
				nextText: '����&#x3e;',
				currentText: '��������',
				monthNames: ['ѳ����','�����','��������','������','�������','�������',
				'������','�������','��������','�������','��������','�������'],
				monthNamesShort: ['ѳ�','���','��','���','���','���',
				'���','���','���','���','���','���'],
				dayNames: ['�����','��������','�������','������','������','�������','������'],
				dayNamesShort: ['���','���','��','���','���','���','���'],
				dayNamesMin: ['��','��','��','��','��','��','��'],
				weekHeader: '��',
				dateFormat: 'dd.mm.yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''};
			$.datepicker.setDefaults($.datepicker.regional['ua']);
	 	 	      
	 		   }
	 		 });

		
		
	}
	function showPerformers(order_id,user_id,detail,refused)
	{
		<?/* ?>
		$.nyroModalManual({
			url: '<?=$cur_dir?>/performers.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':'')
		});
		return false;
		<? */?>
		$.ajax({
 		   type: "GET",
 		   url: "<?=$cur_dir?>/performers.php",
 		   data: 'o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':''),
 		   success: function(msg){
 		   	 	$('#nonebox_ws').html(msg);

	 		   	$('#overlay_ws').fadeIn(0,function(){
	 				$('#nonebox_ws').animate({'top':window.pageYOffset},0);
	 	    	});
	 		  // $('html, body').animate({scrollTop: 0},0);
 	 	      
 		   }
 		 });     
	}

	
	
	function price_1_add(order_id,ibl){

		price_1 = $("#price_1").val();
		if ($("#send_type_new").prop("checked")){
			date = '<?=FormatDate("d.m.Y",mktime(0, 0, 0, date("m")  , date("d"), date("Y")))?>';
		}else{
			date = $("#send_date_p").val();
		}	
		
		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/add_price.php",
	 		   data: 'o_id='+order_id+'&price='+price_1+'&iblock_id='+ibl+"&date="+date,
	 		   success: function(msg){

	 			  //alert(msg);
	 			/*  $('*').css({"cursor":"wait"});
	 			  location.reload();*/



	 			 
	 			 $('.entsss_price').append('<tr class="id_'+msg+'" data="'+price_1+'"><td>'+date+'</td><td><strong>+'+price_1+' ���.</strong></td><td><a href="javascript:void(0);" onclick="delite_1_price('+msg+','+order_id+');">��������</a></td></tr>');
	 			 	newval = $('.ent_price_'+order_id).text()*1+price_1*1;
	 			 	$('.ent_price_'+order_id).html(newval);
	 			 	$('.ent_price_set_'+order_id).html(newval);

	 			 	$('.send_box.send_box_w1').css('z-index', '-9999');
					  $('#price_1').val('');
	 		   }
		});
	}


	function price_2_add(order_id,ibl){

		price_1 = $("#price_2").val();
		
		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/add_price.php",
	 		   data: 'o_id='+order_id+'&price='+price_1+'&iblock_id='+ibl,
	 		   success: function(msg){

	 			  
	 			  /*$('*').css({"cursor":"wait"});
	 			  location.reload();*/

		 			 // alert(msg);

		 			  $('.entsss_price2').append('<tr class="id_'+msg+'" data="0"><td>���������</td><td><strong>+'+price_1+' ���.</strong></td><td><a href="javascript:void(0);" onclick="delite_1_price('+msg+');">��������</a></td></tr>');
					  $('.send_box.send_box_w2').hide();
					  $('#price_2').val('');
			 		   }
		});
	}
	
	function delite_1_price(order_id,oid){
		

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/dell_price.php",
	 		   data: 'o_id='+order_id,
	 		   success: function(msg){
	 			 // $('html').css({"cursor":"wait"});
	 			  //location.reload();
	 			  some = $('.id_'+order_id).attr('data')*1;
	 			 
	 			 	$('.id_'+order_id).remove();

	 			 	
	 			 	newval = $('.ent_price_'+oid).text()*1-some;
	 			 	 
	 			 	$('.ent_price_'+oid).html(newval);
	 			 	$('.ent_price_set_'+oid).html(newval);
	 		   }
		});   
	}

function delite_2_price(order_id,oid){
		

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/dell_price.php",
	 		   data: 'o_id='+order_id,
	 		   success: function(msg){
	 			  /*$('html').css({"cursor":"wait"});
	 			  location.reload();*/

	 			  
	 			 	 some = $('.id_'+order_id).attr('data')*1;
	 			 	$('.id_'+order_id).remove();
	 			 	newval = $('.ent_price2_'+oid).text()*1-some;
	 			 	$('.ent_price2_'+oid).html(newval);
	 			 	$('.ent_price2_set_'+oid).html(newval);
				
	 			 	
	 		   }
		});   
	}
	function showPerform(order_id,user_id,detail,refused)
	{
		<?/* ?>
		$.nyroModalManual({
			url: '<?=$cur_dir?>/performers.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':'')
		});
		return false;
		<? */?>
		

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/perform.php",
	 		   data: 'o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'+(detail?'&detail=Y':'')+(refused?'&refused=Y':''),
	 		   success: function(msg){
	 		   	 	$('#nonebox_w1').html(msg);

	 		   	 $('#overlay_w1').fadeIn(0,function(){
	 				$('#nonebox_w1').animate({'top':window.pageYOffset},0);
	          }); 

	 		   	$('#overlay_w1, #box-close-w1').click(function(){
	 	             $('#nonebox_w1').animate({'top':'-1000px'},0,function(){
	 					$('#overlay_w1').fadeOut(0);
	 	             });
	 				 //return false;
	 	        });	
	 		   //$('html, body').animate({scrollTop: 0},0);
	 		   }
	 		 }); 

		    
	}
	function showPartners(order_id,user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partners.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function editPartners(user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/partner_edit.php?u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function changeState(order_id,comment,state,performer,partner_sum,author_sum_paid,author_sum_total,sum_paid,sum_total,q_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&comment='+escape(comment)+'&state='+state+'&performer='+performer+'&partner_sum='+partner_sum+'&author_sum_paid='+author_sum_paid+'&author_sum_total='+author_sum_total+'&sum_paid='+sum_paid+'&sum_total='+sum_total+'&Q_ID='+q_id+'&lang=<?=LANG_ID?>&save=Y'
		});
		return false;
	}
	function selectAuthor(order_id,user_id,price)
	{
		$('#nonebox_ws').animate({'top':'-2000px'},0,function(){
			$('#overlay_ws').fadeOut(0);
         });

       
		<? /*?>
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&performer='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
		<? */?>

		$.ajax({
	 		   type: "GET",
	 		   url: "<?=$cur_dir?>/order_detail.php",
	 		   data: 'o_id='+order_id+'&performer='+user_id+'&price='+price+'&lang=<?=LANG_ID?>',
	 		   success: function(msg){
	 		   	 	$('#nonebox_w2').html(msg);

	 		   	 	
    	             $('#overlay_w2').fadeIn(0,function(){
    					$('#nonebox_w2').animate({'top':'40px'},0);
    	             });
    				
    	             var select = $('.niceSelect');
    	  	   		$(select).selectbox({
    	  	   			onOpen: function (select) {
    	  	   				$('.sbHolder').addClass('focus');
    	  	   				$('.sbGroup').parent().addClass('sbGroup');
    	  	   			},
    	  	   			onClose: function (inst) {
    	  	   				$('.sbHolder').removeClass('focus');
    	  	   			},
    	  	   			onChange: function (val, inst) {},
    	  	   			effect: "slide"				
    	  	   		});
    	  	   	 //$('html, body').animate({scrollTop: 0},0);
	 	 	      
	 		   }
	 		 });

		 
	}
	function editOrder(order_id)
	{
		//$.nyroModalRemove();
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_edit.php?CODE='+order_id+'&lang=<?=LANG_ID?>',
			
			endShowContent: function(elts, settings) {
				setFormOptions();
				setTimeout('$("#nyroModalContent").scrollTo("p:eq(0)",200);',400);
			}
		});
		$('#nyroModalFull').css('zIndex',99999);
		return false;
	}
	//]]>



	//Pop-up window autor list
    $(function() {
    	 
    	       
    	       
    	    
    	        $('#overlay_w2, #box-close-w2').live("click",function(){
   	             $('#nonebox_w2').animate({'top':'-8000px'},0,function(){
   					$('#overlay_w2').fadeOut(0);
   	             });
   				 return false;
   	        });	
        $('#overlay_ws, #close_ws').live('click',function(){
             $('#nonebox_ws').animate({'top':'-2000px'},0,function(){
				$('#overlay_ws').fadeOut(0);
             });
			 return false;
        });	
    });
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<div class="table_box work_list_box">
					<div class="send_box_hide">
						<div class="send_box">
							<span class="send_box_arr"><span></span></span>
							<div class="send_box_title">��������� ��������:</div>
							<div class="send_box_val">
								<input type="radio" name="send_type" id="send_type" checked="checked" value="0" />
								<label for="send_type">�� ������</label>
								<div class="clr"></div>
							</div>
							<div class="send_box_val">
								<input type="radio" name="send_type" id="send_type2" value="1" />
								<label for="send_type2">�� i���� ����</label>
								<input type="text" id="send_date" value="22.11.13" />
								<div class="clr"></div>
							</div>
							<a class="cancel" href="#" onclick="$(this).parent().remove(); return false;">���������</a>
							<input type="submit" value="OK" />
							<div class="clr"></div>						
						</div>
					</div>
	
	
<? $this_date=0;?>	
<?foreach($arResult["ITEMS"] as $l=>$arItem):?>
<? 
$PARItem = $arItem;
$arItem = $arResult["ITEMS_BY_IDS"][$PARItem["PROPERTY_ORDER_ID_VALUE"]];

/*
echo "<pre>";
var_dump($PARItem['PROPERTY_ORDER_ID_VALUE'],$arItem["ID"]);
echo "</pre>";
*/

$arItem['ACTIVE_FROM'] = $PARItem['DATE_ACTIVE_FROM'];
	
	$style = "am1";
	
	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{	
		case 23:
			$style = 'am12';
			break;
		case 24:
			$style = 'am14';
			break;
		case 21:
			$style = 'am11';
			break;		
		case 22:
			$style = 'am13';
			break;
		
	}
	
	if($arItem["ACTIVE"]=="N")$style = 'am10';
	?> 
<?if($this_date != ConvertDateTime($arItem["ACTIVE_FROM"],"DD.MM.YYYY","ru")){ ?>
	<?if ($l!=0){ ?>
	</tbody>
	</table>
	<? }?>
						
						
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
							<thead>
								<tr>
									<th class="today" colspan="5"><?=ConvertDateTime($arItem["ACTIVE_FROM"],"DD.MM.YYYY","ru")?></th>
								</tr>
							</thead>
							<tbody>					
	<? 
$this_date = ConvertDateTime($arItem["ACTIVE_FROM"],"DD.MM.YYYY","ru");
}?>
<tr class="<?=$style;?>">
								<td class="nomer" width="68px"><?//var_dump($arItem["PROPERTIES"]["partner_id"],$arItem["PROPERTIES"]["agent_id"])?>
								<?if($arItem["PROPERTIES"]["partner_id"]["VALUE"]){
									$rsUser = CUser::GetByID(intval($arItem["PROPERTIES"]["partner_id"]["VALUE"]));
									$arUser = $rsUser->Fetch();
									
									echo "<span title='".$arUser["NAME"]."' style='color:red'>referral</span><br>";}?>
								<?if($arItem["PROPERTIES"]["agent_id"]["VALUE"]){
									$rsUser = CUser::GetByID(intval($arItem["PROPERTIES"]["partner_id"]["VALUE"]));
									$arUser = $rsUser->Fetch();
									
									echo "<span title='".$arUser["NAME"]."' style='color:blue'>agent</span><br>";}?>
								
								<?=$arItem["ID"]?></td>
								
								
									
									<td class="sub" width="570px">
										<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" <?/* ?>onclick="showOrderDetails(<?=$arItem["ID"]?>);"<? */?>><?=$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"]?></a> 
										<br>
										<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?> / <?=$arItem["DISPLAY_PROPERTIES"]["subject"]["DISPLAY_VALUE"]?>&nbsp;&nbsp;<?=$arItem["PROPERTIES"]["period"]["VALUE"]?>
										
										
										<br>
										<? $allprice = 0;
									
									$arSelect = Array("PROPERTY_ADD_PRICE");
									$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_ORDER_ID"=>$arItem["ID"]);
									$res = CIBlockElement::GetList(Array("date_active_from"=>"ASC"), $arFilter, false, false, $arSelect);
									while($ob = $res->GetNextElement())
									{
										$arFields = $ob->GetFields();
										
										$allprice+=$arFields["PROPERTY_ADD_PRICE_VALUE"];
									
									}
									
								
							
							?>				
											<span><span class="ent_price_set_<?=$arItem["ID"]?>"><?=(int)$arItem["PROPERTIES"]["sum_paid"]["VALUE"]+$allprice?></span> i� <span class="<?=$arItem["ID"]?>_sum_total"><?=(int)$arItem["PROPERTIES"]["sum_total"]["VALUE"]?></span></b> ���.
										
										<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
											<?echo $arItem["~PREVIEW_TEXT"];?>
										<?endif;?>
									</td>
									
								<? /*?><td class="tip" width="130px">
								
								
								<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
								
								<?if($arOrder["PROPERTIES"]["WORK_TYPES"]["VALUE"] != 50943){
			
				if($arrTypes[$arOrder["PROPERTIES"]["WORK_TYPES"]["VALUE"]]!=''){
					echo $arrTypes[$arOrder["PROPERTIES"]["WORK_TYPES"]["VALUE"]];
				}else{
					echo $arOrder["PROPERTIES"]["type"]["VALUE"];}
			}else{echo $arOrder["PROPERTIES"]["type"]["VALUE"];}?>
								
								</td><? */?>
								<td width="120px" <? if($arItem["PROPERTIES"]["performer"]["VALUE"]=="" or $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]=='21' or $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]=='22'){?>style="text-align: center;"<?}?>class="autor">
								
								<??>
								<? if($arItem["PROPERTIES"]["performer"]["VALUE"]=="" or $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]=='21' or $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]=='22'){?>									
									<a class="f_la"href="javascript:void(0)" onclick="showPerformers(<?=$arItem["ID"]?>,'');"><?=(sizeof($user_responses[$arItem["ID"]]))?></a>
								<? }else{ 
									$rsUser = CUser::GetByID(intval($arItem["PROPERTIES"]["performer"]["VALUE"]));
									$arUser=$rsUser->Fetch();
									if($arItem["PROPERTIES"]["performer"]["VALUE"]!='0'){
										echo "<a href='javascript:void(0)' onclick='showPerform(".$arItem["ID"].",".$arUser["ID"].")'>".$arUser["NAME"]."</a><br>"; 
									}else{
										echo "<span><b>I���� �����</b></span><br>";
									}
									
									
									$allprice = 0;
									
									$arSelect = Array("PROPERTY_ADD_PRICE");
									$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_ORDER_ID"=>$arItem["ID"]);
									$res = CIBlockElement::GetList(Array("date_active_from"=>"ASC"), $arFilter, false, false, $arSelect);
									while($ob = $res->GetNextElement())
									{
										$arFields = $ob->GetFields();
										
										$allprice+=$arFields["PROPERTY_ADD_PRICE_VALUE"];
									
									}
									
									$rsQuote = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"PROPERTY_order_id"=>$arItem["ID"],"ACTIVE"=>"Y","PROPERTY_author_id"=>$arUser["ID"]),false,array("nTopCount"=>1),array("ID","PROPERTY_SUMM"));
									$arQuote = $rsQuote->Fetch();
									?>
									<strong class="ent_price2_set_<?=$arItem["ID"]?>"><?=$arItem["PROPERTIES"]["author_sum_paid"]["VALUE"]+$allprice?></strong> � <strong><?=(int)$arQuote["PROPERTY_SUMM_VALUE"]?></strong> ���.								
									<? 
								 }
								 
								 
								 ?>
								</td>
								<td width="180px" class="status2">
								
								<b><?=$PARItem["PROPERTY_TYPE_VALUE"];?>:</b> <?=$PARItem["NAME"];?>
								
								
								</td>
								
							</tr>


<?endforeach;?>
</tbody>
					</table>
</div>



<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<h4 align="center" style="color:red"><?ShowError(GetMessage("NOT_FOUND"));?></h4>

<?endif;?>

