<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);

CModule::IncludeModule("iblock");
$IBLOCK_ID =6;
$order_id = intval($_GET["o_id"]);

if ($order_id){
	
	 
	if ($_GET["state"]!=''){
		CIBlockElement::SetPropertyValues($order_id, $IBLOCK_ID, $_GET["state"], "state");
	}
	if ($_GET["sum_total"]!=''){
		CIBlockElement::SetPropertyValues($order_id, $IBLOCK_ID, $_GET["sum_total"], "sum_total");
	}
	if ($_GET["AUTHOR_ID"]!=''){
		CIBlockElement::SetPropertyValues($order_id, $IBLOCK_ID, intval($_GET["AUTHOR_ID"]), "performer");
	}
	if ($_GET["author_sum_total"]!=''){
		CIBlockElement::SetPropertyValues($order_id, $IBLOCK_ID, $_GET["author_sum_total"], "author_sum_total");
		
		$rsQuote = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"PROPERTY_order_id"=>$order_id,"ACTIVE"=>"Y","PROPERTY_author_id"=>intval($_GET["AUTHOR_ID"])),false,array("nTopCount"=>1),array("ID","PROPERTY_SUMM"));
		$arQuote = $rsQuote->Fetch();
		
		CIBlockElement::SetPropertyValues($arQuote["ID"], 7, $_GET["author_sum_total"], "summ");
		
	}
	if ($_GET["PREVIEW_TEXT"]!=''){
		$el = new CIBlockElement;
		$res = $el->Update($order_id, array("PREVIEW_TEXT"=>$_GET["PREVIEW_TEXT"]));		
	}
	
	//var_dump($_GET);
}
?>