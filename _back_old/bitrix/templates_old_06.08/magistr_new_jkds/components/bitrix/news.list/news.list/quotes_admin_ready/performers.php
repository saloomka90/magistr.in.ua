<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
$user_id = intval($_GET["u_id"]);
//echo "<pre>".print_r($_GET,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 5;
	
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
                $arOrder = $obOrder->GetFields();
                $arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($user_id>0)
		{
			$rsUser = CUser::GetByID($user_id);
			if($arUser=$rsUser->Fetch())
			{
				$res .= '<div class="float_l ff"><input type="button" onclick="showOrderDetails('.$arOrder["ID"].')" value="'.GetMessage("BACK_TO_ORDER").'" /></div>
<div class="clear">&nbsp;</div><br />
';
				$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td style="width:25% !important;border-right:none;">&nbsp;</td>
		<td colspan="2" style="width:50% !important;border-left:none;border-right:none;"><strong>'.GetMessage("AUTHOR_INFORMATION").'</strong></td>
		<td style="width:25% !important;border-left:none;">&nbsp;</td>
	</tr>';
				$res .= '
				<tr>
					<td colspan="2" style="width:50% !important;">'.GetMessage("FIO").'</td>
					<td colspan="2" style="width:50% !important;">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</td>
				</tr>
				<tr>
					<td colspan="2">Email</td>
					<td colspan="2">'.$arUser["EMAIL"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("PHONE").'</td>
					<td colspan="2">'.(strlen($arUser["PERSONAL_PHONE"])>0?'<div>'.$arUser["PERSONAL_PHONE"].'</div>':'').(strlen($arUser["PERSONAL_MOBILE"])>0?'<div>'.$arUser["PERSONAL_MOBILE"].'</div>':'').'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_LIVING_PLACE").'</td>
					<td colspan="2">'.$arUser["UF_LIVING_PLACE"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_BIRTH_YEAR").'</td>
					<td colspan="2">'.$arUser["UF_BIRTH_YEAR"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OCCUPATION").'</td>
					<td colspan="2">'.$arUser["UF_OCCUPATION"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OPIT").'</td>
					<td colspan="2">'.$arUser["UF_OPIT"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_WORK_TYPES").'</td>
					<td colspan="2">'.$arUser["UF_WORK_TYPES"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_LANGS").'</td>
					<td colspan="2">'.$arUser["UF_LANGS"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OTHER_DATA").'</td>
					<td colspan="2">'.$arUser["UF_OTHER_DATA"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_SUBJECTS").'</td>
					<td colspan="2">'.$arUser["UF_SUBJECTS"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_PRIVAT_CARD").'</td>
					<td colspan="2">'.$arUser["UF_PRIVAT_CARD"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_WEBMONEY").'</td>
					<td colspan="2">'.$arUser["UF_WEBMONEY"].'</td>
				</tr>';
				$res .= '
</tbody>
</table>';
			}
		}
		echo $res;
	}
}
?>