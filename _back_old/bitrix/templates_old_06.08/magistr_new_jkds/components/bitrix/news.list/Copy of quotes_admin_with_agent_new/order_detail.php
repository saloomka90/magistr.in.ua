<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
//echo "<pre>".print_r($MESS,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 7;
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
		$arOrder = $obOrder->GetFields();
		$arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0)
		{
			$rsPartner = CUser::GetByID($arOrder["PROPERTIES"]["partner_id"]["VALUE"]);
			$arPartner = $rsPartner->Fetch();
		}
// echo "<pre>"; print_r($arOrder);echo "</pre>";
		if($_REQUEST["save"]=="Y")
		{
			$el = new CIBlockElement;
			
			if($_REQUEST["ACTIVATE"] == "Y"){
				$act = "Y";
			}else{
				$act = "N";
			}
			
			// print_r($_REQUEST);
			// sleep(3);
			$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(),
			  "IBLOCK_ID"      => $BID,
			  "PREVIEW_TEXT"   => $_REQUEST["comment"],
			  "PREVIEW_TEXT_TYPE"   => "text",
			  // "ACTIVE" => $act,
			  );
			
			//echo "<pre>".print_r($arLoadProductArray,true)."</pre>";
			if($ID=$el->Update($arOrder["ID"],$arLoadProductArray))
			{
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "state", array("VALUE"=>$_REQUEST["state"]));
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "performer", $_REQUEST["performer"]);
				if($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0)
					CIBlockElement::SetPropertyValueCode($arOrder["ID"], "partner_sum", array("VALUE"=>floatval(str_replace(",",".",$_REQUEST["partner_sum"]))));
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "author_sum_paid", (int)$_REQUEST["author_sum_paid"]);
				if($_REQUEST["Q_ID"]>0)
					CIBlockElement::SetPropertyValueCode($_REQUEST["Q_ID"], "summ", (int)$_REQUEST["author_sum_total"]);
				else
					CIBlockElement::SetPropertyValueCode($arOrder["ID"], "author_sum_total", (int)$_REQUEST["author_sum_total"]);
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "sum_paid", (int)$_REQUEST["sum_paid"]);
				CIBlockElement::SetPropertyValueCode($arOrder["ID"], "sum_total", (int)$_REQUEST["sum_total"]);
				echo '<table width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("SAVED").'!!!</td></tr></table>
<script type="text/javascript">var reload_page = 1; location.reload();</script>';
			}
			else echo "Error: ".$el->LAST_ERROR;
		}
		else
		{
			//$ar_props = array("type","sphere","subject","lang","pages","period","pract","graf_tabl","organ","task","pref_price","discount");
                        $ar_props = array("type","sphere","subject","lang","pages","period","pract","organ","task","discount");

			//echo "<pre>".print_r($arOrder,true)."</pre>";

			if($_REQUEST["back_to_perf_id"]>0)
				$res .= '<div class="float_l ff"><input type="button" onclick="showPerformers('.$_REQUEST["back_to_work_id"].','.intval($_REQUEST["back_to_perf_id"]).',true,'.($_REQUEST["refused"]!="Y"?"false":"true").')" value="'.GetMessage("BACK_TO_LIST").'" /></div>
<div class="clear">&nbsp;</div><br />
';

				if ($arOrder["ACTIVE"] != Y){
					$notective = '<span id="noActTop" style="color:red;"> (������������)<span>';
				}
			
			$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td colspan="2"><strong>'.$arOrder["PROPERTIES"]["topic"]["VALUE"].'</strong>'.$notective.'</td>
	</tr>';

			foreach($ar_props as $pid)
			{
				$arProperty = $arOrder["PROPERTIES"][$pid];

				$name = GetMessage("IBLOCK_PROP_".$pid);
				if(strlen($name)<=0) $name = $arProperty["NAME"];

				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				elseif($pid=="discount")
				{
					if(intval($arProperty["VALUE"])<=0) continue;

					$rsD = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>8,"ID"=>$arProperty["VALUE"]),false,array("nTopCount"=>1),array("ID","DATE_ACTIVE_FROM","DATE_ACTIVE_TO","NAME","PROPERTY_discount_code"));
					if($arD=$rsD->Fetch())
					{
						$value = "[".$arD["ID"]."] ".$arD["NAME"]." (���: ".$arD["PROPERTY_DISCOUNT_CODE_VALUE"].")";
						if(
							$arD["ACTIVE"]=="N"
							|| (strlen($arD["DATE_ACTIVE_FROM"])>0 && MakeTimeStamp($arD["DATE_ACTIVE_FROM"])>time())
							|| (strlen($arD["DATE_ACTIVE_TO"])>0 && MakeTimeStamp($arD["DATE_ACTIVE_TO"])<time())
						) $value .= " (�� ��������)";
					}
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;

				$res .= '
			<tr>
				<td style="width:30% !important;">'.$name.'</td>
				<td style="width:70% !important;">'.TxtToHtml($value_text).'</td>
			</tr>';
			}
			if(!is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && $arOrder["PROPERTIES"]["file"]["VALUE"]>0)
				$arOrder["PROPERTIES"]["file"]["VALUE"] = array($arOrder["PROPERTIES"]["file"]["VALUE"]);
			//$file = CFile::GetFileArray($arOrder["PROPERTIES"]["file"]["VALUE"]);

			//echo "<pre>".print_r($arOrder["PROPERTIES"]["file"],true)."</pre>";
			//if(is_array($file) && sizeof($file))
			if(is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && sizeof($arOrder["PROPERTIES"]["file"]["VALUE"]))
			{
				$res .= '
			<tr>
				<td>'.GetMessage("IBLOCK_PROP_FILE").'</td>
				<td><a href="/zipfiles.php?oid='.$arOrder["ID"].'" class="zip">'.GetMessage("DOWNLOAD")/*.' ('.sprintf("%.1f",$file["FILE_SIZE"]/1024).' Kb)'*/.'</a></td>
			</tr>';
			}

			$res .= '
</tbody>
</table>';
			$ar_contacts_props = array("email","phone","city_region","payment");
			$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td style="width:25% !important;border-right:none;">&nbsp;</td>
		<td colspan="2" style="width:50% !important;border-left:none;border-right:none;"><strong>'.GetMessage("CONTACTS").'</strong></td>
		<td style="width:25% !important;border-left:none;">&nbsp;</td>
	</tr>
';
			$res .= '
	<tr>
		<td colspan="2" style="width:30% !important;">'.GetMessage("IBLOCK_FIO").'</td>
		<td colspan="2" style="width:70% !important;">'.$arOrder["NAME"].'</td>
	</tr>';
			foreach($ar_contacts_props as $pid)
			{
				$arProperty = $arOrder["PROPERTIES"][$pid];

				$name = GetMessage("IBLOCK_PROP_".$pid);
				if(strlen($name)<=0) $name = $arProperty["NAME"];

				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;

				$res .= '
			<tr>
				<td colspan="2" style="width:50% !important;">'.$name.'</td>
				<td colspan="2" style="width:50% !important;">'.$value_text.'</td>
			</tr>';
			}
			$res .= '
</tbody>
</table>';
			
			$db_enum_list = CIBlockProperty::GetPropertyEnum("state", Array("sort"=>"asc"), Array("IBLOCK_ID"=>$arOrder["IBLOCK_ID"]));
			while($ar_enum_list=$db_enum_list->GetNext())
			{
				//echo "<pre>".print_r($ar_enum_list,true)."</pre>";
				$states[$ar_enum_list["ID"]] = GetMessage("IBLOCK_PROP_VALUES_".$ar_enum_list["VALUE"]);
				if(strlen($states[$ar_enum_list["ID"]])<=0) $states[$ar_enum_list["ID"]] = $ar_enum_list["VALUE"];
			}
			$db_performer = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$BID,"ACTIVE"=>"Y","PROPERTY_order_id"=>$arOrder["ID"],"PROPERTY_refused"=>false),false,false,array("ID","PROPERTY_author_id","PROPERTY_summ"));
			while($ar_performer=$db_performer->GetNext())
			{
				$rsUser = CUser::GetByID($ar_performer["PROPERTY_AUTHOR_ID_VALUE"]);
				if($arUser=$rsUser->Fetch())
				{
					$performers[$arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"]] = $arUser;
					$performers[$arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"]]["sum"] = $ar_performer["PROPERTY_SUMM_VALUE"];
					$performers[$arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"]]["q_id"] = $ar_performer["ID"];
				}
			}
			ksort($performers);
			$res .= '
<div class="float_l" style="width:190px;">
	<select id="state" name="state" onchange="stateChange(this);">';
			foreach($states as $state_id => $state)
			{
				$res .= '<option'.((!$_GET["performer"] && $arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==$state_id)||($_GET["performer"]&&$state_id==23) ? ' selected="selected"' : '').' value="'.$state_id.'">'.$state.'</option>';
			}
			$res .= '
	</select>
</div>
<div class="float_l" style="width:430px;">
	<select id="performer" name="performer"'.(!in_array($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"],array(23,24))&&!$_GET["performer"]?' disabled="disabled"':'').' onchange="performerChange(this);">
		<option value="">'.GetMessage("SELECT_PERFORMER").'</option>';
			$p_sum = 0;
			$p_q_id = 0;
			foreach($performers as $perf_name => $perf)
			{
				$res .= '<option'.($arOrder["PROPERTIES"]["performer"]["VALUE"]==$perf["ID"] || $_GET["performer"]==$perf["ID"] ? ' selected="selected"' : '').' value="'.$perf["ID"].'" rel="'.$perf['sum'].';'.$perf['q_id'].'">'.$perf_name.'</option>';
				if($arOrder["PROPERTIES"]["performer"]["VALUE"]==$perf["ID"] || $_GET["performer"]==$perf["ID"])
				{
					$p_sum = $perf['sum'];
					$p_q_id = $perf['q_id'];
				}
			}
			$res .= '
		<option'.($arOrder["PROPERTIES"]["performer"]["VALUE"]==-1 ? ' selected="selected"' : '').' rel="0;-1" value="-1">'.GetMessage("SELECT_OTHER").'</option>
	</select>
	<span id="author_sum" style="padding-left:15px;'.($arOrder["PROPERTIES"]["performer"]["VALUE"]>0 || $arOrder["PROPERTIES"]["performer"]["VALUE"]<0 ? '' : 'display:none;').'">
		<input type="text" id="author_sum_paid" size="4" value="'.(int)$arOrder["PROPERTIES"]["author_sum_paid"]["VALUE"].'" />
		� <input type="text" id="author_sum_total" rel="'.$p_q_id.'" size="4" value="'.($arOrder["PROPERTIES"]["author_sum_total"]["VALUE"]>0 ? (int)$arOrder["PROPERTIES"]["author_sum_total"]["VALUE"] : (int)$p_sum).'" /> ���.
	</span>
</div>
<div class="clear">&nbsp;</div>
<br />
<div class="float_r ff">
	'.($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0 ? '
	<div id="partner_sum_block"'.(in_array($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"], array(23, 24)) ? '' : ' style="display:none;"').'>
		<span id="partner_sum_title">'.($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==23 ? '���������� �����' : '����� �� ������').'</span>
		<input type="text" id="partner_sum" name="partner_sum" value="'.($arOrder["PROPERTIES"]["partner_sum"]["VALUE"]>0 ? $arOrder["PROPERTIES"]["partner_sum"]["VALUE"] : $arOrder["PROPERTIES"]["pref_price"]["VALUE"]*$arPartner["PERSONAL_NOTES"]/100).'" size="5" /> ���.
	</div>' : '').'
</div>
<div class="float_l ff">
	��� ������� <input type="text" id="sum_paid" size="5" value="'.(int)$arOrder["PROPERTIES"]["sum_paid"]["VALUE"].'" />
	� <input type="text" id="sum_total" size="5" value="'.(int)$arOrder["PROPERTIES"]["sum_total"]["VALUE"].'" /> ���.
</div>
<div class="clear">&nbsp;</div>
';

			$res .= '
<br >'.GetMessage('COMMENT').'<br />
<textarea id="comment" name="comment" rows="5" style="width:99%;" cols="55">'.$arOrder["~PREVIEW_TEXT"].'</textarea>
<div class="clear">&nbsp;</div><br />';

// if($arOrder["ACTIVE"] == "Y"){
	// $chek = 'checked="yes"';
// }

// $res .= '
// <br ><br >
// <input id="ACTIVATE" type="checkbox" name="ACTIVATE" value="Y" '.$chek.'  >����������<br>
// <div class="clear">&nbsp;</div><br />
// ';




$res .='<input type="button" onclick="validateStateChange('.$order_id.');" value="'.GetMessage("SAVE").'" />
<div class="clear">&nbsp;</div><br />';

			
			$res .= '
<script type="text/javascript">
	var pf_sum = [];
	'.$res_pf_js.'
	
	function setPartnerSum(caller) {
		var user_id = $(caller).val();
alert(user_id);
alert(pf_sum[user_id]);
		if(!isNaN(user_id) && user_id>0) {
			$(\'#partner_sum_block\').val(pf_sum[user_id]);
		}
	}

	function stateChange(caller) {
		$(\'#performer\').attr(\'disabled\',($(caller).val()!=23&&$(caller).val()!=24?\'disabled\':\'\'));
		if($(caller).val()==23) {
			'.($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0 ? '$(\'#partner_sum_block\').css(\'display\', \'\');
			$(\'#partner_sum_block #partner_sum_title\').text(\'���������� �����\');' : '').'
		} else if($(caller).val()==24) {
			'.($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0 ? '$(\'#partner_sum_block\').css(\'display\', \'\');
			$(\'#partner_sum_block #partner_sum_title\').text(\'����� �� ������\');' : '').'
		} else {
			'.($arOrder["PROPERTIES"]["partner_id"]["VALUE"]>0 ? '$(\'#partner_sum_block\').css(\'display\', \'none\');' : '').'
		}
	}
	
	function performerChange(caller) {
		if($(caller).val()!=\'\') {
			$(\'#author_sum\').css(\'display\', \'\');
			var arr = $(\'option:selected\',caller).attr(\'rel\').split(\';\');
			$(\'#author_sum_total\').attr(\'rel\',arr[1]).val(arr[0]);
		} else {
			$(\'#author_sum\').css(\'display\', \'none\');
		}
	}
	
	function validateStateChange(order_id)
	{
		if($(\'#state\').val()==23||$(\'#state\').val()==24)
		{
			if($(\'#performer\').val()==\'\'&&$(\'#state\').val()==23)
			{
				alert(\''.GetMessage("DIDNT_SELECT_PERFORMER").'\');
			}
			else
			{
				changeState(order_id,$(\'#comment\').val(),$(\'#state\').val(),$(\'#performer\').val(),$(\'#partner_sum\').val(),$(\'#author_sum_paid\').val(),$(\'#author_sum_total\').val(),$(\'#sum_paid\').val(),$(\'#sum_total\').val(),$(\'#author_sum_total\').attr(\'rel\'));
			}
		}
		else
		{
			changeState(order_id,$(\'#comment\').val(),$(\'#state\').val(),\'\',\'\',$(\'#author_sum_paid\').val(),$(\'#author_sum_total\').val(),$(\'#sum_paid\').val(),$(\'#sum_total\').val(),$(\'#author_sum_total\').attr(\'rel\'));
		}

	}
</script>
';
			
			echo $res;
			
			if ($arOrder["ACTIVE"] != Y){
				echo '<div id="activate_blok">
				<hr/>
				<br/><p style="color:red;"><b>���������� ���������</b></p>
				<input type="button" onclick="Activate('.$order_id.');" value="���������� ����������" />
				</div>
				';
			}
			
		}
	}
}
?>
<script>
	function Activate(order)
	{
		$.post("/bitrix/templates/.default/components/bitrix/news.list/quotes_admin/order_activate.php", { order_id: <?=$order_id;?>},
		function(data) {
			if(data == "Updated"){
			   $( "#activate_blok" ).empty();
			   $( "#noActTop" ).empty();
			   
				alert("���������� ������");
			}else{
				alert("��� :(, ������� �������. ��������� ������.");
			}
		});
	}
</script>