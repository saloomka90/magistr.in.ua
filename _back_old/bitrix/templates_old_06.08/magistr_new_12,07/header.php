<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(empty($_REQUEST["edit"])):?>
<!DOCTYPE HTML>
<html>
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="windows-1251">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />

	<link rel="stylesheet" href="<?=$APPLICATION->GetTemplatePath("css/style.css");?>" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/popup.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/checkbox/checkbox.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/jquery-filestyle.css");?>" />
	
	<link rel="stylesheet" href="<?=$APPLICATION->GetTemplatePath("css/datepicker/jquery-ui.css");?>">
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-1.7.1.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/cufon-yui.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/tahoma_cufon.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox-0.2.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/popup.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/checkbox/jquery.checkbox.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-filestyle.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.validate.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-ui.js");?>"></script>
	
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.form-2.25.js");?>"></script>
	<link href="<?=$APPLICATION->GetTemplatePath("css/nyroModal.css");?>" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.nyroModal-1.6.2.pack.js");?>"></script>


    <?$APPLICATION->ShowHead()?>
</head>

<body>

	<?$APPLICATION->ShowPanel();?>
	<div class="bg">
		<div class="overlay" id="overlay_order" style="display:none;"></div>
		<div class="header-bg">
			<div class="header">
				<div class="logo">
					<a href="#"><img src="<?=$APPLICATION->GetTemplatePath('images/logo.png');?>" /></a>
				</div>
				<div class="header-r">
					<? if($USER->IsAuthorized()):?>
					<div class="header_top">
						<div class="name"><?= $USER->GetFullName()?></div>
						<div class="panel_link">
							<a href="/authors/personal_data"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_account.png');?>" /> <span>����i��</span></a> |
							<a href="/?logout=yes"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_exit.png');?>" /> <span>�����</span></a>
						</div>
					</div>
					<div class="clr"></div>

					<?php
						CModule::IncludeModule('iblock');
						$arFilter = array(
							'IBLOCK_ID' => 4,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_works = $el['CNT'];
						}

						$arFilter = array(
							"IBLOCK_ID" => 5,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_quotes = $el['CNT'];
						}

                        $arFilter = array(
                            'IBLOCK_ID' => 6,//4
                            "PROPERTY" => array(
                                "performer"=> array(
                                    "VALUE" => $USER->GetID()
                                    ),
                                "state" => array(
                                	"VALUE_ENUM_ID" =>23
                                	)
                                )
                            // 'CREATED_USER_ID'=>$USER->GetID(),
                        );
                        $res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
                        if ($el = $res->Fetch()) {
                            $in_progress = $el['CNT'];
                        }

						
                        $url_segments = explode('/',$APPLICATION->GetCurPage());
                        $active_item = $url_segments[1];
					?>
					<!-- ��� ����! -->
					<div class="menu">
					<ul>
					
							
							<li class="parent <?if($active_item=="quotes_admin" or $active_item=="notifications")echo 'active';?>">
								<a href="/quotes_admin/">���i<br> ������</a>
								<ul>
									<?/* ?><li <?if($active_item=="stolrabot")echo 'active';?>><a href="#">������� ��i�</a></li><? */?>
									<li <?if($active_item=="quotes_admin")echo 'active';?>><a href="/quotes_admin/">������ ���i�</a></li>
									<li class="<?if($active_item=="notifications")echo 'active';?>"><a href="/notifications/">�����������</a></li>
								</ul>
							</li>
							<li class="parent <?if($active_item=="quotes_admin_ready")echo 'active';?>">
								<a href="/quotes_admin_ready/">�����i<br> ������</a>
								<ul>
									<li class="<?if($active_item=="quotes_admin_ready")echo 'active';?>"><a href="/quotes_admin_ready/">������ �� ����� ������</a></li>
									
								</ul> 
							</li>
							<li class="parent <?if($active_item=="topay" or $active_item=="payments" or $active_item=="income")echo 'active';?>">
								<a href="/topay/">�i�����</a>
								<ul>
									<li class="<?if($active_item=="topay")echo 'active';?>"><a href="/topay/">�� ������</a></li>
									<li class="<?if($active_item=="payments")echo 'active';?>"><a href="/payments/">���i� ������</a></li>
									<li class="<?if($active_item=="income")echo 'active';?>"><a href="/income/">���i� ����������</a></li>
								</ul>
							</li>
							<li class="parent  <?if($active_item=="authors_admin")echo 'active';?>">
								<a href="/authors_admin/">������</a>
								<ul>									
									<li <?if($active_item=="authors_admin")echo 'class="active"';?>><a href="/authors_admin/">������� �����i�</a></li>
								</ul>
							</li>
							<li class="parent <?if($active_item=="agents_admin" or $active_item=="quotes_agents_admin")echo 'active';?>">
								<a href="/agents_admin/">������</a>
								<ul>
									<li <?if($active_item=="agents_admin")echo 'class="active"';?>><a href="/agents_admin/">������ �����i�</a></li>
									<li <?if($active_item=="quotes_agents_admin")echo 'class="active"';?>><a href="/quotes_agents_admin/">������ ������</a></li>
								</ul>							
							</li>
							
							<li class="parent <?if($active_item=="partners_admin" or $active_item=="quotes_partners_admin")echo 'active';?>">
								<a href="/partners_admin/">��������</a>
								<ul>
									<li <?if($active_item=="partners_admin")echo 'class="active"';?>><a href="/partners_admin/">������ ��������</a></li>
									<li <?if($active_item=="quotes_partners_admin")echo 'class="active"';?>><a href="/quotes_partners_admin/">������ ��������</a></li>
								</ul>
							</li>
							<li <?if($active_item=="reviews")echo 'class="active"';?>><a href="/reviews/">�i�����</a></li>
						</ul>
						
					</div>
					<div class="clr"></div>
					<? endif;?>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<div class="wrap-bg">
			<div class="wrap">
			<h1>
				<?$APPLICATION->ShowTitle()?>
				<?if($a):?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="profile-edit">��i���� ������</a>
				<?endif;?>
			</h1>
<?endif;?>
