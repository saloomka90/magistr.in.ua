$(function(){

	Cufon.replace("");

	var flag = false;
	$("#mig").css("opacity", "1");
	setTimeout(function () {
		$("#mig").css("opacity", "1");
		setInterval(function () {
			$("#mig").css("opacity", flag? "1":"0.5");
			flag = !flag;
		}, 500)
	}, 3000);

	
	var currentYear = (new Date).getFullYear();
	$('.copy span').text(currentYear);		
	$(":file").jfilestyle({icon:false, 'buttonText': '������� ����'});
	var select = $('.niceSelect');
	$(select).selectbox({
		onOpen: function (select) {
			$('.sbHolder').addClass('focus');
			$('.sbGroup').parent().addClass('sbGroup');
		},
		onClose: function (inst) {
			$('.sbHolder').removeClass('focus');
		},
		onChange: function (val, inst) {},
		effect: "slide"				
	});
	
	$('.cont-bt > a').toggle(function(){
		$(this).parent().find('.cont-window').show();
		$(this).find('.carr').attr('src', '/bitrix/templates/authors/images/arr-t.png');
		$('body').click(function(){
			$(this).parent().find('.cont-window').hide();
			$(this).find('.carr').attr('src', '/bitrix/templates/authors/images/arr.png');
		});
	}, function() {
		$(this).parent().find('.cont-window').hide();
		$(this).find('.carr').attr('src', '/bitrix/templates/authors/images/arr.png');
	});
	
	$('.header .menu > ul > li.parent').hover(
		function(){
			$('.header .menu > ul > li > ul').hide();
			$(this).addClass('hovered');
			$(this).find('ul').show();
			$(this).find('ul').css('z-index', '10');
		},
		function(){
			$('.header .menu > ul > li > ul').hide();
			$('.header .menu > ul > li.active > ul').show();
			$(this).removeClass('hovered');
			$(this).find('ul').css('z-index', '1');
		}
	);

	$('.send a').click(function(){
		var html = $('.send_box_hide').html();
		$(this).after(html);
		
		return false;
	});




// end
});



function GetByID(id)
{
	return document.getElementById(id);
}

function activateTabE(id, num, t_id)
{
	var tab_c = GetByID(id);
	var tab_t_p = GetByID(t_id);
	if(tab_c && tab_t_p)
	{
		var tabs = tab_c.getElementsByTagName('DIV');
		if(!num) num = 0;
		var cnt = 0;
		for(i in tabs)
		{
			var cls = tabs[i].className;
			if(cls && (cls=="tab" || cls.indexOf("tab ")>-1 || cls.indexOf(" tab")>-1))
			{
				if(cnt==num) tabs[i].style.display = "block";
				else tabs[i].style.display = "none";
				cnt++;
			}
		}
		var tabs_t = tab_t_p.getElementsByTagName('DIV');
		var cnt = 0;
		for(j=0;j<tabs_t.length;j++)
		{
			var cls = tabs_t[j].className;
			if(cls && (cls=="tab" || cls.indexOf("tab ")>-1 || cls.indexOf(" tab")>-1))
			{
				if(cnt==num) tabs_t[j].className += " selected";
				else tabs_t[j].className = tabs_t[j].className.replace(" selected", "");
				cnt++;
			}
		}
	}
}

function setContentHeight()
{
	var id = "content";
	var cont = GetByID(id);
	var bd = document.body;
	if(cont)
	{
		cont.style.height = "auto";
		var min_h = bd.clientHeight - 149 - 156 - 42;
		if(min_h<120) min_h = 120;
		if(cont.clientHeight<min_h) cont.style.height = min_h+"px";
	}
}

function setFocusField(caller, pref)
{
//alert(names);
	if(pref) var names_new = window['names_'+pref];
	else if(typeof(names)!=='undefined') var names_new = names;
	if(names_new[(caller.id)]!=undefined)
	{
		if(caller.value==names_new[(caller.id)])
		{
			caller.value = '';
			caller.style.color = '#000000';
		}
	}
}

function unsetFocusField(caller, pref)
{
	if(pref) var names_new = window['names_'+pref];
	else if(typeof(names)!=='undefined') var names_new = names;
	if(names_new[(caller.id)]!=undefined)
	{
		if(caller.value=='')
		{
			caller.style.color = '';
			caller.value = names_new[(caller.id)];
		}
		else caller.style.color = '#000000';
	}
}

function checkFieldFilling(pref)
{
	if(pref) var names_new = window['names_'+pref];
	else if(typeof(names)!=='undefined') var names_new = names;

	if(names_new)
	{
		for(i in names_new)
		{
			var f = GetByID(i);
			if(f)
			{
				if(f.value!=names_new[i]) f.style.color = '#000000';
				else f.style.color = '';
			}
		}
	}
}

function sw(id, tp)
{
	var node = GetByID(id);
	if(node)
	{
		if(tp!=undefined) node.style.display = tp ? "block" : "none";
		else node.style.display = node.style.display=="none" ? "block" : "none";
	}
}

function submForm(pref)
{
	if(pref) var names_new = window['names_'+pref];
	else if(typeof(names)!=='undefined') var names_new = names;

	for(i in names_new)
	{
		var field = GetByID(i);
		if(field)
		{
			if(field.value==names_new[i])
			{
				field.value = '';
			}
		}
	}
	return true;
}

function selectAll(pid, toggle)
{
	$("#"+pid+" input[type='checkbox']").each(function(){$(this).attr("checked",toggle);});
	return true;
}

function checkSelection(pid, cid)
{
	var checked = "checked";
	$("#"+pid+" input[type='checkbox']").not("#"+cid).each(function() {
		if($(this).attr("checked")==false) {
			$("#"+cid).attr("checked", false);
			checked = false;
			return false;
		}
	});
	$("#"+cid).attr("checked", checked);
	return true;
}

function submitReviews(caller, inc_file, pref) {

			
	var options = {
		url: /*caller.action+(caller.action.indexOf('?')<0?'?':'&')+'inc_file='+*/inc_file,
		beforeSubmit: function(formData, jqForm, options) {
                        for (i in formData) {
				formData[i].value = /*encodeURIComponent*/escape(formData[i].value);
			}

			return true;
		},
		success: function(response) {
			$(caller).before(response);
			$(caller).remove();
		}
	};

	submForm(pref);
	$(caller).css("position","relative");
	$(caller).append('<div style="height:43px;width:43x;background:#ffffff;border:1px solid #cccccc;position:absolute;top:50%;left:50%;margin:-35px 0px 0px -35px;padding:14px 13px 13px 14px;"><img src="/images/ajaxLoader.gif" width="43" height="43" alt="" /></div>');
	$(caller).ajaxSubmit(options);
	return false;
}

$(document).ready(function(){
	checkFieldFilling();
	checkFieldFilling('n');
	checkFieldFilling('p');
	checkFieldFilling('reg');
	//setContentHeight();

	jQuery("input.datepicker").each(function(){
		jQuery(this).datepicker({
			changeMonth: true,
			changeYear: true,
			//yearRange: '1990:'+td.getFullYear(),
			//minDate: new Date(1990, 1 - 1, 1),
			minDate: new Date()
		});
	});
});

$(document).resize(function(){
	//setContentHeight();
});

function setFormOptions() {
	jQuery("#nyroModalContent form").each(function(){
		var options = {
			target: "#nyroModalContent",
			url: $(this).attr("action")+"&ajax=1",
			beforeSubmit: function(formData, jqForm, options) {

				for (i in formData) {
					formData[i].value = escape(formData[i].value);
				}
				return true;
			},
			success: function(data) {
				setFormOptions();
			}
		};
		jQuery(this).ajaxForm(options);
	});
}