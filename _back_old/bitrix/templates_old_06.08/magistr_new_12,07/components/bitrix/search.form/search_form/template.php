<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
?>
<div class="search-form">
<form action="<?=$arResult["FORM_ACTION"]?>">
	<table style="border: 0px; text-align: center; border-spacing:2; border-collapse:collapse;">
		<tr>
			<td style="text-align: center;"><input type="text" name="q" value="" size="15" maxlength="50" /></td>
			<td style="text-align: right;"><input name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" /></td>
		</tr>
	</table>
</form>
</div>