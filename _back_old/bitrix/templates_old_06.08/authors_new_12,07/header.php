<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE HTML>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title><?$APPLICATION->ShowTitle()?></title>
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/css.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/magistr.js"></script>
    <?$APPLICATION->ShowHead()?>
</head>
<body>
	<?$APPLICATION->ShowPanel();?>
    <!--# wrapper #-->
    <div class="wrapper">
        <!--# topbar #-->
        <div class="topbar">
            <div class="logoWrapper">
                <a href="/" class="logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" />
                </a>
                <span id="hamburger">
                    <span></span><span></span><span></span>
                </span>
            </div>
            
            <span id="hamburger2">
                <span></span>
                <span></span>
                <span></span>
            </span>
            
            <div class="topmenu" id="topmenu">
            <?php
						CModule::IncludeModule('iblock');
						$arFilter = array(
							'IBLOCK_ID' => 4,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_works = $el['CNT'];
						}

						$arFilter = array(
							"IBLOCK_ID" => 5,
							'CREATED_USER_ID'=>$USER->GetID(),
						);
						$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
						if ($el = $res->Fetch()) {
							$count_quotes = $el['CNT'];
						}

                        $arFilter = array(
                            'IBLOCK_ID' => 6,//4
                            "PROPERTY" => array(
                                "performer"=> array(
                                    "VALUE" => $USER->GetID()
                                    ),
                                "state" => array(
                                	"VALUE_ENUM_ID" =>23
                                	)
                                )
                            // 'CREATED_USER_ID'=>$USER->GetID(),
                        );
                        $res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
                        if ($el = $res->Fetch()) {
                            $in_progress = $el['CNT'];
                        }

						$url_segments = explode('/',$APPLICATION->GetCurPage());
						$active_item = $url_segments[2];
					?>
               <?/* ?> <a href="#" class="mgIcon_remind mg_small">����������� <span class="counter">25</span></a>
                <a href="/authors/my_works/" class="mgIcon_activeTask mg_small">������� �������� <span class="counter"><?=intval($count_works)?></span></a><? */?>
                <a href="/authors/personal_data/" class="mgIcon_profile mg_small">̳� �������</a>
                <a href="/?logout=yes" class="mgIcon_exit mg_small">�����</a>
            </div>
        </div>
        <!--# end topbar #-->

        <!--# sidebar #-->
        <div class="sidebar" id="sidebar">
        <div>
            <!--# menu group #-->
            <div class="menuGroup">
                <h6>������ ������� ����</h6>
                <div>
                    <a href="/authors/my_works/" class="mgIcon_myWorks mg_normal <?= ($active_item == 'my_works' )?'active':''?>">�� ������</a>
                    <a href="/authors/add_work/" class="mgIcon_addWork mg_normal <?= ($active_item == 'add_work' )?'active':''?>">������ ������</a>
                    <a href="/authors/my_quotes/" class="mgIcon_requests mg_normal <?= ($active_item == 'my_quotes' )?'active':''?>">������ �� �볺���</a>
                </div>
            </div>
            <!--# end menu group #-->
            
            <!--# menu group #-->
            <?		

$arGroups = CUser::GetUserGroup(CUser::GetId());

if (sizeof(array_intersect(array(1,5),$arGroups))){
?>
            <div class="menuGroup">
                <h6>��������� ����� ����</h6>
                <div>
                    <a href="/authors/my_quotes_new/" class="mgIcon_requestList mg_normal <?= ($active_item == 'my_quotes_new' )?'active':''?>">������ ���������</a>
                    <a href="/authors/my_quotes_work/" class="mgIcon_pinned mg_normal <?= ($active_item == 'my_quotes_work' )?'active':''?>">��������� �� ����</a>
                    <a href="/authors/my_quotes_finish/" class="mgIcon_done mg_normal <?= ($active_item == 'my_quotes_finish' )?'active':''?>">�������� ����</a>
                </div>
            </div>
<? }else{?>
			<div class="menuGroup">
				 <h6>��������� ����� ����</h6>
			<div>
                    <a href="/authors/my_quotes_new/" class="mgIcon_requestList mg_normal" <?= ($active_item == 'my_quotes_new' )?'active':''?>>����� �������</a>
            </div>
			</div>
<? }?>
            <!--# end menu group #-->
        </div>
        </div>
        <!--# end sidebar #-->
                       
        <!--# content wrapper #-->
        <div class="contentWrapper">
            <div class="contentInner">                
                <!--# content #-->
                
                
            
        
       