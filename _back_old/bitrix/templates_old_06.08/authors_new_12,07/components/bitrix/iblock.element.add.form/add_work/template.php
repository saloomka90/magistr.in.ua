<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
?><?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
$pref = time();
?>


<!--# content #-->
                <div class="content">
                    <!--# content header #-->
                    <div class="contentHeader">
                        <h1>������ ���� ������</h1>
                    </div>
                    <!--# end content header #-->
                    <?if (count($arResult["ERRORS"])):?>
		<div class="alert a_big"><?=ShowError(implode("<br />", $arResult["ERRORS"]))?></div>
	<?endif?>
	<?if (strlen($arResult["MESSAGE"]) > 0):?>
		<div class="notetext"><?=ShowNote(str_replace("#ID#",$_SESSION["LAST_ORDER_ID"],$arResult["MESSAGE"]))?></div>
		<script type="text/javascript">
			window.location.href = "/authors/my_works/"
		</script>
	<?endif?>
                    <div class="contentDiv my_profile add_work">
                    <div class="formDiv">
                        <form id="formAddWork" name="addwork_form" action="<?= POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
                        <?=bitrix_sessid_post()?>
                            <!--# form row #-->
                            <div class="formRow field_workName focusOn">
                                <div class="fieldLabel">����� ������</div>
                                <div class="formField">
                                    <input type="text" name="PROPERTY[NAME][0]" class="control-it" />
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workCategory">
                                <div class="fieldLabel">�������� ������</div>
                                <div class="formField">
                                    <select class="form-control control-it select-control" name="PROPERTY[IBLOCK_SECTION]">
                                        <option selected="" value="">������� � ������</option>
                                          <? foreach ($arResult["PROPERTY_LIST_FULL"]['IBLOCK_SECTION']["ENUM"] as $key=>$val):?>      
                                         	 <option value="<?=$key?>"><?=$val["VALUE"]?></option>
                                          <? endforeach;?>
                                    </select>
                                </div>
                                <div class="fieldRemark_float">
                                    <p>���� ��������� �������� �������, �������� ����� ��</p>
                                   <b> magistr.in.ua@gmail.com</b>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workType">
                                <div class="fieldLabel">��� ������</div>
                                <div class="formField">
                                    <select name="PROPERTY[7]" class="control-it select-control">
                                        <option value="">������� � ������</option>
                                         <? foreach ($arResult["PROPERTY_LIST_FULL"]['7']["ENUM"] as $key=>$val):?>      
                                         	 <option value="<?=$key?>"><?=$val["VALUE"]?></option>
                                          <? endforeach;?>
                                    </select>
                                </div>
                                <!--
                                <div class="workAddInfo">
                                    <div>
                                        <div class="fieldLabel">г� ���������</div>
                                        <div class="formField">
                                            <input type="text" class="" size="4" maxlength="4" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="fieldLabel">ʳ������ �������</div>
                                        <div class="formField">
                                            <input type="text" />
                                        </div>                                        
                                    </div>
                                </div>-->
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow noBg">
                                <div class="formField workAddInfo">
                                    <div>
                                        <div class="fieldLabel">г� ���������</div>
                                        <div class="formField">
                                            <input type="text" size="4" maxlength="4" placeholder="XXXX" name="PROPERTY[8][0]" class="control-it digit4" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="fieldLabel">ʳ������ �������</div>
                                        <div class="formField">
                                            <input type="text" name="PROPERTY[9][0]" class="control-it"/>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workContent">
                                <div class="fieldLabel">����</div>
                                <div class="formField">
                                    <textarea name="PROPERTY[10][0]" placeholder="�������� ��� ���� (����) �� ���� ������" class="control-it"></textarea>
                                </div>
                            </div>
                            <!--# end form row #-->
                            <!--# form row #-->
                            <div class="formRow field_workPrice">
                                <div class="fieldLabel">ֳ��, ���</div>
                                <div class="formField">
                                    <input type="text" name="PROPERTY[11][0]" class="control-it" />
                                </div>
                                <div class="toggleBlocks">
                                    <div>
                                        <span>������ �������� <span>(���� ����� ���� ���)</span></span>
                                        <div>
                                            <textarea placeholder="..." name="PROPERTY[35][0]"></textarea>
                                        </div>
                                        <p>���������, ���� � ��� � ������ ������� ���� � ���� ������������� - ������ ��� ������� ����� ������</p>
                                    </div>
                                    <div>
                                        <span>��������� ������� ������ ��� ������������</span>
                                        <div>
                                            <textarea placeholder="..." name="PROPERTY[64][0]"></textarea>
                                        </div>
                                        <p>��� ������ ������ ��������� ����-��� ������� ������. �� ������ ������ ���������� ������� ���� ���� ������. <br />��������� ���� �����. ������� �� ������� ������������ �� ������.</p>
                                    </div>
                                </div>
                            </div>
                            <!--# end form row #-->
                            
                            <div class="submitForm">
                                <input type="submit" name="iblock_submit" value="��������" class="btn2 bg-orange" />
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                <!--# end content #--> 
                
                
   <script type="text/javascript">
$(document).ready(function() {
    /*
    $('#formAddWork').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            workName: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workCategory: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            workType: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workYear: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workPages: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workContent: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            workPrice: {
                group: '.formField',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            }
        }
    });*/

    var validIt = function(form, target){
        var form = $(form);
        var errors = 0;
        var phonePat = /^\+[0-9]{12}$/;
        var messages = {
            empty: "��������� �� ����",
            phone: "������� ������ ��������",
            email: "������� ������ email",
            atLeast: "������ ����� ����",
            wrong: "������"
        };
        form.find(".control-it").each(function(){
            var parent = $(this).parents(".formField").eq(0);
            parent.removeClass("has-error");
            if($(this).hasClass("phone-control")){
                if($(this).val().length == 0){
                    errors +=1;
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                }
                else{
                    if(!phonePat.test($(this).val())){
                        errors +=1;
                        if(!parent.hasClass("has-error")){
                            if(parent.find(".error_sub").length == 0){
                                parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.phone+"</div>");
                            }
                        }
                    }
                }
            }
            if($(this).hasClass("email-control")){
                if($(this).val().length == 0){
                    errors +=1;
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                }
                else{
                    var emailPat = /^.{3,}\@.{1,}\..*$/
                    if(!emailPat.test($(this).val())){
                        errors +=1;
                        if(!parent.hasClass("has-error")){
                            if(parent.find(".error_sub").length == 0){
                                parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.email+"</div>");
                            }
                        }
                    }
                }
            }
            if($(this).hasClass("digit4")){
                if($(this).val().length != 4 || !/^[0-9]{4}$/.test($(this).val())){
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.wrong+"</div>");
                        }
                    }
                    errors += 1;
                    
                }
            }
            if($(this).attr("type") == "checkbox"){
                var fieldName = $(this).attr("name");
                if($("input[type=checkbox][name='"+fieldName+"']:checked").length < 1){
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.atLeast+"</div>");
                        }
                    }
                    errors += 1;
                }
            }
            else{
                if($(this).val() !== ""){
                    
                } else{
                    if(!parent.hasClass("has-error")){
                        if(parent.find(".error_sub").length == 0){
                            parent.addClass("has-error").append("<div class=\"error_sub\">"+messages.empty+"</div>");
                        }
                    }
                    errors += 1;
                }
            }
        });
        return errors == 0 ? true : false;
    };

    $('#formAddWork').submit(function(e) {
        if(!validIt('#formAddWork')){
            e.preventDefault();
            console.log("error");
            
        } else{
        	$('input[name="card"]').each(function(){
				procur+=$(this).val();
				
			});

			
			$('input[name="UF_PRIVATCARD"]').val(procur);
        }
    });
});

</script>             
       
                