<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<? 
$order_id = $arResult["ID"];
//echo "<pre>".print_r($_GET,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,5),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 7;
	
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
		$arOrder = $obOrder->GetFields();
		$arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21 && (strlen($_REQUEST["summ"])>0 || $_REQUEST["refuse"]=="Y"))
		{
			$rs_resp = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$BID,"ACTIVE"=>"Y","PROPERTY_author_id"=>$USER->GetID(),"PROPERTY_order_id"=>$order_id),false,array("nTopCount"=>1),array("ID","PREVIEW_TEXT","PROPERTY_summ","PROPERTY_order_id","PROPERTY_author_id","PROPERTY_refused"));
			$ar_resp = $rs_resp->GetNext();
			//echo "<pre>".print_r($ar_resp,true)."</pre>";
	
			$el = new CIBlockElement;
			$PROP = array();
			$PROP["author_id"] = $USER->GetID();
			$PROP["order_id"] = $order_id;
			
			if(strlen($_REQUEST["summ"])>0) $PROP["summ"] = $_REQUEST["summ"];
			else
			{
				$db_enum_list = CIBlockProperty::GetPropertyEnum("refused", Array(), Array("IBLOCK_ID"=>$BID));
				if($ar_enum_list = $db_enum_list->GetNext())
				{
					//echo "<pre>".print_r($ar_enum_list,true)."</pre>";
					$PROP["refused"] = $ar_enum_list["ID"];
				}
			}


			
			$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(),
			  "IBLOCK_SECTION_ID" => false,
			  "IBLOCK_ID"      => $BID,
			  "PREVIEW_TEXT"=> iconv("utf-8", "windows-1251", $_REQUEST["comment"]),
			  "PREVIEW_TEXT_TYPE"=> "text",
			  "PROPERTY_VALUES"=> $PROP,
			  "NAME"           => "������ � ������ �".$order_id,
			  "ACTIVE"         => "Y",
			  );
			//echo "<pre>".print_r($arLoadProductArray,true)."</pre>";
			if($ar_resp)
			{
				if($ID=$el->Update($ar_resp["ID"],$arLoadProductArray)){
					echo '<table width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("RESPONSE_ACCEPTED").'</td></tr></table><script type="text/javascript">var reload_page = 1;</script>';
				}else {
					echo "Error: ".$el->LAST_ERROR;
				}
			}
			else
			{
				
				if($ID=$el->Add($arLoadProductArray)){
					echo '<table width="100%"><tr><td align="center" valign="middle" height="300">'.GetMessage("RESPONSE_ACCEPTED").'</td></tr></table><script type="text/javascript">var reload_page = 1;</script>';
				}else{
					echo "Error: ".$el->LAST_ERROR;
				}
			}
		}
		else
		{
			$ar_props = array("type","subject","lang","pages","period","pract","organ","city_region","task");
			$APPLICATION->SetTitle($arOrder["PROPERTIES"]['type']["VALUE"]." #".$arResult["ID"]);
/*
			//echo "<pre>".print_r($arOrder,true)."</pre>";
			$res = '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td colspan="2"><strong>'.$arOrder["PROPERTIES"]["topic"]["VALUE"].'</strong></td>
	</tr>';

			foreach($ar_props as $pid)
			{
				echo $pid.'<br>';
				$arProperty = $arOrder["PROPERTIES"][$pid];

				$name = GetMessage("IBLOCK_PROP_".$pid);
				if(strlen($name)<=0) $name = $arProperty["NAME"];

				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;

				$res .= '
			<tr>
				<td style="width:30% !important;">'.$name.'</td>
				<td style="width:70% !important;">'.TxtToHtml($value_text).'</td>
			</tr>';
			}
			if(!is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && $arOrder["PROPERTIES"]["file"]["VALUE"]>0)
				$arOrder["PROPERTIES"]["file"]["VALUE"] = array($arOrder["PROPERTIES"]["file"]["VALUE"]);
			//$file = CFile::GetFileArray($arOrder["PROPERTIES"]["file"]["VALUE"]);

			//echo "<pre>".print_r($arOrder["PROPERTIES"]["file"],true)."</pre>";
			//if(is_array($file) && sizeof($file))
			if(is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && sizeof($arOrder["PROPERTIES"]["file"]["VALUE"]))
			{
				$res .= '
			<tr>
				<td>'.GetMessage("IBLOCK_PROP_FILE").'</td>
				<td><a href="/zipfiles.php?oid='.$arOrder["ID"].'" class="zip">'.GetMessage("DOWNLOAD")/*.' ('.sprintf("%.1f",$file["FILE_SIZE"]/1024).' Kb)'.'</a></td>
			</tr>';
			}
			$res .= '
</tbody>
</table>';
			if($arOrder["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21)
				$res .= '
<div><b>'.GetMessage("SUMM_DESCR").'</b></div><br />
<div class="float_r ff"><input type="button" onclick="refuse_order('.$order_id.',$(\'#comment\').val());" value="'.GetMessage("REFUSE").'" /></div>
<form id="sendReq" method="post" action="/bitrix/templates/.default/components/bitrix/news.list/my_quotes_new/order_detail.php" class="nyroModal">
<div class="float_l ff">
	<input name="summ" type="text" id="accept_summ" onkeypress="onlyDigits();" onkeyup="onlyDigits();" style="width:100px;" class="inputtext" value="'.$ar_resp["PROPERTY_SUMM_VALUE"].'" /> ���.
	<input type="button" onclick="checkForm();" value="'.GetMessage("ENTER_SUMM").'" />
</div>
<div class="clear">&nbsp;</div><br />
<b>'.GetMessage("COMMENT").'</b><br />
<textarea id="comment" name="comment" rows="5" style="width:99%;" cols="55">'.$ar_resp["~PREVIEW_TEXT"].'</textarea>
<input type="hidden" name="o_id" value="'.$order_id.'">
<input type="hidden" name="lang" value="'.$_REQUEST["lang"].'">


</form>
<script>
 function checkForm(){
 	if($("#accept_summ").val().length>0){ 
 		$("#sendReq").submit(); 
 	}else{
 		 alert("'.GetMessage("DIDNT_ENTER_SUMM").'");
 	}
  return false;
 }
</script>
<br />
';
			else
				$res .= '
<div><b>'.GetMessage("SUMM_DESCR").'</b></div><br />
<div>'.$ar_resp["PROPERTY_SUMM_VALUE"].' ���.</div>
<div class="clear">&nbsp;</div><br />
<b>'.GetMessage("COMMENT").'</b><br />
<div>'.TxtToHtml($ar_resp["~PREVIEW_TEXT"]).'</div>
<br/>
';

			echo $res;
			*/
			
			?>
			<div class="content">
                    <!--# content header #-->
                    <div class="contentHeader">
                        <h1><?=$arOrder["PROPERTIES"]["topic"]["VALUE"]?> <span class="work_id">ID <?=$order_id?></span></h1>
                    </div>
                    <!--# end content header #-->
                    
                    <div class="contentDiv">
                    <div>
                        <?/* ?><a href="#taskList" class="workStatus pendingTasks"><span>���������</span> <br />������� �볺���</a><? */?>
                         
                        <div class="workInfo">
                            <div class="mgIcon_workType mg_normal"><?=$arOrder["PROPERTIES"]['type']["VALUE"]?></div>
                            <div class="mgIcon_discipline mg_normal"><?=$arOrder["PROPERTIES"]['subject']["VALUE"]?></div>
                            <div class="mgIcon_university mg_normal"><?=$arOrder["PROPERTIES"]['organ']["VALUE"]?></div>
                        </div>
                        <div class="workStats">
                            <div class="mgIcon_period mg_small"><span>�����:</span> <?=$arOrder["PROPERTIES"]['period']["VALUE"]?></div>
                            <div class="mgIcon_pages mg_small"><span>�������:</span> <?=$arOrder["PROPERTIES"]['pages']["VALUE"]?></div>
                             <?if($arOrder["PROPERTIES"]['PLAGIAT']["VALUE"]=='yes'): ?>                            
                            <div class="mgIcon_originality mg_small"><span>�������������:</span> <?=$arOrder["PROPERTIES"]['ERCENT_PLAGIAT']["VALUE"]?> <strong class="mgIcon_question mg_small"><span>���� ������ ���� ����������� �� ������. </span></strong></div>
                        	<? endif;?>
                        </div>
                        
                        <div class="workDesc">
                            <p><?=$arOrder["PROPERTIES"]['task']["VALUE"]?></p>
                            <? if(is_array($arOrder["PROPERTIES"]["file"]["VALUE"]) && sizeof($arOrder["PROPERTIES"]["file"]["VALUE"]))
							{?>
                            <a href="/zipfiles.php?oid=<?=$arOrder["ID"]?>" class="download_file mgIcon_download mg_small">����������� ����������� ����</a>
                            <? }?>
                        </div>
                        
                       <?/* ?> <div class="tasks" id="taskList">
                            <h3>��������:</h3>
                            <div>
                                <table>
                                    <tr class="thead">
                                        <th>��������</th>
                                        <th>ʳ������ �����</th>
                                        <th>��������</th>
                                    </tr>
                                    <tr>
                                        <td title="��������"><a href="#" class="icon_flag-orange">3-� �����</a></td>
                                        <td title="ʳ������ �����">29.04.2016 09:00</td>
                                        <td title="��������"><a href="#" class="commentsLink">2</a></td>
                                    </tr>
                                    <tr>
                                        <td title="��������"><a href="#" class="icon_flag-red">2-� �����</a></td>
                                        <td title="ʳ������ �����">29.04.2016 15:00</td>
                                        <td title="��������"><a href="#" class="commentsLink">5</a></td>
                                    </tr>
                                    <tr class="tr_done">
                                        <td title="��������"><a href="#" class="icon_done">����</a></td>
                                        <td title="ʳ������ �����">29.04.2016 08:00</td>
                                        <td title="��������"><a href="#" class="commentsLink">4</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div><? */?>
                    </div>
                    </div>
                </div>
                <!--# end content #-->
			<?
		}
	}
}



?>
<!--# content #-->
                
                
                
                
                


















