<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//echo "<pre>".print_r($arResult, true)."</pre>";
?>
<?if (!empty($arResult)):?>
<div class="top-menu<?=(substr($APPLICATION->GetCurDir(),0,9+(LANG_ID==LANG_ID_DEF?0:strlen(LANG_ID)+1))==(LANG_ID==LANG_ID_DEF?"":"/".LANG_ID).'/authors/'?' auth-menu':'')?>"><div class="lm"><div class="rm">
<?foreach($arResult as $cnt => $arItem):?>
	<?
	$is_first = $cnt==reset(array_keys($arResult)) ? true : false;
	$is_last = $cnt==end(array_keys($arResult)) ? true : false;
	?>
	<?if($arItem["SELECTED"]):?>
			<div class="sel<?=(!$is_first&&!$is_last?' lb':'')?>"><?=($is_first?'<div class="slm">':($is_last?'<div class="srm">':''))?><a <?if($arItem["LINK"]!=$_SERVER["REQUEST_URI"]):?> href="<?=$arItem["LINK"]?>"<?endif;?>title="<?=htmlspecialchars($arItem["TEXT"]);?>"><?=str_replace(" ", "&nbsp;", $arItem["TEXT"])?></a><?=($is_first||$is_last?'</div>':'')?></div>
	<?else:?>
		<a href="<?=$arItem["LINK"]?>" title="<?=htmlspecialchars($arItem["TEXT"]);?>"><?=str_replace(" ", "&nbsp;", $arItem["TEXT"])?></a>
	<?endif?>
<?endforeach?>
	<div class="clear">&nbsp;</div>
</div></div></div>
<?endif?>