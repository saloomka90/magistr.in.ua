<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
foreach($arResult["ITEMS"] as $arItem) $ids[] = $arItem["PROPERTIES"]["parent"]["VALUE"];

$works = array();
$rs_w = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>4,"ACTIVE"=>"Y","@ID"=>$ids),false,false,array("ID","NAME","PROPERTY_TYPE"));
while($ar_w=$rs_w->GetNext())
{
	//$ar_w = $ob_w->GetFields();
	$works[$ar_w["ID"]] = $ar_w;
	//$works[$ar_w["ID"]]["PROPERTIES"] = $ob_w->GetProperties();
}
//echo "<pre>".print_r($works,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("parent", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 600,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function showPerformers(order_id,user_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/performers.php?o_id='+order_id+'&u_id='+user_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function changeState(order_id,comment,state)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&comment='+escape(comment)+'&state='+state+'&lang=<?=LANG_ID?>&save=Y'
		});
		return false;
	}
	//]]>
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<table class="rounded" id="my_quotes_ready">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<td style="width:100% !important;"><?=GetMessage("IBLOCK_PROP_topic")?></td>
		<td><?=GetMessage("IBLOCK_PROP_type")?></td>
		<?/*<td><?=GetMessage("IBLOCK_PROP_performers")?></td>*/?>
		<?foreach($arParams["FIELD_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_FIELD_".$key));?>
			</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $key):if(strlen($key)>0):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_PROP_".$key));?>
			</td>
		<?endif;endforeach;?>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
//echo "<pre>".print_r($arItem,true)."</pre>";
	$link_o = '<a href="javascript:void(0)" onclick="showOrderDetails('.$arItem["ID"].');">';
	$link_c = '</a>';

	$style = '';

	if(is_array($arItem["PROPERTIES"]["order_of"]["VALUE"])&&sizeof($arItem["PROPERTIES"]["order_of"]["VALUE"])) $style = ' style="background:#fea8a8;"';

	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{
		case 27:
			$style = ' style="background:#eeff61;"';
			break;
		case 28:
			$style = ' style="background:#b4e828;"';
			break;
		case 29:
			$style = ' style="background:#e6e6e6;"';
			break;
		case 30:
			$style = ' style="background:#fa832b;"';
			break;
		default:
			//if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0) $style = ' style="background:#eeff61;"';
			//else
			break;
	}
	?>
	<tr<?=$style?>>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<td><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></td>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
		</td>
		<?endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["topic"]):?>
		<td>
			<?=$link_o?><?=$works[$arItem["PROPERTIES"]["parent"]["VALUE"]]["NAME"]?><?=$link_c?>
			<?if(strlen($arItem["~PREVIEW_TEXT"])>0):?>
			<br />[<?echo $arItem["~PREVIEW_TEXT"];?>]
			<?endif;?>
		</td>
		<?//endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["type"]):?>
		<td>
			<?=$works[$arItem["PROPERTIES"]["parent"]["VALUE"]]["PROPERTY_TYPE_VALUE"]?>
		</td>
		<?//endif?>

		<?foreach($arItem["FIELDS"] as $code=>$value):?>
		<td align="center">
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
		</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
		<td align="center">
			<?
				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;
				
				if($pid=="state"&&($arItem["PROPERTIES"]["performer"]["VALUE"]>0||$arItem["PROPERTIES"]["performer"]["VALUE"]==-1))
				{
					$value_text .= '<br />';
					if($arItem["PROPERTIES"]["performer"]["VALUE"]>0)
					{
						$rsUser = CUser::GetByID($arItem["PROPERTIES"]["performer"]["VALUE"]);
						if($arUser=$rsUser->Fetch())
							$value_text .= '[<a href="javascript:void(0)" onclick="showPerformers('.$arItem["ID"].','.$arUser["ID"].')">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</a>]';
					}
					else
					{
						$value_text .= '['.GetMessage('PERFORMER_OTHER').']';
					}
				}
				
				echo $value_text;
			?>
		</td>
		<?endforeach;?>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>