<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && strlen($_GET['q'])>=2 && sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())))
{
	$rs = CUser::GetList($by="name",$oder="asc",array("NAME"=>"%".$_GET["q"]."%","GROUPS_ID"=>array(5)));
	while($ar=$rs->Fetch())
	{
		print $ar['NAME'].'|'.$ar['ID']."\n";
	}
}
?>