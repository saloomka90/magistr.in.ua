<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
$user_id = intval($_GET["u_id"]);
//echo "<pre>".print_r($_GET,true)."</pre>";
if($order_id>0 && sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$BID = 7;
	
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
                $arOrder = $obOrder->GetFields();
                $arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if($user_id>0)
		{
			$rsUser = CUser::GetByID($user_id);
			if($arUser=$rsUser->Fetch())
			{
				if($_GET["detail"]=="Y")
				{
					if($_GET["refused"]!="Y")
					{
						$res .= '
<div class="float_r ff"><input type="button" onclick="selectAuthor('.$arOrder["ID"].','.$arUser["ID"].')" value="'.GetMessage("SELECT_AS_PERFORMER").'" /></div>';
					}
					$res .= '<div class="float_l ff"><input type="button" onclick="showPerformers('.$arOrder["ID"].',\'\',false,'.($_GET["refused"]!="Y"?"false":"true").')" value="'.GetMessage("BACK_TO_LIST").'" /></div>
<div class="clear">&nbsp;</div><br />
';
				}
				$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td style="width:25% !important;border-right:none;">&nbsp;</td>
		<td colspan="2" style="width:50% !important;border-left:none;border-right:none;"><strong>'.GetMessage("AUTHOR_INFORMATION").'</strong></td>
		<td style="width:25% !important;border-left:none;">&nbsp;</td>
	</tr>';
				$res .= '
				<tr>
					<td colspan="2" style="width:50% !important;">'.GetMessage("FIO").'</td>
					<td colspan="2" style="width:50% !important;">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</td>
				</tr>
				<tr>
					<td colspan="2">Email</td>
					<td colspan="2">'.$arUser["EMAIL"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("PHONE").'</td>
					<td colspan="2">'.(strlen($arUser["PERSONAL_PHONE"])>0?'<div>'.$arUser["PERSONAL_PHONE"].'</div>':'').(strlen($arUser["PERSONAL_MOBILE"])>0?'<div>'.$arUser["PERSONAL_MOBILE"].'</div>':'').'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_LIVING_PLACE").'</td>
					<td colspan="2">'.$arUser["UF_LIVING_PLACE"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_BIRTH_YEAR").'</td>
					<td colspan="2">'.$arUser["UF_BIRTH_YEAR"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OCCUPATION").'</td>
					<td colspan="2">'.$arUser["UF_OCCUPATION"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OPIT").'</td>
					<td colspan="2">'.$arUser["UF_OPIT"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_WORK_TYPES").'</td>
					<td colspan="2">'.$arUser["UF_WORK_TYPES"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_LANGS").'</td>
					<td colspan="2">'.$arUser["UF_LANGS"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_OTHER_DATA").'</td>
					<td colspan="2">'.$arUser["UF_OTHER_DATA"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_SUBJECTS").'</td>
					<td colspan="2">'.$arUser["UF_SUBJECTS"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_PRIVAT_CARD").'</td>
					<td colspan="2">'.$arUser["UF_PRIVAT_CARD"].'</td>
				</tr>
				<tr>
					<td colspan="2">'.GetMessage("UF_WEBMONEY").'</td>
					<td colspan="2">'.$arUser["UF_WEBMONEY"].'</td>
				</tr>';
				$res .= '
</tbody>
</table>
<div class="clear">&nbsp;</div>';
				$rs_perf_w = CIBlockElement::GetList(array("date_create"=>"desc"),array("IBLOCK_ID"=>6,"ACTIVE"=>"Y","PROPERTY_performer"=>$arUser["ID"],"PROPERTY_state"=>24),false,false,array("ID","PREVIEW_TEXT","PROPERTY_topic","PROPERTY_type"));
				$res .= '<center><strong>'.GetMessage("WORKS_COUNT").': '.($rs_perf_w->SelectedRowsCount()).'</strong></center><br />';
				if($rs_perf_w->SelectedRowsCount()>0)
				{
					$res .= '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<td style="width:70% !important;">'.GetMessage("WORK_TITLE").'</td>
		<td style="width:15% !important;">'.GetMessage("WORK_TYPE").'</td>
		<td style="width:15% !important;">'.GetMessage("WORK_PRICE").'</td>
	</tr>';
					while($ar_perf_w=$rs_perf_w->GetNext())
					{
						$rs_perf_w_q = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$BID,"PROPERTY_order_id"=>$ar_perf_w["ID"],"PROPERTY_author_id"=>$arUser["ID"]),false,array("nTopCount"=>1),array("ID","PROPERTY_summ"));
						if($ar_perf_w_q=$rs_perf_w_q->Fetch())
							$res .= '
					<tr>
						<td><a href="javascript:void(0)" onclick="showOrderDetails('.$ar_perf_w["ID"].','.$arUser["ID"].','.($_GET["refused"]!="Y"?"false":"true").',\'&back_to_work_id='.$arOrder["ID"].'\');">'.$ar_perf_w["PROPERTY_TOPIC_VALUE"].'</a>'.(strlen($ar_perf_w["~PREVIEW_TEXT"])>0?'<br />['.$ar_perf_w["~PREVIEW_TEXT"].']':'').'</td>
						<td>'.$ar_perf_w["PROPERTY_TYPE_VALUE"].'</td>
						<td>'.$ar_perf_w_q["PROPERTY_SUMM_VALUE"].' ���.</td>
					</tr>';
					}
					$res .= '
</tbody>
</table>';
				}
			}
		}
		else
		{
			$res = '
<table class="rounded">
<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">';
			if($_GET["refused"]!="Y")
			{
				$res .= '
		<td><strong>������</strong></td>';
			}
			$res .= '
		<td><strong>'.GetMessage("COMMENT").'</strong></td>
		<td style="width:100% !important;"><strong>'.GetMessage("FIO").'</strong></td>
		<td><strong>Email</strong></td>
		<td><strong>'.GetMessage("WORKS_COUNT").'</strong></td>';
			if($_GET["refused"]!="Y")
			{
				$res .= '
		<td></td>';
			}
			$res .= '
	</tr>';
			$db_performer = CIBlockElement::GetList(array("property_summ"=>"asc"),array("IBLOCK_ID"=>$BID,"ACTIVE"=>"Y","PROPERTY_order_id"=>$arOrder["ID"],($_GET["refused"]!="Y"?"":"!")."PROPERTY_refused"=>false),false,false,array("ID","PREVIEW_TEXT","PROPERTY_summ","PROPERTY_author_id"));
			if($db_performer->SelectedRowsCount()>0)
			{
				while($ar_performer=$db_performer->GetNext())
				{
					$rsUser = CUser::GetByID($ar_performer["PROPERTY_AUTHOR_ID_VALUE"]);
					if($arUser=$rsUser->Fetch())
					{
						$res .= '
						<tr>';
						if($_GET["refused"]!="Y")
						{
							$res .= '
							<td>'.$ar_performer["PROPERTY_SUMM_VALUE"].' ���.</td>';
						}
						$res .= '
							<td>'.$ar_performer["~PREVIEW_TEXT"].'</td>
							<td><a href="javascript:void(0)" onclick="showPerformers('.$order_id.','.$arUser["ID"].',true,'.($_GET["refused"]!="Y"?"false":"true").')">'.$arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"].'</a></td>
							<td><a href="mailto:'.$arUser["EMAIL"].'">'.$arUser["EMAIL"].'</a></td>';

						$rs_perf_w = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>6,"ACTIVE"=>"Y","PROPERTY_performer"=>$arUser["ID"],"PROPERTY_state"=>24),false,false,array("ID"));
						$res .= '
							<td>'.($rs_perf_w->SelectedRowsCount()).'</td>';

						if($_GET["refused"]!="Y")
						{
							$res .= '
							<td><input type="button" onclick="selectAuthor('.$order_id.','.$arUser["ID"].');" value="'.GetMessage("SELECT_AUTHOR").'" /></td>';
						}
						$res .= '
						</tr>';
					}
				}
			}
			else
			{
				$res .= '
						<tr>
							<td colspan="6">'.GetMessage("NOT_FOUND").'</td>
						</tr>';
			}
				$res .= '
</tbody>
</table>';
		}
		echo $res;
	}
}
?>