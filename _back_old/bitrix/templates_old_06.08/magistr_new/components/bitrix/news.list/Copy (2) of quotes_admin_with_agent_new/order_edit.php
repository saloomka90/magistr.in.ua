<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

$ar_lang = array("ru");
foreach($ar_lang as $lang)
{
	if(substr($_SERVER["PHP_SELF"], 0, strlen($lang)+2)=="/".$lang."/")
	{
		define("LANG_ID", ToLower($lang));
		break;
	}
}

if(!defined("LANG_ID_DEF")) define("LANG_ID_DEF", "ua");
if(!defined("LANG_ID")) define("LANG_ID", LANG_ID_DEF);

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

?>
<?$APPLICATION->IncludeComponent(
	"custom:iblock.element.add.form",
	"add_edit",
	Array(
		"SEF_MODE" => "N",
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "6",
		"PROPERTY_CODES" => array("ACTIVE", "NAME", "18", "19", "20", "44", "22", "23", "24", "25", "26", "27", "29", "30", "33", "42", "34"),
		"PROPERTY_CODES_REQUIRED" => array("NAME"),
		"GROUPS" => array("1","6"),
		"STATUS_NEW" => "N",
		"STATUS" => "ANY",
		"LIST_URL" => (LANG_ID!="ua"?"/".LANG_ID:"")."/quotes_admin/",
		"ELEMENT_ASSOC" => "DONT_CHECK",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"CUSTOM_TITLE_NAME" => "�.�.�.",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
false
);?>