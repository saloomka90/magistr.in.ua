<?IncludeTemplateLangFile(__FILE__, LANGUAGE_ID);?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />	
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css?v4" type="text/css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/select/selectbox.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/skeleton1200.css" type="text/css" />	
    <link rel="apple-touch-icon" href="/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/touch-icon-ipad-retina.png" />
    
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/topSlider.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/kkcountdown.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.spritely-0.6.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.color.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.animateNumber.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/cufon-yui.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/tahoma_cufon.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/select/jquery.selectbox.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/popup.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.rating.min.js"></script>
	<script type="text/javascript" async="" src="https://apis.google.com/js/plusone.js" gapi_processed="true"></script>
	<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>
	<script type="text/javascript">
		Cufon.replace(".slider-info h3, .title, .title2, .f-title1, .action-txt");
	</script>
	
	<script type="text/javascript" src="/bitrix/templates/magistr/js/jquery.form-2.25.js"></script>

	<script type="text/javascript">
		$(function(){
			var currentYear = (new Date).getFullYear();
			$('.copy span').text(currentYear);			
		});
	    
	   $(document).ready(function() {
				$('.cont-bt > a').toggle(function(){
					$(this).parent().find('.cont-window').show();
					$(this).find('.carr').attr('src', 'images/arr-t.png');
					
					$(document).click(function(event) {
						if(event.target.getAttribute("class") == "next" || 
							event.target.getAttribute("class") == "prev" || 
							event.target.getAttribute("class") == "pag box active"
						){
							return;
							}
						
    					if ($(event.target).closest(".cont-window").length) return;
    						$('.cont-window').hide();
    						event.stopPropagation();
  					});
					

					
				}, function() {
					$(this).parent().find('.cont-window').hide();
					$(this).find('.carr').attr('src', 'images/arr.png');
				});
		});
	</script>

<?$APPLICATION->ShowHead();?>	
	<?$APPLICATION->ShowHeadStrings()?>
	<?$APPLICATION->ShowHeadScripts()?>
</head>

<body>

	<div class="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="bg">
		<div class="header-bg">
			<div class="header">
				<div class="logo">
					<a href="<?=LANG_ROOT_DIR?>/"><?
						// Contacts
						$APPLICATION->IncludeFile("/include/logo.php", Array(), Array(
						    "MODE"      => "html",                                           
						    "NAME"      => "�������",     
						    "TEMPLATE"  => "" 
						    ));
						?>
					</a>
				</div>
				<div class="header-r">
					<a href="<?=LANG_ROOT_DIR?>/authors_register.php" class="authors-bt">�������</a>
					<div class="lang">
							
						<a href="<?if(LANGUAGE_ID!="ua"):?><?=str_replace("/".LANGUAGE_ID."/","/",$_SERVER["REQUEST_URI"])?><?endif;?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/uk.png" /></a>
						<a href="<?if(LANGUAGE_ID!="ru"):?><?="/ru".$_SERVER["REQUEST_URI"];?><?endif;?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/ru.png" /></a>
					</div>
					<?
						// Contacts
						$APPLICATION->IncludeFile("/include/headerPhones.php", Array(), Array(
						    "MODE"      => "text",                                           
						    "NAME"      => "��������",     
						    "TEMPLATE"  => "" 
						    ));
						?>
					<div class="clr"></div>
					<?$APPLICATION->IncludeComponent("magistr:menu", "horizontal", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<script>
			  
			$(document).ready(function() {
			                $('#near-clouds2').pan({fps: 30, speed: 1, dir: 'right', depth: 70});
                
                window.actions = {
                    speedyClouds: function(){
                        $('#near-clouds2').spSpeed(20);
                    },
                    runningClouds: function(){
                        $('#near-clouds2').spSpeed(12);
                    },
                    walkingClouds: function(){
                        $('#near-clouds2').spSpeed(5);
                    },
                    lazyClouds: function(){
                        $('#near-clouds2').spSpeed(1);
                    },
                    stop: function(){
                        $('#near-clouds2').spStop();
                    },
                    start: function(){
                        $('#near-clouds2').spStart();
                    },
                    toggle: function(){
                        $('#near-clouds2').spToggle();
                    },
                    left: function(){
                        $('#near-clouds2').spChangeDir('left');                    
                    },
                    right: function(){
                        $('#near-clouds2').spChangeDir('right');                    
                    }
                };
            }); 
		</script>
		<? 
		if(!$APPLICATION->GetPageProperty("NOTSHOWTITLE")):?>
			<div class="title-line-bg">
				<div id="near-clouds2" class="near-clouds2 stage"></div>
				<div class="title-line"><?$APPLICATION->ShowTitle(false);?></div>
			</div>
		<?endif;?>
