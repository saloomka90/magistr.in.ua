<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<style>
	.work-cat table tbody td {
		border-right: 1px solid #f1dc7b;
	}
	
</style>


<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
<?elseif($arResult["ERROR_CODE"]!=0):?>
	<?ShowError(GetMessage("SEARCH_ERROR"));?>


<?elseif(count($arResult["SEARCH"])>0):?>
	
	<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td><?=GetMessage("SEARCH_RESULT")?>:</td>
						</tr>
					</thead>
					<tbody>

	<?foreach($arResult["SEARCH"] as $arItem):?>
	
			<tr>
				<td>
					<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
					
				</td>
			</tr>
	<?endforeach;?>
		</tbody>
	</table>
	<?=$arResult["NAV_STRING"]?>
	<br />
	
<?else:?>
	<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
<?endif;?>
