<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])>0):?>

				<table cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td><?=GetMessage("IBLOCK_FIELD_NAME")?>:</td>
							<td style="width:98px;"><?=GetMessage("IBLOCK_PROP_cina")?>:</td>
							<td><?=GetMessage("IBLOCK_PROP_storinok")?>:</td>
						</tr>
					</thead>
					<tbody>
						
						<?foreach($arResult["ITEMS"] as $arItem):?>
						
							<tr>
								<td>
									<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
									<span>(<?=$arResult["SECTION"]["NAMES"][$arItem["IBLOCK_SECTION_ID"]]?> / <?=$arItem["PROPERTIES"]["type"]["VALUE"]?> | <?=$arItem["PROPERTIES"]["date_make"]["VALUE"]?>)</span>
								</td> 
								<td ><?=$arItem["PROPERTIES"]["cina"]["VALUE"]?> ���</td>
								<td><?=$arItem["PROPERTIES"]["storinok"]["VALUE"]?></td>
							</tr>
						
						<?endforeach;?>
					</tbody>
				</table>
				<a href="<?=$arResult["SECTION"]["PATH"][0]["LIST_PAGE_URL"]?>" class="add-rev" style="line-height: normal; margin: 10px 0; padding: 3px 0 0 0;"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a>
					<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
						<?=$arResult["NAV_STRING"]?>
					<?endif;?>
		
<?endif;?>