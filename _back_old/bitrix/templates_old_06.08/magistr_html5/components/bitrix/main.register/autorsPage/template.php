<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
  if (strlen($_POST['ajax_key']) && $_POST['ajax_key']==md5('ajax_'.LICENSE_KEY)) {
	$APPLICATION->RestartBuffer();
	if (!defined('PUBLIC_AJAX_MODE')) {
		define('PUBLIC_AJAX_MODE', true);
	}
	header('Content-type: application/json');
 	if (count($arResult["ERRORS"]) > 0) {
 		
 		foreach ($arResult["ERRORS"] as $key => $error)
 		{
 			if (intval($key) <= 0)
 				$arResult["ERRORS"] .=  iconv("Windows-1251", "UTF-8", str_replace("#FIELD_NAME#", "\"".GetMessage("REGISTER_FIELD_".$key)."\"", $error))."<br/>";//str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
 		} 
 		
		echo json_encode(array(  
				'type' => 'error',
				'message' => str_replace("Array", "",$arResult["ERRORS"]),
				'newCaptcha'=> $arResult["CAPTCHA_CODE"],
				
		));
	} else {
		if($arResult["VALUES"]["USER_ID"]>0)
		{	Global $USER;
			$USER->Authorize($arResult["VALUES"]["USER_ID"]);
			echo json_encode(array('type' => 'ok'));
		}	
	} 
	
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
	die();
}  
?>
<form id="reg_form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform">
<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" /> 
<input type="hidden" name="auth_type" value="1">
					<div class="rev-val">
						<label><?=GetMessage("REGISTER_FIELD_NAME");?>:<span>*</span></label>
						<input type="text" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"];?>" />
					</div>
					<div class="rev-val">
						<label>E-mail:<span>*</span></label>
						<input type="text" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["LOGIN"];?>" />
						<input type="hidden" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"];?>" />
					</div>
					<div class="rev-val">
						<label><?=GetMessage("REGISTER_FIELD_PASSWORD");?>:<span>*</span></label>
						<input type="password" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"];?>" />
					</div>
					<div class="rev-val">
						<label><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD");?>:<span>*</span></label>
						<input type="password" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"];?>" />
					</div>
					<div class="rev-val r2">
						<label><?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE");?>:<span>*</span></label>
						<input type="text" value="+38" name="tel1" maxlength="3" />
						<span class="koma">(</span>
						<input type="text" placeholder="000" name="tel2" maxlength="3" />
						<span class="koma">)</span>
						<input type="text" placeholder="0000000" name="tel3" maxlength="7" class="i2" />
						<p><i><?=GetMessage("REGISTER_EXAMPLE");?>:</i> +38 (050) 1234567</p>
						<input id="phone" type="hidden" name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]["PERSONAL_PHONE"];?>" >
						<div class="clr"></div>
					</div>
					<? 
						/* CAPTCHA */
						if ($arResult["USE_CAPTCHA"] == "Y")
						{
							?>
							<div class="rev-val">
								<label><?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span>*</span></label>
									<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
									<img  id='capt' style="vertical-align: middle;" src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
									<input style="width: 200px; margin-left: 60px;" type="text" name="captcha_word" maxlength="50" value="" />
								<div class="clr"></div>
							</div>
							<?
						}
						/* CAPTCHA */
					?>
					<div class="rev-val">
						<input type="submit" class="reg-bt2" value="<?=GetMessage('AUTH_REGISTER');?>" name="register_submit_button" />
						<div class="clr"></div>
					</div>
				</form>	
<?



if (count($arResult["ERRORS"]) > 0)
{
	foreach ($arResult["ERRORS"] as $key => $error)
	{
		if (intval($key) <= 0)  
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
	}

	ShowError(implode("<br />", $arResult["ERRORS"]));
}
elseif($arResult["VALUES"]["USER_ID"]>0)
{
	if($_POST["auth_type"]==2)
	{
		$user_id = $arResult["VALUES"]["USER_ID"];
		$user = new CUser;
		$user->Update($user_id, array("GROUP_ID"=>array(5)));

		$rsUser = CUser::GetByID($user_id);
		if($arUser = $rsUser->Fetch())
		{
			foreach($arUser as $key=>$value) $arEventFields["USER_".$key] = $value;
			CEvent::Send("NEW_AUTHOR", "s1", $arEventFields);
			CEvent::CheckEvents();
		}
	}

	if($arResult["USE_EMAIL_CONFIRMATION"] === "Y")
	{
		?><p><?ShowNote(GetMessage("REGISTER_EMAIL_WILL_BE_SENT"))?></p><?
	}
	else
	{
		?><p><?ShowNote(GetMessage("REGISTER_OK"))?></p><?
	}
}
?>

<script>


$("#reg_form").validate({
	submitHandler: function(form) {
		//set to hidden
		$tel = $("input[name='tel1']").val() + $("input[name='tel2']").val() + $("input[name='tel3']").val();
		$email = $("input[name='REGISTER[LOGIN]']").val();
		$("input[name='REGISTER[PERSONAL_PHONE]']").val($tel);
		$("input[name='REGISTER[EMAIL]']").val($email);


		$("#err_txt").remove();
		  var $this = $(form);
		  var $form = {
		     action: $this.attr('action'),
		     post: {'ajax_key':'<?=md5('ajax_'.LICENSE_KEY)?>'}
		  };
		 
		  $.each($('input', $this), function(){
		     if ($(this).attr('name').length) {
		        $form.post[$(this).attr('name')] = $(this).val();
		     }
		  });
		  $.post($form.action, $form.post, function(data){
		     $('input', $this).removeAttr('disabled');
		    
		     if(data.newCaptcha!= null){
		    	 $("input[name='captcha_sid']").val(data.newCaptcha);
		    	 $("input[name='captcha_word']").val("");
		    	 $("#capt").attr('src','/bitrix/tools/captcha.php?captcha_sid='+data.newCaptcha);
			 }
		     if (data.type == 'error') {

		  	  $("#reg_form").before('<p id="err_txt" style="color:red;">'+data.message+'</p>');
		        $("input[name='REGISTER[PASSWORD]']").val("");
		        $("input[name='REGISTER[CONFIRM_PASSWORD]']").val("");
		       
		        
		     } else {
			     window.location = "<?=LANG_ROOT_DIR?>/authors/personal_data/"
			 }
		  }, 'json');  
		  return false; 
	},
	   rules:{
			'REGISTER[NAME]':{
				required: true,
				minlength: 3,
				maxlength: 50,
			},  
			'REGISTER[LOGIN]':{
				required: true,
				email: true,
			},
			'REGISTER[PASSWORD]':{
				required: true,
				minlength: 6,
				//maxlength: 24,
			},
			'REGISTER[CONFIRM_PASSWORD]':{
				required: true,
				minlength: 6,
				//maxlength: 24,
			},						
			tel1:{
				required: true,
				minlength: 2,
				maxlength: 3,
			},
			tel2:{
				required: true,
				minlength: 3,
				maxlength: 3,
			},
			tel3:{
				required: true,
				minlength: 7,
				maxlength: 7,
			},
			captcha_word:{
				required: true,
			},
	   },
	   messages:{
		   'REGISTER[NAME]':{
				required: "",
				minlength: "",
				//maxlength: "",
			},
			'REGISTER[LOGIN]':{
				required: "",
				email: "",
			},
			'REGISTER[PASSWORD]':{
				required: "",
				minlength: "",
				//maxlength: "",
			},
			'REGISTER[CONFIRM_PASSWORD]':{
				required: "",
				minlength: "",
				maxlength: "",
			},						
			tel1:{
				required: "",
				minlength: "",
				maxlength: "",
			},
			tel2:{
				required: "",
				minlength: "",
				maxlength: "",
			},
			tel3:{
				required: "",
				minlength: "",
				maxlength: "",
			},
			captcha_word:{
				required: "",
			},					
	   }
	});


</script>