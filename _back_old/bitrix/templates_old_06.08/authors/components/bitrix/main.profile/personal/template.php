<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

// echo "<pre>"; print_r($arResult); echo "</pre>";
// exit();




?>
<?=ShowError($arResult["strProfileError"]);?>
<?
$group_ok = in_array(5, $USER->GetUserGroupArray()) ? true : false;
if ($arResult['DATA_SAVED'] == 'Y')
{
	if($_POST["auth_type"]==2&&!$group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(5))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = true;
		}

		$rsUser = CUser::GetByID($user_id);
		if($arUser = $rsUser->Fetch())
		{
			$arEventFields = array(
				"USER_ID"=>$arUser["ID"],
				"LOGIN"=>$arUser["LOGIN"],
				"EMAIL"=>$arUser["EMAIL"],
				"NAME"=>$arUser["NAME"]
			);
			CEvent::Send("NEW_AUTHOR_FROM_OLD", "s1", $arEventFields);
			CEvent::CheckEvents();
		}
	}
	elseif($group_ok&&!$USER->IsAdmin())
	{
		$user_id = $arResult["ID"];
		$user = new CUser;
		if($user->Update($user_id, array("GROUP_ID"=>array(3))))
		{
			$USER->Authorize($USER->GetID());
			$group_ok = false;
		}
	}

	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}
	$user_id = $arResult["ID"];
	$user = new CUser;
	$rsUser = $user->GetByID($user_id);
	$arUser = $rsUser->Fetch();
	$arSubj = unserialize($arUser['UF_ARSUBJ']);
	// $arSubj["type-works-1"] = 'on';
	// echo "<pre>"; print_r($arUser); echo "</pre>";
	// exit();

/**

**/
?>
	<script type="text/javascript">
		Cufon.replace("");
	</script>

    <script type="text/javascript">
		$(function () {
			// var flag = false;
			// $("#mig").css("opacity", "1");
			// setTimeout(function () {
			// 	$("#mig").css("opacity", "1");
			// 	setInterval(function () {
			// 		$("#mig").css("opacity", flag? "1":"0.5");
			// 		flag = !flag;
			// 	}, 500)
			// }, 3000);
		});

		$(document).ready(function() {
			var currentYear = (new Date).getFullYear();
			$('.copy span').text(currentYear);
			$(":file").jfilestyle({icon:false, 'buttonText': '������� ����'});
			var select = $('.niceSelect');
			$(select).selectbox({
				onOpen: function (select) {
					$('.sbHolder').addClass('focus');
					$('.sbGroup').parent().addClass('sbGroup');
				},
				onClose: function (inst) {
					$('.sbHolder').removeClass('focus');
				},
				onChange: function (val, inst) {
					$(this).parent().attr('class','anketa-val sel2');
					if(val == ''){
						$(this).parent().attr('class','anketa-val sel');
					}
				},
				effect: "slide"
			});

			$('form input[type="submit"]').click(function(){
				setTimeout(function(){
					$('form select').each(function(){
						if($(this).attr('class')=='niceSelect error'){
							$(this).parent().addClass('sel');
							$(this).parent().removeClass('sel2');
						}
						else {
							$(this).parent().removeClass('sel');
							$(this).parent().addClass('sel2');
						}
					});
				}, 100);
			});

			$('.cont-bt > a').toggle(function(){
				$(this).parent().find('.cont-window').show();
				$(this).find('.carr').attr('src', 'images/arr-t.png');
				$('body').click(function(){
					$(this).parent().find('.cont-window').hide();
					$(this).find('.carr').attr('src', 'images/arr.png');
				});
			}, function() {
				$(this).parent().find('.cont-window').hide();
				$(this).find('.carr').attr('src', 'images/arr.png');
			});

			$('.header .menu > ul > li.parent').hover(
				function(){
					$('.header .menu > ul > li > ul').hide();
					$(this).addClass('hovered');
					$(this).find('ul').show();
					$(this).find('ul').css('z-index', '10');
				},
				function(){
					$('.header .menu > ul > li > ul').hide();
					$('.header .menu > ul > li.active > ul').show();
					$(this).removeClass('hovered');
					$(this).find('ul').css('z-index', '1');
				}
			);

			$('.send a').click(function(){
				var html = $('.send_box_hide').html();
				$(this).after(html);

				return false;
			});

			$("#profile_form").validate({
			   rules:{
					name:{
						required: true,
						minlength: 3,
						maxlength: 150,
					},
					email:{
						required: true,
						email: true,
					},
					tel1:{
						required: true,
						minlength: 2,
						maxlength: 3,
					},
					tel2:{
						required: true,
						minlength: 3,
						maxlength: 3,
					},
					tel3:{
						required: true,
						minlength: 7,
						maxlength: 7,
					},
					date:{
						required: true,
					},
					month:{
						required: true,
					},
					year:{
						required: true,
					},
					address:{
						required: true,
						minlength: 3,
						maxlength: 150,
					},
					about:{
						required: true,
						minlength: 3,
						maxlength: 300,
					},
			   },
			   messages:{
					name:{
						required: "",
						minlength: "",
						maxlength: "",
					},
					email:{
						required: "",
						email: "",
					},
					tel1:{
						required: "",
						minlength: "",
						maxlength: "",
					},
					tel2:{
						required: "",
						minlength: "",
						maxlength: "",
					},
					tel3:{
						required: "",
						minlength: "",
						maxlength: "",
					},
					date:{
						required: "",
					},
					month:{
						required: "",
					},
					year:{
						required: "",
					},
					address:{
						required: "",
						minlength: "",
						maxlength: "",
					},
					about:{
						required: "",
						minlength: "",
						maxlength: "",
					},
			   	},
			   // 	submitHandler: function(form) {
			   // 		console.log(form);
  				// }
  			});
		});

    </script>
<!-- /bitrix/components/itlogic/ajax/index.php  -->
<div class="wrap-bg">
	<div class="wrap">
		<div class="anketa">
			<form 	name="profile_form"
					id="profile_form"
					method="POST"
					action="<?=$arResult["FORM_TARGET"]?>?"
					enctype="multipart/form-data">

					<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
					<?
						$js_titles = "";
					?>
					<div class="overlay" id="overlay_pass" style="display:none;"></div>
					<div class="nonebox" id="nonebox_pass">
						<span id="pass-close" class="close"></span>
						<h2>��i�� ������</h2>
						<div class="rev-form">
							<div class="rev-val">
								<label>�������� ������:<span>*</span></label>
								<input name="password" type="text" />
							</div>
							<div class="rev-val">
								<label>����� ������:<span>*</span></label>
								<input name="new-password" type="text" />
							</div>
							<div class="rev-val">
								<label>����� ������ (���������):<span>*</span></label>
								<input name="new-password2" type="text" />
							</div>
							<div class="rev-val">
							<a href="#" onclick="$('#submitForm').click(); return false;" class="anketa-bt">��������</a>
								<!-- <input type="submit" class="anketa-bt" value="��������" /> -->
							</div>
						</div>
					</div>

				<div class="anketa-val">
					<label>�.I.�.:<span>*</span></label>
					<input type="text" name="name" placeholder="..." value="<?=$arUser['NAME']?>" />
				</div>
				<div class="anketa-val">
					<label>E-mail:<span>*</span></label>
					<input type="text" name="email" placeholder="..." value="<?=$arUser['EMAIL']?>" />
				</div>
				<div class="anketa-val a-phone">
					<label>���. ���i�����:<span>*</span></label>
					<input type="text" value="+38" name="tel1" />
					<span class="koma">(</span>
					<input type="text" placeholder="000" name="tel2" value="<?=substr($arUser['PERSONAL_PHONE'], 3,3)?>"/>
					<span class="koma">)</span>
					<input type="text" placeholder="000-00-00" class="i2" name="tel3" value="<?=substr($arUser['PERSONAL_PHONE'], 4,7)?>"/>
					<p>���������:<br /> +38 (050) 123-45-67</p>
					<div class="clr"></div>
				</div>
				<div class="anketa-val a-phone">
					<label>���. ������i�:</label>
					<input type="text" value="+38" name="tel01" />
					<span class="koma">(</span>
					<input type="text" name="tel02" value="<?=substr($arUser['PERSONAL_MOBILE'], 3,3)?>"/>
					<span class="koma">)</span>
					<input type="text" class="i2" name="tel03" value="<?=substr($arUser['PERSONAL_MOBILE'], 4,7)?>"/>
					<p>������ ����� ���������� ��� ��������������� ������,<br /> �� ����� � ���� ����� ���� ��'�������.</p>
					<div class="clr"></div>
				</div>
				<div class="anketa-val sel1" style="float: left;">
					<label>���� ����������:<span>*</span></label>
					<select class="niceSelect" name="date">
						<option value="">����</option>
						<option selected value="<?=$arUser['UF_BIRTH_DAY']?>"><?=$arUser['UF_BIRTH_DAY']?></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
				</div>
				<div class="anketa-val s-center" style="float: left;">
					<select class="niceSelect" name="month">
						<option value="">�i����</option>
						<option selected value="<?=$arUser['UF_BIRTH_MONTH']?>"><?=$arUser['UF_BIRTH_MONTH']?></option>
						<option value="ѳ����">ѳ����</option>
						<option value="�����">�����</option>
						<option value="��������">��������</option>
						<option value="������">������</option>
						<option value="�������">�������</option>
						<option value="�������">�������</option>
						<option value="������">������</option>
						<option value="�������">�������</option>
						<option value="��������">��������</option>
						<option value="�������">�������</option>
						<option value="��������">��������</option>
						<option value="�������">�������</option>
					</select>
				</div>
				<div class="anketa-val sel1" style="float: left;">
					<select class="niceSelect" name="year">
						<option value="">�i�</option>
						<option selected value="<?=$arUser['UF_BIRTH_YEAR']?>"><?=$arUser['UF_BIRTH_YEAR']?></option>
						<option value="2000">2000</option>
						<option value="1999">1999</option>
						<option value="1998">1998</option>
						<option value="1997">1997</option>
						<option value="1996">1996</option>
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>
					</select>
				</div>
				<div class="clr"></div>
				<div class="anketa-val">
					<label>������:<span>*</span></label>
					<input type="text" name="address" placeholder="..." value="<?=$arUser['UF_LIVING_PLACE']?>"/>
				</div>
				<div class="anketa-val">
					<label>I�������i� ��� ����:<span>*</span></label>
					<textarea name="about" placeholder="..." value="<?=$arUser['UF_OTHER_DATA']?>" ><?=$arUser['UF_OTHER_DATA']?></textarea>
					<ul class="inf">
						<li>����� (���, �������������, ���� ���������, �������� ������).</li>
						<li>���� ������ � ���� ��������� ������������ ���� �� ����������.</li>
						<li>����� ������ � ������ �������������, �������� � �����.</li>
						<li>����� ��������� ���������� �� ����������� ��� ��������� ����?</li>
						<li>��� � ��� ��� ���� ������ (�������� ��� ��������, ���������� ��������, ����)?</li>
						<li>�� �� ���������� �� �������? �� ���������� ���� � ���� �������?</li>
						<li>�� ���� �������� �� ����� ����?</li>
					</ul>
				</div>
				<div class="anketa-val">
					<div class="anketa-val-l">
						<label>����� ������ �����������:</label>
						<label>���������:</label>
					</div>
					<div class="anketa-val-r">
						<input name="card-0" type="text" maxlength="4" placeholder="0000" value="<?=substr($arUser['UF_PRIVAT_CARD'], 0,4)?>"/>
						<input name="card-1" type="text" maxlength="4" placeholder="0000" value="<?=substr($arUser['UF_PRIVAT_CARD'], 4,4)?>"/>
						<input name="card-2" type="text" maxlength="4" placeholder="0000" value="<?=substr($arUser['UF_PRIVAT_CARD'], 8,4)?>"/>
						<input name="card-3" type="text" maxlength="4" placeholder="0000" value="<?=substr($arUser['UF_PRIVAT_CARD'], 12,4)?>"/>
						<div class="clr"></div>
						<div class="otr-val">
							<input name="receiver" type="text" value="<?=$arUser['UF_RECEIVER']?>" placeholder="..." />
						</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="anketa-val">
					<label style="margin: 0;">���� ����:</label>
					<ul class="type-works">

						<li><input <?=$checked = ($arSubj["type-works-1"]) ? "checked" : "" ;?> name="type-works-1" type="checkbox" /> ����������</li>
						<li><input <?=$checked = ($arSubj["type-works-2"]) ? "checked" : "" ;?> name="type-works-2" type="checkbox" /> �������</li>
						<li><input <?=$checked = ($arSubj["type-works-3"]) ? "checked" : "" ;?> name="type-works-3" type="checkbox" /> �����������</li>
						<li><input <?=$checked = ($arSubj["type-works-4"]) ? "checked" : "" ;?> name="type-works-4" type="checkbox" /> ���������</li>
						<li><input <?=$checked = ($arSubj["type-works-5"]) ? "checked" : "" ;?> name="type-works-5" type="checkbox" /> ��������</li>
						<li><input <?=$checked = ($arSubj["type-works-6"]) ? "checked" : "" ;?> name="type-works-6" type="checkbox" /> ������-����</li>
						<li><input <?=$checked = ($arSubj["type-works-7"]) ? "checked" : "" ;?> name="type-works-7" type="checkbox" /> ������������</li>
						<li><input <?=$checked = ($arSubj["type-works-8"]) ? "checked" : "" ;?> name="type-works-8" type="checkbox" /> ���������</li>
						<li><input <?=$checked = ($arSubj["type-works-9"]) ? "checked" : "" ;?> name="type-works-9" type="checkbox" /> ��� � ��������</li>
						<li><input <?=$checked = ($arSubj["type-works-10"]) ? "checked" : "" ;?> name="type-works-10" type="checkbox" /> �����������</li>
						<li><input <?=$checked = ($arSubj["type-works-11"]) ? "checked" : "" ;?> name="type-works-11" type="checkbox" /> �������</li>
						<li><input <?=$checked = ($arSubj["type-works-12"]) ? "checked" : "" ;?> name="type-works-12" type="checkbox" /> ������</li>
						<li><input <?=$checked = ($arSubj["type-works-13"]) ? "checked" : "" ;?> name="type-works-13" type="checkbox" /> �������</li>
						<li><input <?=$checked = ($arSubj["type-works-14"]) ? "checked" : "" ;?> name="type-works-14" type="checkbox" /> �����</li>
						<li><input <?=$checked = ($arSubj["type-works-15"]) ? "checked" : "" ;?> name="type-works-15" type="checkbox" /> ����������</li>
						<li><input <?=$checked = ($arSubj["type-works-16"]) ? "checked" : "" ;?> name="type-works-16" type="checkbox" checked disabled /> ���� ���� ����</li>
						<li><input <?=$checked = ($arSubj["type-works-17"]) ? "checked" : "" ;?> name="type-works-17" type="checkbox" /> ������</li>
					</ul>
					<div class="clr"></div>
				</div>
				<div class="anketa-val">
					<script type="text/javascript">
						$(function(){
							$('.subjects-menu a').click(function(){
								$('.subjects-menu li').removeClass('active');
								$(this).parent().addClass('active');
								$('.subjects-desc').hide();
								var id = $(this).attr('href');
								$(id).show();
								return false;
							});
						});
					</script>
					<label>�������i��:</label>
					<div class="subjects">
						<div class="subjects-menu">
							<ul>
								<li class="active"><a href="#smb1">�����i����i</a></li>
								<li><a href="#smb2">������i��i</a></li>
								<li><a href="#smb3">���������i</a></li>
								<li><a href="#smb4">����i��i</a></li>
								<li><a href="#smb5">�������i</a></li>
								<li><a href="#smb6">I��i</a></li>
							</ul>
						</div>
						<div class="clr"></div>
						<div class="subjects-desc" id="smb1" style="display: block;">
							<ul>
								<li><input <?=$checked = ($arSubj["humanities-1"]) ? "checked" : "" ;?> name="humanities-1" type="checkbox" /> ����� ��������� ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-2"]) ? "checked" : "" ;?> name="humanities-2" type="checkbox" /> ��������� ����</li>
								<li><input <?=$checked = ($arSubj["humanities-3"]) ? "checked" : "" ;?> name="humanities-3" type="checkbox" /> ������� ������������ (���)</li>
								<li><input <?=$checked = ($arSubj["humanities-4"]) ? "checked" : "" ;?> name="humanities-4" type="checkbox" /> ������</li>
								<li><input <?=$checked = ($arSubj["humanities-5"]) ? "checked" : "" ;?> name="humanities-5" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-6"]) ? "checked" : "" ;?> name="humanities-6" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-7"]) ? "checked" : "" ;?> name="humanities-7" type="checkbox" /> �����</li>
								<li><input <?=$checked = ($arSubj["humanities-8"]) ? "checked" : "" ;?> name="humanities-8" type="checkbox" /> ����������� �� ��������� ������</li>
								<li><input <?=$checked = ($arSubj["humanities-9"]) ? "checked" : "" ;?> name="humanities-9" type="checkbox" /> ������</li>
								<li><input <?=$checked = ($arSubj["humanities-10"]) ? "checked" : "" ;?> name="humanities-10" type="checkbox" /> �����������</li>
								<li><input <?=$checked = ($arSubj["humanities-11"]) ? "checked" : "" ;?> name="humanities-11" type="checkbox" /> ������������</li>
								<li><input <?=$checked = ($arSubj["humanities-12"]) ? "checked" : "" ;?> name="humanities-12" type="checkbox" /> ˳��������</li>
								<li><input <?=$checked = ($arSubj["humanities-13"]) ? "checked" : "" ;?> name="humanities-13" type="checkbox" /> ˳�������� ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-14"]) ? "checked" : "" ;?> name="humanities-14" type="checkbox" /> ˳�������� ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-15"]) ? "checked" : "" ;?> name="humanities-15" type="checkbox" /> ˳�������� ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-16"]) ? "checked" : "" ;?> name="humanities-16" type="checkbox" /> �����</li>
								<li><input <?=$checked = ($arSubj["humanities-17"]) ? "checked" : "" ;?> name="humanities-17" type="checkbox" /> ��������� �� ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-18"]) ? "checked" : "" ;?> name="humanities-18" type="checkbox" /> ͳ������ ����</li>
								<li><input <?=$checked = ($arSubj["humanities-19"]) ? "checked" : "" ;?> name="humanities-19" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-20"]) ? "checked" : "" ;?> name="humanities-20" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-21"]) ? "checked" : "" ;?> name="humanities-21" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-22"]) ? "checked" : "" ;?> name="humanities-22" type="checkbox" /> ���㳺��������, ����� �� �������</li>
								<li><input <?=$checked = ($arSubj["humanities-23"]) ? "checked" : "" ;?> name="humanities-23" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-24"]) ? "checked" : "" ;?> name="humanities-24" type="checkbox" /> �������� ����</li>
								<li><input <?=$checked = ($arSubj["humanities-25"]) ? "checked" : "" ;?> name="humanities-25" type="checkbox" /> ��������� ������</li>
								<li><input <?=$checked = ($arSubj["humanities-26"]) ? "checked" : "" ;?> name="humanities-26" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-27"]) ? "checked" : "" ;?> name="humanities-27" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["humanities-28"]) ? "checked" : "" ;?> name="humanities-28" type="checkbox" /> ��������� ����</li>
								<li><input <?=$checked = ($arSubj["humanities-29"]) ? "checked" : "" ;?> name="humanities-29" type="checkbox" /> Գ��������� �� �����</li>
								<li><input <?=$checked = ($arSubj["humanities-30"]) ? "checked" : "" ;?> name="humanities-30" type="checkbox" /> Գ������</li>
								<li><input <?=$checked = ($arSubj["humanities-31"]) ? "checked" : "" ;?> name="humanities-31" type="checkbox" /> Գ�������</li>
								<li><input <?=$checked = ($arSubj["humanities-32"]) ? "checked" : "" ;?> name="humanities-32" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["humanities-33"]) ? "checked" : "" ;?> name="humanities-33" type="checkbox" /> ���������� ����</li>
							</ul>
						</div>
						<div class="subjects-desc" id="smb2" style="display: none;">
							<ul>
								<li><input <?=$checked = ($arSubj["economy-1"]) ? "checked" : "" ;?> name="economy-1" type="checkbox" /> �����</li>
								<li><input <?=$checked = ($arSubj["economy-2"]) ? "checked" : "" ;?> name="economy-2" type="checkbox" /> ��������� ������</li>
								<li><input <?=$checked = ($arSubj["economy-3"]) ? "checked" : "" ;?> name="economy-3" type="checkbox" /> ������� ������</li>
								<li><input <?=$checked = ($arSubj["economy-4"]) ? "checked" : "" ;?> name="economy-4" type="checkbox" /> �������������� ����</li>
								<li><input <?=$checked = ($arSubj["economy-5"]) ? "checked" : "" ;?> name="economy-5" type="checkbox" /> �������� �������</li>
								<li><input <?=$checked = ($arSubj["economy-6"]) ? "checked" : "" ;?> name="economy-6" type="checkbox" /> ������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-7"]) ? "checked" : "" ;?> name="economy-7" type="checkbox" /> ������� ����������� �� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-8"]) ? "checked" : "" ;?> name="economy-8" type="checkbox" /> ����� �� ������</li>
								<li><input <?=$checked = ($arSubj["economy-9"]) ? "checked" : "" ;?> name="economy-9" type="checkbox" /> �������� ������</li>
								<li><input <?=$checked = ($arSubj["economy-10"]) ? "checked" : "" ;?> name="economy-10" type="checkbox" /> �������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-11"]) ? "checked" : "" ;?> name="economy-11" type="checkbox" /> �������� �������</li>
								<li><input <?=$checked = ($arSubj["economy-12"]) ? "checked" : "" ;?> name="economy-12" type="checkbox" /> ĳ���������, �����������������, ���������������</li>
								<li><input <?=$checked = ($arSubj["economy-13"]) ? "checked" : "" ;?> name="economy-13" type="checkbox" /> ������������</li>
								<li><input <?=$checked = ($arSubj["economy-14"]) ? "checked" : "" ;?> name="economy-14" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["economy-15"]) ? "checked" : "" ;?> name="economy-15" type="checkbox" /> �������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-16"]) ? "checked" : "" ;?> name="economy-16" type="checkbox" /> �������� ����� � ���������-������ ��������</li>
								<li><input <?=$checked = ($arSubj["economy-17"]) ? "checked" : "" ;?> name="economy-17" type="checkbox" /> ��������� ����������</li>
								<li><input <?=$checked = ($arSubj["economy-18"]) ? "checked" : "" ;?> name="economy-18" type="checkbox" /> ���������� �����</li>
								<li><input <?=$checked = ($arSubj["economy-19"]) ? "checked" : "" ;?> name="economy-19" type="checkbox" /> ���������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-20"]) ? "checked" : "" ;?> name="economy-20" type="checkbox" /> ��'���� � ������������, PR</li>
								<li><input <?=$checked = ($arSubj["economy-21"]) ? "checked" : "" ;?> name="economy-21" type="checkbox" /> ������������������ ���������, ���</li>
								<li><input <?=$checked = ($arSubj["economy-22"]) ? "checked" : "" ;?> name="economy-22" type="checkbox" /> ������������, ������������ ���������</li>
								<li><input <?=$checked = ($arSubj["economy-23"]) ? "checked" : "" ;?> name="economy-23" type="checkbox" /> ����������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-24"]) ? "checked" : "" ;?> name="economy-24" type="checkbox" /> ������������ ����������</li>
								<li><input <?=$checked = ($arSubj["economy-25"]) ? "checked" : "" ;?> name="economy-25" type="checkbox" /> ������������ ������</li>
								<li><input <?=$checked = ($arSubj["economy-26"]) ? "checked" : "" ;?> name="economy-26" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["economy-27"]) ? "checked" : "" ;?> name="economy-27" type="checkbox" /> ˳���� ������������</li>
								<li><input <?=$checked = ($arSubj["economy-28"]) ? "checked" : "" ;?> name="economy-28" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["economy-29"]) ? "checked" : "" ;?> name="economy-29" type="checkbox" /> �������������, �������� ����������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-30"]) ? "checked" : "" ;?> name="economy-30" type="checkbox" /> ���������, �������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-31"]) ? "checked" : "" ;?> name="economy-31" type="checkbox" /> ����������, ��������� ����������</li>
								<li><input <?=$checked = ($arSubj["economy-32"]) ? "checked" : "" ;?> name="economy-32" type="checkbox" /> ����� ������</li>
								<li><input <?=$checked = ($arSubj["economy-33"]) ? "checked" : "" ;?> name="economy-33" type="checkbox" /> ̳�������� �������� �� ��������� ��������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-34"]) ? "checked" : "" ;?> name="economy-34" type="checkbox" /> ̳�����������</li>
								<li><input <?=$checked = ($arSubj["economy-35"]) ? "checked" : "" ;?> name="economy-35" type="checkbox" /> ����������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-36"]) ? "checked" : "" ;?> name="economy-36" type="checkbox" /> �������������, �������, ��������� �������</li>
								<li><input <?=$checked = ($arSubj["economy-37"]) ? "checked" : "" ;?> name="economy-37" type="checkbox" /> ϳ�����������</li>
								<li><input <?=$checked = ($arSubj["economy-38"]) ? "checked" : "" ;?> name="economy-38" type="checkbox" /> �����������, ��������� �����, ������ ���������� �����</li>
								<li><input <?=$checked = ($arSubj["economy-39"]) ? "checked" : "" ;?> name="economy-39" type="checkbox" /> ����������-��������� �����, �������� ��������������</li>
								<li><input <?=$checked = ($arSubj["economy-40"]) ? "checked" : "" ;?> name="economy-40" type="checkbox" /> ����� ������ ������</li>
								<li><input <?=$checked = ($arSubj["economy-41"]) ? "checked" : "" ;?> name="economy-41" type="checkbox" /> ��������� ������������ ���, ���������� ��������, ��������� ���������, ���</li>
								<li><input <?=$checked = ($arSubj["economy-42"]) ? "checked" : "" ;?> name="economy-42" type="checkbox" /> ѳ����� ������������ �� ��������������� ��������</li>
								<li><input <?=$checked = ($arSubj["economy-43"]) ? "checked" : "" ;?> name="economy-43" type="checkbox" /> ��������������, ��������� �����</li>
								<li><input <?=$checked = ($arSubj["economy-44"]) ? "checked" : "" ;?> name="economy-44" type="checkbox" /> ����������</li>
								<li><input <?=$checked = ($arSubj["economy-45"]) ? "checked" : "" ;?> name="economy-45" type="checkbox" /> ����������� ����������</li>
								<li><input <?=$checked = ($arSubj["economy-46"]) ? "checked" : "" ;?> name="economy-46" type="checkbox" /> �����������, �������� ������</li>
								<li><input <?=$checked = ($arSubj["economy-47"]) ? "checked" : "" ;?> name="economy-47" type="checkbox" /> �������������� �� ����������</li>
								<li><input <?=$checked = ($arSubj["economy-48"]) ? "checked" : "" ;?> name="economy-48" type="checkbox" /> ������� �� ���������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-49"]) ? "checked" : "" ;?> name="economy-49" type="checkbox" /> ������</li>
								<li><input <?=$checked = ($arSubj["economy-50"]) ? "checked" : "" ;?> name="economy-50" type="checkbox" /> ��������� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-51"]) ? "checked" : "" ;?> name="economy-51" type="checkbox" /> ������������ ����</li>
								<li><input <?=$checked = ($arSubj["economy-52"]) ? "checked" : "" ;?> name="economy-52" type="checkbox" /> Գ�����</li>
								<li><input <?=$checked = ($arSubj["economy-53"]) ? "checked" : "" ;?> name="economy-53" type="checkbox" /> Գ����� ���������</li>
								<li><input <?=$checked = ($arSubj["economy-54"]) ? "checked" : "" ;?> name="economy-54" type="checkbox" /> Գ�������� �����</li>
								<li><input <?=$checked = ($arSubj["economy-55"]) ? "checked" : "" ;?> name="economy-55" type="checkbox" /> Գ�������� ����������</li>
								<li><input <?=$checked = ($arSubj["economy-56"]) ? "checked" : "" ;?> name="economy-56" type="checkbox" /> ֳ�����������</li>
							</ul>
						</div>
						<div class="subjects-desc" id="smb3" style="display: none;">
							<ul>
								<li><input <?=$checked = ($arSubj["nature-1"]) ? "checked" : "" ;?> name="nature-1" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["nature-2"]) ? "checked" : "" ;?> name="nature-2" type="checkbox" /> �������</li>
								<li><input <?=$checked = ($arSubj["nature-3"]) ? "checked" : "" ;?> name="nature-3" type="checkbox" /> ³������� ���������</li>
								<li><input <?=$checked = ($arSubj["nature-4"]) ? "checked" : "" ;?> name="nature-4" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["nature-5"]) ? "checked" : "" ;?> name="nature-5" type="checkbox" /> �������</li>
								<li><input <?=$checked = ($arSubj["nature-6"]) ? "checked" : "" ;?> name="nature-6" type="checkbox" /> �������</li>
								<li><input <?=$checked = ($arSubj["nature-7"]) ? "checked" : "" ;?> name="nature-7" type="checkbox" /> �������</li>
								<li><input <?=$checked = ($arSubj["nature-8"]) ? "checked" : "" ;?> name="nature-8" type="checkbox" /> ����������</li>
								<li><input <?=$checked = ($arSubj["nature-9"]) ? "checked" : "" ;?> name="nature-9" type="checkbox" /> ��������</li>
								<li><input <?=$checked = ($arSubj["nature-10"]) ? "checked" : "" ;?> name="nature-10" type="checkbox" /> ���������������</li>
								<li><input <?=$checked = ($arSubj["nature-11"]) ? "checked" : "" ;?> name="nature-11" type="checkbox" /> ������������</li>
								<li><input <?=$checked = ($arSubj["nature-12"]) ? "checked" : "" ;?> name="nature-12" type="checkbox" /> Գ����</li>
								<li><input <?=$checked = ($arSubj["nature-13"]) ? "checked" : "" ;?> name="nature-13" type="checkbox" /> ճ��</li>
							</ul>
						</div>
						<div class="subjects-desc" id="smb4" style="display: none;">
							<ul>
								<li><input <?=$checked = ($arSubj["tecnical-1"]) ? "checked" : "" ;?> name="tecnical-1" type="checkbox" /> ������ �� ������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-2"]) ? "checked" : "" ;?> name="tecnical-2" type="checkbox" /> �����������</li>
								<li><input <?=$checked = ($arSubj["tecnical-3"]) ? "checked" : "" ;?> name="tecnical-3" type="checkbox" /> ���� �����</li>
								<li><input <?=$checked = ($arSubj["tecnical-4"]) ? "checked" : "" ;?> name="tecnical-4" type="checkbox" /> ����������</li>
								<li><input <?=$checked = ($arSubj["tecnical-5"]) ? "checked" : "" ;?> name="tecnical-5" type="checkbox" /> �����������</li>
								<li><input <?=$checked = ($arSubj["tecnical-6"]) ? "checked" : "" ;?> name="tecnical-6" type="checkbox" /> ��������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-7"]) ? "checked" : "" ;?> name="tecnical-7" type="checkbox" /> ����������� �� ������������� �������</li>
								<li><input <?=$checked = ($arSubj["tecnical-8"]) ? "checked" : "" ;?> name="tecnical-8" type="checkbox" /> ������������ �������</li>
								<li><input <?=$checked = ($arSubj["tecnical-9"]) ? "checked" : "" ;?> name="tecnical-9" type="checkbox" /> ������������-��������� ���������</li>
								<li><input <?=$checked = ($arSubj["tecnical-10"]) ? "checked" : "" ;?> name="tecnical-10" type="checkbox" /> ʳ���������</li>
								<li><input <?=$checked = ($arSubj["tecnical-11"]) ? "checked" : "" ;?> name="tecnical-11" type="checkbox" /> ���������</li>
								<li><input <?=$checked = ($arSubj["tecnical-12"]) ? "checked" : "" ;?> name="tecnical-12" type="checkbox" /> �������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-13"]) ? "checked" : "" ;?> name="tecnical-13" type="checkbox" /> ������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-14"]) ? "checked" : "" ;?> name="tecnical-14" type="checkbox" /> ���������������, �����������</li>
								<li><input <?=$checked = ($arSubj["tecnical-15"]) ? "checked" : "" ;?> name="tecnical-15" type="checkbox" /> ���������� ��������</li>
								<li><input <?=$checked = ($arSubj["tecnical-16"]) ? "checked" : "" ;?> name="tecnical-16" type="checkbox" /> ����� ��������� � ����� (���), ����� ����� (��)</li>
								<li><input <?=$checked = ($arSubj["tecnical-17"]) ? "checked" : "" ;?> name="tecnical-17" type="checkbox" /> ������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-18"]) ? "checked" : "" ;?> name="tecnical-18" type="checkbox" /> �������㳿, ������� ���������</li>
								<li><input <?=$checked = ($arSubj["tecnical-19"]) ? "checked" : "" ;?> name="tecnical-19" type="checkbox" /> ��������� ���������������</li>
								<li><input <?=$checked = ($arSubj["tecnical-20"]) ? "checked" : "" ;?> name="tecnical-20" type="checkbox" /> ��������� ������������ ��</li>
								<li><input <?=$checked = ($arSubj["tecnical-21"]) ? "checked" : "" ;?> name="tecnical-21" type="checkbox" /> ����������� ����������</li>
							</ul>
						</div>
						<div class="subjects-desc" id="smb5" style="display: none;">
							<ul>
								<li><input <?=$checked = ($arSubj["ur-1"]) ? "checked" : "" ;?> name="ur-1" type="checkbox" /> ����������</li>
								<li><input <?=$checked = ($arSubj["ur-2"]) ? "checked" : "" ;?> name="ur-2" type="checkbox" /> �������������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-3"]) ? "checked" : "" ;?> name="ur-3" type="checkbox" /> ���������� ������</li>
								<li><input <?=$checked = ($arSubj["ur-4"]) ? "checked" : "" ;?> name="ur-4" type="checkbox" /> ������������ �����</li>
								<li><input <?=$checked = ($arSubj["ur-5"]) ? "checked" : "" ;?> name="ur-5" type="checkbox" /> ��������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-6"]) ? "checked" : "" ;?> name="ur-6" type="checkbox" /> ������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-7"]) ? "checked" : "" ;?> name="ur-7" type="checkbox" /> �������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-8"]) ? "checked" : "" ;?> name="ur-8" type="checkbox" /> ������ ������� �� �����</li>
								<li><input <?=$checked = ($arSubj["ur-9"]) ? "checked" : "" ;?> name="ur-9" type="checkbox" /> ������������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-10"]) ? "checked" : "" ;?> name="ur-10" type="checkbox" /> ������������ �����</li>
								<li><input <?=$checked = ($arSubj["ur-11"]) ? "checked" : "" ;?> name="ur-11" type="checkbox" /> ������������, ����������</li>
								<li><input <?=$checked = ($arSubj["ur-12"]) ? "checked" : "" ;?> name="ur-12" type="checkbox" /> ���������� �����, ����������</li>
								<li><input <?=$checked = ($arSubj["ur-13"]) ? "checked" : "" ;?> name="ur-13" type="checkbox" /> ����������� ������</li>
								<li><input <?=$checked = ($arSubj["ur-14"]) ? "checked" : "" ;?> name="ur-14" type="checkbox" /> ����� �����</li>
								<li><input <?=$checked = ($arSubj["ur-15"]) ? "checked" : "" ;?> name="ur-15" type="checkbox" /> ̳�������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-16"]) ? "checked" : "" ;?> name="ur-16" type="checkbox" /> ������������ �����</li>
								<li><input <?=$checked = ($arSubj["ur-17"]) ? "checked" : "" ;?> name="ur-17" type="checkbox" /> �������</li>
								<li><input <?=$checked = ($arSubj["ur-18"]) ? "checked" : "" ;?> name="ur-18" type="checkbox" /> ϳ����������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-19"]) ? "checked" : "" ;?> name="ur-19" type="checkbox" /> ��������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-20"]) ? "checked" : "" ;?> name="ur-20" type="checkbox" /> �����</li>
								<li><input <?=$checked = ($arSubj["ur-21"]) ? "checked" : "" ;?> name="ur-21" type="checkbox" /> ����� �������������� ��������</li>
								<li><input <?=$checked = ($arSubj["ur-22"]) ? "checked" : "" ;?> name="ur-22" type="checkbox" /> ѳ����� �����</li>
								<li><input <?=$checked = ($arSubj["ur-23"]) ? "checked" : "" ;?> name="ur-23" type="checkbox" /> �������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-24"]) ? "checked" : "" ;?> name="ur-24" type="checkbox" /> ����� �� ������������� ������</li>
								<li><input <?=$checked = ($arSubj["ur-25"]) ? "checked" : "" ;?> name="ur-25" type="checkbox" /> ������-������� ����������</li>
								<li><input <?=$checked = ($arSubj["ur-26"]) ? "checked" : "" ;?> name="ur-26" type="checkbox" /> ����� ������� �� �����</li>
								<li><input <?=$checked = ($arSubj["ur-27"]) ? "checked" : "" ;?> name="ur-27" type="checkbox" /> ������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-28"]) ? "checked" : "" ;?> name="ur-28" type="checkbox" /> Գ������� �����</li>
								<li><input <?=$checked = ($arSubj["ur-29"]) ? "checked" : "" ;?> name="ur-29" type="checkbox" /> ������� �����</li>
							</ul>
						</div>
						<div class="subjects-desc" id="smb6" style="display: none;">
							<ul>
								<li><input <?=$checked = ($arSubj["other"]) ? "checked" : "" ;?> name="other" type="checkbox" checked /> ���� �������� �� � ������ (�������������)</li>
							</ul>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				<div class="anketa-val sel1">
					<label style="margin:0 10px 0 0;">���������� �������� �<br /> ������� �������i�:</label>
					<select name="spam" class="niceSelect">
						<option value="1">T��</option>
						<option value="0">Hi</option>
					</select>
					<div class="clr"></div>
				</div>
				<a href="#" onclick="$(this).next().find('input').click(); return false;" class="anketa-bt">��������</a>
				<div style="display: none;"><input id="submitForm" type="submit" /></div>
				</form>
			</div>
			<div class="clr"></div>
		</div>
	</div>