<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

function strip_only_tags($str, $tags, $stripContent=false) {
    $content = '';
    if(!is_array($tags)) {
        $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
        if(end($tags) == '') array_pop($tags);
    }
    foreach($tags as $tag) {
        if ($stripContent)
             $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
         $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.$content.'#is', '', $str);
    }
    return $str;
}

//if($USER->IsAdmin()) echo "<pre>".print_r($arResult, true)."</pre>";
?>
	<table class="rounded">
	<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
	<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
	<tbody>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<tr class="head">
			<td colspan="2"><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" /></td>
		</tr>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<tr<?if($arParams["DISPLAY_PICTURE"]=="N" || !is_array($arResult["DETAIL_PICTURE"])):?> class="head"<?endif;?>>
			<td><span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span></td>
		</tr>
		<?endif;?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
			<tr<?if(($arParams["DISPLAY_PICTURE"]=="N" || !is_array($arResult["DETAIL_PICTURE"])) && ($arParams["DISPLAY_DATE"]=="N" || !$arResult["DISPLAY_ACTIVE_FROM"])):?> class="head"<?endif;?>>
				<td class="f"><?=GetMessage("IBLOCK_FIELD_NAME");?></td>
				<td><b><?=$arResult["NAME"]?></b></td>
			</tr>
		<?endif;?>
	<?/*if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
		<?if($arResult["NAV_RESULT"]):?>
			<tr<?if(($arParams["DISPLAY_PICTURE"]=="N" || !is_array($arResult["DETAIL_PICTURE"])) && ($arParams["DISPLAY_DATE"]=="N" || !$arResult["DISPLAY_ACTIVE_FROM"]) && ($arParams["DISPLAY_NAME"]=="N" || !$arResult["NAME"])):?> class="head"<?endif;?>>
				<td class="f"><?=GetMessage("IBLOCK_FIELD_DETAIL_TEXT");?></td>
				<td>
					<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
					<?echo $arResult["NAV_TEXT"];?>
					<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
				</td>
			</tr>
 		<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
			<tr<?if(($arParams["DISPLAY_PICTURE"]=="N" || !is_array($arResult["DETAIL_PICTURE"])) && ($arParams["DISPLAY_DATE"]=="N" || !$arResult["DISPLAY_ACTIVE_FROM"]) && ($arParams["DISPLAY_NAME"]=="N" || !$arResult["NAME"])):?> class="head"<?endif;?>>
				<td class="f"><?=GetMessage("IBLOCK_FIELD_DETAIL_TEXT");?></td>
				<td><?echo $arResult["DETAIL_TEXT"];?></td>
			</tr>
 		<?else:?>
			<tr<?if(($arParams["DISPLAY_PICTURE"]=="N" || !is_array($arResult["DETAIL_PICTURE"])) && ($arParams["DISPLAY_DATE"]=="N" || !$arResult["DISPLAY_ACTIVE_FROM"]) && ($arParams["DISPLAY_NAME"]=="N" || !$arResult["NAME"])):?> class="head"<?endif;?>>
				<td class="f"><?=GetMessage("IBLOCK_FIELD_DETAIL_TEXT");?></td>
				<td><?echo $arResult["PREVIEW_TEXT"];?></td>
			</tr>
		<?endif*/?>
		<?
		$arSection = $arResult["SECTION"]["PATH"][end(array_keys($arResult["SECTION"]["PATH"]))];
		if($arSection):
		?>
		<tr>
			<td class="f"><?=GetMessage("IBLOCK_FIELD_SECTION_ID")?></td>
			<td><a href="<?=str_replace(array("#SECTION_ID#","#SECTION_CODE#"),array($arSection["ID"],$arSection["CODE"]),$arParams["SECTION_URL"])?>"><?=$arSection["NAME"]?></a></td>
		</tr>
		<?endif;?>
		<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<tr>
				<td class="f"><?=GetMessage("IBLOCK_FIELD_".$code)?></td>
				<td><?=$value;?></td>
			</tr>
		<?endforeach;?>
		<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<tr>
				<td class="f"><?if(strlen(GetMessage("IBLOCK_PROP_".$pid))>0):?><?=GetMessage("IBLOCK_PROP_".$pid)?><?else:?><?=$arProperty["NAME"]?><?endif;?></td>
				<td><?if($arProperty["PROPERTY_TYPE"]=="S"&&$arProperty["ROW_COUNT"]>1):?><div class="a_lf"><?endif;?>
				<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
					<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
				<?else:?>
					<?if($arProperty["PROPERTY_TYPE"]=="S"&&$arProperty["ROW_COUNT"]>1):?><?=strip_only_tags(TxtToHtml($arProperty["DISPLAY_VALUE"],false),array("a"));?><?else:?><?=$arProperty["DISPLAY_VALUE"]?><?endif;?>
				<?endif?>
				<?if($arProperty["PROPERTY_TYPE"]=="S"&&$arProperty["ROW_COUNT"]>1):?></div><?endif;?></td>
			</tr>
		<?endforeach;?>
	</tbody>
	</table>