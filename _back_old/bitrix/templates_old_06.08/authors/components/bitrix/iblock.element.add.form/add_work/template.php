<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?><?
global $MESS;
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
?><?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
$pref = time();
?>

<div class="anketa">
	<form onsubmit="return submForm('<?=$pref?>');"
	  	id="addwork_form"
		class="addwork_form agent_form"
		name="addwork_form"
	  	action="<?= POST_FORM_ACTION_URI?>"
	  	method="post"
	 	enctype="multipart/form-data">
	<?if (count($arResult["ERRORS"])):?>
		<div class="alert a_big"><?=ShowError(implode("<br />", $arResult["ERRORS"]))?></div>
	<?endif?>
	<?if (strlen($arResult["MESSAGE"]) > 0):?>
		<div class="notetext"><?=ShowNote(str_replace("#ID#",$_SESSION["LAST_ORDER_ID"],$arResult["MESSAGE"]))?></div>
		<script type="text/javascript">
			window.location.href = "http://magistr.in.ua/authors/my_works/"
		</script>
	<?endif?>
	<?=bitrix_sessid_post()?>
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?>
		<input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
	<?endif?>

	<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):
		foreach ($arResult["PROPERTY_LIST"] as $propertyID):
			//echo $propertyID."<br>";

			if (intval($propertyID) > 0):
				$title = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]);
				$descr = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]."_DESCR");
				if(strlen($title)<=0):
					$title = $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
				endif;
			else:
				$title = !empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID);
				$descr = GetMessage("CUSTOM_TITLE_".$propertyID."_DESCR");
			endif;

			if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) $title.= ':<span>*</span>';

			$js_titles .= (strlen($js_titles)>0?", ":"")."n_".$pref."_".$propertyID.": '".addslashes($title)."'";

			if (intval($propertyID) > 0)
			{
				if (
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
					&&
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
				) {
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
				}
				elseif (
					(
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
						||
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
					)
					&&
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
				)
					$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
			}
			elseif (($propertyID == "TAGS") && CModule::IncludeModule('search')) {
				$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";
			}

			if($propertyID=="IBLOCK_SECTION") $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] = "N";
			if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
			{
				$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
				$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
			}
			else
			{
				$inputNum = 1;
			}

			if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"]) {
				$INPUT_TYPE = "USER_TYPE";
			}
			else {
				$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
			}

			switch ($INPUT_TYPE):
				case "USER_TYPE":
					for ($i = 0; $i<$inputNum; $i++)
					{
						if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
						{
							$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
							$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
						}
						elseif ($i == 0)
						{
							$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
							$description = "";
						}
						else
						{
							$value = "";
							$description = "";
						}
						echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
							array(
								$arResult["PROPERTY_LIST_FULL"][$propertyID],
								array(
									"VALUE" => $value,
									"DESCRIPTION" => $description,
								),
								array(
									"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
									"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
									"FORM_NAME"=>"iblock_add",
								),
							));
						?><br /><?
					}
					break;
				case "TAGS":
					$APPLICATION->IncludeComponent(
						"bitrix:search.tags.input",
						"",
						array(
							"VALUE" => $arResult["ELEMENT"][$propertyID],
							"NAME" => "PROPERTY[".$propertyID."][0]",
							"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
						), null, array("HIDE_ICONS"=>"Y")
					);
					break;
				case "HTML":
					$LHE = new CLightHTMLEditor;
					$LHE->Show(array(
						'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
						'width' => '100%',
						'height' => '200px',
						'inputName' => "PROPERTY[".$propertyID."][0]",
						'content' => $arResult["ELEMENT"][$propertyID],
						'bUseFileDialogs' => false,
						'bFloatingToolbar' => true,
						'bArisingToolbar' => true,
						'toolbarConfig' => array(
							'Bold', 'Italic', 'Underline', 'RemoveFormat',
							'CreateLink', 'DeleteLink', 'Image', 'Video',
							'BackColor', 'ForeColor',
							'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
							'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
							'StyleList', 'HeaderList',
							'FontList', 'FontSizeList',
						),
					));
					break;
				case "T":
					for ($i = 0; $i<$inputNum; $i++)
					{

						if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
						{
							$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
						}
						elseif ($i == 0)
						{
							$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
						}
						else
						{
							$value = "";
						}
						?>
						<? if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="work_prev"):?>
						<div class="anketa-val af_check2">
							<div style="float: left; width: 228px; height: 22px;"></div>
									<span class="niceCheck check2">
										<input type="checkbox" name="active">
									</span>
							<label for="active">��������� ������� ������ ��� ������������.</label><br>
							<i class="af_val2_i">��� ������ ������ ��������� ����-��� ������� ������.<br> �� ������ ������ ���������� ������� ���� ���� ������.<br> ��������� ���� �����. ������� �� ������� ������������ �� ������.</i>
									<textarea placeholder="..."
											  style="display: none;float: left;clear: left;margin-left: 230px;margin-top: 10px;"
											  name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=(strlen($value)>0?$value:'')?></textarea>
							<div class="clr"></div>
						</div>
						<?else:?>
						<div class="anketa-val">
							<label><?=strip_tags($title, '<span></span>')?></label>
							<textarea title="<?=strip_tags($title)?>"
									name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
									placeholder="..."><?=(strlen($value)>0?$value:'')?></textarea>
							<div class="clr"></div>
						</div>
						<?
						endif;
					}
					break;

				case "S":
				case "N":
					for ($i = 0; $i<$inputNum; $i++)
					{
						if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
						{
							$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
						}
						elseif ($i == 0)
						{
							$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

						}
						else
						{
							$value = "";
						}
						?>
						<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="date_make"):?>
						<div class="anketa-val">
							<label><?=strip_tags($title,'<span></span>')?></label>
							<select class="niceSelect s"
									name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
									title="<?=$title?>">
										<option value="">-������-</option>
									<?for($i=(date("Y"));$i>=1950;$i--):?>
										<option value="<?=$i?>"<?if($value==$i):?> selected="selected"<?endif;?>>
											<?=$i;?>
										</option>
									<?endfor;?>
							</select>
						<?else:?>
							<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="storinok"):?>
							<label style="width: 118px"><?=strip_tags($title,'<span></span>')?></label>
							<input title="<?=strip_tags($title)?>"
								   type="text"
								   class="short"
								   name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
								   value="<?=(strlen($value)>0?$value:'')?>"
								   placeholder="<?=strip_tags($title)?>"/>
							<div class="clr"></div>
						</div>
							<?else:?>
								<? if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="note"):?>
								<div class="anketa-val af_check2">
									<div style="float: left; width: 228px; height: 22px;"></div>
									<span class="niceCheck check1">
										<input type="checkbox" name="active">
									</span>
									<label for="active">������ ����i���</label> <i>(���� ����� ���� ���)</i>
									<textarea placeholder="..."
											  name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
											  style="height: 24px; display: none;float: left;clear: left;margin-left: 230px;margin-top: 10px;overflow: hidden;"><?=(strlen($value)>0?$value:'')?></textarea>
									<div class="clr"></div>
								</div>
								<? else:?>
									<div class="anketa-val">
										<label><?=strip_tags($title,'<span></span>')?></label>
										<input title="<?=strip_tags($title)?>"
											   type="text"
											   name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
											   value="<?=(strlen($value)>0?$value:'')?>"
											   placeholder="<?=strip_tags($title)?>"/>
										<div class="clr"></div>
									</div>
								<?endif;?>
							<?endif;?>

						<?endif;
					}
					break;

				case "F":
					for ($i = 0; $i<$inputNum; $i++)
					{
						$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
						?><div class="clear">&nbsp;</div><?=$descr;?><br />
						<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
						<input class="file" type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
						<?

						if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
						{
							?>
							<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
							<?

							if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
							{
								?>
								<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
								<?
							}
							else
							{
								?>
								<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
								<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
								[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
								<?
							}
						}
					}
					if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"]=="Y")
					{
						?><div><a href="javascript:;" onclick="addFileField(this,'property_fields_<?=$pref?>_<?=$propertyID?>','<?=$propertyID?>');"><?=GetMessage("IBLOCK_FORM_FILE_ADD")?></a></div><?
					}

					break;
				case "L":
					if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C") {
						$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
					}
					else {
						$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";
					}

					switch ($type):
						case "checkbox":
						case "radio":
							foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
							{
								$checked = false;
								if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
								{
									if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
									{
										foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
										{
											if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
										}
									}
								}
								else
								{
									if ($arEnum["DEF"] == "Y") $checked = true;
								}

								?>
								<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></label><br />
								<?
							}
							break;

						case "dropdown":
						case "multiselect":
							?>
							<div class="anketa-val wide">
								<label><?=strip_tags($title, '<span></span>')?></label>
								<select class="niceSelect s"
										title="<?=strip_tags($title)?>"
										name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"1\" multiple=\"multiple" : ""?>">
									<option value=""> -������- </option>
								<?
								if (intval($propertyID) > 0) {
									$sKey = "ELEMENT_PROPERTIES";
								}
								else {
									$sKey = "ELEMENT";
								}

								foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
								{
									$checked = false;
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
										{
											if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
										}
									}
									else
									{
										if ($arEnum["DEF"] == "Y") $checked = true;
									}
									?>
									<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></option>
									<?
								}
								?>
								</select>
								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]!="type"):?>
									<p><?= $descr;?></p>
								<?endif;?>

								<div class="clr"></div>
							</div>
							<?
							break;

					endswitch;
					break;
			endswitch;?>
		<?endforeach;?>
	<?endif?>
	<div class="clear">&nbsp;</div>
	<a href="http://magistr.in.ua/authors/my_works/" onclick="$(this).next().find(&#39;input&#39;).click(); return false;" class="anketa-bt">��������</a>
	<div style="display: none;"><input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"></div>

</form>
</div>
<script type="text/javascript">
	$(function(){
		$('.niceCheck.check1').toggle(
			function(){
				$(this).next().next().next().show();
			},
			function(){
				$(this).next().next().next().hide();
			}
		);
		$('.niceCheck.check2').toggle(
			function(){
				$(this).next().next().next().next().show();
			},
			function(){
				$(this).next().next().next().next().hide();
			}
		);
	});
</script>
<script type="text/javascript">
	function addFileField(caller,p_id,n_id) {
		var cnt = $("#"+p_id+" input:file").size();
		$(caller).parent().before('<div><input type="hidden" value="" name="PROPERTY['+n_id+']['+(cnt)+']"><input type="file" name="PROPERTY_FILE_'+n_id+'_'+(cnt)+'" size="30" class="file"></div>');
	}
</script>
<script>
	$(document).ready(function() {
		$('.niceSelect').selectbox();

		var currentYear = (new Date).getFullYear();
		$('.copy span').text(currentYear);
		$(":file").jfilestyle({icon:false, 'buttonText': '������� ����'});
		var select = $('.niceSelect');
		$(select).selectbox({
			onOpen: function (select) {
				$('.sbHolder').addClass('focus');
				$('.sbGroup').parent().addClass('sbGroup');
			},
			onClose: function (inst) {
				$('.sbHolder').removeClass('focus');
			},
			onChange: function (val, inst) {},
			effect: "slide"
		});

		$('.cont-bt > a').toggle(function(){
			$(this).parent().find('.cont-window').show();
			$(this).find('.carr').attr('src', 'images/arr-t.png');
			$('body').click(function(){
				$(this).parent().find('.cont-window').hide();
				$(this).find('.carr').attr('src', 'images/arr.png');
			});
		}, function() {
			$(this).parent().find('.cont-window').hide();
			$(this).find('.carr').attr('src', 'images/arr.png');
		});


		$('.send a').click(function(){
			var html = $('.send_box_hide').html();
			$(this).after(html);

			return false;
		});
		$('form select.s').change(function(e){
			e = e || event;
			var select = e.target;
			if($(select).val() != ''){
				$(this).parent().removeClass('sel01');
				$(this).parent().addClass('sel02');
			}else{
				$(this).parent().addClass('sel01');
				$(this).parent().removeClass('sel02');
			}
		});
		$('form input[type="submit"]').click(function(){
			setTimeout(function(){
				$('form select.s').each(function(){
					if($(this).attr('class')=='niceSelect s error'){
						$(this).parent().addClass('sel01');
						$(this).parent().removeClass('sel02');
					}
					else {
						$(this).parent().removeClass('sel01');
						$(this).parent().addClass('sel02');
					}
				});
			}, 100);
		});

		$("#addwork_form").validate({
			rules:{
				'PROPERTY[NAME][0]':{
					required: true,
					minlength: 3,
					maxlength: 150,
				},
				'PROPERTY[IBLOCK_SECTION]':{
					required: true,
				},
				'PROPERTY[7]':{
					required: true,
				},
				'PROPERTY[8][0]':{
					required: true,
				},
				'PROPERTY[9][0]':{
					required: true,
					minlength: 1,
					maxlength: 9,
				},
				'PROPERTY[11][0]':{
					required: true,
					minlength: 1,
					maxlength: 20,
				},
				'PROPERTY[10][0]':{
					required: true,
					minlength: 3,
					maxlength: 3000,
				},
			},
			messages:{
				'PROPERTY[NAME][0]':{
					required: "",
					minlength: "",
					maxlength: "",
				},
				'PROPERTY[IBLOCK_SECTION]':{
					required: "",
				},
				'PROPERTY[7]':{
					required: "",
				},
				'PROPERTY[8][0]':{
					required: "",
				},
				'PROPERTY[9][0]':{
					required: "",
					minlength: "",
					maxlength: "",
				},
				'PROPERTY[11][0]':{
					required: "",
					minlength: "",
					maxlength: "",
				},
				'PROPERTY[10][0]':{
					required: "",
					minlength: "",
					maxlength: "",
				},
			}
		});
	});
</script>