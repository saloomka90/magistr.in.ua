<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("���������� ������");
$APPLICATION->SetPageProperty("description", '���������� ������ - ��������� ������');
global $USER;
/*
 *
 */

$arResult = [];
if($_REQUEST['action'] == 'ajax_file' && check_bitrix_sessid()){
    $GLOBALS['APPLICATION']->RestartBuffer();
    header('Content-Type: application/json');
    
    $name = $_FILES["file"]['name'];
    
    $arParams = array("replace_space"=>"-","replace_other"=>"-", "safe_chars"=>['.']);
    
    $_FILES["file"]['name'] = Cutil::translit(mb_convert_encoding($_FILES["file"]['name'], "windows-1251", "utf-8"),"ru",$arParams);
    
    $fid = CFile::SaveFile($_FILES["file"], "tmp");
    
    if($fid){
        echo json_encode(['name'=>$name, 'id'=>$fid]);
    }else{
        echo json_encode(['error'=>'������� ������������ �����']);
    }
    exit();
}

if($_REQUEST['action'] == 'order_add' && check_bitrix_sessid()){
    
    
    
    
    
    $arSelect = Array("ID","IBLOCK_ID","NAME");
    $arFilter = Array("IBLOCK_ID"=>17, "PROPERTY_TITLE_SMAIL"=>$_REQUEST["institution"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    
    $arVNZ = $res->Fetch();
    
    if($arVNZ["NAME"] == $_REQUEST["input_institution"]){
        $arVNZ = $arVNZ["ID"];
    }else{
        
        $arSelect = Array("ID","IBLOCK_ID","NAME");
        $arFilter = Array("IBLOCK_ID"=>17, "PROPERTY_TITLE_SMAIL"=>'����');
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        
        $arVNZ = $res->Fetch()["ID"];
    }
    
    
    
    
    
    $arTypes = [];
    $arSelect = Array("NAME","ID");
    $arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arTypes[$arFields["NAME"]] = $arFields["ID"];
        
    }
    
    
    $arInst = [];
    $arSelect = Array("NAME","ID");
    $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arInst[$arFields["NAME"]] = $arFields["ID"];
        
    }
    
    
    $PROP = array();
    
    if(!empty($_REQUEST["file_src"])){
        
        $PROP['file'] = $_REQUEST["file_src"];
        
        /*foreach($_FILES["file"]["name"] as $k=>$name){
         if(empty($name))continue;
         $PROP['file'][] = ['name'=>$name, "size"=>$_FILES["file"]["size"][$k], "tmp_name"=>$_FILES["file"]["tmp_name"][$k], "type"=>$_FILES["file"]["type"][$k]];
         }*/
    }
    
    
    $PROP["18"]= $_REQUEST["email"]; //email
    $PROP["19"]= '+'.preg_replace('~[^0-9]+~','',$_REQUEST["tel"]); //tel
    $PROP["23"]= $_REQUEST["themes"]; //tema
    $PROP["74"]= $_REQUEST["plagiarism"]; //PLAGIAT
    $PROP["75"]= $_REQUEST["originality"]; //PLAGIAT
    $PROP["26"]= $_REQUEST["countPage"]; //storinok
    $PROP["27"]= $_REQUEST["datepicker"]; //termin
    $PROP["30"]= $_REQUEST["input_institution"]; //organ
    $PROP["33"]= $_REQUEST["requirements"]; //termin
    
    $PROP["MIN_CALC_CUMM"]= $_REQUEST["min_summ"]; //termin    
    $PROP["MAX_CALC_CUMM"]= $_REQUEST["max_summ"]; //termin
    
    $PROP["VNZ"]= $arVNZ;
    
    $PROP["ORGAN"]= $_REQUEST["input_institution"];
    
    
    
    if($_REQUEST["subject"]){
        
        
        $PROP["60"]= $arInst[$_REQUEST["subject"]]?$arInst[$_REQUEST["subject"]]:$_REQUEST["subject"];
        //$PROP["44"] = $arSectToCode[$arElementsToSection[$_REQUEST["subject"]]]; // Old
        
    }else{
        $PROP["62"]= $_REQUEST["input_subject"];
        $PROP["60"]="51097";
        $PROP["44"] = "35";
    }
    
    $PROP["22"] = $_REQUEST["input_subject"]; // Old
    
    if($_REQUEST["type"] == $_REQUEST["input_type"]){
        $PROP["61"]= $arTypes[$_REQUEST["type"]]?$arTypes[$_REQUEST["type"]]:$_REQUEST["type"];
        $PROP["24"]= $_REQUEST["type"]; // Old
        
    }else{
        $PROP["63"]= $_REQUEST["input_type"];
        $PROP["61"] = "50943";
        $PROP["24"]= $_REQUEST["input_type"]; // Old
        
    }
    
    $PROP["CURS"]= $_REQUEST["course"];
    
    $PROP["USER_ID"]= $USER->GetID();
    
    
    
    $arLoadProductArray = Array(
        'IBLOCK_ID' => 6,
        'PROPERTY_VALUES' => $PROP,
        'NAME' => htmlspecialcharsEx($_REQUEST["name"]),
        'ACTIVE' => 'N', // �������
        'DATE_ACTIVE_FROM' => date("d.m.Y H:i:s"),
    );
    
    $el = new CIBlockElement;
    
    if(!$orderId=$el->Add($arLoadProductArray)){
        die();
    }
    
    
    
    $fields = Array(
        "NAME" => $_REQUEST["name"],
        "PERSONAL_PHONE" => STR_REPLACE(['-',')','(',' '],'',$_REQUEST["tel"]),
        "UF_VNZ"  => $_REQUEST["input_institution"],
        "UF_KURS" => $_REQUEST["course"],
    );
    if($USER->Update($USER->getID(), $fields)){
        unset($_SESSION["USER_NEW_ORDER"]);
        header("Location: /client/", true, 301);
    }
    
    
}

/*
 *
 */
if(!CModule::IncludeModule("iblock"))die();

echo "<script>";

$by = "ID";
$order = "desc";
$filter = Array("GROUPS_ID" => Array(12), "ID" => $USER->GetID());
$rsUsers = CUser::GetList(($by), ($order), $filter, ['SELECT'=>['UF_*']]);
if(!$arResult['USER'] = $rsUsers->Fetch()){
    die();
}

echo "var types_p = {};";
//echo '<pre>';
$arTypes = [];
$arSelect = Array("NAME","PROPERTY_TITLE_RU","PROPERTY_PRICE",
    "PROPERTY_KOEF",
    "PROPERTY_DPZF0_9",
    "PROPERTY_DPZT0_9",
    "PROPERTY_DPZF0_95",
    "PROPERTY_DPZT0_95",
    "PROPERTY_DPZF1",
    "PROPERTY_DPZT1",
    "PROPERTY_DPZF1_1",
    "PROPERTY_DPZT1_1",
    "PROPERTY_DTF1_2",
    "PROPERTY_DTT1_2",
    "PROPERTY_DTF1_1",
    "PROPERTY_DTT1_1",
    "PROPERTY_DTF1_05",
    "PROPERTY_DTT1_05",
    "PROPERTY_DTF1",
    "PROPERTY_DTT1",
    "PROPERTY_DTF0_95",
    "PROPERTY_DTT0_95",
    "PROPERTY_DPZF1_2",
    "PROPERTY_DPZT1_2",
    "PROPERTY_MID_COUNT",
    "PROPERTY_UMAX",
    "PROPERTY_UMIN",
    "PROPERTY_UBASE",
);



function safe_json_encode($value, $options = 0, $depth = 512){
    $encoded = json_encode($value, $options, $depth);
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            return $encoded;
        case JSON_ERROR_DEPTH:
            return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_STATE_MISMATCH:
            return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_CTRL_CHAR:
            return 'Unexpected control character found';
        case JSON_ERROR_SYNTAX:
            return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_UTF8:
            $clean = utf8ize($value);
            return safe_json_encode($clean, $options, $depth);
        default:
            return 'Unknown error'; // or trigger_error() or throw new Exception()
            
    }
}

function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}



$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arTypes[$arFields["NAME"]] = $arFields["PROPERTY_TITLE_RU_VALUE"];
    echo "types_p['".$arFields["NAME"]."'] = '".safe_json_encode($arFields)."';";
    
}


echo "var predmet_p = {};";
$arPredmet = [];
$arSelect = Array("NAME","PROPERTY_TITLE_RU","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arPredmet[$arFields["NAME"]] = $arFields["PROPERTY_TITLE_RU_VALUE"];
    echo 'predmet_p["'.$arFields["NAME"].'"] = '.($arFields["PROPERTY_KOEF_VALUE"]?$arFields["PROPERTY_KOEF_VALUE"]:1).';';
}

echo "var kurs_p = {};";
$arKurs = [];
$arSelect = Array("NAME","SORT","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arKurs[$arFields["NAME"]] = $arFields["SORT"];
    echo 'kurs_p["'.$arFields["SORT"].'"] = '.($arFields["PROPERTY_KOEF_VALUE"]?$arFields["PROPERTY_KOEF_VALUE"]:1).';';
    
}


echo "var vnz_p = {};";
$arVNZ = [];
$arSelect = Array("ID","NAME","PROPERTY_TITLE_RU","PROPERTY_TITLE_SMAIL","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arVNZ[$arFields["NAME"]] = $arFields;
    echo 'vnz_p["'.str_replace('"','\"',$arFields["PROPERTY_TITLE_SMAIL_VALUE"]).'"] = '.($arFields["PROPERTY_KOEF_VALUE"]?$arFields["PROPERTY_KOEF_VALUE"]:1).';';
    
}


echo "var prov_p = {};";

$arSelect = Array("ID","NAME","PROPERTY_FROM","PROPERTY_TOP","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>18, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    echo 'prov_p["'.$arFields["PROPERTY_FROM_VALUE"].'-'.$arFields["PROPERTY_TOP_VALUE"].'"] = '.($arFields["PROPERTY_KOEF_VALUE"]?$arFields["PROPERTY_KOEF_VALUE"]:0.9).';';
}

echo "</script>";
/*var_dump($arPredmet);
 die();*/


$arResult["TYPES"] = $arTypes;
$arResult["PREDMET"] = $arPredmet;
$arResult["KURS"] = $arKurs;
$arResult["VNZ"] = $arVNZ;




/*
 *
 */





?>

<div class="title">���������� ������</div>
    <form action="/client/order/" class="application-form" method="post" enctype="multipart/form-data">
    	<?=bitrix_sessid_post()?>
    	<input type="hidden" name="action" value="order_add">
        <div class="form-item">
            <div class="form-item__label">��� ������: <span>*</span></div>
            <select data-placeholder="�������, �������, ������������" id="select-type" name="type">
                <option value=""></option>
                <?foreach($arResult["TYPES"] as $ua=>$ru): ?>
                <option data-ru="<?=$ru ?>" value="<?=$ua ?>"><?=$ua ?></option>
                <?endforeach; ?>
            </select>
        </div>
        <div class="form-item">
            <div class="form-item__label">����: <span>*</span></div>
            <input type="text" name="themes" value="<?=str_replace('"','\'',$_SESSION["USER_NEW_ORDER"]['tema']) ?>">
        </div>
        <div class="form-item">
            <div class="form-item__label">�������: <span>*</span></div>
            <select data-placeholder="����������, �������, ������..." id="select-subject" name="subject">
                <option value=""></option>
                <?foreach($arResult["PREDMET"] as $ua=>$ru): ?>
                <option data-ru="<?=$ru ?>" value="<?=$ua ?>"><?=$ua ?></option>
                <?endforeach; ?>
            </select>
        </div>
        <div class="form-item">
            <div class="form-item__label">�����: <span>*</span></div>
            <label class="c-datepicker">
                <input type="text" class="datepicker-i" readonly name="datepicker">
            </label>                       
        </div>
        <div class="form-item">
            <div class="form-item__label">ʳ������ �������:</div>
            <div class="form-item__range">
                <div class="form-item__range-input">
                    <input class="i-count-page" type="text" value="1" readonly>
                </div>
                <div class="form-item__range-slider">
                    <input type="text" class="count-page" name="countPage" value=""/>
                </div>
            </div>
        </div>
        <? /* ?><div class="form-item">
            <div class="form-item__label">
                �������� �� ������: 
                <div class="info"> 
                    <div class="info__icon info__icon_plagiarism"></div>
                    <div class="info__baloon info__baloon_plagiarism">
                        <div class="info__text">���� � ������ ��ǳ ������ ������ ��������� �� ������, �� ���������� ������ ���� ������� ���� ������� ������������ ������</div>
                        <div class="info__close">
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
                        </div>
                    </div>
                </div>
            </div>
            <select name="plagiarism" id="select-plagiarism">
                <option value="">�������</option>
                <option value="yes" selected>���, �������� ����</option>   
                <option value="no">��� ��������</option>
            </select>
            <input type="text" class="plagiarism-i" name="plagiarismi">
        </div> <? */ ?>
        <input type="hidden" class="plagiarism-i" name="plagiarism" value="yes">
        
        <div class="form-item form-item_originality" style="display:block">
            <div class="form-item__label">������������ ������:
             <div class="info"> 
                    <div class="info__icon info__icon_plagiarism"></div> 
                    <div class="info__baloon info__baloon_plagiarism">
                        <div class="info__text">
							���� ���������� ������ �� ������ � ��� ����� ������ � ��������� ������ ���� ����������� ���� �����������. ��� �� ������ �� ������ �������� � ������ ��������� ��������.
                            <br>    �������� �����. 0% ����������� ������ ������, �� ������ �������� ������� � ���������.
                              <br><br>  
                                ������ ��� ��������� ������������ ������� ��� ������ ����������.
							
						</div>
                        <div class="info__close">
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-item__range">
                <div class="form-item__range-input">
                    <input class="i-originality" name="originality" type="text" value="30%" readonly>
                </div>
                <div class="form-item__range-slider">
                    <input type="text" class="originality"  value=""/>
                </div>
            </div>
        </div>
        <div class="form-item">
            <div class="form-item__label">������:</div>
            <textarea placeholder="����, ��������, ������� ������ �� ������. ��� ��������� ����'������ �������� ���������." name="requirements"></textarea>
        </div>
        <div class="form-item">
            <div class="form-item__label form-item__label_file">��������� ����:</div>
            
            
            <div class="file-list">
                <!-- <div class="file-list__item" style="display:none;">
                    <div class="file-list__name"></div>
                    <div class="file-list__status"><a href="#" class="remove-file"></a></div>
                     <input type="hidden" name="file_src[]">
                </div>
                <div class="file-list__item">
                    <div class="file-list__name">Page2_kursova_istoriya_12.jpg</div>
                    <div class="file-list__status"><div id="circle2"></div></div>
                </div> -->
                <div class="file-list__add-item">
                    <a href="#" class="link-style add-file" id="add-file" data-file="#file"><span>�������</span></a>
                    <div class="need-tutorial">������� ���������!</div>
                    <input type="file" multiple="multiple" name="file[]" id="file" class="file-input order" multiple >
                </div>
            </div>
            
            
        </div>
        <div class="contact-information contact-information_mt"> 
            <div class="contact-information__title">��������� ����������</div>
            <div class="contact-information__desc">������������ 1 ��� ���� ��� ������� ����������</div>
            <div class="form-item">
                <div class="form-item__label">��'�: <span>*</span></div>
                <input type="text" name="name" value="<?=$arResult['USER']["NAME"] ?>">
            </div>
            <div class="form-item">
                <div class="form-item__label">�������: <span>*</span></div>
                <input type="tel" name="tel" placeholder="+38 (___) ___-__-__" value="<?=$arResult['USER']["PERSONAL_PHONE"] ?>">
            </div> 
            <div class="form-item form-item_phone-disabled">
                <div class="form-item__label">E-mail: <span>*</span></div>
                <input type="email" name="f-email" value="<?=$arResult['USER']["EMAIL"]?>" disabled>
                <input type="hidden" name="email" value="<?=$arResult['USER']["EMAIL"]?>" >
                <div class="info__baloon info__baloon_phone">
                    <div class="info__text">��� ������ e-mail, <br>�������� ���������</div>
                    <div class="info__close">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
                    </div>
                </div>
            </div>
            <div class="form-item">
                <div class="form-item__label">��� ���: <span>*</span></div>
                <select data-placeholder="������� ����� (���������)..." id="select-institution" name="institution">
                    <option value=""></option>
                    <? foreach($arResult["VNZ"] as $vnz): ?>
                    	<option data-ru='<?=$vnz["PROPERTY_TITLE_RU_VALUE"] ?>' data-ua='<?=$vnz["NAME"] ?>' value='<?=$vnz["PROPERTY_TITLE_SMAIL_VALUE"] ?>' <?if($arResult['USER']["UF_VNZ"] == $vnz["NAME"])echo 'selected'; ?>><?=$vnz["NAME"] ?></option>
                    <?endforeach; ?>
                </select>
                <script>
					var input_institution = '<?=$arResult['USER']["UF_VNZ"] ?>';
                </script>
            </div>
            <div class="form-item">
                <div class="form-item__label">����: <span>*</span></div>
                <select name="course" id="select-course">
                    <option value="�������" <?if(empty($arResult['USER']["UF_KURS"]))echo 'selected'; ?>>�������</option>
                    
                    <?
                    
                    
                    $yer = getYerVik();
                    
                    
                   
                    ?>
                    <?foreach($arResult["KURS"] as $name=>$id): ?>
                    	<option value="<?=$id ?>" <?if($arResult['USER']["UF_KURS"] == $id){echo 'selected';$cursval = $name.' '.$yer;} ?>><?=$name ?> <?=$yer ?></option>
                    <?endforeach; ?>
                    
                </select>
                <input type="text" class="course-i" name="coursei">
            </div>
        </div>
        <div class="approximate-cost">
            <div class="approximate-cost__title">
                ��������� �������
                <div class="info">
                    <div class="info__icon"></div>
                    <div class="info__baloon info__baloon_cost">
                        <div class="info__text">������� ������ � ����� ����� �������� �� ��������� ����.  <br>��� �������� ����� ���� - �������� ������</div>
                        <div class="info__close">
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="approximate-cost__price">0 - 100 <span>���</span></div>
        </div>
        <input type="hidden" name="min_summ" value="0">
        <input type="hidden" name="max_summ" value="0">
        <button type="submit" class="button">³��������</button>
    </form>
    
    
    
    <div class="pop-background pop-pop-2">
        <div class="pop pop-scroll">
            <div class="pop__content pop__content_left">
                <div class="pop__title">�� �� ��������� ���������!</div>
                <div class="pop__desc">���� ����� ������������ ������ ����� ����������� �����.</div>
                <div class="pop__title">�������� �����!</div>
                <div class="pop__desc">������� ������ ���� ����� ����������� ����� ����������� �����. ���� � ������ ��������� ������ ������ ������, �� ������� ������ ���� ����� ������</div>
                <div class="pop__back-ok">
                    <a href="#" class="button button_gray js-exiter1">�����</a>
                    <a href="#" class="button js-exiter2"> ����������</a>
                </div>
            </div>
            <div class="close">
                <svg width="23" height="23" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
            </div>
        </div>
    </div>
    
    
    
    <?if(!empty($_SESSION["USER_NEW_ORDER"]) && $_SESSION["USER_NEW_ORDER"]["SHOW"] != "Y"): ?>
    <div class="pop-background pop-success">
        <div class="pop pop-scroll">
            <div class="pop__content">
                <div class="pop__title pop__title_success">�� ������ ��������������</div>
                <div class="pop__desc">������ ��� ����� <br>��������� �� ��� e-mail (<strong><?=$_SESSION["USER_NEW_ORDER"]["email"] ?></strong>)</div>
                <a href="#" class="button button_continue js-close">����������</a>
            </div>
            <div class="close">
                <svg width="23" height="23" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
            </div>
        </div>
    </div>
    <?$_SESSION["USER_NEW_ORDER"]["SHOW"] = 'Y'; ?>
    <?endif; ?>
    
    <script>
							var input_type = '<?=$_SESSION["USER_NEW_ORDER"]["input_tip"]?>';
							var input_institution = '<?=$_SESSION["USER_NEW_ORDER"]["input_institution"]?$_SESSION["USER_NEW_ORDER"]["input_institution"]:$arResult['USER']["UF_VNZ"] ?>';

							var coursei = '<?=$cursval?$cursval:'�������' ?>';
								
							
						</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>