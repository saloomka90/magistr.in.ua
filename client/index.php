<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");




/*
 * 
 */
$IBLOCK_ID = 6 ;
$arResult = [];
global $USER;




if($_REQUEST['action'] == 'ajax_file' && check_bitrix_sessid()){
    $GLOBALS['APPLICATION']->RestartBuffer();
    header('Content-Type: application/json');
    
    $name = $_FILES["file"]['name'];
    
    $arParams = array("replace_space"=>"-","replace_other"=>"-", "safe_chars"=>['.']);
    
    $_FILES["file"]['name'] = Cutil::translit(mb_convert_encoding($_FILES["file"]['name'], "windows-1251", "utf-8"),"ru",$arParams);
    
    $fid = CFile::SaveFile($_FILES["file"], "tmp");
    
    if($fid){
        echo json_encode(['name'=>$name, 'id'=>$fid]);
    }else{
        echo json_encode(['error'=>'������� ������������ �����']);
    }
    exit();
}

/*
 * LIST
 */
if(empty($_REQUEST["order_id"])):

$APPLICATION->SetTitle("�� ����������");
$APPLICATION->SetPageProperty("description", '�� ���������� - ��������� ������');
/** */

//������ �� ���� ����������� �����
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();

//var_dump($arUser["DATE_REGISTER"]);die();

$arSelect = Array("ID", "IBLOCK_ID", "ACTIVE","DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_state","PROPERTY_topic","PROPERTY_subject","PROPERTY_type","PROPERTY_sum_total");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "PROPERTY_USER_ID"=>$USER->GetID(),">DATE_CREATE"=>$arUser["DATE_REGISTER"]);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch()){
    
    if(in_array($arFields["PROPERTY_STATE_ENUM_ID"],[22,24])){
        $arResult['ARCHIVE'][] = $arFields;
    }else{
        $arResult['ITEMS'][] = $arFields;
    }
    
    
}

if(count($arResult['ITEMS']) == 0):
?>
 <div class="order-empty">
    <div class="order-empty__icon"></div>
    <div class="order-empty__text">� ��� ���� �������� ���������...</div>
    <a href="/client/order/" class="button button_new-order">+   ���� ����������</a>
</div>
<?
else:
?> 


<?//echo '<pre>';var_dump($arResult); ?>


    <div class="order-list">
    <?foreach($arResult['ITEMS'] as $item): ?>
        <div class="order">
            <a href="/client/?order_id=<?=$item["ID"] ?>" class="order__title"><?=$item["PROPERTY_TOPIC_VALUE"] ?></a>
            <div class="order__category">ID:<strong><?=$item["ID"] ?></strong>, <?=$item["PROPERTY_TYPE_VALUE"] ?> / <?=$item["PROPERTY_SUBJECT_VALUE"] ?></div>
            <div class="order__row">
            	<?
            	if($item["ACTIVE"] == 'N'){
            	    echo '<div class="order__status order__status_moderation">�� ���������</div>';
            	}else{
                	switch($item["PROPERTY_STATE_ENUM_ID"]){
                	    case 21:
                	        echo '<div class="order__status order__status_looking">������ ������</div><div class="order__price">
                                <div class="order__price_dotted">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>';
                	        break;
                	    /*case 22:
                	        echo '<div class="order__status order__status_progress">����������</div>';
                	        break;*/
                	    case 23:
                	        echo '<div class="order__status order__status_progress">����������</div>
                                    <div class="order__price">
                                        <div class="order__price-label">ֳ��: </div>
                                        <div class="order__price-number">'.$item["PROPERTY_SUM_TOTAL_VALUE"].' &#8372;</div>
                                    </div>
                                 ';
                	        break;
                	    /*case 24:
                	        echo '<div class="order__status order__status_progress">����������</div>';
                	        break;*/
                	}
            	}
            	?>
            </div>
            <div class="order__bottom">
                
                <div class="order__link">
                	<?$new_cnt =  CIBlockElement::GetList(Array("ID"=>"DESC"), ["IBLOCK_ID"=>19,'PROPERTY_TASKID'=>$item["ID"],'!PROPERTY_NO_READ' => false, 'PROPERTY_USER_ID'=>false], false, false, ['ID'])->SelectedRowsCount(); ?>
                    
                    
                    <a href="/client/?order_id=<?=$item["ID"] ?>#chat" class="order__message <?if($new_cnt == 0) echo' order__message_empty';?>">�����������<?if($new_cnt>0)echo ' ('.$new_cnt.')'; ?></a>
                   <!-- <a href="#" class="order__file order__file_empty">�����</a> -->
                </div>
               
            </div>
        </div>
    <?endforeach; ?>
        
    </div>
    <a href="/client/order" class="button button__new-order">+   ���� ����������</a>

<?
endif;
?>  

<!-- ARCHIVE -->
<?if(!empty($arResult['ARCHIVE'])): ?>
<div class="archive">
    <div class="content content_main">
        <div class="archive__title">����� ���������:</div>
        <div class="archive__list">
        	<?foreach($arResult['ARCHIVE'] as $item): ?>
            <div class="archive__item">
                <a href="/client/?order_id=<?=$item["ID"] ?>" class="archive__name"><?=$item["PROPERTY_TOPIC_VALUE"] ?></a>
                <div class="archive__info"><?=$item["PROPERTY_TYPE_VALUE"] ?>, <?=date('Y',strtotime($item["DATE_ACTIVE_FROM"])); ?> �</div>
                <?
                switch($item["PROPERTY_STATE_ENUM_ID"]){
                     case 22:
                     echo '<div class="archive__status">���������</div>';
                     break;
                     case 24:
                     echo '<div class="archive__rating"><div class="rating-archive" data-rating="0"></div></div>';
                     break;
                }
                ?>
            </div>
            <?endforeach; ?>
        </div>
    </div>
</div>
<?endif; ?>
<?
/*
 * ENDLIST
 */

else:
/*
 * �������� �� �����
 */


$arKurs = [];
$arSelect = Array("NAME","SORT","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arKurs[$arFields["SORT"]] = $arFields["NAME"];
}



$arVNZ = [];
$arSelect = Array("ID","NAME","PROPERTY_TITLE_RU","PROPERTY_TITLE_SMAIL","PROPERTY_KOEF");
$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch())
{
    $arVNZ[$arFields["ID"]] = $arFields["PROPERTY_TITLE_SMAIL_VALUE"];
}





$arSelect = Array("ID", "IBLOCK_ID", "ACTIVE","DATE_CREATE", "NAME", "DATE_ACTIVE_FROM","PROPERTY_file","PROPERTY_organ","PROPERTY_VNZ","PROPERTY_CURS","PROPERTY_task","PROPERTY_PLAGIAT","PROPERTY_ERCENT_PLAGIAT","PROPERTY_state","PROPERTY_topic","PROPERTY_subject","PROPERTY_type","PROPERTY_sum_total","PROPERTY_pages","PROPERTY_period");
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "PROPERTY_USER_ID"=>$USER->GetID(),"ID"=>$_REQUEST["order_id"]);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
if($arFields = $res->Fetch()){
    
    
    $arResult['ITEM'] = $arFields;
    
    
    $pres = CIBlockElement::GetList(Array('ID'=>'ASC'), ["IBLOCK_ID"=>12, "PROPERTY_ORDER_ID"=>(int)$arFields["ID"], 'ACTIVE'=>"Y"], false, false, ["IBLOCK_ID","ID","PROPERTY_ADD_PRICE"]);
    while($arFields = $pres->Fetch()){
        $arResult['ITEM']["PRICE"]+=(float)$arFields["PROPERTY_ADD_PRICE_VALUE"];
    }
    
    
    
    //echo '<pre>';var_dump($arResult['ITEM']);die();
    $APPLICATION->SetTitle("���������� �".$arResult['ITEM']["ID"]);
    $APPLICATION->SetPageProperty("description", '�������� ���������� �� ���������� �'.$arResult['ITEM']["ID"]);
?>

    <div class="back-row">
        <a href="/client/" class="back-link">�����</a>
    </div>
    <div class="single-order">
        <div class="single-order__title"><?=$arResult['ITEM']["PROPERTY_TOPIC_VALUE"] ?></div>
        <div class="single-order__row">
            <div class="single-order__id">ID:  <strong><?=$arResult['ITEM']["ID"] ?></strong></div>
            <a href="#" class="single-order__show-detal"><span>�������� ����� ����������</span></a>
        </div>
        <div class="detal" <?if(( $arResult['ITEM']["ACTIVE"] == 'N' && $arResult['ITEM']["PROPERTY_STATE_ENUM_ID"] != 22 ) || ( $arResult['ITEM']["ACTIVE"] == 'Y' && $arResult['ITEM']["PROPERTY_STATE_ENUM_ID"] == 21))echo ' style="display:block"'; ?>> 
            <ul class="detal__info">
                <li>
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/type.svg" alt="" width="24" height="24"></figure>                   
                    <?=$arResult['ITEM']["PROPERTY_TYPE_VALUE"] ?>  
                </li>
                <li>
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/subject.svg" alt="" width="22" height="22"></figure>
                    <?=$arResult['ITEM']["PROPERTY_SUBJECT_VALUE"] ?>
                </li>
                <li>                	
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/institution.svg" alt="" width="22" height="22"></figure>
                    
                    <?=(isset($arVNZ[$arResult['ITEM']["PROPERTY_VNZ_VALUE"]]) && $arVNZ[$arResult['ITEM']["PROPERTY_VNZ_VALUE"]] != '����')?$arVNZ[$arResult['ITEM']["PROPERTY_VNZ_VALUE"]]:$arResult['ITEM']["PROPERTY_ORGAN_VALUE"] ?><?if(!empty($arResult['ITEM']["PROPERTY_CURS_VALUE"])): ?>, <?=$arKurs[$arResult['ITEM']["PROPERTY_CURS_VALUE"]].' ����'; endif;?>
                </li>
            </ul>
            <ul class="detal__info-2">
                <li>
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/time.svg" alt="" width="22" height="22"></figure>
                    �����: <span><?=$arResult['ITEM']["PROPERTY_PERIOD_VALUE"] ?></span>
                </li>
                <li>
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/forms.svg" alt="" width="20" height="20"></figure>
                    �������: <span><?=$arResult['ITEM']["PROPERTY_PAGES_VALUE"] ?></span>
                </li>
                <?if($arResult['ITEM']["PROPERTY_PLAGIAT_VALUE"] == 'yes'): ?>
                <li>
                    <figure><img src="<?=SITE_TEMPLATE_PATH?>/images/verified.svg" alt="" width="20" height="20"></figure>
                    �������������: <span>min <?=$arResult['ITEM']["PROPERTY_ERCENT_PLAGIAT_VALUE"] ?></span>
                </li>
                <?endif; ?>
            </ul>
            <div class="detal__content">
                <?=str_replace(PHP_EOL,'<br>',$arResult['ITEM']["PROPERTY_TASK_VALUE"]) ?>
            </div>
            
            <?if(!empty($arResult['ITEM']["PROPERTY_FILE_VALUE"])): ?>
            <a href="/zipfiles_client.php?oid=<?=$arResult['ITEM']["ID"]?>" class="link-style link-style_download"><span>����������� ����������� ����</span></a>
            <?endif; ?>
            
        </div>
        <div class="single-order__row-2">
        	<?
        	if($arResult['ITEM']["ACTIVE"] == 'N'){
        	    if($arResult['ITEM']["PROPERTY_STATE_ENUM_ID"] == 22){
        	        echo '<div class="single-order__status order__status_cancel">���������� ���������</div>';
        	    }else{
        	        echo '<div class="single-order__status order__status_moderation">�� ���������</div>';
        	    }
        	}else{
        	    switch($arResult['ITEM']["PROPERTY_STATE_ENUM_ID"]){
                     case 21:
                         echo '<div class="single-order__status order__status_looking">������ ������</div>';
                         break;
                     case 22:
                         echo '<div class="single-order__status order__status_cancel">���������� ���������</div>';
                         break;
                     case 23:
                         echo '<div class="single-order__status order__status_progress">���������� ����������</div>';
                         break;
                     case 24:
                         echo '<div class="single-order__status order__status_success">���������� ������</div>';
                         break;
                }
        	}
            ?>
                
            
             <?/* ?><a href="#" class="link-style link-style_red js-cancel-order"><span>���������</span></a><?*/ ?>
        </div>
        
        <div class="single-order__paid">
            <div class="single-order__paid-line-1">��������: <strong><?=(float)$arResult['ITEM']["PRICE"] ?></strong> �� <strong><?=(float)$arResult['ITEM']["PROPERTY_SUM_TOTAL_VALUE"] ?></strong> ���</div>
        </div>
        <? /*?>
         <div class="single-order__file single-order__file-empty">
            <div class="single-order__file-top">
                <span>�����:</span>
                ����� ���� �� �������
            </div>
        </div>
        <?*/ ?>
    </div>
    
    <div class="chat">
    <a name="chat"></a>
        <div class="chat__title">��� � ����������:</div>
        <div class="chat__c-body">
        <div class="chat__body">
        
        	 <? 
	                    //������� ������
	                    $arSelect = Array("ID", "DATE_ACTIVE_FROM", "PREVIEW_TEXT","PROPERTY_NO_READ","PROPERTY_USERNAME","PROPERTY_MOREFILE","PROPERTY_USER_ID","PROPERTY_DATE");
	                    $arFilter = Array("IBLOCK_ID"=>IntVal(19), /*"PROPERTY_WID"=>$order_id, */"ACTIVE"=>"Y","PROPERTY_TASKID"=>intval($_REQUEST["order_id"]));
	                    $res = CIBlockElement::GetList(Array("DATE_ACTIVE_FROM"=>"ASC"), $arFilter, false, false, $arSelect);
	                    while($obs = $res->Fetch())
	                    {
	                    	if($obs["ID"]==$lastid)continue;
	                    	$lastid = $obs["ID"];
	                    	
	                    	$thisusrid = $USER->GetID();
	                    	
	                    	if($obs["PROPERTY_USER_ID_VALUE"] == $thisusrid){

								
							?>
							
							
							
            
            
            
								
	                            
                            <div class="chat__message chat__message_user">
                                <div class="chat__message-info">�: <span><?=$DB->FormatDate($obs["DATE_ACTIVE_FROM"], FORMAT_DATETIME, "DD.MM.YYYY HH:MI");?></span></div>
                                <div class="chat__message-text">
                                <?=str_replace(PHP_EOL,'<br>',$obs["PREVIEW_TEXT"])?>
                             	<?
								if(
									(is_array($obs["PROPERTY_MOREFILE_VALUE"]) && (count($obs["PROPERTY_MOREFILE_VALUE"])>0)) || 
									(!is_array($obs["PROPERTY_MOREFILE_VALUE"]) && ($obs["PROPERTY_MOREFILE_VALUE"]!=''))
								):
								if($obs["PREVIEW_TEXT"]!='')echo'<br>';
    								$iterator = CIBlockElement::GetPropertyValues(19, array('ID' => $obs["ID"]), true, ["ID"=>132]);
    								while ($row = $iterator->Fetch())
    								{    								    
    								    foreach($row[132] as $fl):
    								    
    								    //var_dump();die();
    								    ?>
    								    <a href="<?=CFile::GetPath($fl); ?>" download class="doc-link"><?=CFile::GetFileArray($fl)['ORIGINAL_NAME'] ?></a><br>    								    
    								    <?
    								    endforeach;
    								}
    								
								endif;?>
                                </div>
                            </div>
							<? 
							}else{

									if($obs['PROPERTY_NO_READ_VALUE']!='' and $obs["PROPERTY_DATE_VALUE"]==''){
										CIBlockElement::SetPropertyValues($obs["ID"], 19,'', 'NO_READ');
										CIBlockElement::SetPropertyValues($obs["ID"], 19, ConvertTimeStamp(date(), 'FULL'), 'DATE');
									}
							?>
							
							<div class="chat__message">
                                <div class="chat__message-info">��������: <span><?=$DB->FormatDate($obs["DATE_ACTIVE_FROM"], FORMAT_DATETIME, "DD.MM.YYYY HH:MI");?></span></div>
                                <div class="chat__message-text"> <?=str_replace(PHP_EOL,'<br>',$obs["PREVIEW_TEXT"])?>
                               <?
								if(
									(is_array($obs["PROPERTY_MOREFILE_VALUE"]) && (count($obs["PROPERTY_MOREFILE_VALUE"])>0)) || 
									(!is_array($obs["PROPERTY_MOREFILE_VALUE"]) && ($obs["PROPERTY_MOREFILE_VALUE"]!=''))
								):
								
								if($obs["PREVIEW_TEXT"]!=''){echo '<br>';}
								
    								$iterator = CIBlockElement::GetPropertyValues(19, array('ID' => $obs["ID"]), true, ["ID"=>132]);
    								while ($row = $iterator->Fetch())
    								{    								    
    								    foreach($row[132] as $fl):
    								    
    								    //var_dump();die();
    								    ?>
    								    <a href="<?=CFile::GetPath($fl); ?>" download class="doc-link"><?=CFile::GetFileArray($fl)['ORIGINAL_NAME'] ?></a><br>    								    
    								    <?
    								    endforeach;
    								}
    								
								endif;?></div>
                            </div>
            
            				
            
						
							
							
							<? 

							}
	                    ?>
	                    <? }?>
            <?/* ?><div class="chat__message">
                <div class="chat__message-info">��������: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">������ ����. <br>� ��� � ���� ������?</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">���. ����� ������</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">
                    <a href="#" class="doc-link">����.doc</a>
                </div>
            </div>
            <div class="chat__message">
                <div class="chat__message-info">��������: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">������ ����. <br>� ��� � ���� ������?</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">���. ����� ������</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">
                    <a href="#" class="doc-link">����.doc</a>
                </div>
            </div>
            <div class="chat__message">
                <div class="chat__message-info">��������: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">������ ����. <br>� ��� � ���� ������?</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">���. ����� ������</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">
                    <a href="#" class="doc-link">����.doc</a>
                </div>
            </div>
            <div class="chat__message">
                <div class="chat__message-info">��������: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">������ ����. <br>� ��� � ���� ������?</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">���. ����� ������</div>
            </div>
            <div class="chat__message chat__message_user">
                <div class="chat__message-info">�: <span>10.05.2019   13:35</span></div>
                <div class="chat__message-text">
                    <a href="#" class="doc-link">����.doc</a>
                </div>
            </div><? */?>
            
            
        </div>
        </div>
        <script> </script>
        <form method="POST" action='/client/add_comment.php' enctype="multipart/form-data" class="chat-form">
        <?=bitrix_sessid_post()?>
    	<input type="hidden" name="action" value="chat_add">
    	<input type="hidden" name="taskid" value="<?=$_GET['order_id'] ?>">
    	<input type="hidden" name="userid" value="<?=$USER->GetID()?>">
	    <input type="hidden" name="username" value="<?=$USER->GetFirstName()?>">
	    <input type="hidden" name="time" value="<?=time()?>">
	    <input type="hidden" name="ordertitle" value="<?=$arResult['ITEM']["PROPERTY_TOPIC_VALUE"] ?>">
        <div class="chat__textarea">
            <textarea name="chat" placeholder="������� / ��������� ���� ���������� ������ ���"></textarea>
        </div>
        <div class="chat__bottom">
        
        
            <div class="chat__add-file">
                <input type="file" name="file" id="file" multiple="multiple" class="file-input chat">
                <a href="#" class="link-style add-file"  id="add-file" data-file="#file"><span>��������� ����</span></a>
            </div>
            <div class="chat__send">
                <button type="submit" class="button button_small">��������</button>
            </div>
        </div>
        <div class="file-list">
        
            <?/* ?><div class="file-list__item" >
                <div class="file-list__name">Page1_kursova_istoriya_4568.jpg</div>
                <div class="file-list__status"><a href="#" class="remove-file"></a></div>
            </div>
            <div class="file-list__item">
                <div class="file-list__name">Page2_kursova_istoriya_12.jpg</div>
                <div class="file-list__status"><div id="circle2"></div></div>
            </div><? */?>
        </div>
        </form>
    </div>
    <script>
    function updateCh(){


    }
    //console.log(1);
    </script>


<?
    
    
}else{
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");
    
    ?>
    <h1>404</h1>
    <?
}


endif;
?>             
                
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>