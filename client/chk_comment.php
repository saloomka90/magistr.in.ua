<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

header('Content-type: text/html; charset=utf-8');

global $USER, $MESS;




if(check_bitrix_sessid() && CModule::IncludeModule("iblock"))
{
    
    
    
    $arSelect = Array("ID", "NAME", "DATE_CREATE", "ACTIVE_FROM", "PREVIEW_TEXT","PROPERTY_DATE",'PROPERTY_NO_READ');
    $arFilter = Array("IBLOCK_ID"=>IntVal(19), '!PROPERTY_USER_ID'=>intval($_POST["userid"]), "PROPERTY_TASKID"=>$_POST["taskid"], "ACTIVE"=>"Y", ">DATE_CREATE"=>ConvertTimeStamp($_POST["time"], "FULL") );
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    
    
    if($res->SelectedRowsCount()>0){
        $text = '';
        while($arFields = $res->Fetch())
        {
            
            if($arFields['PROPERTY_NO_READ_VALUE']!='' and $arFields["PROPERTY_DATE_VALUE"]==''){
                CIBlockElement::SetPropertyValues($arFields["ID"], 19,'', 'NO_READ');
                CIBlockElement::SetPropertyValues($arFields["ID"], 19, ConvertTimeStamp(date(), 'FULL'), 'DATE');
            }
            $text .= '<div class="chat__message">
                <div class="chat__message-info">'.mb_convert_encoding('Менеджер', "utf-8", "windows-1251").': <span>'.$DB->FormatDate($arFields['ACTIVE_FROM'], FORMAT_DATETIME, "DD.MM.YYYY HH:MI").'</span></div>
                <div class="chat__message-text">'.mb_convert_encoding(str_replace(PHP_EOL,'<br>',$arFields["PREVIEW_TEXT"]), "utf-8", "windows-1251").'</div>
            </div>';
            //var_dump($arFields,$text);
        }
        
        echo json_encode(['time'=>time(),'html'=>$text]);
    }else{
        echo json_encode(['time'=>time()]);
    }
}else{
    echo json_encode(['time'=>time(), 'update'=>true]);
}


?>
