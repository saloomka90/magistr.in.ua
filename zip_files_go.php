<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/zip.lib.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;

//if(!$USER->IsAuthorized()) exit();

$order_id = intval($_GET["id"]);
//echo "<pre>".print_r($MESS,true)."</pre>";
if($order_id>0 && /*sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) &&*/ CModule::IncludeModule("iblock"))
{
	$BID = 13;
	$rsOrder = CIBlockElement::GetByID($order_id);
	if($obOrder=$rsOrder->GetNextElement())
	{
		//$arOrder = $obOrder->GetFields();
		$arOrder["PROPERTIES"] = $obOrder->GetProperties();

		if(!is_array($arOrder["PROPERTIES"]["TO_FILE"]["VALUE"]) && $arOrder["PROPERTIES"]["TO_FILE"]["VALUE"]>0)
			$arOrder["PROPERTIES"]["TO_FILE"]["VALUE"] = array($arOrder["PROPERTIES"]["TO_FILE"]["VALUE"]);
		
		if(is_array($arOrder["PROPERTIES"]["TO_FILE"]["VALUE"]) && sizeof($arOrder["PROPERTIES"]["TO_FILE"]["VALUE"]))
		{
			$filename = "files_".$order_id.".zip";
			$zipfile = new zipfile();
			foreach($arOrder["PROPERTIES"]["TO_FILE"]["VALUE"] as $f)
			{
				$file = CFile::GetFileArray($f);
				
				
				$zipfile->addFile(file_get_contents($_SERVER["DOCUMENT_ROOT"].$file["SRC"]), iconv('windows-1251', 'cp866', $file["FILE_NAME"]));
			}
			$zipdata = $zipfile->file();
			
			print($zipdata);
			
			header("Content-Type: application/octet-stream");
			header("content-disposition: attachment; filename=\"".$filename."\"");
			exit();
		}
	}
}
?>