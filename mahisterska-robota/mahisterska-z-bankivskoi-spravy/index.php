<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ����������� ������ � ��������� ������ - ������");
$APPLICATION->SetPageProperty("description", "���������� ����������� ������ � ��������� ������ � ��� - ��� ����� ��� ��������, ���� �� ���� � ������ ������ �� ��������. &#10003; 10 ���� ������ &#10003; ����������!");
$APPLICATION->SetTitle("����������");
?> 	
<div class="wrap-bg"> 			 	 
  <div class="wrap"> 					 	 	 
    <p dir="ltr"></p>
   
    <h1>����������� ������ � ��������� ������ �� ����������</h1>
   
    <div> 
      <br />
     </div>
   
    <p></p>
   
    <p><img src="/upload/medialibrary/4fc/bankivska_sprava4_min.jpg" hspace="30" vspace="10" border="1" align="left" alt="�������� ����������� ������ � ��������� ������ � �������� &quot;������&quot;" width="300" height="225"  /><font size="4">��������� ������������ ���� - �� �������� �� � ������, � � ��� ������������ ������ �� ����� �������. ��������� ������� �� ������ ����� ��� ����, ��� ������ �� ����, � �� ������� ���� �� ���������� ���������. � ������ �� ��, �� ��� ���������� ���� ����������� �� ������������, � ���� ���� � �����, �� �� ������������� ������ ����� ������� ��� �� ���� ����.</font></p>
   
    <p> </p>
   
    <p><font size="4">�� ������ �������� ����� ��������� ���������� �� ��������� ������ � ������ ���������� ����� ������� ����������� � ���������� ���������. ���� ���������� ��������, �� � ��������� ���������: ������ �� ������ ��� ����� ������.</font></p>
   
    <p> </p>
   
    <p><font size="4">��� �� �������� ����� ������� ������ ������, ��� ����� ����� �������� ����������� ������ � ��������� ������ �� ������ ����. 
        <br />
       
        <br />
       </font></p>
   
    <p> </p>
   
    <p><b><font size="5">����� ����������</font></b></p>
   
    <p> </p>
   
    <p><font size="4">����������� ������ � ��������� ������ �� ���������� ���������� ��������� &quot;������&quot; �� �������� ������������� �����. ���� ���� �� ��������, �� ������ ��� ���� �������, ��� ���������� �� ��������� ���� ������� ������ ����� ���� ���� ������������.</font></p>
   
    <p><font size="4">�� �������� ������ � ��������� ������ ����-��� ��������� � ������ ��������� ���������� �� ��� ������������ � ��������� ������ ��������� ����� ��� �����, �� ��������� �볺�����. �� ����� ���� ��������� �������� ��������� ����������� ����������, ���� ���� ������ ���� ������������ �� ������ ������. 
        <br />
       
        <br />
       </font></p>
   
    <p> </p>
   
    <p><b><font size="5">�������� ���������� � �������� &quot;������&quot;</font></b></p>
   
    <p> </p>
   
    <p> </p>
   
    <ol> 
      <li><font size="4">�� ������ �������� ����������� ������ � ��������� ������ �� ���������� �������� �� ����� ��������, ��� ��� ���������� ����������� � ��� ������� ����.</font></li>
     
      <li><font size="4">ֳ�� �� ���������� �������� �� ����� ��� �������� ����� ����, � � ��� �������� ����������� �����.</font></li>
     
      <li><font size="4">�������� ����������� ����� ��������. �� ����� ���� �� �������� � 򳺿 � ��������������. �� �������� ������ ������� ������� ����, �� � ������ �� ��������� ����������� ����� ��������.</font></li>
     </ol>
   
    <p></p>
   
    <p><font size="4">��� ��������� ������ �������, ������ ����������� �����������. ��� ����� ��������� �� ������, ��������� �� ������������ �� �������� � ����� ������� ������� ���� �������. ��� ����������� ���� ������� �������� ������ ������� � ��������� ��������.</font></p>
   
    <p> </p>
   
    <p dir="ltr"> </p>
   
    <p><font size="4">��� �������� ��������� ����������� ������ � ��������� ������, ����������� �� ����������, ��������� �� ����. �� ������ �������� ��� ���� ���� �� ������� ��������� ����������. � ��� ����� ����� �������� ������� �����������. 
        <br />
       
        <br />
       </font></p>
   
    <p dir="ltr"><b><font size="5"> ������ ����� �������� ����������� ������ </font></b></p>
   
    <div dir="ltr"> <font size="4"> 
        <br />
       </font> 
      <table width="100%" height="30%" border="1" cellpadding="5" cellspacing="0" align="left"> 
        <tbody> 
          <tr><td><b><font size="4">��� ������</font></b></td><td><b><font size="4">����� ���������</font></b></td><td><b><font size="4">ֳ�� (���)</font></b></td></tr>
         
          <tr><td><font size="4">�����������</font></td><td><font size="4">20 - 30 ����</font></td><td><font size="4">�� 2500</font></td></tr>
         
          <tr><td><font size="4">����� �����������</font></td><td><font size="4">7 - 15 ����</font></td><td><font size="4">�� 1000</font></td></tr>
         
          <tr><td><font size="4">�������</font></td><td><font size="4">1 - 2 ���</font></td><td><font size="4">�� 100</font></td></tr>
         </tbody>
       </table>
     </div>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       </font></p>
   
    <p dir="ltr"> <font size="4"> 
        <br />
       
        <br />
       </font></p>
   
    <p><font size="4"><b>�������� ���������� ��</b><a href="https://magistr.in.ua/mahisterska-robota/" ><font color="#0000ff">���������� ������</font></a><b>�� ����� ��������:</b></font></p>
   
    <p> </p>
   
    <ul> 
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/" ><font size="4">�������������� ����</font></a></li>
     
      <li><font size="4">��������� ������</font></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-zhurnalistyky/" ><font size="4">�����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-prava/" ><font size="4">�����</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-menedzhmentu/" ><font size="4">����������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-pedahohiky/" ><font size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/" ><font size="4">��������� ����</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-psykhologii/" ><font size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/" ><font size="4">̳�������� ��������</font></a></li>
     
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px;">Գ�����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px;">��������</span></a></li>			
     </ul>
   </div>
 
 <div class="global" style="text-align:center;"><a href="https://magistr.in.ua/order/" class="gbt">�������� ������</a></div>
 </div>
 	 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>