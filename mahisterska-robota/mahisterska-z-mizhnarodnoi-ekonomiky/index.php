<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ����������� ������ � ��������� �������� - �������");
$APPLICATION->SetPageProperty("description", "���������� ����������� ������ � ��������� �������� - ��� ��������� ������ ��� ��������, ���� �� ���� � ������ ������ ��� �� ���� �� ������ &#10003; ����������!");
$APPLICATION->SetTitle("����������");
?><div class="wrap-bg">
	<div class="wrap">
		<p dir="ltr">
		</p>
		<h1>
		<p>
			 ����������� ������ � ��������� �������� �� ����������
		</p>
 </h1>
		<div>
 <br>
		</div>
		<p>
		</p>
		<p>
 <img width="300" alt="�������� ����������� ������ � ��������� �������� � �������� &quot;������&quot;" src="/upload/medialibrary/661/ekonomika2_min.jpg" height="201" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">�������, �� ������ ��������� � ���� �������� ���������� �������, �������� �� ���� ������. ������� �� �������� ������ ������ �������, ������� ������������� ��������. ���� ������ ������� ����� � ������� ��������, �����, �������㳿, ��������� ���. ����������� ������ � ��������� �������� - �������� ���� ������� ��������� �����. ³� ����, �������� ������� ����� ������� ������ ��� ����������, ���� �������� �� ����� ������ � ������, � � ��������� �������� ���'���.</span>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">���������� � ������������ ������� � ��������� �������� ������ �� �� ��������.</span>
		</p>
		<p>
		</p>
		<ol>
			<li><span style="font-size: 18px;">�� ���'����� � ���, �� ��������� ���� ���� ����� ����������� ���������� �� ����� ������. ��� �� ������ ������� ����� ������� ��������� �����.</span></li>
			<li><span style="font-size: 18px;">���� ������ ������ �������� ��������� �� ��������������� �������, � ��� ��������� ������ ������� ������ ������������� ���������� �������, ��������� ����� ���� ��� ������ �� �������� � ������.</span></li>
			<li><span style="font-size: 18px;">�� ��� ���������� �� �� ������� ��������� ��������� �� ����� 2-� ������. � ��� �� ��� ����� ���������� � �� ��������� ������, � ��������� ��� ��� ������� ������ �����. <br>
 <br>
 </span></li>
		</ol>
		<p>
		</p>
		<p>
		</p>
		<p>
 <h2><b><span style="font-size: 24px;">� ���������� "������" �� ������� ��������� ���� �����</span></b></h2> <br>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">�������� "������" ���� ������� � ����� ���������� ��� �������� ��� ����, ��� ���� ���� ��������� ���� �� �������� ���� � ����� �������� ����������� �� ������ ��������� ������. ����� ����� �� ����� ���� ��������� ��� ��������� ������������ ���� � �� ���������� �� ������������� ���� �����.</span>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">��� ��������� ������� ������� ��� ����������� ������ ������ ����������� ������ � ��������� ��������. ��� ����� �������� � ��� ������������, �� ������� ���� ������ ����, �������� � ������ ���� �����, � ����� ����� ����� �������, �� ����� �������� ������. <br>
 <br>
 </span>
		</p>
		<p>
		</p>
		<p>
 <h2><b><span style="font-size: 24px;">���� ����� ���������� � ��������� "������"</span></b></h2>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">�� ������ �������� ����������� ������ � ��������� �������� � ���������� �� ��������� � ��� ���������. �� ��������� ��� ������������ ���������� ��������� ������ ��������� �������� � �������� ������ ��������� � ������ ��������, ������� �������� �� �������� ������.</span>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">��� ������������ ������� ���������� ������ ������ ��� ������������ � �����������. �� ��������� �� ��� � �������. ���� ������� ���������� � ����, �� �� ������ ���������� ��������, � ��������� ������� �� ���������� � ������� - �� ������� ����� �����������. ���� ������� �������� ��� �������, ��� ��� ������� ��������� �����������. ��� ����������� ���� �������� ���� ������������ �������� ��������� ����������� �������� �������� ����. ��� ����� �� ��������� �������� ��������. ֳ�� �� �������� ���������� ����������� ������� ��������� ������� � �������������� �������.</span>
		</p>
		<p>
		</p>
		<p>
		</p>
		<p>
 <span style="font-size: 18px;">��� ����������� ����� � ��������� ���� �������� �������� ����������� ������ � ��������� �������� �� ���������� ������ � �����. ����� �� �������� �� ������������ ��������� �����������, � ��������� ������� ������ ���������� � ���������. �� �������� ��� ����������� ������ ����� ������ ������, ��� �� ����������� ����� ��������� ��������. � ��� � ���� �� 50 - 100% ���� �����. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
 <h2><b><span style="font-size: 24px;"> ������ ����� �������� ����������� ������ </span></b></h2>
		</p>
		<div dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
			<table width="100%" height="30%" border="1" cellpadding="5" cellspacing="0" align="left">
			<tbody>
			<tr>
				<td>
 <b><span style="font-size: 18px;">��� ������</span></b>
				</td>
				<td>
 <b><span style="font-size: 18px;">����� ���������</span></b>
				</td>
				<td>
 <b><span style="font-size: 18px;">ֳ�� (���)</span></b>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">�����������</span>
				</td>
				<td>
 <span style="font-size: 18px;">20 - 30 ����</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 2500</span>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">����� �����������</span>
				</td>
				<td>
 <span style="font-size: 18px;">7 - 15 ����</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 1000</span>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">�������</span>
				</td>
				<td>
 <span style="font-size: 18px;">1 - 2 ���</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 100</span>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 <br>
 </span>
		</p>
		<p>
 <span style="font-size: 18px;"><b>�������� ���������� �� </b><a href="https://magistr.in.ua/mahisterska-robota/"><span style="color: #0000ff;">���������� ������</span></a><b> � ����� ��������:</b></span>
		</p>
		<p>
		</p>
		<ul>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/"><span style="font-size: 18px;">�������������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 18px;">��������� ������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 18px;">�����������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 18px;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 18px;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 18px;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 18px;">��������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 18px;">���������</span></a></li>
			<li><span style="font-size: 18px;">̳�������� ��������</span></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px;">Գ�����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px;">��������</span></a></li>
		</ul>
		<ul>
		</ul>
	</div>
	<div class="global" style="text-align:center;">
 <a href="https://magistr.in.ua/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>