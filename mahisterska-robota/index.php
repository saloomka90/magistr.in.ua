<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ����������� ������ - ��������� ������� - �. ���");
$APPLICATION->SetPageProperty("description", "������� ������ ����������� ������ �������� � ��� ���� ������ ���� - ��� �������! &#10003; 10 ���� ������ &#10003; 100 % ������� &#10003; ����������!");
$APPLICATION->SetTitle("����������");
?><div class="wrap-bg">
	<div class="wrap">
		<p dir="ltr">
		</p>
		<h1><span style="font-size: 32px;">����������� ������ �� ����������</span></h1>
		<div>
 <br>
		</div>
		<p>
		</p>
		<p dir="ltr">
 <img width="300" alt="�������� ����������� ������ � �������� &quot;������&quot;" src="/upload/medialibrary/e2d/mahisterski_min.jpg" height="315" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">����������� ������ � �� ���� ��������� �������-������� ������, ����� ��������� �� ������ ���������� ���, ������� ������ ������ �������. ����� �� ��������� ������ �������� ������� ������� ����� �� ��������� � ������ ���� ��������, �� �������� �� ������� ��������� �������-����������� ���������.</span>
		</p>
		<p dir="ltr">
 <br>
		</p>
		<h2><span style="font-size: 24px;"><b> ���� ������ �������� �������� �������� ����������� ������</b></span></h2>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">��� ������� ����� ������ ������� ������ ����������, ���������� ���������� �������, ���������������� �� �������� ������ ���� ������� ������ ����� ���������� �����. ����� ��� �������, �� ������� ���, �� ������� �����������, ��� ���� �� ��������� �� �������. ����������� ����� �������� ����������, � ����������� ������� �� �������� ��� ������ ����������� ������ � �����������.</span>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <b> <br>
 </b>
		</p>
		<h2><b><span style="font-size: 24px;"> ���� �������� ��������� ����������� ������ � �������� ������� - �������� ������</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<ol>
			<li><span style="font-size: 18px;">� ��� ������ ������� ����������� ����� �����������, �� �� � ������ ��� �������� ������� �����.</span></li>
			<li><span style="font-size: 18px;">����������� ������, �� �������� ������ �����������, ���������� � ����������� ����, ���� ����������� ��������������� ������ �� ������ �������, � ����� ��������� �������.</span></li>
			<li><span style="font-size: 18px;">�� ���� ��������� �������� ������: �� ��������� �� ��������� ����.</span></li>
			<li><span style="font-size: 18px;">�������� ���������� ����� ������� ����� �����������.</span></li>
			<li><span style="font-size: 18px;">����������� ������ �� ���������� �� ��������� ���� �������� �� ������� �� �� ���������.</span></li>
			<li><span style="font-size: 18px;">��� ����������� �������� �������� ������ ����������� ����������� � �������� �����.</span></li>
			<li><span style="font-size: 18px;">����������� �������������� �� ������� �� �������.</span></li>
			<li><span style="font-size: 18px;">��� �������� �볺��� ��������� ������ �� 30%.</span></li>
		</ol>
		<p>
		</p>
		<p dir="ltr">
 <b> <br>
 </b>
		</p>
		<h2><b><span style="font-size: 24px;"> �� �������� �����������</span></b></h2>
		<p>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">�� ���� ������������ ����� ����������. � �� �� ������ ���� ����������� ����� ������, �� ��������� ���� ���. �� ������ ������ ��������� ����������, � ��� �������� � ����� ������������ �� ���������. ���� � ���������� ������ ��������� ��� ��������, �� �������� ��� ��������� ����������� ���������� �������� �� ������ ������ � ��������� ��� ���� ����.</span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">������� ����������� ������� ���� ����� � ���. ϳ��� ��������� ������ ������ ��� ���������� ���� �������� ������� ��, ��� �������� ����������� � �� �������� �� ����.</span>
		</p>
		<p dir="ltr">
 <br>
		</p>
		<h2><b><span style="font-size: 24px;">������� ��������� ������ </span></b></h2>
 <b> <br>
 <br>
 </b>
		<p>
		</p>
		<div dir="ltr">
			<div dir="ltr">
				<table width="100%" height="10%" border="1" cellpadding="5" cellspacing="1" align="left">
 <colgroup><col></colgroup>
				<tbody>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;1.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;������ ����������: ������ ��� ��������� ���� �� ���������� ��� ����������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;2.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;�������� ���� ���� ������ ������� ���� ���������� � ��� ������ ���� �������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;3.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;�� ������� ����������� �� ���� � ������ ����� � ����� 30-50% ������� ������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;4.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;�� ������ ����, � ��������� ��� ��� ������������ ���������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;5.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;���� � ���� ������� ���� ���������, �� �� ������ ���������� ������ �� �������������� �����.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;6.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;�� �������� ������ ����� � ����������� ��� ��� �������� ����� ���������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;7.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;��� ����������� �� ��������� ��� ���������� �������� ��� �����������.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;8.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;��������� �� �������� ������� ��������� ������ � �� �������� ��.</span>
						</p>
					</td>
				</tr>
				<tr>
					<td style="border-image: initial;">
 <span style="font-size: 18px;">&nbsp;9.</span>
					</td>
					<td style="border-image: initial;">
						<p dir="ltr">
 <span style="font-size: 18px;">&nbsp;��� ����������� �� 1,5 ����� ����������� ������� ������ � ������ �� ����������� ��������.</span>
						</p>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
 <span style="font-size: 18px;"> <br>
 </span>
		</div>
		<p dir="ltr">
 <b><span style="font-size: 18px;">&nbsp;</span></b>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <b> <br>
 </b>
		</p>
		<h2><b><span style="font-size: 24px;"> ������ ����� �������� ����������� ������ </span></b></h2>
		<p>
		</p>
		<div dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
			<table width="100%" height="30%" border="1" cellpadding="10" cellspacing="0" align="left">
			<tbody>
			<tr>
				<td>
 <b><span style="font-size: 18px;">��� ������</span></b>
				</td>
				<td>
 <b><span style="font-size: 18px;">����� ���������</span></b>
				</td>
				<td>
 <b><span style="font-size: 18px;">ֳ�� (���)</span></b>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">����������� ������</span>
				</td>
				<td>
 <span style="font-size: 18px;">20 - 30 ����</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 2500</span>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">����� ����������� ������</span>
				</td>
				<td>
 <span style="font-size: 18px;">7 - 15 ����</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 1000</span>
				</td>
			</tr>
			<tr>
				<td>
 <span style="font-size: 18px;">�������</span>
				</td>
				<td>
 <span style="font-size: 18px;">1 - 2 ���</span>
				</td>
				<td>
 <span style="font-size: 18px;">�� 100</span>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 </span>
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;"> <br>
 <br>
 </span>
		</p>
		<p>
 <b><span style="font-size: 24px;"> <br>
 </span></b>
		</p>
		<p>
		</p>
		<h2><b><span style="font-size: 18px;">�������� ���������� ��&nbsp;���������� ������&nbsp;� ����� ��������:</span></b></h2>
		<p>
		</p>
		<p>
		</p>
		<ul>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bukhhalterskoho-obliku/"><span style="font-size: 18px; color: #0000ff;">�������������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-bankivskoi-spravy/"><span style="font-size: 18px; color: #0000ff;">��������� ������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-zhurnalistyky/"><span style="font-size: 18px; color: #0000ff;">�����������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-prava/"><span style="font-size: 18px; color: #0000ff;">�����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-menedzhmentu/"><span style="font-size: 18px; color: #0000ff;">����������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-pedahohiky/"><span style="font-size: 18px; color: #0000ff;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-anhliyskoyi-movy/"><span style="font-size: 18px; color: #0000ff;">��������� ����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-psykhologii/"><span style="font-size: 18px; color: #0000ff;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-mizhnarodnoi-ekonomiky/"><span style="font-size: 18px; color: #0000ff;">̳�������� ��������</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-finansiv/"><span style="font-size: 18px; color: #0000ff;">Գ�����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-obliku-i-audytu/"><span style="font-size: 18px; color: #0000ff;">���� � �����</span></a></li>
			<li><a href="https://magistr.in.ua/mahisterska-robota/mahisterska-z-ekonomiky/"><span style="font-size: 18px; color: #0000ff;">��������</span></a></li>
		</ul>
 <span style="font-size: 18px;"> <br>
		 �� ������ ����� �������� ����������� ������ �� ���������� �� ��� ��������, �� ���������� � ��� ���, ��� � � ����� ��� ������. ����������!</span> <br>
 <br>
	</div>
	<div class="global" style="text-align: center;">
 <a href="https://magistr.in.ua/order/" class="gbt">�������� ������</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>