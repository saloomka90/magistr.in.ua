<?
//define('LANG_ID','ua'); 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?$APPLICATION->IncludeComponent("bitrix:main.register", "autorsPage", array(
	"SHOW_FIELDS" => array(
		0 => "NAME",
		1 => "PERSONAL_PHONE",
		2 => "PERSONAL_MOBILE",
	),
	"REQUIRED_FIELDS" => array(
		0 => "NAME",
		1 => "PERSONAL_PHONE",
	),
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "/authors/registration/",
	"AUTH" => "N",
	"USE_BACKURL" => "Y",
	"SUCCESS_PAGE" => "",
	"SET_TITLE" => "N",
	"USER_PROPERTY" => array(
		0 => "UF_PRIVAT_CARD",
		1 => "UF_WEBMONEY",
		2 => "UF_LIVING_PLACE",
		3 => "UF_BIRTH_YEAR",
		4 => "UF_OCCUPATION",
		5 => "UF_OPIT",
		6 => "UF_WORK_TYPES",
		7 => "UF_LANGS",
		8 => "UF_OTHER_DATA",
		9 => "UF_SUBJECTS",
		10 => "UF_SPHERE",
	),
	"USER_PROPERTY_NAME" => ""
	),
	false
);?>
			
<?/* $APPLICATION->IncludeComponent("bitrix:system.auth.form","popup",Array(
     "REGISTER_URL" => "auth.php",
     "FORGOT_PASSWORD_URL" => "",
     "PROFILE_URL" => "",
     "SHOW_ERRORS" => "Y",
	 		
     )
); */?>