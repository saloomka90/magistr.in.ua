<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$filter = Array("GROUPS_ID" => Array(12));
$by = "ID";
$order = "desc";
$rsUsers = CUser::GetList(($by), ($order), $filter, ['SELECT'=>['UF_*']]);
while ($arUser = $rsUsers->Fetch()) {
    
    if($arUser["UF_KURS"]>0 && $arUser["UF_KURS"]<6){
        $arUser["UF_KURS"]++;
    }
    (new CUser())->Update($arUser["ID"], ["UF_KURS" => $arUser["UF_KURS"] ]);
    
}

echo date("d.m.Y H:i:s").' = OK';

?>