<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ���������� - ��������� ������� - ����������� ������ � �. ���");
$APPLICATION->SetPageProperty("description", "� ��������� ������������ ���������� � ���������� �� ��������� ������� ��� ���������� ���� ������� &#10003; 10 ���� �� ����� &#10003; 100% ������� &#10003; ����������!");
$APPLICATION->SetTitle("����������");
?><div class="wrap-bg">
	<div class="wrap">
		<h1>�������� ������������ ����������</h1>
		<div>
 <br>
		</div>
		<p dir="ltr">
 <img width="300" alt="�������� ������������ ���������� � �������� &quot;������&quot;" src="/upload/medialibrary/5d2/kandydatski2_min.jpg" height="199" hspace="30" vspace="10" border="1" align="left"><span style="font-size: 18px;">������������ ���������� (�� �������� ��������� ������� ������� ���������) �� ������� ������������� ������ �� �������� ����� ��������� ������� ������, ��� �� ��������� ��������. ���� ���������, �� ����� ����� ��������� ��������� ��� ��������� ��������� �������. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <b><span style="font-size: 24px;">���� ������ ���������� �������� ������ ���������� �� ����������?</span></b>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">��� �������� ������������, ������� ������� ������� ������������, �������� ������ ������������ �� ����������. ��� ����� ���������� �������� ����� � �� ����� ��� ��. �� ������� �������������� � ������� �����, �쒿, �� ��� ����������� ��������� ���� �������� ������� �� ���������� �������� ��������� ���������� ��������� �����. ���� ������ ������� � ������ ��������� ��� �������� ������������ ����������. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <b><span style="font-size: 24px;">�� ������� ��������� ����������?</span></b>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">��������� ���������� � �� ��������� �����, ��� ������ ������ ����, ��� �� ���������. ����������� ��������� ����� ��������� �� ���������� ��������. ��� �� ���� �������, �� ������ ��������� �������� ��������. ����� ���� ��������� ������ �����, ����� �������� �������� ������� �� ������� � �����. ����� �������� � ��, �� ��� �� ��������� ���������� � �����������. � ���������, �������� �� ������ �������� �� ��������� ���� �����, ��������� ������ �� ���� ����� �� ����. <br>
 <br>
 </span>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <b><span style="font-size: 24px;">���� �������� ���������� � �������� ������� ��������� ������?</span></b>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">������� � ���� ��������, ���� ��������� ����������� ����������� ��� ��� �� �������, ��� ��������� �� ������������� �����������, �� ��������� ���������� �������� ����. � �������� �������:</span>
		</p>
		<p dir="ltr">
		</p>
		<ol>
			<li><span style="font-size: 18px;">��������� ���������� �� ���������� ���������� �������������������� ��������� � �������� �������� � ����������� ������� ������.</span></li>
			<li><span style="font-size: 18px;">�������� ������ ����� ������� ����� �����������, ���������� ������� ��� ������ �� ������� ��������� ��������.</span></li>
			<li><span style="font-size: 18px;">�� ���������� ��������������� �� ������� �������.</span></li>
			<li><span style="font-size: 18px;">�������� ���� ������������ ���������� ���������� �������� �� ��� ������ ��������� ������.</span></li>
			<li><span style="font-size: 18px;">������ ���������� �������� � ������� ������.</span></li>
			<li><span style="font-size: 18px;">��� ������ ������ ���� �� �� �����, ���� ��������� ����� �������.</span></li>
			<li><span style="font-size: 18px;">�� ���� ������ ������ ���� ��������� ��������� �����������, �� �� ��� ���������, ��� � ���� ��������� ������.</span></li>
		</ol>
		<p>
		</p>
		<p dir="ltr">
		</p>
		<p dir="ltr">
 <span style="font-size: 18px;">����, ���� ��� ������� �������� ������������ ���������� �� ���������� ����� �� ��������, � ��� �� ����� ����� ������, �� �� ������ ����� ��� ���������. ����������!</span>
		</p>
		<div>
 <span style="font-size: large;">&nbsp;&nbsp;</span>
		</div>
		<p>
 <b><span style="font-size: 18px;">�� �������� ���������� �� ����������� ���������� � ����� ��������������:</span></b>
		</p>
		<p>
		</p>
		<ul>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-ekonomiky/"><span style="font-size: 18px;">��������</span></a></li>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-psykholohii/"><span style="font-size: 18px;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-sotsiolohii/"><span style="font-size: 18px;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-politolohii/"><span style="font-size: 18px;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-pedahohiky/"><span style="font-size: 18px;">���������</span></a></li>
			<li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-prava/"><span style="font-size: 18px;">�����</span></a></li>
		</ul>
	</div>
	<div class="global" style="text-align:center;">
 <a href="https://magistr.in.ua/order/" class="gbt">�������� ������</a>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>