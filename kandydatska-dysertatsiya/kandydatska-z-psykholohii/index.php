<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������������ ���������� � �������㳿 � ��� - �������\"");
$APPLICATION->SetPageProperty("description", "�������� ������������ ���������� � �������㳿 ����� ��� ����, ��� �� ����� ��������� ���������. � ��� �������� ������������� - ������� ������ &#10003; ����������!");
$APPLICATION->SetTitle("�����������");
?> 
<div class="wrap-bg"> 			 	 
  <div class="wrap"> 					 	 	 
    <p dir="ltr"></p>
   
    <h1>������������ ���������� � �������㳿 �� ����������</h1>
   
    <div> 
      <br />
     </div>
   
    <p></p>
   
    <div><img src="/upload/medialibrary/980/psykhologia0_min.jpg" hspace="30" vspace="10" border="1" align="left" alt="�������� ������������ ���������� � �������㳿 � �������� &quot;������&quot;" width="300" height="168"  /><font size="4">������ ������ ������� ��������� ������������ ��������� ��� ������������, �� ������� ���� �������� � �������� 2-� ���� �������� ���� ������������� ������. 
        <br />
       
        <br />
       </font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><b><font size="5">��� ���� ��������� ������������ ���������� � �������㳿</font></b></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">������� ������ ��������� ��������� ��� ����������� ������� �����. �� � ��������� ���������-��������, �� ��������� ��������� ������� ������� ���� ������ � �������� ���'���. 
        <br />
       
        <br />
       </font></div>
   
    <div><font size="4">� ��� �� ���'�����?</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">� ����� �����, ��������� ��� ������ ��� ������ � �������㳿, ����������� ������� ���������, ��� ����������, ����� ������ � ���������� �������� ������������, �� ������ ���������������� � � ���, � � ����� ����� ����. �� ������������ �������� ��:</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div> 
      <ul> 
        <li><font size="4">��������� ���� ������� ����������;</font></li>
       
        <li><font size="4">�������� ��� �������� ��������;</font></li>
       
        <li><font size="4">������������� � ���������� ������������.</font></li>
       </ul>
     
      <ol> </ol>
     </div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">���� �� ������� ��������� ��������� ������ ���'��� � �������㳿 - �������� ������������ ���������� ����� ��� ����, ��� �� ����� ��������� ���������. � ��� �������� ������������� - ����� ������ ������ �� ��� ������ ���������� � �� �������. 
        <br />
       
        <br />
       </font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><b><font size="5">��� &quot;������&quot; ������ �� ���� ������������ ������</font></b></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">������������ ���������� � �������㳿 �� ���������� �� ������� ��� ������, ���'������ � ���������� ��������� �������. ��� ���������� ��������� � �������� ���������, ���� ������� ���� ������� ������ � ����� ��������. ��� �����, ��� ��������� �������� ���� �� ������������ ���������, ��������� �� ������� �������� ����������� �� ��� ���������� �� ��������� �������. ���� �� ������� ������ ������ ������������ ���������� � �������㳿 � �������� ������� ������, �� ��� �� �����. ������ ������ - �� ����� ��������, ������ ������ ��� ��� ���� ���������� ����� �� ����. ��� � ������ ���� �� ���������:</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div> 
      <ul> 
        <li><font size="4">������� �������� ���� ���������� �� ������� �� �������;</font></li>
       
        <li><font size="4">��������� ���������������� - ��������� ������� ����������, � ��� �� ����� �������� � ����� ����� ������ ����������� ���������� ���� �������������, ����� ������ � ��������� � �������㳿.</font></li>
       </ul>
     </div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">�������� ������������ ������ ����� �, ��� ��� ������� �� �������� ����� � �� �����. �� ������ ������������� ������ ����������� ����������� � ��������� ����.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><b><font size="5">�� �������� ������������ � �������㳿</font></b></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <div><font size="4">��� �������� ��������� ������������ ���������� � �������㳿, ��� ��������� ��'������� � ���� � ������ ����������� ���� ����������. ҳ���� ��� ����������� ���� � ����� ���������.</font></div>
   
    <div> <font size="4"> 
        <br />
       </font></div>
   
    <p><b><font size="4">�� �������� ���������� ��</font></b><a href="https://magistr.in.ua/kandydatska-dysertatsiya/" ><font size="4">����������� ����������</font></a><b><font size="4">�� ����� ��������������:</font></b></p>
   
    <p> </p>
   
    <ul> 
      <li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-ekonomiky/" ><font size="4">��������</font></a></li>
     
      <li><font size="4">���������</font></li>
     
      <li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-sotsiolohii/" ><font size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-politolohii/" ><font size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-pedahohiky/" ><font size="4">���������</font></a></li>
     
      <li><a href="https://magistr.in.ua/kandydatska-dysertatsiya/kandydatska-z-prava/" ><font size="4">�����</font></a></li>
     </ul>
   </div>

 <div class="global" style="text-align:center;"><a href="https://magistr.in.ua/order/" class="gbt">�������� ������</a></div>

 </div>
  	 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>