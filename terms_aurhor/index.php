<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "��� ��� ����� �� ����'���� ��������� �� ���������� ��� ������� ��������������� ������");
$APPLICATION->SetTitle("����� �������� � ��������");
?><div class="wrap-bg">
	<div class="wrap">
 <span id="docs-internal-guid-c377a334-9af2-c8a0-efe1-585549261633">
		<h1 style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;"><span style="font-size: 14.6667px; font-family: Arial; font-weight: 700; vertical-align: baseline; white-space: pre-wrap;"><b><span style="font-size: 16pt;">������ ��� ������� ��������������� ������ ( ������� ������)</span></b></span></h1>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; font-weight: 700; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt; text-indent: -18pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">1.</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space: pre;"> </span></span><span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">������� ��������</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 36pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">1.1. Գ����� ����� � ��������� ���������� ����� ��������, (��� � ����������) �������� �� ���� ����� �������� ���� ������� ���� (��� � ����������) ������������-�������������� ������� � ������� ��������� ������ �� ���� <a href="http://www.magistr.in.ua">www.magistr.in.ua</a> (��� � ����) ��� ��������� ��� ��������� ������ �� ����.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">1.2. ������� ������ ������������� ��������� ��� ���������� ����������, �� ����������� � ���������� �� �������, ���� ��������� �� ���� ���������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">1.3. ����� ������� ������ ���������� ������ 365 ����������� ���� � ������� ��������� ����� ��������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt; text-indent: -18pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space: pre;"> </span></span><span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">����'���� ���в�</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.1. ���������� �����'�������:</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.1.1. ������ ���������� ������ ��� ������� � �������, ������������� ��� ���������;</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.1.2. ����������� ���������������� ��������� ��������������� ������ � �� ������������� ������� ���� ��������� � ���������� ��� ���������� ����� ������. ����� 䳿 ���������������� �����������;</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.2. �������� �����'�������:</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">2.2.1. �������� � �������� ������� � �������, ������������� ��� ��������� �� �������� �� �������, ���� ��������� �� ���� ���������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt; text-indent: -18pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">3.</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space: pre;"> </span></span><span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">����� ������� ������</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">3.1. ��������� �������� ���� �� ���������� � ���������� �� ��������� ������������-��������������� ������ ����������� &nbsp;�������� �� �������, ���� ��������� �� ���� ���������</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt; text-indent: -18pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">4.</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space: pre;"> </span></span><span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">²���²����Ͳ��� ���в� � ������� ��в����� ���������</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">4.1. &nbsp;�� ����� � ���������, �� ��������� �� ����� ���������, ������� �������������� ������� ������ ���������� ���������� � ������������. � ������� ���� ����� �� ��������� �� ���� ��������� �������� ���� ������, �� ����� ��������� �������� � ��� �� ���� ����������� ���������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">4.2. ³������������� ����� ���������� ����� ������, ������������� ������������ ���������� ��� ���������� �������</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">4.3. ���������� �� ���� ������������� ����� ���������� �� ������ � ����-��� ����, �� ������� � ��������� ����-�������� �������� ��� ������������ ���������� ���� ����� ��������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">4.4. ³������������� �� ��������� ����������� ������������� ����� ������� ���� ��������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt; text-indent: -18pt; text-align: center;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">5.</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span><span style="font-size: 9.33333px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space: pre;"> </span></span><span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">��ز �����</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; margin-left: 54pt;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;"> </span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">5.1. ��� ������ ������ ������� � ������� ��������� ��������� �� ����, �� 䳺 �� ������� ��������� ��������� ��������� �� ���� �����'�����.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">5.2. � ������ ������, �� �� ����������� ��� ���������, ������� ��������� ������ �������������� ������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">5.3. ���������, �������� �� ������������ ��'����, ����� ���� � �������� ���� ��� ����� ����� ��������, �� � �������� ���������.</span>
		</p>
		<p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-indent: 36pt; text-align: justify;">
 <span style="font-size: 14.6667px; font-family: Arial; vertical-align: baseline; white-space: pre-wrap;">5.4. ������� ������������ ��, �� ���� � ���� ��������� ������� (����������), ����������� �� ������ �������, � ����� ������ ��������� �� ��������� ������� ����������� ������ ��ᒺ���� ��������.</span>
		</p>
 <br>
 </span>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>