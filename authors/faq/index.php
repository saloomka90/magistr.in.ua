<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("FAQ");
?> 
<!--# content #-->
 
<div class="content"> 
<!--# content header #-->
 
  <div class="contentHeader"> 
    <h1>���� ���������</h1>
   </div>
 
<!--# end content header #-->
 
  <div class="contentDiv"> 
    <div> 
<!--# faq group #-->
 
      <div class="faq_group faq_sell"> 
        <h3>������ ������� ����</h3>
       
        <div> 
          <div class="faqDiv"> <span>�� ����� ���������� �� ������ ������, �� ������������ ��� ��������� &ldquo;������&rdquo;?</span> 
            <p>�����, ��� ���� ����� 3 ���� ���� ���������.</p>
           </div>
         
          <div class="faqDiv"> <span>�� � �������, �� � ���� ������ ������?</span> 
            <p>���� � ��� ��������� ������ �������, �� �� ���� ����� ������ ���� �� ���������� �볺���. ��� ������� ���� ��&rsquo;������� � ��� � ���������� ����� �������. 
              <br />
             ���� � ��� ��������� ������ �� �������������� ������ ���������, �� �� ���� ��������� ��� �������� (� ����������� ����� ��� �� ����). </p>
           </div>
         
          <div class="faqDiv"> <span>��� ����� � ���� �����������?</span> 
            <p>� ��������� ���� ����� �� ���������</p>
           </div>
         
          <div class="faqDiv"> <span>�� ����� � ���������� ��� ������ ������ ��������� �� ��������� ����, ��� �볺��� ����������� � ���� �������?</span> 
            <p>���, �����.</p>
           </div>
         
          <div class="faqDiv"> <span>����� ���������� ������ ������?</span> 
            <p>���������� ������� ������ ��� ������������, ���� ����� ����������, �� ������� ���� ���� ������:
              <br />
             1. ������, � ���� �������� ������� ������ ��� ������������ ����������� ���� � ��������� �������� � ��������� ����� ���������
              <br />
             2. �������� ������� �������� ������� ��� �������, � ����� ����� ���������� ������������. </p>
           </div>
         
          <div class="faqDiv"> <span>����� ����������� ���� ���������, ���� ���� ����� ��� ���� �� ������?</span> 
            <p>��������, � ���� ������� ��� ���� ��������� �� ������, � ���� ��������� ������ �� ������ ������, �� ����� ������������ ��� ���� ������ ������.</p>
           </div>
         
          <div class="faqDiv"> <span>�� � ������� ������?</span> 
            <p>�� ����������� ����� �� ������ �����������. ���� � ��� �� ���� - ������ ����������� �������� �� � ����-����� ��������. ���� ���� �������� - ������ �� �������� �� �������� �������. </p>
           </div>
         </div>
       </div>
     
<!--# end faq group #-->
 
<!--# faq group #-->
 
      <div class="faq_group faq_writing"> 
        <h3>��������� ���� �� ����������</h3>
       
        <div> 
          <div class="faqDiv"> <span>��� ���� ������� �� ��������� ������?</span> 
            <p>����������� ����, �� ��� �� ������ ����� �������� ������. </br>
��� ����� ����, ��� ����� ����������, �� ��� ������� �������.</p>
           </div>
         
          <div class="faqDiv"> <span>� ����� ������ �� ��������� ������. �� � �������, �� ���� ������ �������?</span> 
            <p>���� ��� �������� ������� ������, �� ��� �� ��� ����������� ��������� �������� - �� �������� ��� �� ��. ����.</p>
           </div>
         
          <div class="faqDiv"> <span>� ��� ����� ������ ������, ��� ���� �� �������� �������. � ���� �������?</span> 
            <p>������ ���� ���� �������:</br>
1. �� ����� �����, ���� �������� �� ������ ���������� ����� ����� ������ (��������, ����������, ������). � ���� ���� ��� ������ ������� ������� ��������� ���� � ����������.</br>
2. ���� ���� ������ ����, ��� ���� ����� ������.</br>
3. ������ ������, �� �� ��� ��������� ������, �� ����������� �볺�����, ���� ���� ������ ������������.</p>
           </div>

          <div class="faqDiv"> <span>���� � ������� ������ �� �������� ������?</span> 
            <p>���������� � �������� ����������� ���� ������� ��������� ������. </br>
� ����������� �������� ������������� �������� 1 - 2-� ���� � ������� ���������.</br>
� ������  �������� - ���� �������� ������ �볺���� ��� ���� �������� ���������, ��� �� ����� 5-�� ����.
</p>
           </div>
         
          <div class="faqDiv"> <span>��� ����� ���������?</span> 
            <p>�� �� ������ ����� � ������. �� �������� �� ����, ��� ������� � ������ �� ��������� ������.</p>
           </div>
         
          <div class="faqDiv"> <span>�� � ������� ������?</span> 
            <p>�� ����������� ����� �� ������ �����������. ���� � ��� �� ���� - ������ ����������� �������� �� � ����-����� ��������.</p>
           </div>
                  
          <div class="faqDiv"> <span>�� ���� � �������� ����� �� ��������� �������?</span> 
            <p>���, ��� ���� � ��� ��������, ���� �볺�� ����� ���� ��� �������� ������ �� ������.
ֳ�� �� ������� ������� ���� ����������.</p>
           </div>
         
          <div class="faqDiv"> <span>���� ������ �������, ��� � ���� ���������� �� ��������� ������. �� ������?</span> 
            <p>³����� �� ��������� ������ ������� ��� ������� � ���� ��������� ����� �� ���������� ��������. </br>
���� �� ���-���� ������ ���������� �� ��������� ������, ������� ������ ��������� ��� �� ���������, ��� �� ����� ������ �����. 
</p>
           </div>
         </div>
       </div>
     
<!--# end faq group #-->
 </div>
   </div>
 </div>
 
<!--# end content #-->
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>