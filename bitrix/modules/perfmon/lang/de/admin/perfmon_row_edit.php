<?
$MESS["PERFMON_ROW_EDIT_MODULE_ERROR"] = "Fehler der Verbindung mit dem Performance-Modul.";
$MESS["PERFMON_ROW_EDIT_TABLE_ERROR"] = "Die angegebene Tabelle (#TABLE_NAME#) existiert nicht.";
$MESS["PERFMON_ROW_EDIT_PK_ERROR"] = "Diese Schl�sselwerte sind nicht ausreichend f�r eine definitive Identifizierung des Eintrags.";
$MESS["PERFMON_ROW_EDIT_NOT_FOUND"] = "Der Eintrag wurde nicht gefunden.";
$MESS["PERFMON_ROW_EDIT_SAVE_ERROR"] = "Fehler beim Speichern von Daten.";
$MESS["PERFMON_ROW_EDIT_TITLE"] = "Eintrag #TABLE_NAME# bearbeiten";
$MESS["PERFMON_ROW_EDIT_MENU_LIST_TITLE"] = "Zur�ck zu Tabelleneintr�gen";
$MESS["PERFMON_ROW_EDIT_TAB"] = "Eintrag";
$MESS["PERFMON_ROW_EDIT_TAB_TITLE"] = "Eintrag in der Tabelle #TABLE_NAME# bearbeiten";
$MESS["PERFMON_ROW_CACHE_TAB"] = "Verwaltender Cache";
$MESS["PERFMON_ROW_CACHE_TAB_TITLE"] = "Parameter der Zur�cksetzung des verwaltenden Cache";
$MESS["PERFMON_ROW_CACHE_CLEAR"] = "Verwaltenden Cache zur�cksetzen";
$MESS["PERFMON_ROW_EDIT_MENU_DELETE"] = "L�schen";
$MESS["PERFMON_ROW_EDIT_MENU_DELETE_TITLE"] = "Eintrag l�schen";
$MESS["PERFMON_ROW_EDIT_MENU_DELETE_CONF"] = "M�chten Sie diesen Eintrag wirklich l�schen?";
?>