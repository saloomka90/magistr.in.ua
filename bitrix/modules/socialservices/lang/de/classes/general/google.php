<?
$MESS["socserv_google_client_id"] = "Kunde-ID:";
$MESS["socserv_google_client_secret"] = "Geheimcode (Client secret):";
$MESS["socserv_google_form_note"] = "Benutzen Sie Ihr Google-Account, um sich einzuloggen.";
$MESS["socserv_google_form_note_intranet"] = "Login mit dem Google-Account.";
$MESS["socserv_google_note_2"] = "<a href=\"https://console.developers.google.com/\">Eine App</a> in der Google API Console<br>erstellen
Geben Sie diese Adresse im Feld \"Authorized redirect URIs&quot; an: <a href=\"#URL#\">#URL#</a>
<ul style=\"text-align: left;\">
	<li>Erlauben Sie den Zugriff auf Kalender-API und CalDAV API in den App-Einstellungen, wenn Sie den Bitrix24 Kalender mit dem Google Kalender synchronisieren m�chten.</li>
	<li>Erlauben Sie den Zugriff auf Drive-API in den App-Einstellungen, um eine Integration mit Bitrix24.Drive zu machen.</li>
	<li>Um den Zugriff auf Mailboxen zu erlauben, geben Sie diese Adresse im Feld &quot;Authorized redirect URIs&quot; an: <a href=\"#MAIL_URL#\">#MAIL_URL#</a>, dann erlauben Sie den Zugriff auf Gmail API in den App-Einstellungen.
	</li>
</ul>";
$MESS["socserv_googleplus_note"] = "Benutzen Sie die Parameter der Google-Anwendung.";
?>