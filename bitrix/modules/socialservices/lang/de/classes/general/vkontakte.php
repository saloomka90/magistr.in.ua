<?
$MESS['socserv_vk_id'] = "Anwendung-ID:";
$MESS['socserv_vk_key'] = "Geschützter Schlüssel:";
$MESS['socserv_vk_note'] = "Benutzen Sie Ihr VKontakte Profil, um sich einzuloggen.";
$MESS['socserv_vk_note_intranet'] = "Login mit dem VKontakte -Account.";
$MESS['socserv_vk_sett_note'] = "Um die ID und den Schlüssel zu erhalten, <a href=\"http://vk.com/editapp?act=create\">registrieren Sie die Anwendung Vkontakte (\"Website\"-Typ)</a>.";
$MESS['socserv_vk_sett_note1'] = "Um den Code zu bekommen, bitte <a href=\"http://vk.com/editapp?act=create\">registrieren Sie die VK App</a> als eine Website-App.<br />Nutzen Sie die URL <a href=\"#URL#\">#URL#</a> als die \"Trusted redirect URL\".";
?>