<?
$MESS["SOC_OPT_B24NET_CLIENT_ID"] = "Portal-ID";
$MESS["SOC_OPT_B24NET_CLIENT_SECRET"] = "Geheimcode des Portals";
$MESS["SOC_OPT_B24NET_GET"] = "Anfordern";
$MESS["SOC_OPT_B24NET_PUT"] = "Einf�gen";
$MESS["SOC_OPT_B24NET_SITE"] = "Autorisierungsparameter f�r die Website anfordern";
$MESS["SOC_OPT_B24NET_TITLE"] = "Website in Bitrix24.Network registrieren";
$MESS["SOC_OPT_CRYPTO_ACTIVATE"] = "Codierung der Authentifizierungstoken aktivieren";
$MESS["SOC_OPT_CRYPTO_CONFIRM"] = "M�chten Sie die Codierung der Authentifizierungstoken wirklich aktivieren?";
$MESS["SOC_OPT_CRYPTO_FIELD_TITLE"] = "Status:";
$MESS["SOC_OPT_CRYPTO_MESSAGE_ACTIVE"] = "Die Codierung der Authentifizierungstoken ist aktiviert";
$MESS["SOC_OPT_CRYPTO_NOTE"] = "Wird diese Option aktiviert, werden die Authentifizierungstoken der Nutzer f�r Websites der sozialen Medien in codierter Form gespeichert.
<br>
<b>Achtung!</b> Diese Aktion kann nicht r�ckg�ngig gemacht werden.";
$MESS["SOC_OPT_CRYPTO_NO_CRYPTOKEY"] = "Codierungsschl�ssel ist nicht installiert";
$MESS["SOC_OPT_CRYPTO_TAB_DESCR"] = "Codierung der Authentifizierungstoken";
$MESS["SOC_OPT_CRYPTO_TAB_TITLE"] = "Codierung";
$MESS["SOC_OPT_MAIN_DENY_AUTH"] = "Nutzergruppen, die sich nicht �ber soziale Services einloggen k�nnen";
$MESS["SOC_OPT_MAIN_DENY_SPLIT"] = "Nutzergruppe, die soziale Services nicht anbinden k�nnen";
$MESS["SOC_OPT_MAIN_REG"] = "Registrierung neuer Nutzer";
$MESS["SOC_OPT_MAIN_REG_N"] = "nicht erlaubt";
$MESS["SOC_OPT_MAIN_REG_Y"] = "erlaubt";
$MESS["SOC_OPT_SOC_REG"] = "Registrierung neuer Nutzer via Soziale Netzwerke erlauben";
$MESS["soc_serv_opt_allow"] = "Autorisierung �ber externe Services erlauben:";
$MESS["soc_serv_opt_down"] = "Nach unten";
$MESS["soc_serv_opt_list"] = "Services:";
$MESS["soc_serv_opt_list_title"] = "Externe Services:";
$MESS["soc_serv_opt_settings_of"] = "Einstellungen f�r #SERVICE#";
$MESS["soc_serv_opt_up"] = "Nach oben";
$MESS["soc_serv_send_activity"] = "Nutzeraktivit�ten in sozialen Netzwerken ver�ffentlichen";
$MESS["socserv_sett_common"] = "Allgemeine";
$MESS["socserv_sett_common_title"] = "Allgemeine Parameter f�r alle Websites";
$MESS["socserv_sett_site"] = "Einstellungen f�r Websites";
$MESS["socserv_sett_site_apply"] = "Individuelle Einstellungen f�r diese Website anwenden:";
$MESS["socserv_twit_to_buzz"] = "Die Nutzertweets mit dem Hashtag #hash# im Activity Stream ver�ffentlichen.";
?>