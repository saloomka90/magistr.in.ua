<?
$MESS["FILE_UPLOAD_ENTITY_ID_FIELD"] = "ID der Aufgabe zum Hochladen";
$MESS["FILE_UPLOAD_ENTITY_TIMESTAMP_X_FIELD"] = "Aufgabe erstellt am";
$MESS["FILE_UPLOAD_ENTITY_FILE_PATH_FIELD"] = "Pfad zur Datei";
$MESS["FILE_UPLOAD_ENTITY_FILE_SIZE_FIELD"] = "Dateigr��e";
$MESS["FILE_UPLOAD_ENTITY_TMP_FILE_FIELD"] = "Pfad zur tempor�ren Datei";
$MESS["FILE_UPLOAD_ENTITY_BUCKET_ID_FIELD"] = "ID des Speichers";
$MESS["FILE_UPLOAD_ENTITY_PART_SIZE_FIELD"] = "Gr��e des Datenblocks zum Hochladen";
$MESS["FILE_UPLOAD_ENTITY_PART_NO_FIELD"] = "Aktuell wird Datenblock Nr. hochgeladen";
$MESS["FILE_UPLOAD_ENTITY_PART_FAIL_COUNTER_FIELD"] = "Fehlerz�hler";
$MESS["FILE_UPLOAD_ENTITY_NEXT_STEP_FIELD"] = "Einstellungen";
?>