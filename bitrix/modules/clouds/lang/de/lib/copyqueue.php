<?
$MESS["COPY_QUEUE_ENTITY_ID_FIELD"] = "ID der Aufgabe zum Kopieren der Datei";
$MESS["COPY_QUEUE_ENTITY_TIMESTAMP_X_FIELD"] = "Aufgabe erstellt am";
$MESS["COPY_QUEUE_ENTITY_OP_FIELD"] = "Operationscode";
$MESS["COPY_QUEUE_ENTITY_SOURCE_BUCKET_ID_FIELD"] = "Urspr�nglicher Speicher";
$MESS["COPY_QUEUE_ENTITY_SOURCE_FILE_PATH_FIELD"] = "Pfad zur Datei im urspr�nglichen Speicher";
$MESS["COPY_QUEUE_ENTITY_TARGET_BUCKET_ID_FIELD"] = "Zielspeicher";
$MESS["COPY_QUEUE_ENTITY_TARGET_FILE_PATH_FIELD"] = "Pfad zur Datei im Zielspeicher";
$MESS["COPY_QUEUE_ENTITY_FILE_SIZE_FIELD"] = "Dateigr��e";
$MESS["COPY_QUEUE_ENTITY_FILE_POS_FIELD"] = "Aktuelle Kopierposition";
$MESS["COPY_QUEUE_ENTITY_FAIL_COUNTER_FIELD"] = "Fehlerz�hler";
$MESS["COPY_QUEUE_ENTITY_STATUS_FIELD"] = "Operationsstatus";
$MESS["COPY_QUEUE_ENTITY_ERROR_MESSAGE_FIELD"] = "Fehlermeldung";
?>