<?
$MESS["USER_TYPE_ADDRESS_DESCRIPTION"] = "Adresse";
$MESS["USER_TYPE_ADDRESS_NO_KEY_HINT"] = "Ein Google API Schl�ssel ist erforderlich, um Maps nutzen zu k�nnen. Geben Sie bitte Ihren Schl�ssel <a href=\"#settings_path##google_api_key\" target=\"_blank\">im Einstellungsformular</a>ein.";
$MESS["USER_TYPE_ADDRESS_NO_KEY_HINT_B24"] = "Ein Google API Schl�ssel ist erforderlich, um Maps nutzen zu k�nnen. Geben Sie bitte Ihren Schl�ssel <a href=\"#settings_path##google_api_key\" target=\"_blank\">im Einstellungsformular</a>ein.";
$MESS["USER_TYPE_ADDRESS_SHOW_MAP"] = "Karte anzeigen";
$MESS["USER_TYPE_ADDRESS_TRIAL"] = "<span>Nutzen Sie Google Maps, um:</span><ol><li>die Hilfe bei Adressfindung zu bekommen;</li><li>Adressen auf einer interaktiven Karte anzuzeigen.</li></ol><span>Google Maps in CRM Elementen sind in bestimmten kostenpflichtigen Tarifen verf�gbar.</span>";
$MESS["USER_TYPE_ADDRESS_TRIAL_TITLE"] = "Ihre Testzeit f�r Google Map ist nun abgelaufen.";
?>