<?
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_CAN_USE_ERROR'] = "Anbieter des SMS-Assistenten ist nicht konfiguriert";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_1'] = "Guthaben nicht ausreichend";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_10'] = "Service ist vor�bergehend nicht verf�gbar";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_11'] = "Wert der SMS-ID ist nicht korrekt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_12'] = "Unbekannter Fehler";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_13'] = "Gesperrt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_2'] = "Login oder Passwort nicht korrekt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_3'] = "SMS-Text fehlt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_4'] = "Telefonnummer des Empf�ngers ist nicht korrekt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_5'] = "Telefonnummer des Absenders ist nicht korrekt";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_6'] = "Login ist erforderlich";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_7'] = "Passwort ist erforderlich";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_ERROR_OTHER'] = "Systemfehler. Versuchen Sie bitte erneut.";
$MESS['MESSAGESERVICE_SENDER_SMS_SMSASTBY_NAME'] = "SMS-Assistant";
?>