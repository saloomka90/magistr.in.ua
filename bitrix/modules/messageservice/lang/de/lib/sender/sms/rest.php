<?
$MESS["MESSAGESERVICE_SENDER_SMS_REST_NAME"] = "SMS via REST API";
$MESS["MESSAGESERVICE_SENDER_SMS_REST_ERROR_CAN_USE"] = "REST API der Messaging-Services ist nicht verf�gbar";
$MESS["MESSAGESERVICE_SENDER_SMS_REST_ERROR_APP_NOT_FOUND"] = "Anwendung zum Nachrichtenversand ist nicht registriert oder wurde deinstalliert";
$MESS["MESSAGESERVICE_SENDER_SMS_REST_ERROR_SESSION"] = "REST-Sitzung gesperrt";
$MESS["MESSAGESERVICE_SENDER_SMS_REST_ERROR_PAYMENT_ALLOW"] = "App wurde nicht bezahlt";
?>