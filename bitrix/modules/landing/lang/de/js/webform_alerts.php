<?
$MESS["LANDING_BLOCK_WEBFORM_CONNECT_SUPPORT"] = "Bitte kontaktieren Sie den Helpdesk";
$MESS["LANDING_BLOCK_WEBFORM_ERROR"] = "Fehler beim Erstellen des Formulars";
$MESS["LANDING_BLOCK_WEBFORM_NO_FORM"] = "Es gibt keine aktiven Online-Formulare";
$MESS["LANDING_BLOCK_WEBFORM_NO_FORM_BUS"] = "Ihre Website hat keine Online-Formulare. �berpr�fen Sie, ob das Modul \"Bitrix24 Connector\" (b24connector) installiert ist: <a target=\"_blank\" class=\"landing-trusted-link\" href=\"/bitrix/admin/module_admin.php\">Einstellungen > Systemeinstellungen > Module</a>. Au�erdem stellen Sie bitte sicher, dass Ihr Bitrix24 Portal in den Einstellungen \"<a target=\"_blank\" class=\"landing-trusted-link\" href=\"/bitrix/admin/settings.php?mid=b24connector\">Kundenkommunikation</a>\" verkn�pft ist.";
$MESS["LANDING_BLOCK_WEBFORM_NO_FORM_CP"] = "Ihre Website hat keine Online-Formulare. Stellen Sie sicher, es gibt mindestens eine aktive Vorlage auf der Seite <a target=\"_blank\" class=\"landing-trusted-link\" href=\"/crm/webform\">CRM > Online-Formulare</a>.";
?>