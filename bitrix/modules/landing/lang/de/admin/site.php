<?
$MESS["LANDING_ADMIN_ACTION_ADD"] = "Website oder Onlineshop";
$MESS["LANDING_ADMIN_ACTION_ADD_ONE"] = "Seite erstellen";
$MESS["LANDING_ADMIN_ACTION_ADD_PAGE"] = "Vorlagen f�r Website und Landing";
$MESS["LANDING_ADMIN_ACTION_ADD_STORE"] = "Vorlagen f�r Onlineshop";
$MESS["LANDING_ADMIN_ACTION_CATALOG"] = "Katalogeinstellungen";
$MESS["LANDING_ADMIN_ACTION_SETTINGS"] = "Website-Einstellungen";
$MESS["LANDING_ADMIN_SITE_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["LANDING_ADMIN_SITE_NOT_FOUND"] = "Website wurde nicht gefunden.";
?>