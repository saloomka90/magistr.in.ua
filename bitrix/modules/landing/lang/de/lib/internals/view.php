<?
$MESS["LANDING_TABLE_FIELD_FIRST_VIEW"] = "Datum der ersten Ansicht";
$MESS["LANDING_TABLE_FIELD_LAST_VIEW"] = "Datum der letzten Ansicht";
$MESS["LANDING_TABLE_FIELD_LID"] = "ID der Seite";
$MESS["LANDING_TABLE_FIELD_USER_ID"] = "ID des Nutzers";
$MESS["LANDING_TABLE_FIELD_VIEWS"] = "Angezeigt pro Nutzer";
?>