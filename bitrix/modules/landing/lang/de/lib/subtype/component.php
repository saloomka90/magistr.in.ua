<?
$MESS["LANDING_BLOCK_CATALOG_CONFIG"] = "Kommerziellen Katalog �ffnen";
$MESS["LANDING_BLOCK_CATALOG_CONFIG_FEATURE"] = "Die Option \"Parameter der Eigenschaften in Komponenten und Formularen nutzen\" in den Einstellungen des Moduls Informationsbl�cke aktivieren. Danach m�ssen die Eigenschaften konfiguriert werden, wie auf <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=42&LESSON_ID=1986\" target=\"_blank\">dieser Seite</a> beschrieben.";
$MESS["LANDING_BLOCK_EMPTY_CATLOG_DESC"] = "W�hlen Sie bitte zuerst die Quelle des Katalogs aus.";
$MESS["LANDING_BLOCK_EMPTY_CATLOG_LINK"] = "Konfigurieren";
$MESS["LANDING_BLOCK_EMPTY_CATLOG_TITLE"] = "Katalog ist nicht konfiguriert";
?>