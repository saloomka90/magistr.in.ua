<?
$MESS["LANDING_BLOCK_FORM_CONFIG"] = "Online-Formulare verwalten";
$MESS["LANDING_BLOCK_WEBFORM"] = "Online-Formular";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER"] = "Formularüberschrift";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER_Y"] = "Anzeigen";
$MESS["LANDING_BLOCK_WEBFORM_SHOW_HEADER_N"] = "Ausblenden";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE"] = "Formularstil";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE_Y"] = "Container-Stil nutzen";
$MESS["LANDING_BLOCK_WEBFORM_USE_STYLE_N"] = "Stil des Online-Formulars nutzen";
$MESS["LANDING_BLOCK_WEBFORM_NO_FORM"] = "Es gibt keine aktiven Online-Formulare";
?>