<?
$MESS["LANDING_HOOK_METAMAIN_NAME"] = "Meta-Elemente der Seite";
$MESS["LANDING_HOOK_METAMAIN_DESCRIPTION"] = "Geben Sie Meta-Elemente f�r eine bessere Indexierung bei Suchmaschinen an. Die Meta-Elemente werden automatisch hinzugef�gt, wenn es keine angegeben sind.";
$MESS["LANDING_HOOK_METAMAIN_USE"] = "Meta-Elemente bearbeiten";
$MESS["LANDING_HOOK_METAMAIN_TITLE"] = "�berschrift (Tag TITLE)";
$MESS["LANDING_HOOK_METAMAIN_TITLE_PLACEHOLDER"] = "�berschrift der Seite angeben";
$MESS["LANDING_HOOK_METAMAIN_DESCRIPTION_TITLE"] = "Beschreibung";
$MESS["LANDING_HOOK_METAMAIN_DESCRIPTION_PLACEHOLDER"] = "Seitenbeschreibung eingeben";
$MESS["LANDING_HOOK_METAMAIN_KEYWORDS_TITLE"] = "Keywords";
?>