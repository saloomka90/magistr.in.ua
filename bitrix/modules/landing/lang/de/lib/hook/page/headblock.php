<?
$MESS["LANDING_HOOK_HEADBLOCK_CODE"] = "HEAD-Block";
$MESS["LANDING_HOOK_HEADBLOCK_CODE_HELP2"] = "F�gen Sie benutzerdefinierten HTML-Code (Statistik-Tracker, Meta-Tags etc.) zum HEAD-Tag auf allen Seiten hinzu.";
$MESS["LANDING_HOOK_HEADBLOCK_NAME2"] = "Benutzerdefiniertes HTML";
$MESS["LANDING_HOOK_HEADBLOCK_USE"] = "Hinzuf�gen / Bearbeiten";
$MESS['LANDING_HOOK_HEADBLOCK_LOCKED'] = '<p>Ihr aktueller Tarif erlaubt kein benutzerdefiniertes HTML.</p><p>Sie sollten auf einen h�heren Tarif upgraden, um Ihr eigenes HTML zum Website-Code hinzuf�gen zu k�nnen.</p>';
?>