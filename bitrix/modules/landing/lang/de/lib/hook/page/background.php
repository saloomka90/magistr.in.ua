<?
$MESS["LANDING_HOOK_BG_NAME"] = "Seitenhintergrund";
$MESS["LANDING_HOOK_BG_DESCRIPTION"] = "Hintergrund der aktuellen Seite �ndern. Die Websiteeinstellungen werden genutzt, wenn kein Hintergrund ausgew�hlt wurde.";
$MESS["LANDING_HOOK_BG_USE"] = "Benutzerdefinierter Seitenhintergrund";
$MESS["LANDING_HOOK_BG_PICTURE"] = "Hintergrundbild";
$MESS["LANDING_HOOK_BG_POSITION"] = "Position";
$MESS["LANDING_HOOK_BG_POSITION_HELP"] = "Definiert die Position des Hintergrundbildes.  \"Kacheln\" wird das Bild �ber den ganzen Bildschirm wiederholen. \"Spannen\" wird das Bild skalieren und an die Breite und H�he des Bildschirms anpassen.";
$MESS["LANDING_HOOK_BG_POSITION_CENTER"] = "Spannen ";
$MESS["LANDING_HOOK_BG_POSITION_REPEAT"] = "Kacheln";
$MESS["LANDING_HOOK_BG_COLOR"] = "Hintergrundfarbe";
?>