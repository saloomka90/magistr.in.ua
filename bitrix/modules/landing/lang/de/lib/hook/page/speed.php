<?
$MESS["LANDING_HOOK_SPEED_HELP"] = "Mehr zur Website-Beschleunigung";
$MESS["LANDING_HOOK_SPEED_TTILE"] = "Website-Beschleunigung";
$MESS["LANDING_HOOK_SPEED_USE_LAZY"] = "Den Modus Lazy-Load f�r Bilder aktivieren";
$MESS["LANDING_HOOK_SPEED_USE_WEBP"] = "Bilder zu WEBP konvertieren";
$MESS["LANDING_HOOK_SPEED_USE_WEBPACK"] = "Laden der Stile und Skripts verz�gern";
$MESS["LANDING_HOOK_SPEED_USE_WEBPACK2"] = "Ladezeit der Seite optimieren";
?>