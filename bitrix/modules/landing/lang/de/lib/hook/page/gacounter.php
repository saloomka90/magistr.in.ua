<?
$MESS["LANDING_HOOK_DETAIL_HELP"] = "Mehr";
$MESS["LANDING_HOOK_GACOUNTER_CLICK_TYPE"] = "Datentyp";
$MESS["LANDING_HOOK_GACOUNTER_CLICK_TYPE_HREF"] = "Link";
$MESS["LANDING_HOOK_GACOUNTER_CLICK_TYPE_TEXT"] = "Linktext";
$MESS["LANDING_HOOK_GACOUNTER_COUNTER"] = "Google Analytics";
$MESS["LANDING_HOOK_GACOUNTER_LOCKED"] = "<p>Nutzen Sie Google Analytics, um die Statistik zum Website-Traffic zu sammeln und zu analysieren. Diese Funktion ist nur in bestimmten kostenpflichtigen Tarifen verf�gbar.</p><p>Upgraden Sie jetzt, um den Zugriff auf erweiterte Website-Einstellungen zu bekommen.</p>";
$MESS["LANDING_HOOK_GACOUNTER_PLACEHOLDER"] = "ID eingeben";
$MESS["LANDING_HOOK_GACOUNTER_SEND_CLICK"] = "Statistik zu Klicks auf Schaltfl�chen und Links senden";
$MESS["LANDING_HOOK_GACOUNTER_SEND_SHOW"] = "Statistik zum Anzeigen der Seitenbl�cke senden";
$MESS["LANDING_HOOK_GACOUNTER_USE"] = "Google Analytics";
?>