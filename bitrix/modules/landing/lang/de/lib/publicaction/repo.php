<?
$MESS["LANDING_APP_CONTENT_IS_BAD"] = "Die Blockinhalte wurden als nicht sicher markiert. Nutzen Sie die Methode landing.repo.checkcontent, um nicht sichere Bereiche zu markieren.";
$MESS["LANDING_APP_PRESET_CONTENT_IS_BAD"] = "Die Inhalte vom Preset \"#preset#\" (\"#card#\") wurden als nicht sicher markiert. Netzen Sie die Methode landing.repo.checkcontent, um nicht sichere Bereiche zu markieren.";
$MESS["LANDING_APP_PLACEMENT_EXIST"] = "Dieser Einbettungsbereich existiert bereits";
$MESS["LANDING_APP_PLACEMENT_NO_EXIST"] = "Dieser Einbettungsbereich existiert nicht";
$MESS["LANDING_APP_NOT_FOUND"] = "Anwendung wurde nicht gefunden.";
?>