<?
$MESS["LANDING_BLOCK_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["LANDING_BLOCK_BAD_ANCHOR"] = "Anker soll mit einem alphabetischen Zeichen (a-z) anfangen und darf nur die Zeichen \"a-z\", \"0-9\", \"-\", \"_\", \".\" und \":\" enthalten.";
$MESS["LANDING_BLOCK_BR1"] = "Katalog";
$MESS["LANDING_BLOCK_BR2"] = "Produktseite";
$MESS["LANDING_BLOCK_CARD_NOT_FOUND"] = "Blockkarte nicht gefunden";
$MESS["LANDING_BLOCK_LANDING_NOT_EXIST"] = "Landingpage existiert nicht";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_DYNAMIC_LIMIT_TEXT"] = "In Ihrem aktuellen Tarif k�nnen Sie bis zu zwei dynamischen Bl�cken ver�ffentlichen. Bitte l�schen Sie die vorher erstellten Bl�cke oder upgraden Sie auf einen h�heren Tarif.";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_DYNAMIC_LIMIT_TITLE"] = "Zugriff auf Funktionen eingeschr�nkt";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_EVAL"] = "Beim Ausf�hren des Blocks ist ein Fehler aufgetreten. Versuchen Sie bitte erneut, ihn hinzuzuf�gen.";
$MESS["LANDING_BLOCK_MESSAGE_ERROR_LIMIT_BUTTON"] = "Jetzt upgraden";
$MESS["LANDING_BLOCK_NOT_FOUND"] = "Block bzw. dessen Inhalte wurden nicht gefunden";
$MESS["LANDING_BLOCK_SEPARATOR_PARTNER"] = "Anwendungen";
$MESS["LANDING_BLOCK_SUBSCRIBE_EXP_BUTTON"] = "Abonnement jetzt aktivieren";
$MESS["LANDING_BLOCK_SUBSCRIBE_EXP_HEADER"] = "Abonnement ist erforderlich";
$MESS["LANDING_BLOCK_SUBSCRIBE_EXP_MESSAGE"] = "<p>Dieser Block wurde von einer der Bitrix24 Anwendungen hinzugef�gt, die in Ihrem Bitrix24 installiert sind. Damit dieser Block auch auf Ihrer Website verf�gbar wird, m�ssen Sie das Abonnement f�r Ihre Anwendungen aktivieren.</p>";
$MESS["LANDING_BLOCK_TEXT_FULL"] = "Block zu gro�";
$MESS["LANDING_BLOCK_TITLE"] = "Seiten�berschrift";
$MESS["LANDING_BLOCK_WRONG_VERSION"] = "Die Blockversion entspricht nicht der Modulversion.";
$MESS["LD_BLOCK_SECTION_LAST"] = "Zuletzt verwendet";
?>