<?
$MESS["LANDING_ACCESS_DENIED"] = "Der Service Landingpage ist momentan nur f�r Administratoren verf�gbar.";
$MESS["LANDING_ACCESS_DENIED2"] = "Sie haben nicht gen�gend Rechte.";
$MESS["LANDING_METHOD_NOT_FOUND"] = "Methode wurde nicht gefunden";
$MESS["LANDING_MISSING_PARAMS"] = "Einige aufgerufene Parameter fehlen: #MISSING#";
$MESS["LANDING_REST_DELETE_EXIST_BLOCKS"] = "Es gibt Bl�cke, die d�r diese Anwendung mit Bitrix24 Sites erstellt wurden. Zuerst sollten Sie diese Bl�cke l�schen.";
$MESS["LANDING_REST_DELETE_EXIST_PAGES"] = "Es gibt Seiten und/oder Websites, die f�r diese Anwendung erstellt wurden. Zuerst sollten Sie diese l�schen.";
$MESS["LANDING_SESSION_EXPIRED"] = "Ihre Sitzung ist abgelaufen.";
?>