<?
$MESS["LANDING_CLB_ERROR_DELETE_SMN"] = "Bitte l�schen Sie zuerst die Seiten der Website im Bereich Sites24, bevor Sie die Website selbst l�schen werden.";
$MESS["LANDING_COPY_ERROR_SITE_NOT_FOUND"] = "Website nicht gefunden, oder Zugriff verweigert";
$MESS["LANDING_EXPORT_ERROR"] = "Der Parameter \"code\" kann nur lateinische Zeichen sowie Zahlen enthalten.";
$MESS["LANDING_TYPE_GROUP"] = "Arbeitsgruppen";
$MESS["LANDING_TYPE_KNOWLEDGE"] = "Wissensbasis";
$MESS["LANDING_TYPE_PAGE"] = "Landing";
$MESS["LANDING_TYPE_PREVIEW"] = "Vorschau";
$MESS["LANDING_TYPE_SMN"] = "Projekt";
$MESS["LANDING_TYPE_STORE"] = "Onlineshop";
?>