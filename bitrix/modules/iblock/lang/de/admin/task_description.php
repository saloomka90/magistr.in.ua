<?
$MESS["TASK_NAME_IBLOCK_DENY"] = "Zugriff verweigert";
$MESS["TASK_DESC_IBLOCK_DENY"] = "Zugriff auf diesen Informationsblock ist verweigert";
$MESS["TASK_NAME_IBLOCK_READ"] = "Lesen";
$MESS["TASK_DESC_IBLOCK_READ"] = "Elemente und Bereiche im Bereich Ansicht anzeigen";
$MESS["TASK_NAME_IBLOCK_ELEMENT_ADD"] = "Hinzuf�gen";
$MESS["TASK_DESC_IBLOCK_ELEMENT_ADD"] = "Informationsblockelemente hinzuf�gen";
$MESS["TASK_NAME_IBLOCK_ADMIN_READ"] = "Im administrativen Bereich anzeigen";
$MESS["TASK_DESC_IBLOCK_ADMIN_READ"] = "Elemente und Bereiche im administrativen Bereich anzeigen";
$MESS["TASK_NAME_IBLOCK_ADMIN_ADD"] = "Im administrativen Bereich erstellen";
$MESS["TASK_DESC_IBLOCK_ADMIN_ADD"] = "Informationsblockelemente im administrativen Bereich erstellen";
$MESS["TASK_NAME_IBLOCK_LIMITED_EDIT"] = "Eingeschr�nkt bearbeiten";
$MESS["TASK_DESC_IBLOCK_LIMITED_EDIT"] = "Elemente mit R�cksicht auf Dokumenten-Workflow-Status und Workflows bearbeiten";
$MESS["TASK_NAME_IBLOCK_FULL_EDIT"] = "Bearbeiten";
$MESS["TASK_DESC_IBLOCK_FULL_EDIT"] = "Bearbeitet Elemente. Zugriffsberechtigungen k�nnen dabei nicht ge�ndert werden.";
$MESS["TASK_NAME_IBLOCK_FULL"] = "Voller Zugriff";
$MESS["TASK_DESC_IBLOCK_FULL"] = "Bearbeitet Informationsbl�cke, Elemente und Bereiche ohne jegliche Einschr�nkungen";
$MESS["TASK_BINDING_IBLOCK"] = "Infoblock";
?>