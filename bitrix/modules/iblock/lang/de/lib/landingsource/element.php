<?
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_ACTIONS"] = "Aktionen";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_ACTION_TITLE_ADD_TO_CART"] = "Zum Warenkorb hinzuf�gen";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_ACTION_TITLE_BUY"] = "Kaufen";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_DETAIL_PICTURE"] = "Detailliertes Bild";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_DETAIL_TEXT"] = "Detaillierter Text";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_NAME"] = "Name";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_PREVIEW_PICTURE"] = "Vorschaubild";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_PREVIEW_TEXT"] = "Beschreibungstext";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_FIELD_SORT"] = "Sortierung";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PRODUCT_FIELD_AVAILABLE"] = "Verf�gbarkeit";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PRODUCT_FIELD_PRICE"] = "Preis";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PRODUCT_FIELD_SIZES"] = "Gr��e";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PRODUCT_FIELD_WEIGHT"] = "Gewicht";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PROPERTY_DETAIL_TITLE"] = "Eigenschaft #NAME# (nur die Seite mit Details)";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PROPERTY_LIST_TITLE"] = "Eigenschaft #NAME# (nur die Elementliste)";
$MESS["IBLOCK_LANDING_SOURCE_ELEMENT_PROPERTY_TITLE"] = "Eigenschaft #NAME#";
?>