<?
$MESS["IBLOCK_GRID_PANEL_ACTION_ADD_SECTION"] = "Link in den Bereich hinzuf�gen";
$MESS["IBLOCK_GRID_PANEL_ACTION_ADJUST_SECTION"] = "In den Bereich verschieben";
$MESS["IBLOCK_GRID_PANEL_ACTION_CLEAR_COUNTER"] = "Ansichtsz�hler zur�cksetzen";
$MESS["IBLOCK_GRID_PANEL_ACTION_CLEAR_COUNTER_CONFIRM"] = "M�chten Sie f�r die ausgew�hlten Elemente den Ansichtsz�hler sowie die Zeit der ersten Ansicht wirklich zur�cksetzen?";
$MESS["IBLOCK_GRID_PANEL_ACTION_CODE_TRANSLITERATION"] = "Symbolischen Code erstellen";
$MESS["IBLOCK_GRID_PANEL_ACTION_CODE_TRANSLITERATION_CONFIRM"] = "M�chten Sie den symbolischen Code f�r die ausgew�hlten Elemente wirklich erstellen?";
$MESS["IBLOCK_GRID_PANEL_ACTION_MESS_SECTION_TOP_LEVEL"] = "Obere Ebene";
$MESS["IBLOCK_GRID_PANEL_ACTION_ACTIVATE"] = "aktivieren";
$MESS["IBLOCK_GRID_PANEL_ACTION_DEACTIVATE"] = "deaktivieren";
$MESS["IBLOCK_GRID_PANEL_ACTION_DELETE"] = "l�schen";
$MESS["IBLOCK_GRID_PANEL_ACTION_EDIT"] = "Bearbeiten";
$MESS["IBLOCK_GRID_PANEL_ACTION_ELEMENT_UNLOCK"] = "freigeben";
$MESS["IBLOCK_GRID_PANEL_ACTION_ELEMENT_LOCK"] = "blockieren";
$MESS["IBLOCK_GRID_PANEL_ACTION_ELEMENT_WORKFLOW_STATUS"] = "Status �ndern";
?>