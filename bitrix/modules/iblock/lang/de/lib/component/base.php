<?
$MESS["CATALOG_EMPTY_BASKET_PROPERTIES_ERROR"] = "Produkteigenschaften, die zum Warenkorb hinzugefügt werden sollen, sind nicht ausgefüllt ";
$MESS["CATALOG_ERROR2BASKET"] = "Beim Hinzufügen des Produkts in den Warenkorb ist ein Fehler aufgetreten";
$MESS["CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR"] = "Nicht alle Produkteigenschaften, die Sie zum Warenkorb hinzufügen wollen, wurden ausgefüllt";
$MESS["CATALOG_PRODUCT_ID_IS_ABSENT"] = "Kein Produkt ausgewählt";
$MESS["CATALOG_PRODUCT_NOT_FOUND"] = "Das Produkt wurde nicht gefunden.";
$MESS["CATALOG_SUCCESSFUL_ADD_TO_BASKET"] = "Produkt wurde zum Warenkorb erfolgreich hinzugefügt";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert";
$MESS["INVALID_IBLOCK"] = "Informationsblock ist nicht korrekt";
?>