<?
$MESS["BLOG_C_HIDE"] = "Kommentare ausblenden";
$MESS["BLOG_C_REPLY"] = "Antworten";
$MESS["BLOG_C_VIEW"] = "Kommentare anzeigen";
$MESS["BPC_MES_COPYLINK"] = "Link kopieren";
$MESS["BPC_MES_CREATETASK"] = "Aufgabe erstellen";
$MESS["BPC_MES_DELETE"] = "L�schen";
$MESS["BPC_MES_EDIT"] = "Bearbeiten";
$MESS["BPC_MES_HIDE"] = "Ausblenden";
$MESS["BPC_MES_SHOW"] = "Anzeigen";
$MESS["BPC_MES_VOTE"] = "\"Gef�llt mir\"-Angaben";
$MESS["BPC_MES_VOTE1"] = "Gef�llt mir";
$MESS["BPC_MES_VOTE2"] = "Gef�llt mir nicht";
$MESS["B_B_PC_COM_ERROR"] = "Fehler:";
$MESS["INCORRECT_SERVER_RESPONSE"] = "Die Antwort des Servers ist nicht korrekt.";
?>