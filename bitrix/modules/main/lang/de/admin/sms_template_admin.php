<?
$MESS['sms_template_admin_add'] = "Vorlage hinzuf�gen";
$MESS['sms_template_admin_add_title'] = "Eine neue Vorlage der SMS-Nachricht hinzuf�gen";
$MESS['sms_template_admin_all'] = "(alle)";
$MESS['sms_template_admin_copy'] = "Kopie hinzuf�gen";
$MESS['sms_template_admin_del'] = "L�schen";
$MESS['sms_template_admin_del_conf'] = "M�chten Sie diese Vorlage wirklich l�schen?";
$MESS['sms_template_admin_edit'] = "Vorlage bearbeiten";
$MESS['sms_template_admin_edit1'] = "Bearbeiten";
$MESS['sms_template_admin_find'] = "Finden:";
$MESS['sms_template_admin_message'] = "Nachrichtentext";
$MESS['sms_template_admin_nav'] = "Vorlagen";
$MESS['sms_template_admin_no'] = "Nein";
$MESS['sms_template_admin_receiver'] = "Empf�nger";
$MESS['sms_template_admin_sender'] = "Absender";
$MESS['sms_template_admin_site'] = "Website";
$MESS['sms_template_admin_sites'] = "Websites";
$MESS['sms_template_admin_title'] = "SMS-Vorlagen";
$MESS['sms_template_admin_yes'] = "Ja";
?>