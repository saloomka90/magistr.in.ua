<?
$MESS["log_notifications_active"] = "Aktiv";
$MESS["log_notifications_add"] = "Eine Benachrichtigung hinzuf�gen";
$MESS["log_notifications_add_title"] = "Zum Ereignisprotokoll eine neue Benachrichtigung hinzuf�gen";
$MESS["log_notifications_browser"] = "Browser";
$MESS["log_notifications_copy"] = "Kopieren";
$MESS["log_notifications_count"] = "Ereignisse";
$MESS["log_notifications_date_last"] = "Datum der letzten Pr�fung:";
$MESS["log_notifications_delete"] = "L�schen";
$MESS["log_notifications_delete_conf"] = "M�chten Sie diese Benachrichtigung wirklich l�schen?";
$MESS["log_notifications_edit"] = "Benachrichtigung bearbeiten";
$MESS["log_notifications_edit1"] = "Bearbeiten";
$MESS["log_notifications_interval"] = "Intervall";
$MESS["log_notifications_ip"] = "IP-Adresse";
$MESS["log_notifications_name"] = "Name";
$MESS["log_notifications_object"] = "Objekt";
$MESS["log_notifications_page"] = "Seite";
$MESS["log_notifications_title"] = "Benachrichtigungen des Ereignisprotokolls";
$MESS["log_notifications_type"] = "Ereignistyp";
$MESS["log_notifications_user"] = "Nutzer";
?>