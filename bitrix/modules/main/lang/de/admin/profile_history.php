<?
$MESS["main_profile_history_error_date1"] = "Startdatum ist nicht korrekt.";
$MESS["main_profile_history_error_date2"] = "Enddatum ist nicht korrekt.";
$MESS["main_profile_history_records"] = "Eintr�ge";
$MESS["main_profile_history_date"] = "Datum";
$MESS["main_profile_history_user"] = "Nutzer";
$MESS["main_profile_history_event"] = "Ereignis";
$MESS["main_profile_history_updated_by"] = "Ge�ndert von";
$MESS["main_profile_history_changes"] = "Was ge�ndert";
$MESS["main_profile_history_ip"] = "IP-Adresse";
$MESS["main_profile_history_browser"] = "Browser";
$MESS["main_profile_history_url"] = "URL der Seite";
$MESS["main_profile_history_adding"] = "Hinzuf�gen";
$MESS["main_profile_history_updating"] = "�ndern";
$MESS["main_profile_history_deleting"] = "L�schen";
$MESS["main_profile_history_title"] = "Profil-History";
$MESS["main_profile_history_filter_id"] = "Nutzer-ID";
$MESS["main_profile_history_filter_event"] = "Ereignistyp";
$MESS["main_profile_history_filter_date"] = "Erstellt am";
$MESS["main_profile_history_filter_ip"] = "IP-Adresse";
$MESS["main_profile_history_filter_browser"] = "Browser";
$MESS["main_profile_history_filter_url"] = "URL der Seite";
$MESS["main_profile_history_filter_field"] = "Profilfeld";
$MESS["main_profile_history_filter_all"] = "Alle";
$MESS["main_profile_history_note"] = "Auf der Seite der Einstellungen des Hauptmoduls k�nnen Sie die History der �nderungen der Nutzerprofilfelder aktivieren bzw. deaktivieren.";
$MESS["ACCESS_DENIED"] = "Zugriff verweigert.";
?>