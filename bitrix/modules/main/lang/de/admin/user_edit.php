<?
$MESS["ACCOUNT_INSERT"] = "Der Administrator hat Sie auf der Seite angemeldet ";
$MESS["ACCOUNT_UPDATE"] = "Der Administrator hat Ihre pers�nlichen Informationen aktualisiert.";
$MESS["ACTIVE"] = "Aktiv:";
$MESS["ADD"] = "Neuer Nutzer";
$MESS["AUTH_NONSECURE_NOTE"] = "Das Passwort wird in offener Form versendet. Aktivieren Sie JavaScript  in Ihrem Web-Browser, um das Passwort vor dem Versenden zu verschl�sseln.";
$MESS["AUTH_SECURE_NOTE"] = "Das Passwort wird verschl�sselt, bevor es versendet wird. So wird das Passwort w�hrend der �bertragung nicht offen angezeigt.";
$MESS["EDIT_USER_TITLE"] = "Nutzer Nr. #ID#";
$MESS["EMAIL"] = "E-Mail:";
$MESS["GROUPS"] = "Gruppen";
$MESS["INFO_FOR_USER"] = "Nutzer benachrichtigen:";
$MESS["INFO_FOR_USER_LANG"] = "Seitenvorlage";
$MESS["LAST_LOGIN"] = "Letzte Anmeldung:";
$MESS["LAST_NAME"] = "Nachname:";
$MESS["LAST_UPDATE"] = "Aktualisierungsdatum:";
$MESS["LOGIN"] = "Loginname (min. 3 Zeichen):";
$MESS["MAIN_ADMIN_AUTH"] = "Anmelden";
$MESS["MAIN_ADMIN_AUTH_TITLE"] = "Sich unter dem bestimmten Nutzer anmelden";
$MESS["MAIN_COPY_RECORD"] = "Kopieren";
$MESS["MAIN_COPY_RECORD_TITLE"] = "Nutzerkopie hinzuf�gen";
$MESS["MAIN_DEFAULT_SITE"] = "Standardseite f�r Mitteilungen:";
$MESS["MAIN_DELETE_RECORD"] = "L�schen";
$MESS["MAIN_DELETE_RECORD_CONF"] = "Wollen Sie diesen Nutzer wirklich l�schen?";
$MESS["MAIN_DELETE_RECORD_TITLE"] = "Aktuellen Nutzer l�schen";
$MESS["MAIN_ERROR_SAVING"] = "Beim Speichern ist ein Fehler aufgetreten";
$MESS["MAIN_NEW_RECORD"] = "Hinzuf�gen";
$MESS["MAIN_NEW_RECORD_TITLE"] = "Neuer Nutzer";
$MESS["MAIN_USERED_AUTH_INT"] = "(interne Anmeldung)";
$MESS["MAIN_USERED_AUTH_TYPE"] = "Login-Typ:";
$MESS["MAIN_USER_EDIT_EXT"] = "Externe ID:";
$MESS["MAIN_USER_EDIT_HISTORY"] = "History";
$MESS["MAIN_USER_EDIT_HISTORY_TITLE"] = "Protokoll der �nderungen des Nutzerprofils";
$MESS["MAIN_USER_TAB1"] = "Nutzer";
$MESS["MAIN_USER_TAB1_TITLE"] = "Registrierungsinformation";
$MESS["MAIN_USER_TAB2_TITLE"] = "Gruppenzugeh�rigkeit";
$MESS["MAIN_USER_TAB4"] = "Arbeit";
$MESS["MAIN_USER_TAB5"] = "Anmerkungen";
$MESS["MAIN_VIEW_GROUP"] = "Nutzergruppen-Parameter anzeigen";
$MESS["NAME"] = "Name:";
$MESS["NEW_PASSWORD"] = "Neues Passwort (min. 6 Zeichen):";
$MESS["NEW_PASSWORD_CONFIRM"] = "Neues Passwort best�tigen:";
$MESS["NEW_PASSWORD_REQ"] = "Neues Passwort";
$MESS["NEW_USER_TITLE"] = "Neuer Nutzer";
$MESS["RATING_BONUS"] = "Anfangswert";
$MESS["RATING_BONUS_NOTICE"] = "Ein entsprechendes Kriterium soll in Ranking-Eigenschaften eingeschlossen werden, um bei dieser Option ber�cksichtigt zu werden.";
$MESS["RATING_CURRENT_VALUE"] = "Aktueller Wert";
$MESS["RATING_NORM_VOTE_WEIGHT"] = "Normiertes Stimmgewicht";
$MESS["RATING_NOT_AVAILABLE"] = "Rankings m�ssen im Bereich \"Services > Rankings\" aktiviert werden, um sichtbar zu sein.";
$MESS["RATING_POSITION"] = "Aktueller Platz";
$MESS["RATING_PREVIOUS_VALUE"] = "Vorheriger Wert";
$MESS["RATING_TAB_INFO"] = "Information";
$MESS["RATING_VOTE_AUTHORITY_COUNT"] = "Max. Stimmenanzahl f�r Abstimmung auf eine Autorit�t";
$MESS["RATING_VOTE_NORM_VOTE"] = "Anzahl der normierten Stimmen";
$MESS["RATING_VOTE_WEIGHT"] = "Stimmgewicht f�r Abstimmung auf ein Ranking";
$MESS["RATING_VOTE_WEIGHT_AUTHORITY"] = "Stimmgewicht f�r Abstimmung auf eine Autorit�t";
$MESS["RECORD_LIST"] = "Nutzerliste";
$MESS["RECORD_LIST_TITLE"] = "Nutzerliste";
$MESS["RESET"] = "Zur�cksetzen";
$MESS["SAVE"] = "�nderungen speichern";
$MESS["SECOND_NAME"] = "Zweiter Name:";
$MESS["TBL_GROUP"] = "Gruppe";
$MESS["TBL_GROUP_DATE"] = "Dauer der Aktivit�t";
$MESS["USER_ADMIN_NOTES"] = "Anmerkungen des Administrators";
$MESS["USER_BIRTHDAY"] = "Geburtsdatum (Text)";
$MESS["USER_BIRTHDAY_DT"] = "Geburtsdatum";
$MESS["USER_CITY"] = "Ort:";
$MESS["USER_COMPANY"] = "Firma:";
$MESS["USER_COUNTRY"] = "Land:";
$MESS["USER_DEPARTMENT"] = "Abteilung:";
$MESS["USER_DONT_KNOW"] = "(unbekannt)";
$MESS["USER_EDIT_DATE_REGISTER"] = "Registrierungsdatum:";
$MESS["USER_EDIT_TITLE"] = "Anrede:";
$MESS["USER_EDIT_WARNING_MAX"] = "Achtung! Sie haben die Anzahl der lizensierten Nutzer �berschritten!";
$MESS["USER_FAX"] = "Fax:";
$MESS["USER_FEMALE"] = "Weiblich";
$MESS["USER_GENDER"] = "Geschlecht:";
$MESS["USER_GROUP_DATE_FROM"] = "von";
$MESS["USER_GROUP_DATE_TO"] = "bis";
$MESS["USER_ICQ"] = "ICQ:";
$MESS["USER_LOGO"] = "Firmenlogo:";
$MESS["USER_MAILBOX"] = "Postfach:";
$MESS["USER_MALE"] = "M�nnlich";
$MESS["USER_MOBILE"] = "Handy:";
$MESS["USER_NOTES"] = "Anmerkungen:";
$MESS["USER_PAGER"] = "Pager:";
$MESS["USER_PERSONAL_INFO"] = "Pers�nliche Informationen";
$MESS["USER_PHONE"] = "Telefon:";
$MESS["USER_PHONES"] = "Telefonnummern";
$MESS["USER_PHOTO"] = "Foto:";
$MESS["USER_POSITION"] = "Position:";
$MESS["USER_POST_ADDRESS"] = "Adresse";
$MESS["USER_PROFESSION"] = "Beruf:";
$MESS["USER_RATING_INFO"] = "Ranking";
$MESS["USER_SHOW_HIDE"] = "anzeigen / verbergen";
$MESS["USER_STATE"] = "Bundesland:";
$MESS["USER_STREET"] = "Stra�e, Nr.:";
$MESS["USER_WORK_INFO"] = "Gesch�ftliche Informationen";
$MESS["USER_WORK_PROFILE"] = "T�tigkeit:";
$MESS["USER_WWW"] = "WWW-Seite:";
$MESS["USER_ZIP"] = "PLZ:";
$MESS["main_profile_decode_err"] = "Fehler der Passwortentschl�sselung (#ERRCODE#).";
$MESS["main_profile_sess_expired"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["main_user_edit_phone_number"] = "Telefonnummer:";
$MESS["user_edit_form_groups"] = "Benutzergruppen";
$MESS["user_edit_form_settings"] = "Einstellungen";
$MESS["user_edit_form_settings_title"] = "Formfelder definieren";
$MESS["user_edit_lang"] = "Sprache f�r Benachrichtigungen:";
$MESS["user_edit_lang_not_set"] = "(nicht definiert)";
$MESS["user_edit_time_zones"] = "Zeitzonen";
$MESS["user_edit_time_zones_auto"] = "Zeitzone automatisch festlegen:";
$MESS["user_edit_time_zones_auto_def"] = "(standardm��ig)";
$MESS["user_edit_time_zones_auto_no"] = "Nein, aus der Liste ausw�hlen";
$MESS["user_edit_time_zones_auto_yes"] = "Ja, Browsereinstellungen benutzen";
$MESS["user_edit_time_zones_zones"] = "Zeitzone:";
?>