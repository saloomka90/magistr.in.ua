<?
$MESS['sms_template_edit_add'] = "Neue SMS-Vorlage";
$MESS['sms_template_edit_add_btn'] = "Hinzuf�gen";
$MESS['sms_template_edit_add_btn_title'] = "Neue SMS-Vorlage hinzuf�gen";
$MESS['sms_template_edit_copy'] = "Kopieren";
$MESS['sms_template_edit_copy_title'] = "Eine Kopie von dieser Vorlage erstellen";
$MESS['sms_template_edit_del'] = "L�schen";
$MESS['sms_template_edit_del_conf'] = "M�chten Sie diese Vorlage wirklich l�schen?";
$MESS['sms_template_edit_del_title'] = "SMS-Vorlage l�schen";
$MESS['sms_template_edit_err_site'] = "Geben Sie zumindest eine Website an.";
$MESS['sms_template_edit_field_senser'] = "Standardm��ige Absendernummer (in Einstellungen angegeben)";
$MESS['sms_template_edit_field_server'] = "Server-URL (in Einstellungen angegeben)";
$MESS['sms_template_edit_field_site'] = "Websitename (in Einstellungen angegeben)";
$MESS['sms_template_edit_fields'] = "Verf�gbare Felder:";
$MESS['sms_template_edit_insert'] = "In Text einf�gen";
$MESS['sms_template_edit_list'] = "Vorlagen";
$MESS['sms_template_edit_list_title'] = "SMS-Vorlagen anzeigen";
$MESS['sms_template_edit_mess'] = "Nachricht";
$MESS['sms_template_edit_not_set'] = "(nicht definiert)";
$MESS['sms_template_edit_sites'] = "Websites:";
$MESS['sms_template_edit_tab'] = "Parameter";
$MESS['sms_template_edit_tab_title'] = "Parameter der SMS-Vorlagen";
$MESS['sms_template_edit_title'] = "SMS-Vorlage bearbeiten";
?>