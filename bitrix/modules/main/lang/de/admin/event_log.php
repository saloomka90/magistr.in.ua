<?
$MESS["MAIN_ALL"] = "(alle)";
$MESS["MAIN_EVENTLOG_AUDIT_TYPE_ID"] = "Event";
$MESS["MAIN_EVENTLOG_DESCRIPTION"] = "Beschreibung";
$MESS["MAIN_EVENTLOG_FORUM_MESSAGE"] = "Nachricht";
$MESS["MAIN_EVENTLOG_FORUM_TOPIC"] = "Thema";
$MESS["MAIN_EVENTLOG_GUEST_ID"] = "Gast";
$MESS["MAIN_EVENTLOG_IBLOCK"] = "Informationsblock";
$MESS["MAIN_EVENTLOG_IBLOCK_DELETE"] = "Gel�scht";
$MESS["MAIN_EVENTLOG_ID"] = "ID";
$MESS["MAIN_EVENTLOG_ITEM_ID"] = "Objekt";
$MESS["MAIN_EVENTLOG_LIST_PAGE"] = "Eintr�ge";
$MESS["MAIN_EVENTLOG_MODULE_ID"] = "Quelle";
$MESS["MAIN_EVENTLOG_PAGE_TITLE"] = "Event-Protokoll";
$MESS["MAIN_EVENTLOG_REMOTE_ADDR"] = "IP";
$MESS["MAIN_EVENTLOG_REQUEST_URI"] = "URL";
$MESS["MAIN_EVENTLOG_SEARCH"] = "Suchen";
$MESS["MAIN_EVENTLOG_SEVERITY"] = "Dringlichkeit";
$MESS["MAIN_EVENTLOG_SITE_ID"] = "Seite";
$MESS["MAIN_EVENTLOG_STOP_LIST"] = "Sperrliste";
$MESS["MAIN_EVENTLOG_TIMESTAMP_X"] = "Zeit";
$MESS["MAIN_EVENTLOG_USER_AGENT"] = "NutzerAgent";
$MESS["MAIN_EVENTLOG_USER_ID"] = "Nutzer";
$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_FROM"] = "Geben Sie das Eintragsdatum \"Von\" korrekt ein";
$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_TO"] = "Geben Sie das Eintragsdatum \"Bis\" korrekt ein";
$MESS["eventlog_notifications"] = "Benachrichtigungen des Ereignisprotokolls";
$MESS["eventlog_notifications_title"] = "Parameter der Benachrichtigungen des Ereignisprotokolls";
?>