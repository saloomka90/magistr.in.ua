<?
$MESS["TOP_PANEL_AI_MODULE_UPDATE"] = "Marketplace-L�sung aktualisiert";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_DESC"] = "Eine Aktualisierung f�r <b>#NAME#</b> ist zur Installation bereit.";
$MESS["TOP_PANEL_AI_MODULE_END_UPDATE"] = "Das Abonnement f�r Aktualisierungen der Marketplace-L�sungen l�uft bald ab";
$MESS["TOP_PANEL_AI_MODULE_END_UPDATE_DESC"] = "Das Abonnement f�r Aktualisierungen f�r <b>#NAME#</b> l�uft bald ab";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_BUTTON_VIEW"] = "Marketplace-L�sung";
$MESS["TOP_PANEL_AI_MODULE_UPDATE_BUTTON_HIDE"] = "Ausblenden";
$MESS["TOP_PANEL_AI_NEW_MODULE_TITLE"] = "Neue L�sungen verf�gbar";
$MESS["TOP_PANEL_AI_NEW_MODULE_DESC"] = "Neue L�sungen wurden gerade eben von <b>#PARTNER#</b>:<br> ver�ffentlicht";
?>