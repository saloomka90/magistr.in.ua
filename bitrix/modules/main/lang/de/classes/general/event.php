<?
$MESS["ACTION_EMPTY"] = "Aktion ist nicht definiert";
$MESS["BAD_EMAIL_FROM"] = "Geben Sie die E-Mail-Adresse des Absenders ein.";
$MESS["BAD_EMAIL_TO"] = "Geben Sie die E-Mail-Adresse des Empfängers ein.";
$MESS["BAD_EVENT_TYPE"] = "Falscher Eventtyp.";
$MESS["EVENT_ID_EMPTY"] = "Event nicht definiert";
$MESS["EVENT_NAME_EMPTY"] = "Eventtyp ist nicht definiert";
$MESS["EVENT_NAME_EXIST"] = "In der Sprache #SITE_ID# existiert bereits ein Postevent \"#EVENT_NAME#\"";
$MESS["EVENT_TYPE_EMPTY"] = "Ereignistyp ist nicht angegeben";
$MESS["LID_EMPTY"] = "Sprache des Eventtyps ist nicht definiert";
$MESS["MAIN_BAD_EVENT_NAME_NA"] = "Geben Sie einen Typ an.";
$MESS["MAIN_BAD_SITE_NA"] = "Weisen Sie eine&nbsp;:Seite zu.";
$MESS["MAIN_EVENT_BAD_SITE"] = "Falsche Seite!";
?>