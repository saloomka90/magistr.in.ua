<?
$MESS['sms_template_active_title'] = "Aktiv";
$MESS['sms_template_event_name_title'] = "Ereignisname";
$MESS['sms_template_id_title'] = "ID der Vorlage";
$MESS['sms_template_language_title'] = "Nachrichtsprache";
$MESS['sms_template_message_title'] = "Nachrichtvorlage";
$MESS['sms_template_receiver_title'] = "Telefonnummer des Empfängers";
$MESS['sms_template_sender_title'] = "Telefonnummer des Absenders";
?>