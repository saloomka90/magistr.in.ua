<?
$MESS["MAIN_EXPORT_EXPECTED_DURATION"] = "Zeit verblieben:";
$MESS["MAIN_EXPORT_EXPECTED_DURATION_HOURS"] = "#HOURS# St. #MINUTES# Min.";
$MESS["MAIN_EXPORT_EXPECTED_DURATION_MINUTES"] = "#MINUTES# Min.";
$MESS["MAIN_EXPORT_COMPLETED"] = "Exportdatei wurde erstellt.";
$MESS["MAIN_EXPORT_ACTION_CANCEL"] = "Export wurde abgebrochen.";
$MESS["MAIN_EXPORT_ACTION_EXPORT"] = "Eintr�ge verarbeitet: #PROCESSED_ITEMS# von #TOTAL_ITEMS#. Dateigr��e: #FILE_SIZE_FORMAT#.";
$MESS["MAIN_EXPORT_ACTION_UPLOAD"] = "Exportdatei wird in die Cloud hochgeladen. Erledigt: #UPLOADED_SIZE_FORMAT#.";
$MESS["MAIN_EXPORT_ERROR_NO_CLOUD_BUCKET"] = "Die Cloud ist mit dem Portal nicht verbunden.";
$MESS["MAIN_EXPORT_DOWNLOAD"] = "Exportdatei herunterladen";
$MESS["MAIN_EXPORT_CLEAR"] = "Exportdatei l�schen";
$MESS["MAIN_EXPORT_FILE_DROPPED"] = "Exportdatei wurde gel�scht";
$MESS["MAIN_EXPORT_VOID"] = "Keine Daten zum Export verf�gbar";
$MESS["MAIN_EXPORT_PURGE"] = "Tempor�re Dateien, die bei vorherigem Exportversuch erstellt wurden, wurden gel�scht.";
?>