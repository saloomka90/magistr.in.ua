<?
$MESS["BXU_FileIsBlockedByOtherProcess"] = "Die Datei ist durch einen anderen Prozess gesperrt.";
$MESS["BXU_RequiredParamCIDIsNotEntered"] = "Der Parameter CID ist erforderlich.";
$MESS["BXU_RequiredParamPackageIndexIsNotEntered"] = "Der Parameter packageIndex ist erforderlich.";
$MESS["BXU_EmptyData"] = "Leere Daten";
$MESS["BXU_SessionIsExpired"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["BXU_AccessDenied"] = "Sie haben nicht gen�gend Rechte, um Ordner hochzuladen. Wenden Sie sich bitte an Ihren Administrator.";
$MESS["BXU_TemporaryDirectoryIsNotCreated"] = "Ein tempor�res Upload-Verzeichnis wurde nicht erstellt. Wenden Sie sich bitte an Ihren Administrator.";
$MESS["BXU_FileIsNotUploaded"] = "Die Datei wurde  nicht hochgeladen.";
$MESS["BXU_FileIsFailed"] = "Die Dateiverifizierung ist fehlgeschlagen.";
$MESS["BXU_FileIsNotFullyUploaded"] = "Die Datei wurde nur teilweise hochgeladen.";
$MESS["BXU_FileIsLost"] = "Datei wurde nicht gefunden.";
$MESS["BXU_TemporaryFileIsNotCreated"] = "Eine tempor�re Upload-Datei wurde nicht erstellt.";
$MESS["BXU_FilePartCanNotBeRead"] = "Ein Dateifragment kann nicht gelesen werden.";
$MESS["BXU_FilePartCanNotBeOpened"] = "Ein Dateifragment kann nicht ge�ffnet werden.";
$MESS["BXU_FilesIsNotGlued"] = "Fehler der Vereinigung.";
$MESS["BXU_UserHandlerError"] = "Fehler im Nutzer-Handler.";
$MESS["BXU_UPLOAD_ERR_INI_SIZE"] = "Die Gr��e der hochgeladenen Datei �berschreitet das Limit.";
$MESS["BXU_UPLOAD_ERR_FORM_SIZE"] = "Die Gr��e der hochgeladenen Datei �berschreitet den Wert MAX_FILE_SIZE der im HTML-Formular angegeben ist.";
$MESS["BXU_UPLOAD_ERR_PARTIAL"] = "Die Datei wurde nur teilweise hochgeladen.";
$MESS["BXU_UPLOAD_ERR_NO_FILE"] = "Die Datei wurde  nicht hochgeladen.";
$MESS["BXU_UPLOAD_ERR_NO_TMP_DIR"] = "Ein tempor�res Verzeichnis fehlt.";
$MESS["BXU_UPLOAD_ERR_CANT_WRITE"] = "Die Datei kann nicht auf den Drive geschrieben werden.";
$MESS["BXU_UPLOAD_ERR_EXTENSION"] = "Eine PHP-Erweiterung hat den Datei-Upload gestoppt.";
?>