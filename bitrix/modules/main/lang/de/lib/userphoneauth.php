<?
$MESS['user_phone_auth_err_duplicte'] = "Nutzer mit der Telefonnummer #VALUE# existiert bereits.";
$MESS['user_phone_auth_err_incorrect_number'] = "Telefonnummer ist nicht korrekt angegeben.";
$MESS['user_phone_auth_err_number'] = "Telefonnummer kann nicht leer sein.";
?>