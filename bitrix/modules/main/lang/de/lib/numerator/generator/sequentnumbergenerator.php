<?
$MESS["BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_WORD_NUMBER"] = "Sequenzielle Nummern";
$MESS["NUMERATOR_SEQUENT_DEFAULT_INTERNAL_ERROR"] = "Interner Fehler.";
$MESS["NUMERATOR_UPDATE_SEQUENT_IS_NOT_SET_YET"] = "Sequenzielle Nummer soll generiert werden.";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_ISDIRECTNUMERATION"] = "Fortlaufende Nummerierung in allen meinen Unternehmen nutzen";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_LENGTH"] = "L�nge der fortlaufenden Nummer";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PAD_STRING"] = "F�llzeichen";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PERIODICBY"] = "Zeitraum, wann automatische Nummerierungsvorlage aktiv ist";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PERIODICBY_DAY"] = "Tag";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PERIODICBY_DEFAULT"] = "St�ndig ";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PERIODICBY_MONTH"] = "Monat";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_PERIODICBY_YEAR"] = "Jahr";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_START"] = "Sequenzielle Nummerierung starten mit";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_STEP"] = "Sequenzielle Nummern erh�hen um";
$MESS["TITLE_BITRIX_MAIN_NUMERATOR_GENERATOR_SEQUENTNUMBERGENERATOR_TIMEZONE"] = "W�hlen Sie Ihre Zeitzone aus";
?>