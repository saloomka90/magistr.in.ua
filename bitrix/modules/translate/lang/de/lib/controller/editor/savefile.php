<?
$MESS["TR_EDIT_ERROR_FILE_NOT_LANG"] = "Die Datei \"#FILE#\" ist keine Sprachdatei";
$MESS["TR_EDIT_FILE_PATH_ERROR"] = "Lokalisierungsdatei nicht angegeben";
$MESS["TR_EDIT_FILE_WRONG_NAME"] = "Der Dateiname ist nicht korrekt.";
$MESS["TR_EDIT_PARAM_ERROR"] = "Nicht genug Daten, um Vorgang abzuschlie�en";
$MESS["TR_EDIT_SAVING_COMPLETED"] = "Gespeichert";
?>