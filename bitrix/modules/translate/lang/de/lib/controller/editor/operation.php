<?
$MESS["TR_CREATE_BACKUP_ERROR"] = "Die Datensicherung f�r die Datei  \"#FILE#\" ist fehlgeschlagen";
$MESS["TR_EDIT_SAVING_COMPLETED"] = "Gespeichert";
$MESS["TR_ERROR_DELETE"] = "Datei \"#FILE#\" kann nicht gel�scht werden";
$MESS["TR_ERROR_WRITE_CREATE"] = "Datei \"#FILE#\" kann nicht erstellt werden";
$MESS["TR_ERROR_WRITE_UPDATE"] = "Die Datei \"#FILE#\" kann nicht �berschrieben werden";
?>