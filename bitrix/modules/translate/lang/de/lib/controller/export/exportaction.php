<?
$MESS["TR_EXPORT_EMPTY_PATH_LIST"] = "Pfade zum Export wurden nicht angegeben";
$MESS["TR_EXPORT_ERROR_32K_LENGTH"] = "Achtung! Einige der exportierten Nachrichten überschreiten 32K Zeichen.";
$MESS["TR_EXPORT_FILE_NOT_LANG"] = "Die Datei \"#FILE#\" ist keine Sprachdatei";
?>