<?
$MESS["TR_IMPORT_ACTION_CANCEL"] = "Import wurde abgebrochen.";
$MESS["TR_IMPORT_ACTION_STATS"] = "Nachrichten importiert: #PROCESSED_PHRASES# von #TOTAL_PHRASES#.";
$MESS["TR_IMPORT_COMPLETED"] = "Importdatei wurde geladen.";
$MESS["TR_IMPORT_EMPTY_FILE_ERROR"] = "Keine Importdatei angegeben";
$MESS["TR_IMPORT_FILE_DROPPED"] = "Importdatei gel�scht";
$MESS["TR_IMPORT_UPLOAD_OK"] = "Die Datei wurde erfolgreich importiert";
$MESS["TR_IMPORT_VOID"] = "Keine Daten zum Importieren";
$MESS["TR_INDEX_ACTION_STATS"] = "Dateien indexiert:  #PROCESSED_FILES# von #TOTAL_FILES#.";
$MESS["TR_INDEX_COMPLETED"] = "Importierte Nachrichten wurden indexiert.";
?>