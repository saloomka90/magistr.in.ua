<?
$MESS["TR_IMPORT_EMPTY_FILE_ERROR"] = "Keine Importdatei angegeben";
$MESS["TR_IMPORT_ERROR_BAD_FILEPATH"] = "Nicht korrekter Pfad \"#FILE#\"";
$MESS["TR_IMPORT_ERROR_CREATE_BACKUP"] = "Die Datensicherung f�r die Datei  \"#FILE#\" ist fehlgeschlagen";
$MESS["TR_IMPORT_ERROR_DESTINATION_FILEPATH_ABSENT"] = "Wert \"file\" ist nicht angegeben";
$MESS["TR_IMPORT_ERROR_FILE_NOT_LANG"] = "Die Datei \"#FILE#\" ist keine Sprachdatei";
$MESS["TR_IMPORT_ERROR_LINE_FILE_BIG"] = "Fehler in der Zeile #LINE#:<br>&nbsp;&nbsp;#FILENAME#&nbsp;&nbsp;#PHRASE#&nbsp;&nbsp;#ERROR#";
$MESS["TR_IMPORT_ERROR_LINE_FILE_EXT"] = "Fehler in der Zeile #LINE#: #ERROR#";
$MESS["TR_IMPORT_ERROR_NO_VALID_UTF8_PHRASE"] = "Ung�ltige UTF-8 Zeichen f�r die Sprach-ID \"#LANG#\"";
$MESS["TR_IMPORT_ERROR_PHRASE_CODE_ABSENT"] = "Wert \"key\" ist nicht angegeben";
$MESS["TR_IMPORT_ERROR_ROW_LANG_ABSENT"] = "Zeile f�r die Sprach-ID \"#LANG#\" fehlt";
$MESS["TR_IMPORT_ERROR_WRITE_CREATE"] = "Datei \"#FILE#\" kann nicht erstellt werden";
$MESS["TR_IMPORT_ERROR_WRITE_UPDATE"] = "Die Datei \"#FILE#\" kann nicht �berschrieben werden";
$MESS["TR_IMPORT_ERR_DESTINATION_FIELD_ABSENT"] = "Feld \"file\" fehlt";
$MESS["TR_IMPORT_ERR_EMPTY_FIRST_ROW"] = "�berschriftzeile ist leer oder enth�lt ung�ltige Daten";
$MESS["TR_IMPORT_ERR_LANGUAGE_LIST_ABSENT"] = "Datei enth�lt keine aktiven Sprachen";
$MESS["TR_IMPORT_ERR_PHRASE_CODE_FIELD_ABSENT"] = "Feld \"key\" fehlt";
?>