<?
$MESS["TR_ERROR_CREATE_TARGET_FOLDER"] = "Der Empfängerverzeichnis \"#PATH#\" kann nicht erstellt werden";
$MESS["TR_ERROR_CREATE_TEMP_FOLDER"] = "Ein temporäres Verzeichnis \"#PATH#\" kann nicht erstellt werden. ";
$MESS["TR_ERROR_DELETE_TARGET_FOLDER"] = "Der Empfängerverzeichnis \"#PATH#\" kann nicht gelöscht werden";
$MESS["TR_ERROR_ENCODING"] = "Die ausgewählte Kodierung ist nicht korrekt";
$MESS["TR_ERROR_LANGUAGE_CHARSET_NON_UTF"] = "Kodierung der ausgewählten Sprache ist nicht UTF-8. Sie sollten die Konvertierung in die Kodierung mit nationalen Symbolen deaktivieren.";
$MESS["TR_ERROR_LANGUAGE_DATE"] = "Lokalisierungsdatum muss eingegeben werden";
$MESS["TR_ERROR_LANGUAGE_ID"] = "Ausgewählte Sprache wurde nicht gefunden";
$MESS["TR_ERROR_OPEN_FILE"] = "Die Datei  \"#FILE#\" kann nicht geöffnet werden";
$MESS["TR_ERROR_SELECT_LANGUAGE"] = "Lokalisierungssprache muss ausgewählt werden";
$MESS["TR_LANGUAGE_COLLECTED_FOLDER"] = "Lokalisierung \"#LANG#\" ist zum Verzeichnis \"#PATH#\" kompiliert";
?>