<?
$MESS["TR_ERROR_CREATE_TARGET_FOLDER"] = "Der Empfängerverzeichnis \"#PATH#\" kann nicht erstellt werden";
$MESS["TR_ERROR_ENCODING"] = "Die ausgewählte Kodierung ist nicht korrekt";
$MESS["TR_ERROR_LANGUAGE_ID"] = "Ausgewählte Sprache wurde nicht gefunden";
$MESS["TR_ERROR_SELECT_LANGUAGE"] = "Lokalisierungssprache muss ausgewählt werden";
$MESS["TR_LANGUAGE_DOWNLOADED"] = "Lokalisierung wurde heruntergeladen";
?>