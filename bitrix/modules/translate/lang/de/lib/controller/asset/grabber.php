<?
$MESS["TR_ERROR_DELETE_TEMP_FOLDER"] = "Tempor�res Verzeichnis \"#PATH#\" kann nicht gel�scht werden";
$MESS["TR_ERROR_TARFILE"] = "Die Datei wurde nicht ausgew�hlt";
$MESS["TR_ERROR_TARFILE_EXTENTION"] = "Ung�ltiger Dateityp";
$MESS["TR_ERROR_UPLOAD_SIZE"] = "Die Gr��e der hochgeladenen Datei �berschreitet das Limit von #SIZE#";
$MESS["TR_EXPORT_ACTION_CANCELED"] = "Lokalisierungsexport wurde abgebrochen.";
$MESS["TR_EXPORT_FILE_DROPPED"] = "Exportdatei wurde gel�scht";
$MESS["TR_IMPORT_ACTION_CANCELED"] = "Lokalisierungsimport wurde abgebrochen.";
$MESS["TR_IMPORT_EMPTY_FILE_ERROR"] = "Es wurde keine Importdatei angegeben";
$MESS["TR_IMPORT_UPLOAD_OK"] = "Datei erfolgreich hochgeladen";
?>