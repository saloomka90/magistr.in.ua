<?
$MESS["TR_ERROR_ARCHIVE"] = "Fehler des Archivs";
$MESS["TR_ERROR_CREATE_TEMP_FOLDER"] = "Tempor�res Verzeichnis \"#PATH#\" kann nicht erstellt werden";
$MESS["TR_ERROR_SOURCE_FOLDER"] = "Verzeichnis \"#PATH#\", das zum Archiv hinzugef�gt werden soll, wurde nicht gefunden oder Zugriff wurde verweigert.";
$MESS["TR_LANGUAGE_COLLECTED_ARCHIVE"] = "Lokalisierung \"#LANG#\" ist zum Archiv \"#FILE_PATH#\" kompiliert, <a href='#LINK#'>Download</a>";
$MESS["TR_PACK_ACTION_EXPORT"] = "Dateien exportiert: #TOTAL_FILES#. Gr��e der Archivdatei: #FILE_SIZE_FORMAT#.";
?>