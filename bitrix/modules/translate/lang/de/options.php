<?
$MESS["MAIN_RESTORE_DEFAULTS"] = "Standard";
$MESS["TRANS_BACKUP_FILES"] = "Datensicherung f�r die Dateien, die bearbeitet werden, durchf�hren";
$MESS["TRANS_BACKUP_FOLDER"] = "Backup-Ordner f�r ge�nderte Dateien";
$MESS["TRANS_BUTTON_LANG_FILES"] = "Die Schaltfl�che \"Sprachdateien anzeigen\" auf der Toolbar anzeigen";
$MESS["TRANS_DONT_SORT_LANGUAGES"] = "Nachrichten nicht sortieren f�r Sprachen";
$MESS["TRANS_EXPORT_CSV_DELIMITER"] = "Trennzeichen der Felder in der CSV-Datei";
$MESS["TRANS_EXPORT_CSV_DELIMITER_COMMA"] = "Komma";
$MESS["TRANS_EXPORT_CSV_DELIMITER_SEMICOLON"] = "Semikolon";
$MESS["TRANS_EXPORT_CSV_DELIMITER_TABULATION"] = "Tab";
$MESS["TRANS_RESTRICTED_FOLDERS"] = "Eingeschr�nkte Lokalisierungsbereiche (Ordnerpfade, mit Komma getrennt)";
$MESS["TRANS_SORT_PHRASES"] = "Beim Speichern die Nachrichten in der Lokalisierungsdatei nach ID sortieren";
?>