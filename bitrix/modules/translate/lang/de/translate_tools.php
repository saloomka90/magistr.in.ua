<?
$MESS['BX_TRANSLATE_IMPORT_ERR_BAD_FILEPATH'] = "Nicht korrekter Pfad #FILE#";
$MESS['BX_TRANSLATE_IMPORT_ERR_DESTINATION_FIELD_ABSENT'] = "Feld \"file\" fehlt";
$MESS['BX_TRANSLATE_IMPORT_ERR_DESTINATION_FILEPATH_ABSENT'] = "Wert \"file\" ist nicht angegeben";
$MESS['BX_TRANSLATE_IMPORT_ERR_EMPTY_FIRST_ROW'] = "�berschriftzeile ist leer oder enth�lt ung�ltige Daten";
$MESS['BX_TRANSLATE_IMPORT_ERR_LANGUAGE_LIST_ABSENT'] = "Datei enth�lt keine aktiven Sprachen";
$MESS['BX_TRANSLATE_IMPORT_ERR_NO_VALID_UTF8_PHRASE'] = "Ung�ltige UTF-8 Zeichen f�r die Sprach-ID \"#LANG#\"";
$MESS['BX_TRANSLATE_IMPORT_ERR_PHRASE_CODE_ABSENT'] = "Wert \"key\" ist nicht angegeben";
$MESS['BX_TRANSLATE_IMPORT_ERR_PHRASE_CODE_FIELD_ABSENT'] = "Feld \"key\" fehlt";
$MESS['BX_TRANSLATE_IMPORT_ERR_ROW_LANG_ABSENT'] = "Zeile f�r die Sprach-ID \"#LANG#\" fehlt";
$MESS['BX_TRANSLATE_IMPORT_MESS_FILENAME'] = "Datei #FILENAME#";
$MESS['BX_TRANSLATE_IMPORT_MESS_PHRASE_CODE'] = "Nachricht #PHRASE#";
$MESS['BX_TRANSLATE_LIST_GROUP_ERR_CANNOT_CREATE_BACKUP_LANG_FILE'] = "Backup f�r #FILEPATH# kann nicht erstellt werden";
$MESS['BX_TRANSLATE_LIST_GROUP_ERR_CANNOT_REWRITE_LANG_FILE'] = "Die Datei #FILEPATH# kann nicht �berschrieben werden";
$MESS['TR_CREATE_BACKUP_ERROR'] = "Backup f�r \"#FILE#\" kann nicht erstellt werden";
$MESS['TR_TOOLS_ERROR_CREATE_BACKUP'] = "Die Datensicherung f�r die Datei  \"#FILE#\" ist fehlgeschlagen";
$MESS['TR_TOOLS_ERROR_EMPTY_FILE'] = "Keine Datei";
$MESS['TR_TOOLS_ERROR_FILE_NOT_LANG'] = "Die Datei #FILE# ist keine Sprachdatei";
$MESS['TR_TOOLS_ERROR_LINE_FILE_BIG'] = "Fehler in der Zeile #LINE#:<br>&nbsp;&nbsp;#FILENAME#&nbsp;&nbsp;#PHRASE#&nbsp;&nbsp;#ERROR#";
$MESS['TR_TOOLS_ERROR_LINE_FILE_EXT'] = "Fehler in der Zeile #LINE#: #ERROR#";
$MESS['TR_TOOLS_ERROR_RIGHTS'] = "Keine Schreibrechte";
$MESS['TR_TOOLS_ERROR_WRITE_FILE'] = "Die Datei #FILE# kann nicht �berschrieben werden";
?>