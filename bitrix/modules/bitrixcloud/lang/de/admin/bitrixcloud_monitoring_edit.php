<?
$MESS["BCL_MONITORING_DOMAIN"] = "Domain";
$MESS["BCL_MONITORING_EMAIL"] = "E-Mail f�r Meldungen";
$MESS["BCL_MONITORING_ADD_BTN"] = "Hinzuf�gen";
$MESS["BCL_MONITORING_TEST_LICENSE"] = "Ablauf der Bitrix-Lizenz �berwachen";
$MESS["BCL_MONITORING_LANGUAGE"] = "Sprache in den Meldungen";
$MESS["BCL_MONITORING_TEST_SSL_CERT_VALIDITY"] = "Ablauf des SSL-Zertifikats �berwachen";
$MESS["BCL_MONITORING_TEST_DOMAIN_REGISTRATION"] = "Domainablauf �berwachen";
$MESS["BCL_MONITORING_TEST_HTTP_RESPONSE_TIME"] = "Website-Uptime �berwachen";
$MESS["BCL_MONITORING_TITLE"] = "Cloud-Inspektor";
$MESS["BCL_MONITORING_TAB"] = "Parameter des Cloud-Inspektors";
$MESS["BCL_MONITORING_TAB_TITLE"] = "Parameter des Cloud-Inspektors konfigurieren";
$MESS["BCL_MONITORING_IS_HTTPS"] = "HTTPS f�r Monitoring benutzen";
?>