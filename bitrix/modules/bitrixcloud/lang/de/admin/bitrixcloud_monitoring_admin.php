<?
$MESS["BCL_MONITORING_DOMAIN"] = "Domain";
$MESS["BCL_MONITORING_LICENSE"] = "Lizenz l�uft ab in:";
$MESS["BCL_MONITORING_NO_DOMAINS_CONFIGURED"] = "Es wurden keine Domains f�r keine einzige Website konfiguriert.";
$MESS["BCL_MONITORING_DELETE"] = "L�schen";
$MESS["BCL_MONITORING_EDIT"] = "Bearbeiten";
$MESS["BCL_MONITORING_DOMAIN_REGISTRATION"] = "Die Domain l�uft ab in:";
$MESS["BCL_MONITORING_SSL"] = "Das SSL-Zertifikat l�uft ab in:";
$MESS["BCL_MONITORING_RESPONSE_TIME"] = "Antwort der Website";
$MESS["BCL_MONITORING_NO_DATA"] = "noch keine Daten verf�gbar";
$MESS["BCL_MONITORING_NO_DATA_AVAILABLE"] = "nicht verf�gbar";
$MESS["BCL_MONITORING_PERIOD"] = "Monitor-Zeitraum:";
$MESS["BCL_MONITORING_FAILED_PERIOD"] = "Downtime:";
$MESS["BCL_MONITORING_DOMAIN_REGISTRATION_NOTE"] = "F�r einige Top-Level-Domains kann die Ablaufzeit nicht erkannt werden, weil nicht alle Domain-Registrar diese Informationen angeben.";
$MESS["BCL_MONITORING_TITLE"] = "Cloud-Inspektor";
$MESS["BCL_MONITORING_START"] = "Website zum Inspektor hinzuf�gen";
$MESS["BCL_MONITORING_DELETE_CONF"] = "M�chten Sie diese Domain wirklich nicht mit dem Cloud Inspektor pr�fen?";
$MESS["BCL_MONITORING_RESULT"] = "Inspektor-Ergebnisse";
?>