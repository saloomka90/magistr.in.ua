<?
$MESS["REST_IBLOCK_NAME"] = "Datenspeicher f�r Marketplace24 Anwendungen";
$MESS["REST_IBLOCK_SECTION_NAME"] = "Bereiche";
$MESS["REST_IBLOCK_ELEMENT_NAME"] = "Elemente";
$MESS["REST_MODULE_NAME"] = "REST API";
$MESS["REST_MODULE_DESCRIPTION"] = "Programmierschnittstelle f�r interne und externe Anwendungen.";
$MESS["REST_INSTALL_TITLE"] = "Installation des Moduls REST API";
$MESS["REST_UNINSTALL_TITLE"] = "Deinstallation des Moduls REST API";
$MESS["REST_DB_NOT_SUPPORTED"] = "Dieses Modul unterst�tzt nur MySQL.";
$MESS["REST_MOD_REWRITE_ERROR"] = "Das Modul mod_rewrite ist erforderlich, um mit Apache korrekt zu funktionieren.";
?>