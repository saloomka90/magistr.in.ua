<?
$MESS["REST_MP_APP_CLEAN"] = "Einstellungen und Daten der Anwendung l�schen";
$MESS["REST_MP_APP_DELETE"] = "L�schen";
$MESS["REST_MP_APP_INSTALL"] = "Installieren";
$MESS["REST_MP_APP_INSTALL_CANCEL"] = "Abbrechen";
$MESS["REST_MP_APP_POPUP_LOAD"] = "Wird geladen";
$MESS["REST_MP_APP_REINSTALLED"] = "Anwendung neu installieren";
$MESS["REST_MP_DELETE_CONFIRM"] = "M�chten Sie die Anwendung wirklich l�schen?";
$MESS["REST_MP_DELETE_CONFIRM_CLEAN"] = "Einstellungen und Daten der Anwendung l�schen";
$MESS["REST_MP_SUBSCRIPTION_BUTTON_TITLE"] = "Abonnement kaufen";
$MESS["REST_MP_SUBSCRIPTION_BUTTON_TITLE2"] = "30 Tage kostenlos";
$MESS["REST_MP_SUBSCRIPTION_TEXT1"] = "St�rken Sie Ihr Bitrix24.";
$MESS["REST_MP_SUBSCRIPTION_TEXT2"] = "Installieren und nutzen Sie Anwendungen zur Verwaltung Ihres Vertriebs und Ihrer Aufgaben; Scripts und analytischer Berichte; Lead-Generierung und Tools der Integration mit externen Services etc. �ber 100 Automatisierungsregeln und Aktionen f�r Ihre Workflows zur Automatisierung Ihrer Prozesse. �ber 300 Vorlagen f�r Websites und Onlineshops, mit denen Sie den Verkauf Ihrer Produkte sofort starten k�nnen.";
$MESS["REST_MP_SUBSCRIPTION_TEXT3"] = "Abonnieren Sie jetzt diese Apps und Widgets ohne jegliche Einschr�nkungen und ohne etwas f�r individuelle Installation zahlen zu m�ssen.";
$MESS["REST_MP_SUBSCRIPTION_TITLE"] = "Bitrix24 Abonnement";
?>