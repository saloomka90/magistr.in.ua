<?
$MESS["MARKETPLACE_TITLE"] = "Anwendung hinzuf�gen";
$MESS["MARKETPLACE_BUTTON"] = "Fortfahren";
$MESS["MARKETPLACE_PAGE_TITLE"] = "So k�nnen Sie Ihre Anwendung zu Bitrix24 hinzuf�gen";
$MESS["MARKETPLACE_PAGE_TITLE2"] = " ";
$MESS["MARKETPLACE_BLOCK1_TITLE"] = "Zur privaten Nutzung";
$MESS["MARKETPLACE_BLOCK1_INFO"] = "<p>- erstellen Sie eine Anwendung</p>
<p>- f�gen Sie sie zu Ihrem Portal hinzu</p>
";
$MESS["MARKETPLACE_BLOCK2_TITLE"] = "Zum Ver�ffentlichen f�r andere";
$MESS["MARKETPLACE_BLOCK2_INFO"] = "<p>- werden Sie ein Partner</p>
<p>- erstellen Sie eine Anwendung</p>
<p>- laden Sie sie in Ihrem Partnerprofil hoch</p>
<p>- ver�ffentlichen Sie Ihre Anwendung</p>
";
$MESS["MARKETPLACE_BLOCK2_LINK"] = "https://www.bitrix24.de/apps/dev.php";
$MESS["MARKETPLACE_OR"] = "oder";
$MESS["MARKETPLACE_BUTTON_ADD"] = "Hinzuf�gen";
?>