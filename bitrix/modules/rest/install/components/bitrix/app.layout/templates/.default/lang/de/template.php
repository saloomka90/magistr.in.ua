<?
$MESS["REST_ALT_UPDATE_AVAIL_ADMIN"] = "Eine neue Version ist verf�gbar. <a href=\"#DETAIL_URL#\">Jetzt installieren</a>";
$MESS["REST_ALT_UPDATE_AVAIL"] = "Eine neue Version der Anwendung ist verf�gbar. Wenden Sie sich bitte an Ihren Intranet-Administrator, damit er sie installiert.";
$MESS["REST_LOADING"] = "#APP_NAME# wird geladen";
$MESS["PAYMENT_MESSAGE_D_N_Y"] = "Einige Funktionen der Anwendung sind nicht verf�gbar. Wenden Sie sich bitte an Ihren Administrator, um die volle Version zu installieren.";
$MESS["PAYMENT_MESSAGE_D_N_Y_A"] = "Einige Funktionen der Anwendung sind nicht verf�gbar. <a href=\"#DETAIL_URL#\">Volle Version installieren</a>";
$MESS["PAYMENT_MESSAGE_D_Y_Y"] = "Ihr Abonnement f�r die Anwendung ist abgelaufen. Einige Funktionen der Anwendung sind nicht verf�gbar. Wenden Sie sich bitte an Ihren Administrator, um die volle Version zu installieren.";
$MESS["PAYMENT_MESSAGE_D_Y_Y_A"] = "Ihr Abonnement f�r die Anwendung ist abgelaufen. Einige Funktionen der Anwendung sind nicht verf�gbar. <a href=\"#DETAIL_URL#\">Volle Version installieren</a>";
$MESS["PAYMENT_MESSAGE_T_N_Y"] = "Die Testzeit l�uft in <b>#DAYS#</b> Tagen ab. Wenden Sie sich bitte am Ihren Administrator, um die volle Version zu installieren.";
$MESS["PAYMENT_MESSAGE_T_N_Y_A"] = "Die Testzeit l�uft in <b>#DAYS#</b> Tagen ab. <a href=\"#DETAIL_URL#\">Volle Version installieren</a>";
$MESS["PAYMENT_MESSAGE_T_Y_N"] = "Die Testzeit ist abgelaufen. Wenden Sie sich bitte an Ihren Administrator, um die volle Version zu installieren.";
$MESS["PAYMENT_MESSAGE_T_Y_N_A"] = "Die Testzeit ist abgelaufen. <a href=\"#DETAIL_URL#\">Volle Version installieren</a>";
$MESS["PAYMENT_MESSAGE_P_N_Y"] = "Ihr Abonnement f�r die Anwendung l�uft in <b>#DAYS#</b> Tagen ab. Wenden Sie sich bitte an Ihren Administrator, um das Abonnement zu verl�ngern.";
$MESS["PAYMENT_MESSAGE_P_N_Y_A"] = "Ihr Abonnement f�r die Anwendung l�uft in <b>#DAYS#</b> Tagen ab. <a href=\"#DETAIL_URL#\">Abonnement verl�ngern</a>";
$MESS["PAYMENT_MESSAGE_P_Y_Y"] = "Ihr Abonnement f�r die Anwendung ist abgelaufen. Wenden Sie sich bitte an Ihren Administrator, um die volle Version zu installieren.";
$MESS["PAYMENT_MESSAGE_P_Y_Y_A"] = "Ihr Abonnement f�r die Anwendung ist abgelaufen. <a href=\"#DETAIL_URL#\">Abonnement verl�ngern</a>";
$MESS["PAYMENT_MESSAGE_P_Y_N"] = "Die Anwendung ist nicht mehr verf�gbar, weil Ihr Abonnement abgelaufen ist. Wenden Sie sich bitte an Ihren Administrator, um das Abonnement zu verl�ngern.";
$MESS["PAYMENT_MESSAGE_P_Y_N_A"] = "Die Anwendung ist nicht mehr verf�gbar, weil Ihr Abonnement abgelaufen ist. <a href=\"#DETAIL_URL#\">Abonnement verl�ngern</a>";
$MESS["REST_ALT_NEED_REINSTALL_ADMIN"] = "<b>Wichtig!</b> Das Sicherheitssystem wurde aktualisiert. Es wird empfohlen, die Anwendung neu zu installieren und dabei die aktuellen Daten beizubehalten. <a href=\"#DETAIL_URL#\">Jetzt neu installieren</a>";
$MESS["REST_ALT_NEED_REINSTALL"] = "<b>Wichtig!</b> Das Sicherheitssystem wurde aktualisiert. Sie sollten sich an Ihren Intranet-Administrator wenden, damit er die Anwendung neu installiert, wobei die aktuellen Daten beibehalten werden sollten.";
?>