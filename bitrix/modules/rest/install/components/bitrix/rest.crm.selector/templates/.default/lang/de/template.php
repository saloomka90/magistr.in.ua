<?
$MESS["REST_CRM_FF_LEAD"] = "Leads";
$MESS["REST_CRM_FF_CONTACT"] = "Kontakte";
$MESS["REST_CRM_FF_COMPANY"] = "Unternehmen";
$MESS["REST_CRM_FF_DEAL"] = "Auftr�ge";
$MESS["REST_CRM_FF_OK"] = "Ausw�hlen";
$MESS["REST_CRM_FF_CANCEL"] = "Abbrechen";
$MESS["REST_CRM_FF_CLOSE"] = "Schlie�en";
$MESS["REST_CRM_FF_SEARCH"] = "Suchen";
$MESS["REST_CRM_FF_NO_RESULT"] = "Leider wurden keine Ergebnisse auf Ihre Suchanfrage gefunden.";
$MESS["REST_CRM_FF_CHOISE"] = "Ausw�hlen";
$MESS["REST_CRM_FF_CHANGE"] = "�ndern";
$MESS["REST_CRM_FF_LAST"] = "Letzte";
$MESS["REST_CRM_FF_QUOTE"] = "Angebote";
