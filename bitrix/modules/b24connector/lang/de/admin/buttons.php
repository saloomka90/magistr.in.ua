<?
$MESS["B24C_BUTT_TITLE"] = "Widgets";
$MESS["B24C_BL_OL_U"] = "Dieses Widget vereinigt alle digitalen Kan�le der Kommunikation mit Kunden in Ihrem Bitrix24.";
$MESS["B24C_BL_ALL_C"] = "Alle Kommunikationsmittel in einem Widget.";
$MESS["B24C_BL_AC_C"] = "Live-Chat, R�ckrufe, Online-Formulare alles, was Sie brauchen, um mit Ihren Kunden in Verbindung zu bleiben.";
$MESS["B24C_BL_AC_C2"] = "Das Widget wird die Konversion der Website sowie die Verkaufszahlen erh�hen.";
$MESS["B24C_BL_AC_C3"] = "Mit einem Klick installieren Sie das Widget auf Ihrer Website.";
$MESS["B24C_BL_AC_C4"] = "Auf der Website erscheint jetzt eine Schaltfl�che.";
$MESS["B24C_BL_AC_C5"] = "Besucher klicken die Schaltfl�che an und w�hlen einen bevorzugten Kommunikationskanal aus: Live-Chat, Anruf oder Web-Formular.";
$MESS["B24C_BUTT_EMPTY"] = "Bitrix24 Widgets anzeigen";
?>