<?
$MESS ['EDIT_ACCESS_NOT_SET'] = "-- nicht installiert --";
$MESS ['EDIT_ACCESS_SELECT_GROUP'] = "-- Gruppe w�hlen --";
$MESS ['EDIT_ACCESS_PERMISSION_INFO'] = "Die <b>Zugangsberechtigung</b> ist eine Regelsammlung, die die Zugangslevels f�r Usergruppen bestimmt (z.B. \"Lesen\" ist die Ansicht einer Seite oder eines Verzeichnisses). Zugriffsrechte k�nnen geerbt oder vererbt werden: Wenn ein Verzeichnis oder eine Seite keine explizit angegebene Zugriffsberechtigung haben, treten die Regeln des Eltern-Verzeichnisses in Kraft.";
$MESS ['EDIT_ACCESS_CURRENT_PERMISSION'] = "Aktuelle Berechtigungen";
$MESS ['EDIT_ACCESS_ALL_GROUPS'] = "Standard Zugriffberechtigungen f�r alle Gruppen";
$MESS ['ACCESS_EDIT_FILE_NOT_FOUND'] = "Angegebene Datei existiert nicht.";
$MESS ['EDIT_ACCESS_TO_FILE'] = "Dateiberechtigungen";
$MESS ['EDIT_ACCESS_USER_GROUP'] = "Gruppe";
$MESS ['EDIT_ACCESS_SET_INHERIT'] = "Vererben";
$MESS ['EDIT_ACCESS_INHERIT_PERMISSION'] = "Vererbbare Berechtigung";
$MESS ['EDIT_ACCESS_SET_INHERITED'] = "Vererbt";
$MESS ['EDIT_ACCESS_TO_DENIED'] = "Sie haben nicht genug Rechte, um die Zugangsparameter zu �ndern von";
$MESS ['EDIT_ACCESS_ADD_PERMISSION'] = "Neue Berechtigung";
$MESS ['EDIT_ACCESS_PERMISSION'] = "Zugriffsregel";
$MESS ['EDIT_ACCESS_SET_PERMISSION'] = "Genehmigung wird vom Unterverzeichnis geerbt. Klicken Sie auf dieses Eingabefeld, um die Berechtigungslevel zu �ndern.";
$MESS ['FOLDER_EDIT_ACCESS_DENIED'] = "Der Zugriff zum Bearbeiten wurde verweigert.";
$MESS ['EDIT_ACCESS_TO_FOLDER'] = "Verzeichnis-Zugriffsberechtigung";
$MESS ['EDIT_ACCESS_OWN_CHANGE_RESTRICT'] = "Sie k�nnen die Berechtigung von allen Usergruppen (#GROUPS#) nicht �ndern, dadurch k�nnte Sie Ihre eigene Zugriffsberechtigung beschr�nken. Der Zugriffslevel von mindestens einer Gruppe sollte unge�ndert bleiben.";
?>