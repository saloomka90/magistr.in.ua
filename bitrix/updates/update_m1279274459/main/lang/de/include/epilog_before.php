<?
$MESS ['main_epilog_before_menu_title'] = "Aktuelle Seite bearbeiten";
$MESS ['main_epilog_before_menu_edit_html'] = "Seite als HTML bearbeiten";
$MESS ['main_epilog_before_menu_edit_html_title'] = "Seite im HTML-Modus bearbeiten";
$MESS ['main_epilog_before_menu_edit'] = "Seite im Editor bearbeiten";
$MESS ['main_epilog_before_menu_edit_title'] = "Seite im visuellen Editor bearbeiten";
$MESS ['main_epilog_before_menu_prop'] = "Seiteneigenschaften bearbeiten";
$MESS ['main_epilog_before_menu_prop_title'] = "Überschrift und andere Seiteneigenschaften bearbeiten";
?>