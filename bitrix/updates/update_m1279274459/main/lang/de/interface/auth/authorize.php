<?
$MESS ['admin_authorize_error'] = "Es ist ein Fehler beim Autorisieren aufgetreten";
$MESS ['AUTH_AUTHORIZE'] = "Login";
$MESS ['AUTH_CHANGE_FORM'] = "Formularpasswort �ndern";
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "Passwort vergessen?";
$MESS ['AUTH_GO'] = "Wechseln zum";
$MESS ['admin_authorize_info'] = "Information";
$MESS ['AUTH_LOGIN'] = "Loginname";
$MESS ['AUTH_PASSWORD'] = "Passwort";
$MESS ['AUTH_PLEASE_AUTH'] = "Bitte melden Sie sich an";
$MESS ['AUTH_REMEMBER_ME'] = "Auf diesem Computer merken";
$MESS ['AUTH_GO_AUTH_FORM'] = "Formular f�r die Passwortanforderung";
$MESS ['AUTH_CAPTCHA_PROMT'] = "Geben Sie das Wort vom Bild ein";
$MESS ['AUTH_MESS_1'] = "Wenn Sie Ihren Pr�fcode erhalten haben, gehen Sie bitte zum Formular";
?>