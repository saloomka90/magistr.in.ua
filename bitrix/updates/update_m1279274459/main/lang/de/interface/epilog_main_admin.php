<?
$MESS ['EPILOG_ADMIN_COPY_1c_bitrix_eduportal'] = "&copy; 2010 Bitrix";
$MESS ['EPILOG_ADMIN_COPY_1c_bitrix_gosportal'] = "&copy; 2010 Bitrix";
$MESS ['EPILOG_ADMIN_COPY_1c_bitrix_gossite'] = "&copy; 2010 Bitrix";
$MESS ['EPILOG_ADMIN_SM_1c_bitrix_eduportal'] = "Bitrix Education Portal";
$MESS ['EPILOG_ADMIN_SM_1c_bitrix_gosportal'] = "Bitrix Government Portal";
$MESS ['EPILOG_ADMIN_SM_1c_bitrix_gossite'] = "Bitrix Government Site";
$MESS ['EPILOG_ADMIN_SM_1c_bitrix_portal'] = "Bitrix Intranet Portal";
$MESS ['EPILOG_ADMIN_SM_1c_bitrix'] = "Bitrix Site Manager";
$MESS ['EPILOG_ADMIN_SM_bitrix_portal'] = "Bitrix Intranet Portal";
$MESS ['EPILOG_ADMIN_SM_bitrix'] = "Bitrix Site Manager";
$MESS ['EPILOG_ADMIN_COPY_1c_bitrix'] = "Copyright &copy; 2002-2009 Bitrix, Inc.";
$MESS ['EPILOG_ADMIN_COPY_1c_bitrix_portal'] = "Copyright &copy; 2002-2009 Bitrix, Inc.";
$MESS ['EPILOG_ADMIN_COPY_bitrix'] = "Copyright &copy; 2002-2009 Bitrix, Inc.";
$MESS ['EPILOG_ADMIN_COPY_bitrix_portal'] = "Copyright &copy; 2002-2010 Bitrix, Inc.";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_1c_bitrix_eduportal'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_MAIN_1c_bitrix_eduportal'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_1c_bitrix_gosportal'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_MAIN_1c_bitrix_gosportal'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_1c_bitrix_gossite'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_MAIN_1c_bitrix_gossite'] = "http://www.1c-bitrix.ru/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_1c_bitrix_eduportal'] = "http://www.1c-bitrix.ru/support/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_1c_bitrix_gosportal'] = "http://www.1c-bitrix.ru/support/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_1c_bitrix_gossite'] = "http://www.1c-bitrix.ru/support/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_1c_bitrix'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_MAIN_1c_bitrix'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_bitrix'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_MAIN_bitrix'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_1c_bitrix_portal'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_MAIN_1c_bitrix_portal'] = "http://www.bitrixsoft.com/";
$MESS ['EPILOG_ADMIN_URL_PRODUCT_bitrix_portal'] = "http://www.bitrix.de/";
$MESS ['EPILOG_ADMIN_URL_MAIN_bitrix_portal'] = "http://www.bitrix.de/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_1c_bitrix'] = "http://www.bitrixsoft.com/support/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_bitrix'] = "http://www.bitrixsoft.com/support/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_1c_bitrix_portal'] = "http://www.bitrixsoft.com/support/";
$MESS ['EPILOG_ADMIN_URL_SUPPORT_bitrix_portal'] = "http://www.bitrix.de/support/";
$MESS ['EPILOG_ADMIN_POWER'] = "Erstellt mit";
$MESS ['epilog_support_link'] = "Online-Support";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_1c_bitrix_eduportal'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_1c_bitrix_gosportal'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_1c_bitrix_gossite'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_1c_bitrix'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_bitrix'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_1c_bitrix_portal'] = "www.bitrixsoft.com";
$MESS ['EPILOG_ADMIN_URL_MAIN_TEXT_bitrix_portal'] = "www.bitrix.de";
?>