<?
$MESS ['fav_general_err_url'] = "Feld &quot;Link&quot; kann nicht leer sein.";
$MESS ['fav_general_err_name'] = "Feld &quot;Name&quot; kann nicht leer sein.";
$MESS ['fav_general_err_lang1'] = "Geben Sie eine Sprache f�r den administrativen Bereich an.";
$MESS ['fav_general_err_user1'] = "Geben Sie den User an, dem das Lesezeichen geh�rt.";
$MESS ['fav_general_err_lang'] = "Angegebene Sprache existiert nicht.";
$MESS ['fav_general_err_user'] = "Angegebener User existiert nicht.";
?>