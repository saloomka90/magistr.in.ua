<?
$MESS ['FILE_TEXT'] = "Datei";
$MESS ['FILE_WIDTH'] = "Breite";
$MESS ['FILE_HEIGHT'] = "Höhe";
$MESS ['FILE_SIZE'] = "Größe";
$MESS ['FILE_DELETE'] = "Datei löschen";
$MESS ['FILE_NOT_FOUND'] = "Datei wurde nicht gefunden";
$MESS ['FILE_BAD_FILE_TYPE'] = "Die Datei ist in keinem gültigen Bildformat";
$MESS ['FILE_BAD_MAX_RESOLUTION'] = "Maximale Bilddateigröße wurde überschritten";
$MESS ['FILE_BAD_SIZE'] = "Maximale Uploaddateigröße ist überschritten";
$MESS ['FILE_BAD_TYPE'] = "Falscher Dateityp oder die maximale Größe wurde überschritten";
$MESS ['FILE_ENLARGE'] = "Vergrößern";
$MESS ['MAIN_BAD_FILENAME'] = "Der Dateiname sollte nur lateinische Buchstaben beinhalten.";
$MESS ['MAIN_FIELD_FILE_DESC'] = "Beschreibung";
$MESS ['FILE_FILE_DOWNLOAD'] = "Datei herunterladen";
$MESS ['FILE_DOWNLOAD'] = "Download";
$MESS ['main_include_dots'] = "Pixel";
$MESS ['main_include_bytes'] = "Byte";
$MESS ['main_js_img_title'] = "Bild";
?>