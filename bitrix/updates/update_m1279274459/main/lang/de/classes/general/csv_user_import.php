<?
$MESS ['CSV_IMPORT_LAST_NAME_NOT_FOUND'] = "In der CSV Datei fehlt das Feld LAST_NAME (User Nachname)";
$MESS ['CSV_IMPORT_NAME_NOT_FOUND'] = "In der CSV Datei fehlt das Feld NAME (User Vorname).";
$MESS ['CSV_IMPORT_MUST_BE_UTF'] = "Die CSV-Datei sollte in UTF-8 Codiert werden.";
$MESS ['CSV_IMPORT_DELIMETER_NOT_FOUND'] = "Die CSV Datei�berschrift wurde nicht gefunden, oder die Trennzeichen wurden nicht korrekt gew�hlt.";
$MESS ['CSV_IMPORT_HEADER_NOT_FOUND'] = "Die Datei oder die CSV Datei-�berschrift wurde nicht gefunden.";
?>