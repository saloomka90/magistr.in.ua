<?
$MESS ['USMP_VIEW'] = "Durchschauen";
$MESS ['USMP_MODULES'] = "Module ";
$MESS ['USMP_H_ID'] = "Modulcode";
$MESS ['USMP_H_NAME'] = "Modulname";
$MESS ['USMP_H_DESCR'] = "Modulbeschreibung  ";
$MESS ['USMP_H_PARTNER'] = "Partner";
$MESS ['USMP_H_DATE_UPDATE'] = "�nderungsdatum";
$MESS ['USMP_H_DATE_CREATE'] = "Erstelldatum";
$MESS ['USMP_H_IMAGE'] = "Bild";
$MESS ['USMP_H_CAT'] = "Kategorie ";
$MESS ['USMP_H_TYPE'] = "Typ";
$MESS ['USMP_H_LOADED'] = "Heruntergeladen ";
$MESS ['USMP_YES'] = "Ja";
$MESS ['USMP_NO'] = "Nein";
$MESS ['USMP_DO_LOAD'] = "Herunterladen";
$MESS ['USMP_F_NAME'] = "Name";
$MESS ['USMP_CAT_0'] = "[beliebig]";
$MESS ['USMP_TYPE_0'] = "[beliebig]";
$MESS ['SUP_SUAC_LOAD_M_BUTTON_CONF'] = "Achtung!  Bitrix Inc. haftet nicht f�r  Software, die von Partnern entwickelt wurden.  Mit allen Fragen bez�glich Arbeit fremder Module und auch mit Fragen wegen St�rungen  der Sitearbeit, die durch eine nicht korrekte Arbeit fremder Module verursacht wurden, wenden Sie sich bitte an Partner. Soll das Modul      heruntergeladen werden?";
?>