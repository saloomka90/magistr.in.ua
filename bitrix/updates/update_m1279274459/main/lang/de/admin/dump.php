<?
$MESS ['MAIN_DUMP_ACTIONS'] = "Aktion";
$MESS ['MAIN_DUMP_ARC_SIZE'] = "Archivgr��e:";
$MESS ['MAIN_DUMP_ALERT_DELETE'] = "Wollen Sie die Datei wirklich l�schen?";
$MESS ['MAIN_RIGHT_CONFIRM_EXECUTE'] = "Achtung! Das Entpacken der Sicherungskopie auf einer funktionierenden Seite kann das Archiv besch�digen! Wollen Sie fortfahren?";
$MESS ['MAIN_DUMP_ENCODE'] = "Achtung! Sie benutzten eine codierte Produktversion";
$MESS ['MAIN_DUMP_FILE_DUMP_BUTTON'] = "Archivieren";
$MESS ['MAIN_DUMP_BASE_TRUE'] = "Datenbank archivieren:";
$MESS ['MAIN_DUMP_FILE_STEPPED'] = "Schrittweise archivieren:";
$MESS ['MAIN_DUMP_FILE_KERNEL'] = "Kernel archivieren:";
$MESS ['MAIN_DUMP_FILE_PUBLIC'] = "�ffentlichen Teil archivieren:";
$MESS ['MAIN_DUMP_TAB'] = "Backup";
$MESS ['MAIN_DUMP_PAGE_TITLE'] = "Backup";
$MESS ['MAIN_DUMP_FILE_FINISH'] = "Backup ist vollst�ndig";
$MESS ['MAIN_DUMP_FILE_PAGES'] = "Backup-Kopie";
$MESS ['MAIN_DUMP_TAB_TITLE'] = "Backup-Parameter";
$MESS ['MAIN_DUMP_SITE_PROC'] = "Komprimieren...";
$MESS ['MAIN_DUMP_BASE_TITLE'] = "Datenbank";
$MESS ['MAIN_DUMP_DELETE'] = "L�schen";
$MESS ['MAIN_DUMP_FILE_MAX_SIZE'] = "Dateien ausschlie�en, die gr��er sind als (0 - ohne Begrenzung):";
$MESS ['MAIN_DUMP_ACTION_DOWNLOAD'] = "Download";
$MESS ['MAIN_DUMP_GET_SCRIPT'] = "Download";
$MESS ['MAIN_DUMP_MASK'] = "Dateien und Verzeichnisse aus dem Archiv nach der Maske ausschlie�en:";
$MESS ['MAIN_DUMP_BASE_IGNORE'] = "Aus dem Archiv ausschlie�en:";
$MESS ['MAIN_DUMP_FILE_SIZE_FIELD'] = "Dateigr��e, MB";
$MESS ['MAIN_DUMP_FILE_NAME'] = "Name";
$MESS ['MAIN_DUMP_FILE_TITLE'] = "Dateien";
$MESS ['MAIN_DUMP_FILE_CNT'] = "Dateien komprimiert:";
$MESS ['MAIN_DUMP_FILE_SIZE'] = "Dateigr��e:";
$MESS ['MAIN_DUMP_FILE_MAX_SIZE_kb'] = "KB";
$MESS ['MAIN_DUMP_LOADING'] = "Laden...";
$MESS ['MAIN_DUMP_BASE_SIZE'] = "MB";
$MESS ['MAIN_DUMP_FILE_TIMESTAMP'] = "Ge�ndert";
$MESS ['MAIN_DUMP_MORE'] = "Mehr...";
$MESS ['MAIN_DUMP_DB_NOTE'] = "Beachten Sie, dass beim Aktualisieren des Produkts, die Kerneldaten und die Struktur der Datenbank ge�ndert werden: Wenn die Kernelversion nicht der Datenbankversion entspricht, kann die Seite funktionsunf�hig werden.";
$MESS ['MAIN_DUMP_BASE_SINDEX'] = "Suchindex";
$MESS ['MAIN_DUMP_FILE_STEP_sec'] = "Sekunden";
$MESS ['MAIN_DUMP_SKIP_SYMLINKS'] = "Symbolische Links zu Verzeichnissen �berspringen:";
$MESS ['MAIN_DUMP_BASE_STAT'] = "Statistik";
$MESS ['MAIN_DUMP_FILE_STEP'] = "Schritt:";
$MESS ['MAIN_DUMP_FILE_STOP_BUTTON'] = "Stop";
$MESS ['MAIN_DUMP_TABLE_FINISH'] = "Tabellen bearbeitet:";
$MESS ['MAIN_DUMP_MYSQL_ONLY'] = "Das Backupsystem arbeitet nur mit MySQL Datenbankdaten.<br>Bitte verwenden Sie externe Tools um eine Datenbankkopie zu erstellen.";
$MESS ['MAIN_DUMP_FOOTER_MASK'] = "F�r die Ausschlussmaske gelten folgenden Regeln:
<p>
 <li>Die Maskenvorlage kann Symbole &quot;*&quot; beinhalten, die einem beliebigen Anzahl verschiedener Symbole, die im Datei- oder Ordnernamen enthalten sind, entsprechen;</li>
 <li>Wenn am Anfang ein Slash oder ein Backslash steht (&quot;/&quot; oder &quot;\\&quot;), gilt der Pfad vom Root-Verzeichnis aus;</li>
 <li>Im Gegenfall wird die Vorlage auf jede Datei und jeden Ordner angewendet;</li>
 <p>Vorlagenbeispiele:</p>
 <li>/content/photo - Ausschlu� des ganzen Ordners /content/photo;</li>
 <li>*.zip - Ausschlu� aller Dateien mit der Dateiendung &quot;zip&quot;;</li>
 <li>.access.php - Ausschlu� aller Dateien &quot;.access.php&quot;;</li>
 <li>/files/download/*.zip - Ausschlu� aller Dateien mit der Dateiendung &quot;zip&quot; im /files/download Ordner;</li>
 <li>/files/d*/*.ht* - Ausschlu� der Dateien aus den Ordner, die mit &quot;/files/d&quot anfangen und Dateiendungen, die mit &quot;ht&quot; anfangen.</li>";
$MESS ['MAIN_DUMP_HEADER_MSG'] = "Um das Seitenarchiv zum anderen Host zu �bertragen, f�gen Sie bitte im Root-Verzeichnis der neuen Seite das Wiederherstellungsskript <a href='/bitrix/admin/restore_export.php'>restore.php</a> und das Archiv selbst ein. Dann geben Sie in der Browser-Zeile &quot;&lt;Seitenname&gt;/restore.php&quot; und folgen Sie den Instruktionen.";
$MESS ['MAIN_DUMP_FOOTER_MSG'] = "Um das Archiv zum anderen Ort zu �bertragen oder zur Wiederherstellung des Systems vom Archiv, verwenden Sie ein spezielles Wiederherstellungsskript:";
$MESS ['MAIN_DUMP_RESTORE'] = "Entpacken";
$MESS ['MAIN_DUMP_ENCODE_DETAILS'] = "Bei der �bertragung zum anderen Host, muss dort die PHP-Version <b>#VER#.x.x</b> und <b>Zend Optimizer 3.3</b> oder h�her installiert sein.<br>Wenn Sie einen funktionierenden kommerziellen Schl�ssel besitzen, laden Sie die Quellcodes �ber das <a href=\"/bitrix/admin/sysupdate.php\">Update-System</a> runter.";
$MESS ['MAIN_DUMP_FILE_STEP_SLEEP'] = "Interval:";
?>