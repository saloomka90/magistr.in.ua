<?
$MESS ['MAIN_ADMIN_GROUP_NAME'] = "Administratoren";
$MESS ['MAIN_ADMIN_GROUP_DESC'] = "Voller Zugriff zur Seitenverwaltung.";
$MESS ['MAIN_EVERYONE_GROUP_NAME'] = "Alle User, einschließlich nicht autorisierte";
$MESS ['MAIN_EVERYONE_GROUP_DESC'] = "Alle User, einschließlich nicht autorisierte";
$MESS ['MAIN_DEFAULT_SITE_NAME'] = "Standardseite";
$MESS ['MAIN_DEFAULT_LANGUAGE_NAME'] = "German";
$MESS ['MAIN_DEFAULT_LANGUAGE_FORMAT_DATE'] = "DD.MM.YYYY";
$MESS ['MAIN_DEFAULT_LANGUAGE_FORMAT_DATETIME'] = "DD.MM.YYYY HH:MI:SS";
$MESS ['MAIN_DEFAULT_LANGUAGE_FORMAT_CHARSET'] = "iso-8859-1";
$MESS ['MAIN_DEFAULT_SITE_FORMAT_DATE'] = "DD.MM.YYYY";
$MESS ['MAIN_DEFAULT_SITE_FORMAT_DATETIME'] = "DD.MM.YYYY HH:MI:SS";
$MESS ['MAIN_DEFAULT_SITE_FORMAT_CHARSET'] = "iso-8859-1";
$MESS ['MAIN_MODULE_NAME'] = "Hauptmodul";
$MESS ['MAIN_MODULE_DESC'] = "Produktkernel";
$MESS ['MAIN_INSTALL_DB_ERROR'] = "Verbindung zur Datenbank nicht möglich. Überprüfen Sie die angegebenen Parameter.";
$MESS ['MAIN_NEW_USER_TYPE_NAME'] = "Ein Neuer User hat sich registriert";
$MESS ['MAIN_NEW_USER_TYPE_DESC'] = "#USER_ID# - User ID
#LOGIN# - Loginname
#EMAIL# -E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - User IP
#USER_HOST# - User Host";
$MESS ['MAIN_USER_INFO_TYPE_NAME'] = "Userinformation";
$MESS ['MAIN_USER_INFO_TYPE_DESC'] = "#USER_ID# - User ID
#STATUS# - Accountstatus
#MESSAGE# - Nachricht an den User
#LOGIN# - Loginname
#CHECKWORD# - Kontrollwort für die Passwortänderung
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - User IP
#USER_HOST# - User Host";
$MESS ['MAIN_NEW_USER_CONFIRM_TYPE_NAME'] = "Registrierungsbestätigung für neue User";
$MESS ['MAIN_NEW_USER_CONFIRM_TYPE_DESC'] = "#USER_ID# - User ID
#LOGIN# - Loginname
#EMAIL# - E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - User IP
#USER_HOST# - User Host
#CONFIRM_CODE# - Bestätigungscode";
$MESS ['MAIN_USER_INVITE_TYPE_NAME'] = "Einladung eines neuen Users";
$MESS ['MAIN_USER_INVITE_TYPE_DESC'] = "#ID# - User ID
#LOGIN# - Loginname
#URL_LOGIN# - Verschlüsselter Login bei der Übergabe über URL
#EMAIL# - E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#PASSWORD# - Passwort
#CHECKWORD# - Kontrollwort für die Passwortänderung
#XML_ID# - User ID, um sich mit externen Datenquellen zu verbinden
";
$MESS ['MAIN_NEW_USER_EVENT_NAME'] = "#SITE_NAME#: Neuer User hat sich auf der Seite registriert";
$MESS ['MAIN_NEW_USER_EVENT_DESC'] = "Nachricht von #SITE_NAME#
---------------------------------------

Ein neuer User wurde auf der Seite registriert #SERVER_NAME#.

Details:
User ID: #USER_ID#

Vorname: #NAME#
Nachname: #LAST_NAME#
E-Mail: #EMAIL#

Username: #LOGIN#

Dies ist eine automatisch generierte Nachricht.";
$MESS ['MAIN_USER_INFO_EVENT_NAME'] = "#SITE_NAME#: Registrierungsinformationen";
$MESS ['MAIN_USER_INFO_EVENT_DESC'] = "Nachricht von #SITE_NAME#
---------------------------------------

#NAME# #LAST_NAME#,

#MESSAGE#

Ihre Registrierungsinformation:

User ID: #USER_ID#
Kontostatus: #STATUS#
Loginname: #LOGIN#

Um Ihr Passwort zu ändern, klicken Sie bitte auf den folgenden Link:
http://#SERVER_NAME#/bitrix/admin/index.php?change_password=yes&lang=de&
USER_CHECKWORD=#CHECKWORD#

Dies ist eine automatisch generierte Nachricht.";
$MESS ['MAIN_NEW_USER_CONFIRM_EVENT_NAME'] = "#SITE_NAME#: Neue Registrierungsbestätigung";
$MESS ['MAIN_NEW_USER_CONFIRM_EVENT_DESC'] = "Nachricht von #SITE_NAME#!
------------------------------------------

Hallo,

Sie haben diese Nachricht erhalten, weil Sie (oder jemand Anderes) Ihre E-Mail benutzt hat, um sich auf #SERVER_NAME# anzumelden.

Ihr Registrierungsbestätigungscode lautet: #CONFIRM_CODE#

Bitte benutzen Sie den folgenden Link, um Ihre Anmeldung zu bestätigen und zu aktivieren:
http://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#&confirm_code=#CONFIRM_CODE#

Oder Sie öffnen diesen Link in Ihrem Browser und tragen den Code manuell ein:
http://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#

Warnung! Ihr Account wird nicht aktiviert, bis Sie Ihre Anmeldung bestätigt haben.

---------------------------------------------------------------------

Dies ist eine automatisch erstellte Nachricht.";
$MESS ['MAIN_USER_INVITE_EVENT_NAME'] = "#SITE_NAME#: Einladung zur Seite";
$MESS ['MAIN_USER_INVITE_EVENT_DESC'] = "Nachricht von der Seite #SITE_NAME #
------------------------------------------
Hallo #NAME# #LAST_NAME#!

Der Administrator hat Sie zu den registrierten User hinzugefügt. 

Wir laden Sie ein, unsere Seite zu besuchen.

Ihre Anmeldedaten:

User ID: #ID#
Loginname: #LOGIN#

Wir empfehlen Ihnen, das automatisch generierte Passwort zu ändern.

Um das Passwort zu ändern, benutzen Sie bitte den folgenden Link:
http://#SERVER_NAME#/auth.php?change_password=yes&USER_LOGIN=#URL_LOGIN#&USER_CHECKWORD=#CHECKWORD#";
$MESS ['MF_EVENT_NAME'] = "Nachricht über das Rückmeldeformular senden";
$MESS ['MF_EVENT_DESCRIPTION'] = "#AUTHOR# - Nachrichtenautor
#AUTHOR_EMAIL# - Autoradresse
#TEXT# - Nachricht
#EMAIL_FROM# - Absenderadresse
#EMAIL_TO# - Empfängeradresse";
$MESS ['MF_EVENT_SUBJECT'] = "#SITE_NAME#: Nachricht aus der Rückmeldungsform";
$MESS ['MF_EVENT_MESSAGE'] = "Benachrichtigung von #SITE_NAME#
------------------------------------------

Sie haben eine Nachricht erhalten. 

Gesendet von: #AUTHOR#
Absender-E-Mail: #AUTHOR_EMAIL#

Nachricht:
#TEXT#

Diese Benachrichtigung wurde automatisch erstellt.";
?>