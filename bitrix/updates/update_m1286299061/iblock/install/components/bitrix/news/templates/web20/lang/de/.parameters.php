<?
$MESS ['TP_CBIV_AVERAGE'] = "Durchschnittswert";
$MESS ['SEARCH_PERIOD_NEW_TAGS'] = "Zeitraum, in dem der Tag als neu gilt (Tage)";
$MESS ['T_IBLOCK_DESC_NEWS_DATE'] = "Elementdatum anzeigen";
$MESS ['T_IBLOCK_DESC_NEWS_PICTURE'] = "Vorschaubild anzeigen";
$MESS ['T_IBLOCK_DESC_NEWS_TEXT'] = "Vorschautext anzeigen";
$MESS ['SEARCH_COLOR_NEW'] = "Farbe des letzten Tags (Beispiel: \"C0C0C0\")";
$MESS ['SEARCH_FONT_MAX'] = "Maximale Schriftgrösse (px)";
$MESS ['SEARCH_COLOR_OLD'] = "Farbe des ersten Tags (Beispiel: \"FEFEFE\")";
$MESS ['SEARCH_PAGE_ELEMENTS'] = "Anzahl der Tags";
$MESS ['TP_CBIV_RATING'] = "Bewertung";
$MESS ['TP_CBIV_DISPLAY_AS_RATING'] = "Als Bewertung anzeigen";
$MESS ['SEARCH_FONT_MIN'] = "Minimale Schriftgröße (px)";
$MESS ['SEARCH_WIDTH'] = "Breite der Tag-Wolke (z.B. \"100%\" oder \"100px\", \"100pt\", \"100in\")";
?>