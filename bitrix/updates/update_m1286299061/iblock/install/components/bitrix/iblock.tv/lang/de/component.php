<?
$MESS ['CC_BIT_ADD_VIDEO'] = "Neues Video";
$MESS ['CC_BIT_ADD_VIDEO_TITLE'] = "Video zu Bibliothek hinzufügen";
$MESS ['CC_BIT_MODULE_NOT_INSTALLED'] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS ['CC_BIT_MANAGE_VIDEO'] = "Video verwalten";
$MESS ['CC_BIT_MANAGE_VIDEO_TITLE'] = "Videobibliothek verwalten";
?>