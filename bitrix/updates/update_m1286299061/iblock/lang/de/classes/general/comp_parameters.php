<?
$MESS ['T_IBLOCK_DESC_PAGER_SHOW_ALWAYS'] = "Immer anzeigen";
$MESS ['T_IBLOCK_DESC_PAGER_DESC_NUMBERING_CACHE_TIME'] = "Cache-Laufzeit der Seiten f�r die r�ckw�rtige Navigation";
$MESS ['T_IBLOCK_DESC_PAGER_TITLE'] = "Kategorie-�berschrift";
$MESS ['T_IBLOCK_DESC_BOTTOM_PAGER'] = "Unter der Liste anzeigen";
$MESS ['T_IBLOCK_DESC_TOP_PAGER'] = "�ber der Liste anzeigen";
$MESS ['IB_COMPLIB_POPUP_ELEMENT_EXTERNAL_ID'] = "Externe ID des Elements";
$MESS ['IB_COMPLIB_POPUP_ELEMENT_ID'] = "Element ID";
$MESS ['IB_COMPLIB_POPUP_ELEMENT_CODE'] = "Mnemonischer Code des Elements";
$MESS ['IB_COMPLIB_POPUP_IBLOCK_EXTERNAL_ID'] = "Externe ID des Informationsblocks";
$MESS ['IB_COMPLIB_POPUP_IBLOCK_ID'] = "Informationsblock ID";
$MESS ['IB_COMPLIB_POPUP_IBLOCK_CODE'] = "Mnemonischer Code des Informationsblocks";
$MESS ['IB_COMPLIB_POPUP_IBLOCK_TYPE_ID'] = "Informationsblocktyp";
$MESS ['T_IBLOCK_DESC_PAGER_TEMPLATE'] = "Vorlagenname";
$MESS ['T_IBLOCK_DESC_PAGER_SETTINGS'] = "Einstellungen f�r die Seitennavigation";
$MESS ['IB_COMPLIB_POPUP_SECTION_EXTERNAL_ID'] = "Externe Bereichs ID";
$MESS ['IB_COMPLIB_POPUP_SECTION_ID'] = "Bereichs ID";
$MESS ['IB_COMPLIB_POPUP_SECTION_CODE'] = "Mnemonischer Code des Bereichs";
$MESS ['T_IBLOCK_DESC_SHOW_ALL'] = "Link \"Alle\" anzeigen";
$MESS ['IB_COMPLIB_POPUP_SITE_DIR'] = "Root-Verzeichnis der Seite";
$MESS ['IB_COMPLIB_POPUP_SERVER_NAME'] = "Server URL";
$MESS ['T_IBLOCK_DESC_PAGER_DESC_NUMBERING'] = "R�ckw�rtige Breadcrumb-Navigation benutzen";
?>