<?
$MESS["IBLOCK_BAD_BLOCK_SECTION_RECURSE"] = "Sie k�nnen den Bereich nicht in sich selbst verschieben.";
$MESS["IBLOCK_BAD_BLOCK_SECTION_PARENT"] = "Falscher Elternbereich!";
$MESS["IBLOCK_BAD_BLOCK_SECTION_ID_PARENT"] = "Der Kurscode stimmt nicht mit dem Eltern-Kurscode �berein!";
$MESS["IBLOCK_BAD_SECTION"] = "Die �berschrift wurde nicht angegeben.";
$MESS["IBLOCK_DUP_SECTION_CODE"] = "Der Bereich mit diesem Zeichencode existiert bereits.";
$MESS["IBLOCK_BAD_SECTION_FIELD"] = "Das ben�tigte Feld  \"#FIELD_NAME#\" wurde leer gelassen.";
?>