<?
$MESS ['IBLOCK_MENU_ALL_EL'] = "Alle Elemente";
$MESS ['IBLOCK_MENU_ELEMENTS'] = "Elemente";
$MESS ['IBLOCK_MENU_SEC_EL'] = "Elemente dieser Gruppe";
$MESS ['IBLOCK_MENU_EXPORT'] = "Export";
$MESS ['IBLOCK_MENU_EXPORT_ALT'] = "Datenexport im CSV Format";
$MESS ['IBLOCK_MENU_ALL_OTH_TITLE'] = "Alle Bereiche";
$MESS ['IBLOCK_MENU_IMPORT'] = "Import";
$MESS ['IBLOCK_MENU_IMPORT_ALT'] = "Daten ins CSV-Format importieren";
$MESS ['IBLOCK_MENU_SETTINGS_TITLE'] = "Informationsblock konfigurieren";
$MESS ['IBLOCK_MENU_ITYPE'] = "Informationsblocktypen";
$MESS ['IBLOCK_MENU_ITYPE_TITLE'] = "Informationsblocktypen";
$MESS ['IBLOCK_MENU_SEPARATOR'] = "Infoblöcke";
$MESS ['IBLOCK_MENU_ALL_OTH'] = "Andere...";
?>