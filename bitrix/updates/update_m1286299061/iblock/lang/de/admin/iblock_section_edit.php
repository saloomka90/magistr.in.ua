<?
$MESS["IBSEC_E_NEW_TITLE"] = "#IBLOCK_NAME#: #SECTION_TITLE#: Hinzuf�gen";
$MESS["IBSEC_E_EDIT_TITLE"] = "#IBLOCK_NAME#: #SECTION_TITLE#: Bearbeiten";
$MESS["IBSEC_E_CONFIRM_DEL_MESSAGE"] = "Alle mit diesem Eintrag verbundenen Informationen gehen verloren! Wollen Sie fortfahren?";
$MESS["IBSEC_E_BACK_TO_ADMIN"] = "Zur�ck zu den Informationsbl�cken";
$MESS["IBSEC_E_CREATED"] = "Erstellt:";
$MESS["IBSEC_E_DESCRIPTION"] = "Beschreibung";
$MESS["IBSEC_E_DESC_TYPE"] = "Beschreibungstyp:";
$MESS["IBSEC_E_DESC_TYPE_HTML"] = "html";
$MESS["IBSEC_E_IBLOCK_MANAGE_HINT_HREF"] = "Informationsblock  konfigurieren.";
$MESS["IBSEC_E_LAST_UPDATE"] = "Ge�ndert:";
$MESS["IBSEC_E_TAB2"] = "Mehr";
$MESS["IBSEC_E_TAB2_TITLE"] = "Weitere Parameter";
$MESS["IBSEC_E_PARENT_SECTION"] = "�bergeordneter Bereich";
$MESS["IBSEC_E_PICTURE"] = "Bild:";
$MESS["IBLOCK_SECTION"] = "Bereich";
$MESS["IBSEC_E_ACTIVE"] = "Der Bereich ist aktiv:";
$MESS["IBSEC_E_BAD_IBLOCK"] = "Der Informationsblock wurde nicht gefunden oder der Zugriff wurde verweigert.";
$MESS["IBSEC_E_DESC_TYPE_TEXT"] = "Text";
$MESS["IBSEC_E_IBLOCK_MANAGE_HINT"] = "Sie k�nnen Eigenschaften und Zugriffsrechte des Informationsblocks bearbeiten unter";
$MESS["IBSEC_E_USER_FIELDS_ADD_HREF"] = "Benutzerdefiniertes Feld";
$MESS["IBSEC_E_LINK_TIP"] = "Den Code aus dem Namen generieren ";
?>