<?
$MESS ['SUPPORT_FAQ_GROUP_SETTINGS'] = "Komponenteinstellungen";
$MESS ['SUPPORT_FAQ_SETTING_IBTYPES'] = "Informationsblocktypen";
$MESS ['SUPPORT_FAQ_SETTING_IBLIST'] = "Informationsblöcke";
$MESS ['SUPPORT_FAQ_SETTING_SECTIONS_LIST'] = "Verzeichnisse";
$MESS ['SUPPORT_FAQ_SETTING_EXPAND_LIST'] = "Untersektionen anzeigen";
$MESS ['SEF_PAGE_FAQ'] = "Startseite";
$MESS ['SEF_PAGE_FAQ_SECTION'] = "Seite des Bereichs";
$MESS ['SEF_PAGE_FAQ_DETAIL'] = "Seite mit Detailansicht";
$MESS ['CP_BSF_CACHE_GROUPS'] = "Zugriffsrechte berücksichtigen";
?>