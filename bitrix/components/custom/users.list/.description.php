<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_USERS_DESC_LIST"),
	"DESCRIPTION" => GetMessage("T_USERS_DESC_LIST_DESC"),
	"ICON" => "/images/news_list.gif",
	"PATH" => array(
			"ID" => "utility",
			"CHILD" => array(
				"ID" => "user",
				"NAME" => GetMessage("MAIN_USER_GROUP_NAME")
			),
		),
);

?>