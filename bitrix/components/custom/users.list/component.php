<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if(!is_array($arParams["GROUP_ID"])) $arParams["GROUP_ID"] = array();

$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
	$arParams["SORT_BY1"] = "name";
if($arParams["SORT_ORDER1"]!="asc")
	 $arParams["SORT_ORDER1"]="desc";

	 if(strlen($arParams["FILTER_NAME"])<=0/* || !preg_match("^[A-Za-z_][A-Za-z01-9_]*$", $arParams["FILTER_NAME"])*/)
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
	$arParams["NEWS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["NEWS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["NEWS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
	$arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
	$arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
	foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
	{
		if(in_array($PERM, $arUserGroupArray))
		{
			$bUSER_HAVE_ACCESS = true;
			break;
		}
	}
}



if($this->StartResultCache(false, array($USER->GetGroups(), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter)))
{
	/*if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}*/
	if(is_array($arParams["GROUP_ID"]) && sizeof(is_array($arParams["GROUP_ID"])) && is_array($arrFilter["GROUP_ID"]) && sizeof($arrFilter["GROUP_ID"]))
	{
		$gid_list = array();
		foreach($arrFilter["GROUP_ID"] as $gid)
			if(in_array($gid,$arParams["GROUP_ID"])) $gid_list[] = $gid;
		$arParams["GROUP_ID"] = $gid_list;
	}
	elseif(is_array($arrFilter["GROUP_ID"]) && sizeof($arrFilter["GROUP_ID"])) $arParams["GROUP_ID"] = $arrFilter["GROUP_ID"];
	unset($arrFilter["GROUP_ID"]);
	
	//if(is_array($arParams["GROUP_ID"]))
	{
		$rsGroup = CGroup::GetList($by="id",$order="asc", array(
			"ACTIVE" => "Y",
			"ID" => implode(" | ",$arParams["GROUP_ID"]),
		));
		while($arGroup=$rsGroup->GetNext())
		{
			$arResult["GROUPS"][$arGroup["ID"]] = $arGroup;
		}
	}
	if(sizeof($arResult["GROUPS"]))
	{
		//WHERE
		$arFilter = array (
			"GROUPS_ID" => array_keys($arResult["GROUPS"]),
			/*"ACTIVE" => "Y",*/
		);
//echo "<pre>".print_r(array_merge($arFilter, $arrFilter),true)."</pre>";
		$rsElement = CUser::GetList($by=$arParams["SORT_BY1"], $order=$arParams["SORT_ORDER1"], array_merge($arFilter, $arrFilter), array("SELECT"=>array("UF_*")));

		//$rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
		$arResult["ITEMS"] = array();
		$rsElement->NavStart($arParams["NEWS_COUNT"]); 
		while($arItem = $rsElement->GetNext())
		{
			if(array_key_exists("PERSONAL_PHOTO", $arItem))
				$arItem["PERSONAL_PHOTO"] = CFile::GetFileArray($arItem["PERSONAL_PHOTO"]);

			$arItem["DETAIL_PAGE_URL"] = str_replace("#USER_ID#",$arItem["ID"],$arParams["DETAIL_URL"]);
			$arResult["ITEMS"][]=$arItem;
		}
		$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
		$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
		$arResult["NAV_RESULT"] = $rsElement;
		$this->SetResultCacheKeys(array(
			"ID",
			"IBLOCK_TYPE_ID",
			"NAV_CACHED_DATA",
			"NAME",
			"SECTION",
		));
		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
		ShowError(GetMessage("T_NEWS_NEWS_NA"));
		@define("ERROR_404", "Y");
		if($arParams["SET_STATUS_404"]==="Y")
			CHTTP::SetStatus("404 Not Found");
	}
}

if(isset($arResult["ID"]))
{

	$arTitleOptions = null;
	/*if($USER->IsAuthorized())
	{
		if(
			$arParams["DISPLAY_PANEL"]
			|| $APPLICATION->GetShowIncludeAreas()
			|| $arParams["SET_TITLE"]
		)
		{
			if(CModule::IncludeModule("iblock"))
			{
				$arButtons = CIBlock::GetPanelButtons($arResult["ID"], 0, $arParams["PARENT_SECTION"]);

				if($arParams["DISPLAY_PANEL"])
					CIBlock::AddPanelButtons($APPLICATION->GetPublicShowMode(), $this->GetName(), $arButtons);

				if($APPLICATION->GetShowIncludeAreas())
					$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

				if($arParams["SET_TITLE"])
				{
					$arTitleOptions = array(
						'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
						'PUBLIC_EDIT_LINK' => "",
						'COMPONENT_NAME' => $this->GetName(),
					);
				}
			}
		}
	}*/

	$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

	/*if($arParams["SET_TITLE"])
	{
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
	}

	if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["NAME"]))
	{
		$APPLICATION->AddChainItem($arResult["NAME"]);
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}*/
}

?>