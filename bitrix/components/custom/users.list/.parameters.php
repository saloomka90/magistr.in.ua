<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	exit();

$arGroups=Array();
$db_group = CGroup::GetList($by="sort",$order="asc", Array("ACTIVE"=>"Y"));
while($arRes = $db_group->Fetch())
	$arGroups[$arRes["ID"]] = $arRes["NAME"];

$arSorts = Array("asc"=>GetMessage("T_USERS_DESC_ASC"), "desc"=>GetMessage("T_USERS_DESC_DESC"));
$arSortFields = Array(
		"id"=>GetMessage("T_USERS_DESC_FID"),
		"name"=>GetMessage("T_USERS_DESC_FNAME"),
		"second_name"=>GetMessage("T_USERS_DESC_FSECOND_NAME"),
		"last_Name"=>GetMessage("T_USERS_DESC_FLAST_NAME")
	);

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"GROUP_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_USERS_DESC_GROUP_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arGroups,
			"ADDITIONAL_VALUES" => "N",
			"MULTIPLE" => "Y",
		),
		"NEWS_COUNT" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_USERS_DESC_NEWS_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		),
		"FILTER_NAME" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => "�������� ���������� �������",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"SORT_BY1" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_USERS_DESC_IBORD1"),
			"TYPE" => "LIST",
			"DEFAULT" => "NAME",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		),
		"SORT_ORDER1" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_USERS_DESC_IBBY1"),
			"TYPE" => "LIST",
			"DEFAULT" => "desc",
			"VALUES" => $arSorts,
		),
		"DETAIL_URL" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("T_USERS_DESC_DETAIL_PAGE_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => $GLOBALS["APPLICATION"]->GetCurDir(),
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("T_USERS_DESC_PAGER_NEWS"), true, true);
?>
