<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$link = DeleteParam(array("delete"));
$link = strpos($link,"?")===false ? "?" : "&";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

//echo "<pre>".print_r($arResult,true)."</pre>";
?>
<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	jQuery(function() {
		jQuery.nyroModalSettings({
			//debug: true,
			//width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			},
			endShowContent: function(elts, settings) {
				jQuery("#nyroModalContent form").each(function(){
					var options = {
						target: "#nyroModalContent",
						url: jQuery(this).attr("action")
					};
					jQuery(this).ajaxForm(options);
				});
			}
		});
	});
	
	function showUserDetails(user_id,other_params)
	{
		jQuery.nyroModalManual({
			url: '<?=$cur_dir?>/user_detail.php?u_id='+user_id+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
	}
	//]]>
</script>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="4"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tbody>
	<tr class="head">
		<td style="width:0%;">���� �����������</td>
		<td style="width:100% !important;">�.�.�. (�������� ��������, ���������)</td>
		<td>Email</td>
		<td>��� �����������</td>
		<td>&nbsp;</td>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$ar_groups = CUser::GetUserGroup($arItem["ID"]);
	
	if($arItem["ACTIVE"]!="Y") $td_class = ' class="yellow"';
	?>
	<tr>
		<td style="width:0%;"<?=$td_class?>><?=$arItem["DATE_REGISTER"]?></td>
		<td><a href="javascript:void(0)" onclick="showUserDetails(<?=$arItem["ID"]?>);"><?//=$arItem["LAST_NAME"]?><?=$arItem["NAME"]?><?//=$arItem["SECOND_NAME"]?></a></td>
		<td><?=$arItem["EMAIL"]?></td>
		<td><?=$arResult["GROUPS"][reset($ar_groups)]["NAME"]?></td>
		<td>
			<a href="javascript:void(0)" onclick="if(confirm('�� ������ ������� ������������?')) location.href='<?=$link?>delete=<?=$arItem["ID"]?>'" title="�������" class="delete-pic">&nbsp;</a>
		</td>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
