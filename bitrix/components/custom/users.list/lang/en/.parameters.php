<?
$MESS ['T_USERS_DESC_ASC'] = "Ascending";
$MESS ['T_USERS_DESC_DESC'] = "Descending";
$MESS ['T_USERS_DESC_FID'] = "ID";
$MESS ['T_USERS_DESC_FNAME'] = "Name";
$MESS ['T_USERS_DESC_FLAST_NAME'] = "Last name";
$MESS ['T_USERS_DESC_FSECOND_NAME'] = "Second name";
$MESS ['T_USERS_DESC_FSORT'] = "Sorting";
$MESS ['T_USERS_DESC_IBORD1'] = "Field for the users sorting pass";
$MESS ['T_USERS_DESC_IBBY1'] = "Direction for the users sorting pass";
$MESS ['T_USERS_DESC_GROUP_ID'] = "Select users from groups";
$MESS ['T_USERS_DESC_DETAIL_PAGE_URL'] = "Detail page URL";
$MESS ['T_USERS_DESC_PAGER_NEWS'] = "Users";
$MESS ['T_USERS_DESC_NEWS_COUNT'] = "Users per page";
?>