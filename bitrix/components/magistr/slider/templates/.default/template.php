<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<script type="text/javascript">

            $(document).ready(function() {
                $('#far-clouds').pan({fps: 30, speed: 0.7, dir: 'left', depth: 30});
                $('#near-clouds').pan({fps: 30, speed: 1, dir: 'right', depth: 70});
                
                window.actions = {
                    speedyClouds: function(){
                        $('#far-clouds').spSpeed(12);
                        $('#near-clouds').spSpeed(20);
                    },
                    runningClouds: function(){
                        $('#far-clouds').spSpeed(8);
                        $('#near-clouds').spSpeed(12);
                    },
                    walkingClouds: function(){
                        $('#far-clouds').spSpeed(3);
                        $('#near-clouds').spSpeed(5);
                    },
                    lazyClouds: function(){
                        $('#far-clouds').spSpeed(0.7);
                        $('#near-clouds').spSpeed(1);
                    },
                    stop: function(){
                        $('#far-clouds, #near-clouds').spStop();
                    },
                    start: function(){
                        $('#far-clouds, #near-clouds').spStart();
                    },
                    toggle: function(){
                        $('#far-clouds, #near-clouds').spToggle();
                    },
                    left: function(){
                        $('#far-clouds, #near-clouds').spChangeDir('left');                    
                    },
                    right: function(){
                        $('#far-clouds, #near-clouds').spChangeDir('right');                    
                    }
                };
            });    
    </script>
        <div class="slider-bg">
			<div id="far-clouds" class="far-clouds stage"></div>
			<div id="near-clouds" class="near-clouds stage"></div>		
			<div class="slider0">
			<div class="slider">
				<ul id="mycarousel">
					<li id="1" class="v">
						<div class="slider-desc">
							<div class="slider-desc-l">
								<div class="start-img s-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/check4.png" />
								</div>

								<div class="slider-info" style="opacity: 0;">
									<h3><?=GetMessage("SLIDE1_TITLE")?></h3>
									<p><?=GetMessage("SLIDE1_TEXT")?></p>
									<div class="clr"></div>
								</div>
							</div>
							<div class="slider-icon slider-desc-r">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/slider4.png" />
							</div>
						</div>
					</li>				
				
					<li id="2">
						<div class="slider-desc">
							<div class="slider-desc-l">
								<div class="start-img s-image" style="margin-left: -205px;">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/check.png" />
								</div>

								<div class="slider-info" style="opacity: 0;">
									<h3><?=GetMessage("SLIDE2_TITLE")?></h3>
									<p><?=GetMessage("SLIDE2_TEXT")?></p><div class="clr"></div>
								</div>
							</div>
							<div class="slider-icon slider-desc-r">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/slider1.png" />
							</div>
						</div>
					</li>

					<li id="3">
						<div class="slider-desc">
							<div class="slider-desc-l">
								<div class="start-img s-image" style="margin-left: -205px;">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/check2.png" />
								</div>

								<div class="slider-info" style="opacity: 0;">
									<h3><?=GetMessage("SLIDE3_TITLE")?></h3>
									<p><?=GetMessage("SLIDE3_TEXT")?></p><div class="clr"></div>
									<div class="clr"></div>
								</div>
							</div>
							<div class="slider-icon slider-desc-r">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/slider2.png" />
							</div>
						</div>
					</li>

					<li id="4" class="">
						<div class="slider-desc">
							<div class="slider-desc-l">
								<div class="start-img s-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/check3.png" />
								</div>

								<div class="slider-info" style="opacity: 0;">
									<h3><?=GetMessage("SLIDE4_TITLE")?></h3>
									<p><?=GetMessage("SLIDE4_TEXT")?></p><div class="clr"></div>									
									<div class="clr"></div>
								</div>
							</div>
							<div class="slider-icon slider-desc-r">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/slider3.png" />
							</div>
						</div>
					</li>
				</ul>
				<div class="prev"></div>
				<img alt="" class="corner" />
				<div class="next"></div>
				<div class="pagination icon-box">
					<div class="pag box" id="1">
						
					</div>				
				
					<div class="pag box" id="2">
						
					</div>

					<div class="pag box" id="3">
						
					</div>

					<div class="pag box" id="4">
						
					</div>
				</div>				
			</div>
			</div>
        </div>
		<div class="sale-bg">
			<div class="sale">
				<?//��� ��������� �������� ������������ � ����������?>
				<?$APPLICATION->IncludeComponent("magistr:timerTo", "withOrder", array(
					"TIME_TO"=>$arParams["TIME_TO"], 
					"SKIDKA"=>$arParams["SKIDKA"],
					"ORDER_PAGE"=>$arParams["ORDER_PAGE"],
					), 
					$component);?>
				<? /*?><p><?=GetMessage("WE_SELL")?></p><?*/?>
			</div>
		</div>