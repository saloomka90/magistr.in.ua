<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?


	
if(!CModule::IncludeModule("iblock")) return;
	
	
	// Get Woerks Type List
	$arSelect = array("ID", "NAME");
	if(LANGUAGE_ID=="ru"){
		$arSelect[] = "PROPERTY_TITLE_RU";
	}
	$arSelect[] = "PROPERTY_TITLE_RU";
	$arFilter= array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_WORK_TYPES_ID"]);
		
	$arSort = array("SORT" => "ASC");	
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>900), $arSelect);		

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			$arResult["WORK_TYPES"][$arItem["ID"]] = (LANGUAGE_ID=="ru" ? $arItem["PROPERTY_TITLE_RU_VALUE"]: $arItem["NAME"]); 
		}
	
	
	//Get Predmets List
	$arSelect = array();
	$arFilter = array();
	$arSort = array();
	
	$arSelect = array("ID", "NAME", "IBLOCK_SECTION_ID");
	
	if(LANGUAGE_ID=="ru"){
		$arSelect[] = "PROPERTY_TITLE_RU";
	}
	$arSelect[] = "PROPERTY_TITLE_RU";
	$arFilter= array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_PREDMETS_ID"]);
		
	$arSort = array("NAME"=>"ASC","SORT" => "ASC");	
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>900), $arSelect);		

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			$arResult["WORK_PREDMETS"][$arItem["IBLOCK_SECTION_ID"]][$arItem["ID"]] = (LANGUAGE_ID=="ru" ? $arItem["PROPERTY_TITLE_RU_VALUE"]: $arItem["NAME"]);
			$arElementsToSection[$arItem["ID"]] = $arItem["IBLOCK_SECTION_ID"];
		}
	
	//Get Predmets Sections
	
	$arSelect = array("CODE", "ID", "DESCRIPTION", "NAME");
	$arFilter = array();
	$arSort = array();
	
	$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_PREDMETS_ID"], 'ACTIVE'=>'Y');
  	$db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false, $arSelect);
	  
	  while($ar_result = $db_list->GetNext())
	  {
	   $arResult["PREDMETS_RUBRIK"][$ar_result["ID"]]= (LANGUAGE_ID=="ru" ? $ar_result["DESCRIPTION"]: $ar_result["NAME"]);;
	  
	  	$arSectToCode[$ar_result["ID"]] = $ar_result["CODE"];

	  }
	/*
	 echo "<pre>";
		print_r($arSectToCode);
	echo "</pre>";
	
	 echo "<pre>";
		print_r($arElementsToSection);
	echo "</pre>";
	*/
	



$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ORDER_ID"], "CODE"=>"lang"));
while($enum_fields = $property_enums->GetNext())
{
	$arResult["LANGS"][$enum_fields["ID"]] = $enum_fields["VALUE"];
 
}


$arRequaredFields = array();



//POST REQUEST
if(
	check_bitrix_sessid() &&
	$_REQUEST["send"] && 
	$_REQUEST["tema"] && 
	//$_REQUEST["mova"] && 
	$_REQUEST["PLAGIAT"] &&
	$_REQUEST["termin2"] &&
	$_REQUEST["storinok"] &&
	$_REQUEST["vuz"] &&
	//$_REQUEST["vimogi"] &&
	$_REQUEST["name"] &&
	$_REQUEST["email"] &&
	$_REQUEST["tel"] &&
	$_REQUEST["input_predmet"] &&
	$_REQUEST["input_tip"] 
){
	
	


	$PROP = array();
	
	$PROP["18"]= $_REQUEST["email"]; //email
	$PROP["19"]= '+'.preg_replace('~[^0-9]+~','',$_REQUEST["tel"]); //tel
	$PROP["23"]= $_REQUEST["tema"]; //tema
	$PROP["74"]= $_REQUEST["PLAGIAT"]; //PLAGIAT
	$PROP["75"]= $_REQUEST["ERCENT_PLAGIAT"]; //PLAGIAT
	$PROP["26"]= $_REQUEST["storinok"]; //storinok
	$PROP["27"]= $_REQUEST["termin2"]; //termin
	$PROP["30"]= $_REQUEST["vuz"]; //termin
	$PROP["33"]= $_REQUEST["vimogi"]; //termin
	
	if($_REQUEST["predmet"]){
		$PROP["60"]= $_REQUEST["predmet"]; 
		$PROP["44"] = $arSectToCode[$arElementsToSection[$_REQUEST["predmet"]]]; // Old
		
	}else{
		$PROP["62"]= $_REQUEST["input_predmet"];
		$PROP["60"]="51097";
		$PROP["44"] = "35";
	}
	
	$PROP["22"] = $_REQUEST["input_predmet"]; // Old
	
	if($_REQUEST["tip"]){
    	$PROP["61"]= $_REQUEST["tip"]; 
		$PROP["24"]= $arResult["WORK_TYPES"][$_REQUEST["tip"]]; // Old
		
	}else{
		$PROP["63"]= $_REQUEST["input_tip"];
		$PROP["61"] = "50943";
		$PROP["24"]= $_REQUEST["input_tip"]; // Old
		
	}
	
	if($_REQUEST["USER_FILES"]){
		$i=0;
		 foreach($_REQUEST["USER_FILES"] as $fileID){
		 	$PROP["34"]["n".$i] = $fileID;
			 $i++;
		 }
	}
    
    	$arLoadProductArray = Array(  
		   'IBLOCK_ID' => $arParams["IBLOCK_ORDER_ID"],
		   'PROPERTY_VALUES' => $PROP,  
		   'NAME' => htmlspecialcharsEx($_REQUEST["name"]),  
		   'ACTIVE' => 'N', // �������  
		   'DATE_ACTIVE_FROM' => date("d.m.Y H:i:s"),
	);
	
	$el = new CIBlockElement;
	
	if($orderId=$el->Add($arLoadProductArray)){
		$to = (LANGUAGE_ID == "ru")?"/ru":"";
 		LocalRedirect($to."/order/?order=".$orderId); 
 	}else{
  		$arResult["ERROR"] = true;
		
	}
   
}

$this->IncludeComponentTemplate();
?>