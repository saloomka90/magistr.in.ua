<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<!--# Net metodichki #-->
	<div id="noMethod">
		<div>
			<h4>�� �� ��������� ���������!</h4>
			<p>���� ����� ������������ ������ ����� ����������� �����.</p>
			<h4>�������� �����!</h4>
			<p>������� ������ ���� ����� ����������� ����� ����������� �����. ���� � ������ ��������� ������ ������ ������, �� ������� ������ ���� ����� ������</p>
			<div class="noMethod-controls">
				<input type="button" id="noMethod-cancel" value="�����" />
				<input type="button" id="noMethod-agree" value="����������" />
			</div>
		</div>
	</div>
	<!--# end Net metodichki #-->


<div class="wrap">
				<div class="zakaz-pagination">
					<div class="zakaz-pag <?if(!$_REQUEST["order"]):?>act<?endif;?>">
						<div class="zakaz-num">1</div>
						<span><?=GetMessage("STEP1")?></span>
					</div>
					<div class="arrow-z act"></div>
					<div class="zakaz-pag <?if($_REQUEST["order"]):?>act<?endif;?>">
						<div class="zakaz-num">2</div>
						<span><?=GetMessage("STEP2")?></span>
					</div>
					<div class="arrow-z"></div>
					<div class="zakaz-pag">
						<div class="zakaz-num">3</div>
						<span><?=GetMessage("STEP3")?></span>
					</div>
					<div class="clr"></div>
				</div>
				<div class="zakaz-form">
					<?if(intval($_REQUEST["order"])>0):?>
					
						<p style="text-align: center; font-weight: bold; color: green;"><?=GetMessage("OREDER_OK", array("#ID#"=>intval($_REQUEST["order"])))?></p>
					
					<?else:?>
					
					
					<? if($arResult["ERROR"]):?>
						<p style="text-align: center; font-weight: bold; color: red;"><?=GetMessage("OREDER_ERROR")?></p>
					
					<?endif;?>
					<?
							$APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/js/select/jquery.selectbox.css" />');
							$APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/jquery-filestyle.css" />');
							$APPLICATION->AddHeadString('<link rel="stylesheet" href="'.SITE_TEMPLATE_PATH.'/css/datepicker/jquery-ui.css">');
							$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/js/select/jquery.selectbox-0.2.js"></script>');
							$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/js/jquery-filestyle.js"></script>');
							$APPLICATION->AddHeadString('<script src="'.SITE_TEMPLATE_PATH.'/js/jquery-ui.js"></script>');
							$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/js/bootstrap-filestyle.min.js"> </script>');
							$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.js"></script>');
							?>
							
							
							
							<script>
								$(document).ready(function(){
									
								//$(":file").jfilestyle({icon:false, 'buttonText': '������� ����', input: false});
								$(":file").filestyle({'buttonText': '<?=GetMessage('WORK_CHOSE_FILE')?>', icon:false, input: false});
								//Calendar
										$("#datepicker").datepicker({
										  showOn: "button",
										  buttonImage: "<?=SITE_TEMPLATE_PATH?>/images/calendar.png",
										  buttonImageOnly: true,
										  dateFormat: "dd/mm/yy"
										});
										
										$.datepicker.regional['ua'] = {
											closeText: '<?=GetMessage("CAL_CLOSE")?>',
											prevText: '&#x3c;<?=GetMessage("CAL_PREV")?>',
											nextText: '<?=GetMessage("CAL_NEXT")?>&#x3e;',
											currentText: '<?=GetMessage("currentText")?>',
											monthNames: [<?=GetMessage("monthNames")?>],
											monthNamesShort: [<?=GetMessage("monthNamesShort")?>],
											dayNames: [<?=GetMessage("dayNames")?>],
											dayNamesShort: [<?=GetMessage("dayNamesShort")?>],
											dayNamesMin: [<?=GetMessage("dayNamesMin")?>],
											weekHeader: '��',
											dateFormat: 'dd.mm.yy',
											firstDay: 1,
											minDate:"0d",
											isRTL: false,
											showMonthAfterYear: false,
											yearSuffix: ''};
										$.datepicker.setDefaults($.datepicker.regional['ua']);
										
										/*$('form input[type="submit"]').click(function(){
											setTimeout(function(){
												$('form select').each(function(){
													if($(this).attr('class')=='niceSelect error'){
														$(this).parent().addClass('sel');
														$(this).parent().removeClass('sel2');
													}
													else {
														$(this).parent().removeClass('sel');
														$(this).parent().addClass('sel2');
													}
												});
											}, 100);
										});*/

										$(".full_phone_input").mask("+38 (999) 999-99-99");

										$('form#zakaz_form #submite').click(function(){
											
											/*USER_FILES[]*/
											if(window.workFileRequired && $("#zakaz_form input[name='USER_FILES[]']").val() == null){
												$("#noMethod").fadeIn(150);
												$("#noMethod-cancel").click(function(){
													$("#noMethod").fadeOut(150);
												});
												$("#noMethod-agree").click(function(){
													$("#noMethod").hide();
													$("form#zakaz_form").submit();
												
												});
												
											} else{
												
												setTimeout(function(){
													$('form select').each(function(){
														if($(this).attr('class')=='niceSelect error'){
															$(this).parent().addClass('sel');
															$(this).parent().removeClass('sel2');
														}
														else {
															$(this).parent().removeClass('sel');
															$(this).parent().addClass('sel2');
														}
													});
												}, 100);
												$("#zakaz_form").submit();
											}
										});
								
								});
								
							</script>
							  
							<? $APPLICATION->AddHeadString("
							<script>
							
										$(function() {
											var select = $('.niceSelect');
											$(select).selectbox({
												onOpen: function (select) {
													$('.sbHolder').addClass('focus');
													$('.sbGroup').parent().addClass('sbGroup');
												},
												onClose: function (inst) {
													$('.sbHolder').removeClass('focus');
												},
												onChange: function (val, inst) {
													$(this).parent().attr('class','sel2');
													if(val == ''){
														$(this).parent().attr('class','sel');
													}
														if($(this).attr('id') == 'plagiatus'){
															plagiatus($(this).val()); // ��������� �������, ���� �����������, ��������, ������������ �� �������,  �� ����� ���� ���������� ���� ���������� �����: ����������� �������� ���������
														}
												},
												effect: 'slide'				
											});
										});
							</script>"); ?>
							
							   <script language="javascript" type="text/javascript">
							   
							        (function($){
							        $.widget( "ui.combobox", {
							            _create: function() {
							                var self = this,
							                    select = this.element.hide(),
							                    selected = select.children( ":selected" ),
							                    value = selected.val() ? selected.text() : "";
							                var input = this.input = $( "<input>" )
							                    .insertAfter( select )
							                    .val( value )
							                    .attr("id", 'input_' + select.attr("id"))
												.attr("name", 'input_' + select.attr("name"))
												.attr("placeholder", select.attr("placeholder"))
							                    .autocomplete({
							                        bullet: (this.options.bullet) ? this.options.bullet : null,
							                        delay: 0,            
							                        minLength: 0,
							                        source: function( request, response ) {
							                            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							                            response( select.find( "option" ).map(function() {
							                                    optgroup = $(select).find('option[value="'+this.value+'"]').parent('optgroup');
							                                    if (optgroup.length == 0) {
							                                        var item_category = 0;
							                                    } else {
							                                        var item_category = optgroup.attr('label');
							                                    }
							                                    var text = $( this ).text();
							                                    if ( this.value && ( !request.term || matcher.test(text) ) )
							                                        return {
							                                            label: text.replace(
							                                                new RegExp(
							                                                    "(?![^&;]+;)(?!<[^<>]*)(" +
							                                                    $.ui.autocomplete.escapeRegex(request.term) +
							                                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
							                                                ), "<strong>$1</strong>" ),
							                                            value: text,
							                                            category: item_category,
							                                            option: this
							                                        };
							                            }) );
							                        },
							                        select: function( event, ui ) {
							                            ui.item.option.selected = true;
							                            self._trigger( "selected", event, {
							                                item: ui.item.option
							                            });
							                            if(select.attr("id") == "cpecialities_id"){
															checkType(select.val()); // ��������� �������, ���� �����������, ��������, ������������ �� �������,  �� ����� ���� ���������� ���� ���������� �����: ����������� �������� ���������
														}
							                        },
							                        change: function( event, ui ) {
							                            if ( !ui.item ) {
							                                var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
							                                    valid = false;
							                                select.children( "option" ).each(function() {
							                                    if ( $( this ).text().match( matcher ) ) {
							                                        this.selected = valid = true;
							                                        return false;
							                                    }
							                                });
							                                if ( !valid && select.attr("id")=="cpecialities_id") {
							                               //     ���� ���������� - �������� ������ � ���� �� �� �������! 
							                              //       $( this ).val( "" );
							                                       select.val( "" );
							                              //       input.data( "autocomplete" ).term = "";
							                             //       return false;
							                                }
							                                if ( !valid && select.attr("id")=="categories_id") {
							                               //     ���� ���������� - �������� ������ � ���� �� �� �������! 
							                              //       $( this ).val( "" );
							                                       select.val( "" );
							                              //       input.data( "autocomplete" ).term = "";
							                             //       return false;
							                                }
							                            }
							                        }
							                    })
							                    .addClass( "ui-widget ui-widget-content ui-corner-left" );
							 
							                input.data( "autocomplete" )._renderItem = function( ul, item ) {
							                    if (this.options.bullet) {
							                        bullet = '&#8227;';
							                    } else {
							                        bullet = '';
							                    }
							                    return $( "<li></li>" )
							                        .data( "item.autocomplete", item )
							                        .append( "<a>" + bullet + item.label + "</a>" )
							                        .appendTo( ul );
							                };
							 
							                input.data( "autocomplete" )._renderMenu = function( ul, items ) {
							                    var self = this,
							                        currentCategory = "";
							                    $.each( items, function( index, item ) {
							                        if (item.category != 0) {
							                            if ( item.category != currentCategory ) {
							                                ul.append( "<li class='ui-menu-category'>" + item.category + "</li>" );
							                                currentCategory = item.category;
							                            }
							                        }
							                        self._renderItem( ul, item );
							                    });
							                };
							 
							                this.button = $( "<button type=\"button\">&nbsp;</button>" )
							                    .attr( "tabIndex", -1 )
							               //     .attr( "title", "�������� �� ���������" )
							                    .insertAfter( input )
							                    .button({
							                        icons: {
							                            primary: "ui-icon-triangle-1-s"
							                        },
							                        text: false
							                    })
							                    .removeClass( "ui-corner-all" )
							                    .addClass( "ui-corner-right ui-button-icon" )
							                    .click(function() {
							                        // close if already visible
							                        if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							                            input.autocomplete( "close" );
							                            return;
							                        }
							 
							                        // pass empty string as value to search for, displaying all results
							                        input.autocomplete( "search", "" );
							                        input.focus();
							                    });
							            },
							            destroy: function() {
							                this.input.remove();
							                this.button.remove();
							                this.element.show();
							                $.Widget.prototype.destroy.call( this );
							            }
							        });
							    })( jQuery );
							           
							        jQuery(document).ready(function(){

							        	

										
							                jQuery("#cpecialities_id").combobox();
							                jQuery("#categories_id").combobox();
							                jQuery("#btn-slide").click(function(){
							                    jQuery("#cat_hide").slideToggle("slow");
							                    jQuery(this).toggleClass("active_cat_hide"); return false;
							                });
							                jQuery('#attachment').change(function(){
							                    jQuery('#attach_text').val(jQuery(this).val());
							                });             
							                jQuery('#attach_text').click(function(){
							                    jQuery('#attachment').trigger('click');
							                });
							        });
							  </script>
							  
							  <script>
							  jQuery(document).ready(function(){

								  $('#datepicker').on('change',function(){
									  if($(this).val() !=''){
											$(this).removeClass('error');
													$(this).addClass('valid');
													
										}
								  });
							  $('#input_categories_id, #input_cpecialities_id').on('focusout',function(){
									if($(this).val() !=''){
										$(this).removeClass('error');
												$(this).addClass('valid');
												
									}else{
    									if($(this).hasClass( "valid" )){
    												$(this).removeClass('valid');
    												$(this).addClass('error');
    									}
									}
									
							  });

							  


							  /*$('#input_categories_id, #input_cpecialities_id').on('focus',function(){
								  if($(this).val() !=''){
										$(this).removeClass('error');
												$(this).addClass('valid');
												
									}else{
												$(this).removeClass('valid');
												$(this).addClass('error');
									}
									
							  });*/

							  });
							  $(function() {
										$("#zakaz_form").validate({
										   rules:{
												tema:{
													required: true,
													minlength: 3,
													maxlength: 150,
												},
												PLAGIAT:{
													required: true,
													minlength: 1,
													maxlength: 30,
													
												},
												ERCENT_PLAGIAT:{
													required: false,
													minlength: 1,
													maxlength: 30,
													
												},
												input_tip:{
													required: true,
													minlength: 3,
													maxlength: 450,
												},
												input_predmet:{
													required: true,
													minlength: 1,
													minlength: 3,
													maxlength: 450,
												},
												
												termin:{
													required: true,
												},
												storinok:{
													required: true,
												},
												vuz:{
													required: true,
													minlength: 3,
													maxlength: 80,
												},
												name:{
													required: true,
													minlength: 3,
													maxlength: 50,
												},
												email:{
													required: true,
													email: true,
												},
												tel:{
													required: true,
													minlength: 2,
													maxlength: 20,
												},
												filed:{
													required: false,
												},					
										   },
										   messages:{
												tema:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												input_tip:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												input_predmet:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												PLAGIAT:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												ERCENT_PLAGIAT:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												termin:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												storinok:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												vuz:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												name:{
													required: "",
													minlength: "",
													maxlength: "",
												},					
												email:{
													required: "",
													email: "",
												},
												tel:{
													required: "",
													minlength: "",
													maxlength: "",
												},
												filed:{
													required: "",
												},					
										   }
										});	
							  });
							  </script>	
					
					
					
					
					<form id="zakaz_form" method="post" name="zakaz_form" enctype="multipart/form-data">
					<?=bitrix_sessid_post()?>
					<div class="zakaz-form-l">
						<div class="z-val">
							<label><?=GetMessage("WORK_TEMA")?>:<span>*</span></label>
							<input type="text" class="act" name="tema" value="<?=($_REQUEST["tema"]?:$_REQUEST["tema"])?>" />							
						</div>
						<div class="z-val">
							<label><?=GetMessage("WORK_PREDMET")?>:<span>*</span></label>
							<div class="ui-widget">
								<select name="predmet" id="categories_id" size="1" placeholder="������� ..." style="display: none;">
									<option value=""></option>
									<?foreach($arResult["PREDMETS_RUBRIK"] as $rubId=>$nameRubrik):?>
										<optgroup label="<?=$nameRubrik;?>:">
											<?foreach($arResult["WORK_PREDMETS"][$rubId] as $predmId=>$namePredmet):?>
												<option value="<?=$predmId;?>" ><?=$namePredmet;?></option>
											<?endforeach;?>
										</optgroup>
									<?endforeach;?>
								</select>
							</div>
						</div>
<?// ========================================================= ?>
						<div class="z-val">
							<label>�������� �� ������: <span data-tooltip="���� � ������ ��ǳ ������ ������ ��������� �� ������, �� ���������� ������ ���� ������� ���� ������� ������������ ������"></span></label>
							<div>
								<select class="niceSelect" name="PLAGIAT" id="plagiatus" placeholder="��������...">								
									<option value="no" selected>��� ��������</option>
									<option value="yes" >���, �������� ����</option>
								</select>
							</div>
							<div id="plagiatusWrapper" class="">
							<label style="display:none;">�������������</label>							
								<select class="niceSelect" id='plagiatus_procent' name='ERCENT_PLAGIAT'>									
									<option value="min 40%" selected>min 40%</option>
									<option value="min 50%">min 50%</option>
									<option value="min 60%">min 60%</option>
									<option value="min 70%">min 70%</option>
									<option value="min 80%">min 80%</option>
									<option value="min 90%">min 90%</option>
									<option value="100%">100%</option>
								</select>
							</div>
						</div>
						
						<? /*?>
						<div class="z-val">
							<label><?=GetMessage("WORK_MOVA")?>:<span>*</span></label>
							<div>
								<select class="niceSelect" name="mova">
									
									<option value=""><?=GetMessage("WORK_VIBRAT")?></option>
									<?foreach ($arResult["LANGS"] as $langId=>$langName):?>
										<option value="<?=$langId;?>" <?=($_REQUEST["mova"]==$langId ? ' selected="selected"': "");?>><?=$langName?></option>
									<?endforeach;?>
								</select>
							</div>
						</div>
						<? */?>
<?// ========================================================= ?>
					</div>
					<div class="zakaz-form-r">
						<div class="z-val">
							<label><?=GetMessage("WORK_TIP")?>:<span>*</span></label>
							<div class="ui-widget">
								<select name="tip" id="cpecialities_id" size="1" placeholder="<?=GetMessage("WORK_KURSOVA_ECT")?>" style="display: none;">
									<option value=""></option>
									<?foreach($arResult["WORK_TYPES"] as $typeID=>$nameType) :?>
										<option value="<?=$typeID?>"><?=$nameType?></option>
									<?endforeach;?>
								</select>
							</div>							
						</div>
						<div class="z-val">
							<div class="z-val-l" style="position: relative;" id="terminB">
								<label><?=GetMessage("WORK_TERMIN")?>:<span>*</span></label>
								<input type="text" readonly  id="datepicker" name="termin" value="<?=($_REQUEST["termin2"]?:$_REQUEST["termin2"])?>" autocomplete="off"/>
								<input type="hidden"  id="termin2"  name="termin2" value="<?=($_REQUEST["termin2"]?:$_REQUEST["termin2"])?>" />
							</div>
							<div class="z-val-r">
								<label><?=GetMessage("WORK_STOR")?>:<span>*</span></label>
								<input type="text" name="storinok" value="<?=($_REQUEST["storinok"]?:$_REQUEST["storinok"])?>" />
							</div>
							<div class="clr"></div>
						</div>
						<div class="z-val">
							<label><?=GetMessage("WORK_VUZ")?>:<span>*</span></label>
							<input type="text" name="vuz" value="<?=($_REQUEST["vuz"]?:$_REQUEST["vuz"])?>" />
						</div>
					</div>
					<div class="clr"></div>
					<div class="zakaz-form-c">
						<label><?=GetMessage("WORK_VIMOGA")?>:</label>
						<textarea name="vimogi" placeholder="<?=GetMessage("WORK_VIMOGA_PLACE")?>"><?=($_REQUEST["vimogi"]?:$_REQUEST["vimogi"])?></textarea>
					</div>
					<div class="zakaz-form-l">
						<label style="color: #574641; font-size: 16px; margin: 0 0 5px 0; display: block; margin: 0 0 5px 0;">
							<?=GetMessage("WORK_ATACHE_FILE")?>:</label>
						<div style="min-height: 240px;">
	 						<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "drag_n_drop",
							   array(
							      "INPUT_NAME"=>"USER_FILES",
							      "MULTIPLE"=>"Y",
							      "MODULE_ID"=>"main",
							      "MAX_FILE_SIZE"=>20*1024*1024, //5��
							      "ALLOW_UPLOAD"=>"A", 
							      "ALLOW_UPLOAD_EXT"=>""
							   ),
							   false
							);?>
						</div>
					</div>
					<div class="zakaz-form-r">
						<div class="z-val">
							<label><?=GetMessage("WORK_PIB")?>:<span>*</span></label>
							<input type="text" name="name" value="<?=($_REQUEST["name"]?:$_REQUEST["name"])?>" />
						</div>
						<div class="z-val">
							<label>E-mail:<span>*</span></label>
							<input type="text" name="email" value="<?=($_REQUEST["email"]?:$_REQUEST["email"])?>" />
						</div>
					
					<div class="z-val tell">
					
							<label><?=GetMessage("WORK_TEL")?>:<span>*</span></label>
							
							<input class="full_phone_input" style="width: auto!important;" type="text" name="tel" value="<?=$_REQUEST["tel"]?>" autocomplete="off" placeholder="+38 (___) ___-__-__"/>
							
							<p><?=GetMessage("WORK_TEL_COMMENT")?></p>
							<div class="clr"></div>
						</div>
					
					</div>
					
	
						<div class="clr"></div>
					<input  type="button" id="submite" value="<?=GetMessage("WORK_SEND")?>" name="send" />
					<input type="hidden" name="send" value="<?=GetMessage("WORK_SEND")?>">
					</form>
					
						<script>
							$( document ).ready(function() {
								<?if($_REQUEST["input_predmet"]):?>
									$("input[name='input_predmet'").val('<?=$_REQUEST["input_predmet"];?>');
								<?endif;?>
								
								<?if($_REQUEST["input_tip"]):?>
									$("input[name='input_tip'").val('<?=$_REQUEST["input_tip"];?>');
								<?endif;?>
								
								<?if($_REQUEST["predmet"]):?>
									$("#categories_id").val('<?=$_REQUEST["predmet"];?>');
								<?endif;?>
								
								<?if($_REQUEST["tip"]):?>
									$("#cpecialities_id").val('<?=$_REQUEST["tip"];?>');
								<?endif;?>
								
						   
								   $("#datepicker").bind("change ", function() {
								   		$("#termin2").val($(this).val()); 
									});
									
								
									
									$('#terminB').click(function(){
										$(".ui-datepicker-trigger").click(); 
									});
									
							
							});
							
						</script>	
					
					<?endif;?>
				</div>
			</div>
			<div class="clr"></div>
