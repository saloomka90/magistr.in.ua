<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));
$arMenu = GetMenuTypes($site);

$arComponentParameters = array(
	"GROUPS" => array(
		"CACHE_SETTINGS" => array(
			"NAME" => GetMessage("COMP_GROUP_CACHE_SETTINGS"),
			"SORT" => 600
		),
	),
	"PARAMETERS" => array(
		
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
		
		"IBLOCK_ORDER_ID" => array(
			"PARENT" => "",
			"NAME" => "IBLOCK_ORDER_ID",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		
		"IBLOCK_WORK_TYPES_ID" => array(
			"PARENT" => "",
			"NAME" => "IBLOCK_WORK_TYPES_ID",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		
		"IBLOCK_PREDMETS_ID" => array(
			"PARENT" => "",
			"NAME" => "IBLOCK_PREDMETS_ID",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		
		
		
		
	)
			
);
?>
