<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?
if($this->StartResultCache(false))
{
	
			
	if(!CModule::IncludeModule("iblock")) return;
	
	
	
	$res = CIBlockElement::GetList(
	    ['SORT' => 'ASC', 'NAME' => 'ASC'],
	    [
	        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
	        'ACTIVE' => 'Y',
	        'SECTION_ID' => $arParams['SECTION_ID']
	    ],
	    false,
	    [
	        'nElementID' => $arParams['NOT_SHOW_ID'],
	        'nPageSize' => 5
	    ]
	    );
	//var_dump($res);
	while ($obElement = $res->GetNextElement()) {
	    
	    $arElem = $obElement->GetFields();
	    if ($arElem['ID'] == $arParams['NOT_SHOW_ID']) {
	        continue;
	    }
	    $arResult["ITEMS"][$arElem["ID"]]= $arElem;
	}
	
	
	

$this->SetResultCacheKeys(array(
			"ITEMS",
));	

$this->IncludeComponentTemplate();

}
?>