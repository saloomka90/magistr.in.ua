<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	$(window).scroll(function () {
			if ($(this).scrollTop() > 1000) {
				$('.students em.t').animateNumber({ number: <?=$arResult["TOTAL_WORKS"];?> }, 2000, function(){});
				$('.students em.t2').animateNumber({ number: <?=$arResult["TOTAL_AUTORS"];?> }, 2000, function(){});
				$('.students em.t3').animateNumber({ number: 14 }, 2000, function(){});
				$('.students em.t4').animateNumber({ number: 90 }, 2000, function(){});
				$('.students em.t').removeClass('t');
				$('.students em.t2').removeClass('t2');
				$('.students em.t3').removeClass('t3');
				$('.students em.t4').removeClass('t4');				
			}
		});		
</script>

		<div class="students-bg">
			<div class="students">
				<div class="title"><?=GetMessage("WHY_ME")?></div>
				<table>
					<tr>
						<td>
							<div class="s-image"><img src="<?=SITE_TEMPLATE_PATH?>/images/s1.png" /></div>
							<p><em class="t">0</em> <?=GetMessage("WORK_BY_ATHORS")?></p>
						</td>
						<td>
							<div class="s-image"><img src="<?=SITE_TEMPLATE_PATH?>/images/s2.png" /></div>
							<p><em class="t2">0</em> <?=GetMessage("ATHORS_READY_FOR_WORK")?></p>
						</td>
						<td>
							<div class="s-image"><img src="<?=SITE_TEMPLATE_PATH?>/images/s3.png" /></div>
							<p><em class="t3">0</em> <?=GetMessage("STUDENTS_AGE")?></p>
						</td>
						<td>
							<div class="s-image"><img src="<?=SITE_TEMPLATE_PATH?>/images/s1.png" /></div>
							<p><em class="t4">0</em><em>%</em> <?=GetMessage("STUDENTS_RECOMMENDS")?></p>
						</td>
					</tr>
				</table>
			</div>
		</div>
	