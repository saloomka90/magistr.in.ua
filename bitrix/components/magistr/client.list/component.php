<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;
?>

<?

if(!empty($_REQUEST["user_id"])){
    $arResult['TEMPLATE'] = 'user';
    $rsUser = CUser::GetByID($_REQUEST["user_id"]);
    $arResult['USER'] = $rsUser->Fetch();
    
    $arKurs = [];
    $arSelect = Array("NAME","SORT");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arKurs[$arFields["SORT"]] = $arFields["NAME"];
        
    }
    
    $arVNZ = [];
    $arSelect = Array("ID","NAME","PROPERTY_TITLE_RU","PROPERTY_TITLE_SMAIL","PROPERTY_KOEF");
    $arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arVNZ[$arFields["NAME"]] = $arFields;
        
    }
    
    $arResult["KURS"] = $arKurs;
    $arResult["VNZ"] = $arVNZ;
    
    
    if($_REQUEST["edits"]=='Y' && check_bitrix_sessid() && $_REQUEST['action'] == 'user_cng'){
        if($_REQUEST["type"] == '��������'){
            if(CUser::Delete($_REQUEST["user_id"])){
                header("Location: /users/", true, 301);
            }
        }else{
            $user = new CUser;
            $fields = Array(
                "NAME"              => $_REQUEST["NAME"],
                "PERSONAL_PHONE"              => $_REQUEST["PERSONAL_PHONE"],
                "EMAIL"              => $_REQUEST["EMAIL"],
                "UF_VNZ"              => !empty($_REQUEST["UF_VNZ2"])?$_REQUEST["UF_VNZ2"]:$_REQUEST["UF_VNZ"],
                "UF_KURS"              => $_REQUEST["UF_KURS"],
                "ACTIVE"              => $_REQUEST["ACTIVE"],
                "UF_OTHER_DATA"              => $_REQUEST["UF_OTHER_DATA"],
                
            );
            
            if(!empty($_REQUEST["PASS"])){
                $fields["PASSWORD"] = $_REQUEST["PASS"];
                $fields["CONFIRM_PASSWORD"] = $_REQUEST["PASS"];
            }
            if($user->Update($_REQUEST["user_id"], $fields)){
                $arResult['MSG'] = '���i���';
                header("Location: /users/?user_id=".$_REQUEST["user_id"], true, 301);
            }else{
                $arResult['MSG'] = '������� �������';
            }
        }
        
    }
    $this->IncludeComponentTemplate();
}else{
    if(isset($_REQUEST['del_filter'])){
        header("Location: /users/", true, 301);
    }
    
    
    $arKurs = [];
    $arSelect = Array("NAME","SORT");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arKurs[$arFields["SORT"]] = $arFields["NAME"];
        
    }
    
    $arResult["KURS"] = $arKurs;
    
    
    
    
    
    $arResult['TEMPLATE'] = 'list';
    $arResult['USERS'] = [];
    global $USER;
    $filter = Array("GROUPS_ID" => Array(12));
    $reqFilter =  array_diff($_REQUEST["arrUsersFilter"], array(''));
    
    
    if(!empty($reqFilter) && $_REQUEST["set_filter"] == 'Y'){
        $filter = array_merge($filter, $reqFilter);
    }
    
    if(!empty($_REQUEST["sort"])){
        $by = explode('-',$_REQUEST["sort"])[0];
        $order = explode('-',$_REQUEST["sort"])[1];
       
    }else{
        $by = "ID";
        $order = "desc";
    }
    
    /*if($_REQUEST["sort"] == 'UF_FDATE-ASC'){
        $by = "UF_FDATE";
        $order = "asc";
    }
    echo '<pre>';var_dump($by,$_REQUEST["sort"]);*/
   
    $rsUsers = CUser::GetList(($by), ($order), $filter, ['SELECT'=>['UF_*']]);
    $rsUsers->NavStart(20);
    while ($arUser = $rsUsers->Fetch()) {
        
        $arUser["SUM_ALL"] = $arUser["UF_PRICE"];
        $arUser["ORDER_COUNT"] = $arUser["UF_COUNT"];
        $arUser["ORDER_ACTIVE_COUNT"] = $arUser["UF_ACOUNT"];
        $arUser["FIRST_ORDER"] = $arUser["UF_FDATE"];
        
        $arResult['USERS'][] = $arUser;
    }
    
    $arResult["NAV_STRING"] = $rsUsers->GetPageNavStringEx($navComponentObject, '�볺���', '', 'N');
    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    $arResult["NAV_RESULT"] = $rsUsers;
    
    $this->IncludeComponentTemplate();
}






?>