<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<form action="/client/profile/" method="POST" class="form-profile">
	<?=bitrix_sessid_post()?>
	
	<input type="hidden" name="action" value="form-profile">
    <div class="contact-information">
        <div class="title-18">��������� ����������</div>
        <div class="form-item">
            <div class="form-item__label">��'�: <span>*</span></div>
            <input type="text" name="name" value="<?=$arResult['USER']["NAME"] ?>">
        </div>
        <div class="form-item">
            <div class="form-item__label">�������: <span>*</span></div>
            <input type="tel" name="tel" placeholder="+38 (___) ___-__-__" value="<?=$arResult['USER']["PERSONAL_PHONE"] ?>">
        </div> 
        <div class="form-item form-item_phone-disabled">
            <div class="form-item__label">E-mail: <span>*</span></div>
            <input type="email" name="email" value="<?=$arResult['USER']["EMAIL"] ?>" disabled>
            <div class="info__baloon info__baloon_phone">
                <div class="info__text">��� ������ e-mail, <br>�������� ���������</div>
                <div class="info__close">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.364 2.63603C13.6641 0.936172 11.4039 0 9 0C6.59605 0 4.33589 0.936172 2.63603 2.63603C0.936172 4.33589 0 6.59605 0 9C0 11.4041 0.936172 13.6641 2.63603 15.364C4.33589 17.0638 6.59605 18 9 18C11.4039 18 13.6641 17.0638 15.364 15.364C17.0638 13.6641 18 11.4041 18 9C18 6.59605 17.0638 4.33589 15.364 2.63603ZM12.8927 11.6496C13.2359 11.993 13.2359 12.5494 12.8927 12.8926C12.7211 13.0643 12.4961 13.1501 12.2712 13.1501C12.0462 13.1501 11.8213 13.0643 11.6496 12.8926L9 10.243L6.35037 12.8927C6.17871 13.0643 5.95377 13.1501 5.72882 13.1501C5.50388 13.1501 5.27893 13.0643 5.10727 12.8927C4.76408 12.5494 4.76408 11.993 5.10727 11.6498L7.75703 9L5.10727 6.35037C4.76408 6.00705 4.76408 5.45059 5.10727 5.10741C5.45059 4.76408 6.00705 4.76408 6.35023 5.10741L9 7.75703L11.6496 5.10741C11.993 4.76422 12.5494 4.76408 12.8926 5.10741C13.2359 5.45059 13.2359 6.00705 12.8926 6.35037L10.243 9L12.8927 11.6496Z" fill="currentColor"/></svg>
                </div>
            </div>
        </div>
    </div>    
    <div class="contact-information">
        <div class="title-18">���������� ��� ���</div>
        <div class="form-item">
            <div class="form-item__label">��� ���: <span>*</span></div>
            <select data-placeholder="������� ����� (���������)..." id="select-institution" name="institution">
                <option value=""></option>
                <? foreach($arResult["VNZ"] as $vnz): ?>
                    	<option data-ru='<?=$vnz["PROPERTY_TITLE_RU_VALUE"] ?>' data-ua='<?=$vnz["NAME"] ?>' value='<?=$vnz["PROPERTY_TITLE_SMAIL_VALUE"] ?>' <?if($arResult['USER']["UF_VNZ"] == $vnz["NAME"])echo 'selected'; ?>><?=$vnz["NAME"] ?></option>
                    <?endforeach; ?>
            </select>
        </div>
        <div class="form-item">
            <div class="form-item__label">����: <span>*</span></div>
            <select name="course" id="select-course">
                <option value="�������">�������</option>
                <?
                $yer = getYerVik();
                    
                   
                    ?>
                    <?foreach($arResult["KURS"] as $name=>$id): ?>
                    	<option value="<?=$id ?>" <?if($arResult['USER']["UF_KURS"] == $id){echo 'selected';$cursval = $name.' '.$yer;} ?>><?=$name ?> <?=$yer ?></option>
                    <?endforeach; ?>
            </select>
            <input type="text" class="course-i" name="coursei">
        </div>
    </div>
    <a href="/client/password/" class="link-style link-style__password"><span>������ ������</span></a>
    <div class="form-item-bottom form-item-bottom_320">
        <div class="form-item-bottom__save">
            <button type="submit" class="button">��������</button>
        </div>
        <div class="form-item-bottom__back">
            <a href="/client/" class="button button_gray">�����</a>
        </div>
    </div>
</form> 

<script>
							
							var input_institution = '<?=$arResult['USER']["UF_VNZ"] ?>';

							var coursei = '<?=$cursval ?>';
							
							
						</script>

<?//=$arResult["NAV_STRING"]?>