<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;
?>

<?

if(!empty($_REQUEST['action']) == 'form-profile' && check_bitrix_sessid()){
    
    
    
    $fields = Array(
        "NAME" => $_REQUEST["name"],
        "PERSONAL_PHONE" => STR_REPLACE(['-',')','(',' '],'',$_REQUEST["tel"]),
        "UF_VNZ"  => $_REQUEST["input_institution"],
        "UF_KURS" => $_REQUEST["course"],
    );
    if($_REQUEST["ajax"] == 'Y'){
        $fields["NAME"] = mb_convert_encoding($fields["NAME"], "windows-1251", "utf-8");
        $fields["UF_VNZ"] = mb_convert_encoding($fields["UF_VNZ"], "windows-1251", "utf-8");
    }
    
    
     
    if($USER->Update($USER->getID(), $fields)){
        if($_REQUEST["ajax"] == 'Y'){
            $GLOBALS['APPLICATION']->RestartBuffer();
            echo json_encode(["error"=>false]);die();
        }
        header("Location: /client/", true, 301);
    }
    
    if($_REQUEST["ajax"] == 'Y'){
        $GLOBALS['APPLICATION']->RestartBuffer();
        echo json_encode(["error"=>true]);die();
    }
    $this->IncludeComponentTemplate();
}else{
    $arKurs = [];
    $arSelect = Array("NAME","SORT");
    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arKurs[$arFields["NAME"]] = $arFields["SORT"];
        
    }
    
    $arVNZ = [];
    $arSelect = Array("ID","NAME","PROPERTY_TITLE_RU","PROPERTY_TITLE_SMAIL","PROPERTY_KOEF");
    $arFilter = Array("IBLOCK_ID"=>17, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->Fetch())
    {
        $arVNZ[$arFields["NAME"]] = $arFields;
        
    }
    
    $arResult["KURS"] = $arKurs;
    $arResult["VNZ"] = $arVNZ;
    
    
    
    global $USER;
    $rsUsers = CUser::GetList(($by = "ID"), ($order = "desc"), ['ID'=>$USER->GetID()], ['SELECT'=>['UF_*']]);
    if ($arUser = $rsUsers->Fetch()) {
        $arResult['USER'] = $arUser;
    }
    $this->IncludeComponentTemplate();
}






?>