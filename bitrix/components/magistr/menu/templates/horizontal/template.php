<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
					<div class="menu">
						<ul>
							
							<?foreach($arResult as $arItem):?>
							
							<?if ($arItem["PARAMS"]["ISHOME"] == "Y"):?>
								<li class="home"><a href="<?=$arItem["LINK"];?>"></a></li>
							
							<?else:?>
								<?if ($arItem["PERMISSION"] > "D"):?>
									<li <?=($arItem["SELECTED"])? 'class="active"' : ''; ?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
								<?endif?>
								
							<?endif?>	
							<?endforeach?>

						</ul>
					</div>

<?endif?>