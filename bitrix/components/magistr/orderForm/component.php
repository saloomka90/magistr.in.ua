<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;
?>

<?
$arResult['SW_BTN'] = true;
if($USER->IsAuthorized()){
    header("Location: /client/order/", true, 301);
}
if(!CModule::IncludeModule("iblock")) return;
	$arSelect = array("ID", "NAME");
	if(LANGUAGE_ID=="ru"){
		$arSelect[] = "PROPERTY_TITLE_RU";
	}
	$arSelect[] = "PROPERTY_TITLE_RU";
	$arFilter= array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_WORK_TYPES_ID"]);
		
	$arSort = array("SORT" => "ASC");	
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>900), $arSelect);		

	while($obElement = $rsElement->GetNextElement())
	{
		$arItem = $obElement->GetFields();
		$arResult["WORK_TYPES"][$arItem["ID"]] = (LANGUAGE_ID=="ru" ? $arItem["PROPERTY_TITLE_RU_VALUE"]: $arItem["NAME"]); 
	}
	
	if($_REQUEST['action']=='preorder' && !empty($_REQUEST['email']) && check_bitrix_sessid()){
	    
	    $userAuth = false;
	    $userAuthBlock = false;
	    //��������� �����
	    
	    
	    if($USER->IsAuthorized()){
	        header("Location: /client/order/", true, 301);
	        $ID = $USER->GetID();
	    }else{
    	    $rsUser = CUser::GetByLogin($_REQUEST['email']);
    	    if($arUser = $rsUser->Fetch()){    	        
    	        $ID = $arUser["ID"];
    	        $userAuth = true;    	        
    	    }else{
    	        //�������-����������    	        
    	        $pass = randString(6);    	           
    	        $user = new CUser;
    	        $arFields = Array(
    	            "NAME"              => $_REQUEST["name"],
    	            "EMAIL"             => trim($_REQUEST["email"]),
    	            "LOGIN"             => trim($_REQUEST["email"]),
    	            "LID"               => "ru",
    	            "ACTIVE"            => "Y",
    	            "GROUP_ID"          => array(2,12),
    	            "PASSWORD"          => $pass,
    	            "CONFIRM_PASSWORD"  => $pass,
    	            "UF_STATUS"         => 1,
    	            "UF_MD5"            => MD5(randString(12)),
    	            
    	        );    	        
    	        $ID = $user->Add($arFields);
    	        if (intval($ID) > 0){
    	            //������� ������ ������    	            
    	            $updater = ["UF_PRICE"=>0, "UF_COUNT"=>0, "UF_ACOUNT"=>0, "UF_FDATE"=>""];
    	            $arSelect = Array("ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_state", "PROEPRTY_sum_paid");
    	            $arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ORDER_ID"], 'PROPERTY_email'=>trim($_REQUEST["email"]));
    	            $res = CIBlockElement::GetList(Array('ID'=>'DESC'), $arFilter, false, false, $arSelect);
    	            while($arFields = $res->Fetch())
    	            {
    	                CIBlockElement::SetPropertyValues($arFields["ID"], $arFields["IBLOCK_ID"], $ID, 'USER_ID');
    	                $updater["UF_COUNT"]++;
    	                if($arFields['PROPERTY_state_VALUE'] == '���������� ���������'){
    	                    $updater["UF_ACOUNT"]++;
    	                }
    	                
    	                $updater["UF_FDATE"] = $arFields["DATE_CREATE"];
    	                
    	                
    	                $pres = CIBlockElement::GetList(Array('ID'=>'ASC'), ["IBLOCK_ID"=>12, "PROPERTY_ORDER_ID"=>(int)$arFields["ID"], 'ACTIVE'=>"Y"], false, false, ["IBLOCK_ID","ID","PROPERTY_ADD_PRICE"]);
    	                while($arFields = $pres->Fetch()){
    	                    $updater["UF_PRICE"]+=(float)$arFields["PROPERTY_ADD_PRICE_VALUE"];
    	                }
    	                
    	                
    	            }
    	            //������� ��� � ����
    	            $user->Update($ID,$updater);    	            
    	            //����������
    	            $USER->Authorize($ID,true);
    	            //��������� ����
    	            CEvent::Send("ORDER_USER_REGISTER", SITE_ID, ["NAME"=>$_REQUEST["name"], "MAIL_TO"=>trim($_REQUEST["email"]), "PASS"=>$pass, "NAME"=> $_REQUEST["name"],]);    	            
    	        }else{
    	        
            	        
            	        $fp = fopen($_SERVER["DOCUMENT_ROOT"]."/logger.txt", "a+");
            	        $mytext = '['.date('H:i:s d.m.Y').']'.$user->LAST_ERROR.PHP_EOL;
            	        $test = fwrite($fp, $mytext);
            	        fclose($fp);
    	           }
    	    }
	    }
	    
	    
	    if(!in_array(12, CUser::GetUserGroup($ID))){
	        if (intval($ID) > 0){
	           $arResult['MSG_INFO'] = '<p style="text-align:center;color:red">������ ���� ��� �볺���</p>';
	           
	           $arResult['SW_BTN'] = false;
	        }else{
	            $arResult['MSG_INFO'] = '<p style="text-align:center;color:red">'.$user->LAST_ERROR.PHP_EOL.'</p>';
	            $arResult['SW_BTN'] = true;
	        }
	    }else{
	        
	        
	        
	        $_SESSION["USER_NEW_ORDER"] = [
	            'email' => trim($_REQUEST["email"]), 
	            'input_tip' => $_REQUEST["input_tip"],
	            'tip' => $_REQUEST["tip"],
	            'tema' => $_REQUEST["tema"],
	            'name' => $_REQUEST["name"],
	        ];
	        
	        if($userAuth){
	            $_SESSION["USER_NEW_ORDER"]["SHOW"] = 'Y';
	        }
	        
	        
	        
	        
	        
	        
	        /*$PROP = array();
	        $PROP["18"]= $_REQUEST["email"]; //email
	        $PROP["23"]= $_REQUEST["tema"]; //tema
	        if($_REQUEST["tip"]){
	            $PROP["61"]= $_REQUEST["tip"];
	            $PROP["24"]= $arResult["WORK_TYPES"][$_REQUEST["tip"]]; // Old
	        }else{
	            $PROP["63"]= $_REQUEST["input_tip"];
	            $PROP["61"] = "50943";
	            $PROP["24"]= $_REQUEST["input_tip"]; // Old
	            
	        }
	        $PROP["USER_ID"] = $ID;
	        
	        $arLoadProductArray = Array(
	            'IBLOCK_ID' => $arParams["IBLOCK_ORDER_ID"],
	            'PROPERTY_VALUES' => $PROP,
	            'NAME' => htmlspecialcharsEx($_REQUEST["name"]),
	            'ACTIVE' => 'N', // �������
	            'DATE_ACTIVE_FROM' => date("d.m.Y H:i:s"),
	        );
	        
	        $el = new CIBlockElement;
	        
	        if(!$orderId=$el->Add($arLoadProductArray)){
	            
	            
	            echo json_encode(['error'=>true, 'messeg'=>'']);die();
	        }
	        */
	        
	        
	        //$_COOKIE["ORDER_ID"] = $orderId;
    	    if($userAuth){
    	        $arResult['MSG_INFO'] = '<p style="text-align:center">���������� �� ����� ������ ��� ����.</p> <p style="text-align:center"><a style="text-decoration: underline;" href="/client/order/?logins='.trim($_REQUEST["email"]).'">�������������� �� '.$_REQUEST["email"].'</a></p>';
    	        $arResult['SW_BTN'] = false;
	        }else{
    	        //$_POST['orderid'] = $orderId;
    	        header("Location: /client/order/", true, 307);
    	    }
	    }
	    
	    
	    
	    
	}



/*
 * AJAX
 */



$this->IncludeComponentTemplate();
?>