<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="faq">
    <div class="faq__title">���� �������</div>
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="faq__item">
        <div class="faq__question ">
            <div class="faq__icon"></div>
            <?=$arItem["NAME"] ?>
        </div>
        <div class="faq__answer"><?=htmlspecialchars_decode(str_replace(PHP_EOL,"<br>",$arItem["PREVIEW_TEXT"])) ?></div>
    </div>
    <?endforeach; ?>
</div>
<?//=$arResult["NAV_STRING"]?>