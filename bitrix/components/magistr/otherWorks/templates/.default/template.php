<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult["ITEMS"]):
?>


				<div class="work_rec">
					<div class="work_rec_t"><?=GetMessage("OTHER_WORKS");?></div>
					<ul>
						<?foreach($arResult["ITEMS"] as $arItem):?>
							<li><a href="<?=$arParams["FOLDER"]?><?=$arItem["ID"]?>/"><?=$arItem["NAME"]?></a></li>
						<?endforeach;?>
					</ul>
				</div>

<?endif;?>	