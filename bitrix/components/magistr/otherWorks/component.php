<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?
if($this->StartResultCache(false))
{
	
			if($arParams["NOT_SHOW_ID"])
				$arNotshow[] = $arParams["NOT_SHOW_ID"];	
	
	if(!CModule::IncludeModule("iblock")) return;
	$arSelect = array("ID", "NAME", "SECTION_ID");
	$arFilter= array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"<ID" => $arParams["NOT_SHOW_ID"],
		"SECTION_ID" => $arParams["SECTION_ID"],
		"ACTIVE" => "Y",
		);
		
	$arSort = array("ID" => "DESC" );	
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>1), $arSelect);		

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			
			$arResult["PREV"] = $arItem; 
		}
	
	$arFilter = array();
	$arSort = array();
	$arItem = array();
	$rsElement = false;
	$obElement = false;
	
	if($arResult["PREV"]){
		

	
	if($arResult["PREV"])
		$arNotshow[] = $arResult["FIRST"];
		
		$arSort = array("ACTIVE_DATE" => "DESC" );
		$arFilter= array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"!ID" => array($arParams["NOT_SHOW_ID"],$arResult["PREV"]["ID"]) ,
			"SECTION_ID" => $arParams["SECTION_ID"],
			"ACTIVE" => "Y",
		);
		
		$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>1), $arSelect);		

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			
			$arResult["FIRST"] = $arItem; 
		}
		
	}
	
	
	
	$arFilter = array();
	$arSort = array();
	$arItem = array();
	$rsElement = false;
	$obElement = false;
	
	
	
	if($arResult["FIRST"])
		$arNotshow[] = $arResult["FIRST"];
	
	
	
		$arSort = array("RAND" => "ASC" );
		$arFilter= array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"!ID" => $arNotshow,
			"SECTION_ID" => $arParams["SECTION_ID"],
			"ACTIVE" => "Y",
		);
		
		$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, array("nTopCount"=>3), $arSelect);		

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();
			
			$arResult["RAND"][] = $arItem; 
		}
$getCount = 3;
	
if($arResult["FIRST"]){
	$arResult["ITEMS"][$arResult["FIRST"]["ID"]]= $arResult["FIRST"];
	$getCount = $getCount-1;
}

if($arResult["PREV"]){
	$arResult["ITEMS"][$arResult["PREV"]["ID"]]= $arResult["PREV"];
	$getCount = $getCount-1;
}


if($arResult["RAND"]){
	$i=1;
	foreach ($arResult["RAND"] as $arRand) {
		if($i<=$getCount){
			$arResult["ITEMS"][$arRand["ID"]]= $arRand;
		}
		$i++;	
	}
	
}


$this->SetResultCacheKeys(array(
			"ITEMS",
));	

$this->IncludeComponentTemplate();

}
?>