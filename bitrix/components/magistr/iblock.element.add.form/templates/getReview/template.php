<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?

//echo "<pre>Template arParams: "; print_r($arResult["PROPERTY_LIST"]); echo "</pre>";
	$arResult["PROPERTY_LIST"] = false;
	$arResult["PROPERTY_LIST"] = array("NAME", "56", "PREVIEW_TEXT", "57");
$pref = $arParams["PREFIX"];
if(
	count($arResult["ERRORS"])<1 && 
	strlen($arResult["MESSAGE"]) > 0
	
	){
		
	$ClearForm = true;
	
	}

?>
<style>
	
		.rating {
	float: left;
	width: 180px;
}

.rating a {
	float: left;
	width: 23px;
	height: 22px;
	background: url(/bitrix/templates/magistr_html5/images/st2.png) no-repeat;
	cursor: pointer;
	margin-right: 2px;
}

.rating a.active {
	background: url(/bitrix/templates/magistr_html5/images/st1.png) no-repeat;
}
	
</style>

<?if (count($arResult["ERRORS"])):?>
	<div class="alert a_big"><?=ShowError(implode("<br />", $arResult["ERRORS"]))?></div>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<div class="ok_message">
		<p><font class="notetext"><?
		echo GetMessage("OK_MESSAGE");
		$arResult["MESSAGE"]= false;
		?></font>
		</p></div>
<?else:?>
		<form  id="add_form_<?=$pref?>" name="iblock_add_<?=$pref?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		
			<?=bitrix_sessid_post()?>
			<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
				
				<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):
					foreach ($arResult["PROPERTY_LIST"] as $propertyID):
						//echo $propertyID."-".intval($propertyID)."<br/>";
								if (intval($propertyID) > 0):
									$title = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]);
									
									//$descr = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]."_DESCR");
									if(strlen($title)<=0):
										$title = $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
									endif;
								else:
									
								/*if($USER->isAdmin()){
								    echo "<pre>";var_dump($propertyID,GetMessage("IBLOCK_FIELD_".$propertyID));
								}*/
								if($propertyID == 'NAME'){
								    $title =  GetMessage("CUSTOM_TITLE_".$propertyID);
								}else{
									$title =  GetMessage("IBLOCK_FIELD_".$propertyID);
								}
									//$descr = GetMessage("CUSTOM_TITLE_".$propertyID."_DESCR");
								endif;
								
								if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) $title.= '<span>*</span>';
								$descr = false;
								?>
								
								<?if($propertyID == "57"):
								?>
		
										<input id="n_<?=$pref?>_<?=$propertyID?>" type="hidden" name="PROPERTY[<?=$propertyID?>][0]" value="<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][0]["~VALUE"];?>">
										
								<?else:?>	
								
								
								<div class="rev-val">
									<label for="n_<?=$pref?>_<?=$propertyID?>"><?=$title;?></label>
								<?
								//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
								if (intval($propertyID) > 0)
								{
									if (
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
										&&
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
									)
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
									elseif (
										(
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
											||
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
										)
										&&
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
									)
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
								}
								elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";
		
								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
								{
									$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
									$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
								}
								else
								{
									$inputNum = 1;
								}
		
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
									$INPUT_TYPE = "USER_TYPE";
								else
									$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
		
		
								switch ($INPUT_TYPE):
									case "USER_TYPE":
										for ($i = 0; $i<$inputNum; $i++)
										{
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
												$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												$description = "";
											}
											else
											{
												$value = "";
												$description = "";
											}
											echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
												array(
													$arResult["PROPERTY_LIST_FULL"][$propertyID],
													array(
														"VALUE" => $value,
														"DESCRIPTION" => $description,
													),
													array(
														"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
														"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
														"FORM_NAME"=>"iblock_add",
													),
												));
										?><br/><?
										}
									break;
									case "TAGS":
										$APPLICATION->IncludeComponent(
											"bitrix:search.tags.input",
											"",
											array(
												"VALUE" => $arResult["ELEMENT"][$propertyID],
												"NAME" => "PROPERTY[".$propertyID."][0]",
												"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
											), null, array("HIDE_ICONS"=>"Y")
										);
										break;
									case "HTML":
										$LHE = new CLightHTMLEditor;
										$LHE->Show(array(
											'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
											'width' => '100%',
											'height' => '200px',
											'inputName' => "PROPERTY[".$propertyID."][0]",
											'content' => $arResult["ELEMENT"][$propertyID],
											'bUseFileDialogs' => false,
											'bFloatingToolbar' => true,
											'bArisingToolbar' => true,
											'toolbarConfig' => array(
												'Bold', 'Italic', 'Underline', 'RemoveFormat',
												'CreateLink', 'DeleteLink', 'Image', 'Video',
												'BackColor', 'ForeColor',
												'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
												'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
												'StyleList', 'HeaderList',
												'FontList', 'FontSizeList',
											),
										));
										break;
									case "T":
										for ($i = 0; $i<$inputNum; $i++)
										{
		
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
											}
											else
											{
												$value = "";
											}
										?>
								<textarea class="big"   id="n_<?=$pref?>_<?=$propertyID?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value;?></textarea><br />
										<?
										}
									break;
		
									case "S":
									case "N":
									case "E":	
										for ($i = 0; $i<$inputNum; $i++)
										{
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
		
											}
											else
											{
												$value = "";
											}
										?>
										<input type="text"  id="n_<?=$pref?>_<?=$propertyID?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" /><?
										/*if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
											$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'FORM_NAME' => 'iblock_add',
													'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
													'INPUT_VALUE' => $value,
												),
												null,
												array('HIDE_ICONS' => 'Y')
											);
											?><br /><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
										endif*/
										?><?
										}
									break;
		
									case "F":
										for ($i = 0; $i<$inputNum; $i++)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
											?>
								<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
								<input class="file" type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
											<?
		
											if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
											{
												?>
							<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" />
												<?
		
												if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
												{
													?>
							<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
													<?
												}
												else
												{
													?>
							<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
							<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
							[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
													<?
												}
											}
										}
										if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"]=="Y")
										{
											?><div><a href="javascript:;" onclick="addFileField(this,'property_fields_<?=$pref?>_<?=$propertyID?>','<?=$propertyID?>');"><?=GetMessage("IBLOCK_FORM_FILE_ADD")?></a></div><?
										}
		
									break;
									case "L":
		
										if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
											$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
										else
											$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";
		
										switch ($type):
											case "checkbox":
											case "radio":
		
												//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";
		
												foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
												{
													$checked = false;
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													{
														if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
														{
															foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
															{
																if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
															}
														}
													}
													else
													{
														if ($arEnum["DEF"] == "Y") $checked = true;
													}
		
													?>
									<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></label><br />
													<?
												}
											break;
		
											case "dropdown":
											case "multiselect":
											?>
									<select class="wide"  name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
										<option value="">(<?=$title;?>)</option>
											<?
												if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
												else $sKey = "ELEMENT";
		
												foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
												{
													$checked = false;
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
													{
														foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
														{
															if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
														}
													}
													else
													{
														if ($arEnum["DEF"] == "Y") $checked = true;
													}
													?>
										<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></option>
													<?
												}
											?>
									</select><br />
											<?
											break;
		
										endswitch;
									break;
								endswitch;?>
								</div>
								<?endif;?>	
					<?endforeach;?>
					
					<div class="rev-val">
							<p><label ><?=GetMessage("CUSTOM_BALL");?>:<span>*</span></label></p>
							<div class="rating">
								<a class="giveRating" id="1"></a>
								<a class="giveRating" id="2"></a>
								<a class="giveRating" id="3"></a>
								<a class="giveRating" id="4"></a>
								<a class="giveRating" id="5"></a>
							</div>
							<div id="errRating"></div>
							<div class="clr"></div>
						</div>
					
					<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):
					$arResult["capCode"] = $arResult["CAPTCHA_CODE"];
					?>
					
					<?php /*?><div class="rev-val">
							<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="140" height="30" alt="CAPTCHA">
							<input type="text" name="captcha_word" size="30" maxlength="50" value="" > 
							
							<input type="submit" name="<?=$pref?>iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"  />
								
							</div><?php */?>
						<?php //if($USER->isAdmin()):?>	
						<div class="rev-val">
							<div class="g-recaptcha"  data-sitekey="6LcxFosUAAAAAFQyJo0kt-KYJIFqQnenCj43aQ7t"></div>
							
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<input id="n_<?=$pref?>_captcha_word" type="hidden" name="captcha_word" value="12345"/>
							<div class="clr"></div>
							</div>
							<div class="rev-val" style="text-align:center;">
							<input type="submit" style="float:none" name="<?=$pref?>iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"  />
								<div class="clr"></div>
							</div>
						<?php /*else:?>
						<div class="rev-val">
								<label for="n_<?=$pref?>_captcha_word"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?>:<span>*</span></label>
								<div class="cod">
									<div class="cod-img">
										<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" />
									</div>
									<input type="hidden" name="<?=$pref?>captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
									<input id="n_<?=$pref?>_captcha_word" type="text" name="<?=$pref?>captcha_word" />
								</div>
								<input type="submit" name="<?=$pref?>iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"  />
								
								
								
								<div class="clr"></div>
							</div>
							<?php endif;*/?>
					<?endif?>
				<?endif?>
				<script>
					<?if($_REQUEST["bxajaxid"]):?>
					bxAjaxid = $("#add_form_<?=$pref;?> input[name='bxajaxid']").val();
					if(bxAjaxid){
						Recaptchafree.reset();
						$(".bx-core-waitwindow").remove();
						InitForm_<?=$pref;?>();
						<?if(!$ClearForm):?>
							$("#add_form_<?=$pref;?>").valid();
						<?endif;?>
					}
					<?endif;?>
								
				</script>
		</form>
		
		
		
		<script>
		
				function setRating(rating){
							if(rating > 0){
								rat = rating;
							}else{
								rat = 0;
							}	
								$('.giveRating').removeClass('active');
								$("#"+rat).addClass('active');
								$("#"+rat).prevAll('.giveRating').addClass('active');
							
				}
				
		
				
				initRating();
		
				$(document).ready(function(){
					
				});		
					
				function initRating(){
					ratVal = $('#n_author2Order_57').val();
					setRating(ratVal);
				
					
					var timeOut;
					var hasClicked=false;
		
						var ratingValue = $('#n_author2Order_57').val();
						
						$('.giveRating').click(function(){
							hasClicked = true;
							var clicked = $(this);
							var clickedVal = $(this).attr('id');
							$('#n_author2Order_57').val(clickedVal);
							clicked.prevAll('.giveRating').addClass('active');
							clicked.addClass('active');
							//$('#rating').val(clicked);
						})
		
						//Change css on mouseEnter to new CSS
						$('.giveRating').mouseenter(function(){
							//console.log("enter");
							clearTimeout(timeOut);
							$(this).prevAll('.giveRating').addClass('active');
							$(this).addClass('active');
							$(this).nextAll('.giveRating').removeClass('active');
						});
		
						//Change CSS on mouseLeave to old CSS
						$('.giveRating').mouseleave(function(){
							//console.log("live");
							//if(!hasClicked){
								var ratingValue = $('#n_author2Order_57').val();
								timeOut = setTimeout(function(){
									//$('.giveRating').addClass('active');
									
									setRating(ratingValue);
								},100);
							//}
							//hasClicked = false;
						});
				
			}
		
			</script>
		
		<?echo '
		<script>
		
		
		
		$( document ).ready(function() {
			InitForm_'.$pref.'();
		});
		
		
		function InitForm_'.$pref.'(){
		
			$(function(){
			
					$("#add_form_'.$pref.'").validate({
						
						 ignore: [],
						
						invalidHandler: function() {
							$(".bx-core-waitwindow").remove();
							$("#add_form_'.$pref.' .ok_message").remove();
							
						},
						
						 errorPlacement: function(error, element) {
						 	if(element.attr("id")=="n_'.$pref.'_57"){
						   		  error.appendTo("#errRating");
								 
							}
						}
					});
		
					$("#n_'.$pref.'_NAME").rules("add", {required:true, minlength: 3,
						messages: {
					    required: "",
					    minlength: "",
					    
					  }
					});
		
					$("#n_'.$pref.'_captcha_word").rules("add", {required: true, minlength: 5, maxlength: 5,
						messages: {
						    required: "",
						    minlength: "",
						    maxlength: "",
						  }
					});	
					$("#n_'.$pref.'_56").rules("add", {required: true,
							messages: {
						    required: "",
						    
						    
						  }
					});
					
					$("#n_'.$pref.'_57").rules("add", {required: true,minlength: 1,
							messages: {
						    required: "!",
						    minlength: "!",
						    }, 
		
					});
					
					
					
					$("#n_'.$pref.'_PREVIEW_TEXT").rules("add", {required: true, minlength: 2,
							messages: {
						    required: "",
						    minlength: "",
					 }
					
					});
		
		
					
		
			
			
			});
		
		 }
		
		
		</script>';?>
<?endif;// if (strlen($arResult["MESSAGE"]) > 0)?>