<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?><?
//global $MESS;
//include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

//echo dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__);
?><?
//echo "<pre>Template arParams: "; print_r($arResult["PROPERTY_REQUIRED"]); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult["PROPERTY_LIST"]); echo "</pre>";
//exit();
//$pref = time();
$pref = $arParams["PREFIX"];
//echo $arParams["WORK_ELEMENT_ID"];
if(
	count($arResult["ERRORS"])<1 && 
	strlen($arResult["MESSAGE"]) > 0
	
	){
		
	$ClearForm = true;
	
	}
if(count($arResult["ERRORS"])>0){
	$errors = true;
}

?>

<?if (count($arResult["ERRORS"])):?>
	<div class="alert a_big"><?=ShowError(implode("<br />", $arResult["ERRORS"]))?></div>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<div class="notetext">
		<?
		echo GetMessage("OK_ORDER_".$arParams["PREFIX"]);
		$arResult["MESSAGE"]= false;
		?>
	</div>

<?else:?>

<form <?//=( (count($arResult["ERRORS"])<1) && (strlen($arResult["MESSAGE"]) > 0) ? 'style="display:none;"':'')?> id="add_form_<?=$pref?>" name="iblock_add_<?=$pref?>" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" >

	<?=bitrix_sessid_post()?>
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

	<?if(in_array(16, $arResult["PROPERTY_LIST"])):?>
		<input type="hidden" name="PROPERTY[16]" value="10" />
		<?unset($arResult["PROPERTY_LIST"][array_search(16, $arResult["PROPERTY_LIST"])]);?>
	<?endif;?>
	<?if(in_array(14, $arResult["PROPERTY_LIST"])):?>
		<input type="hidden" name="PROPERTY[14][0]" value="<?=($arResult["ELEMENT_PROPERTIES"]["14"][0]["~VALUE"]? $arResult["ELEMENT_PROPERTIES"]["14"][0]["~VALUE"] : $arParams["WORK_ELEMENT_ID"]);?>" />
		<?unset($arResult["PROPERTY_LIST"][array_search(14, $arResult["PROPERTY_LIST"])]);?>
	<?endif;?>
	
	
		<?if (is_array($arResult["PROPERTY_LIST"]) && count($arResult["PROPERTY_LIST"] > 0)):
			foreach ($arResult["PROPERTY_LIST"] as $propertyID):
				
						if (intval($propertyID) > 0):
							$title = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]);
							
							//$descr = GetMessage("CUSTOM_TITLE_".$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]."_DESCR");
							if(strlen($title)<=0):
								$title = $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
							endif;
						else:
							//$title = !empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID);
							$title = GetMessage("IBLOCK_FIELD_".$propertyID);
							//$descr = GetMessage("CUSTOM_TITLE_".$propertyID."_DESCR");
						endif;
						
						if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])) $title.= '<span>*</span>';
						$descr = false;
						?>
						
						<?if($propertyID == "13"):
						?>

					<div id="phones" class="rev-val r2">
						<input type="hidden" name="PROPERTY[<?=$propertyID?>][0]" value="<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][0]["~VALUE"];?>">
						<label><?=GetMessage("CUSTOM_TITLE_phone")?>:<span>*</span></label>
						<input type="text" name="n_<?=$pref?>_tel1" id="n_<?=$pref?>_tel1" maxlength="3" value="<?=($_REQUEST["n_".$pref."_tel1"])? $_REQUEST["n_".$pref."_tel1"] : "+38";?>" />
						<span class="koma">(</span>
						<input type="text" placeholder="000" name="n_<?=$pref?>_tel2" id="n_<?=$pref?>_tel2" maxlength="3" value="<?=($_REQUEST["n_".$pref."_tel2"])? $_REQUEST["n_".$pref."_tel2"] : "";?>" />
						<span class="koma">)</span>
						<input type="text" placeholder="0000000" name="n_<?=$pref?>_tel3" id="n_<?=$pref?>_tel3" maxlength="7" class="i2" value="<?=($_REQUEST["n_".$pref."_tel3"])? $_REQUEST["n_".$pref."_tel3"] : "";?>" />
						<p><?=GetMessage("CUSTOM_TITLE_phone_EXAMPLE")?></p>
						<div class="clr"></div>
					</div>
						<?else:?>	
						
						
						<div class="rev-val">
							<label for="n_<?=$pref?>_<?=$propertyID?>"><?=$title;?></label>
						<?
						//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
						if (intval($propertyID) > 0)
						{
							if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
							elseif (
								(
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
									||
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
						}
						elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
						{
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						}
						else
						{
							$inputNum = 1;
						}

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
							$INPUT_TYPE = "USER_TYPE";
						else
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

						switch ($INPUT_TYPE):
							case "USER_TYPE":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
										$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										$description = "";
									}
									else
									{
										$value = "";
										$description = "";
									}
									echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
										array(
											$arResult["PROPERTY_LIST_FULL"][$propertyID],
											array(
												"VALUE" => $value,
												"DESCRIPTION" => $description,
											),
											array(
												"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
												"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
												"FORM_NAME"=>"iblock_add",
											),
										));
								?><br /><?
								}
							break;
							case "TAGS":
								$APPLICATION->IncludeComponent(
									"bitrix:search.tags.input",
									"",
									array(
										"VALUE" => $arResult["ELEMENT"][$propertyID],
										"NAME" => "PROPERTY[".$propertyID."][0]",
										"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
									), null, array("HIDE_ICONS"=>"Y")
								);
								break;
							case "HTML":
								$LHE = new CLightHTMLEditor;
								$LHE->Show(array(
									'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
									'width' => '100%',
									'height' => '200px',
									'inputName' => "PROPERTY[".$propertyID."][0]",
									'content' => $arResult["ELEMENT"][$propertyID],
									'bUseFileDialogs' => false,
									'bFloatingToolbar' => true,
									'bArisingToolbar' => true,
									'toolbarConfig' => array(
										'Bold', 'Italic', 'Underline', 'RemoveFormat',
										'CreateLink', 'DeleteLink', 'Image', 'Video',
										'BackColor', 'ForeColor',
										'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
										'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
										'StyleList', 'HeaderList',
										'FontList', 'FontSizeList',
									),
								));
								break;
							case "T":
								for ($i = 0; $i<$inputNum; $i++)
								{

									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									}
									else
									{
										$value = "";
									}
								?>
						<textarea class="big"   id="n_<?=$pref?>_<?=$propertyID?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=(strlen($value)>0?$value:$title)?></textarea><br />
								<?
								}
							break;

							case "S":
							case "N":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

									}
									else
									{
										$value = "";
									}
								?>
								<input type="text"  id="n_<?=$pref?>_<?=$propertyID?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" value="<?=$value?>" /><?

								?><?
								}
							break;

							case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?>
						<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
						<input class="file" type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
									<?

									if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
									{
										?>
					<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" />
										<?

										if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
										{
											?>
					<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
											<?
										}
										else
										{
											?>
					<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
					<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
					[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
											<?
										}
									}
								}
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"]=="Y")
								{
									?><div><a href="javascript:;" onclick="addFileField(this,'property_fields_<?=$pref?>_<?=$propertyID?>','<?=$propertyID?>');"><?=GetMessage("IBLOCK_FORM_FILE_ADD")?></a></div><?
								}

							break;
							case "L":

								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
								else
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

								switch ($type):
									case "checkbox":
									case "radio":

										//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
												{
													foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
													{
														if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}

											?>
							<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></label><br />
											<?
										}
									break;

									case "dropdown":
									case "multiselect":
									?>
							<select class="wide"  name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
								<option value="">(<?=$title;?>)</option>
									<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}
											?>
								<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=(strlen(GetMessage($arEnum["VALUE"]))>0&&LANG_ID!=LANG_ID_DEF?GetMessage($arEnum["VALUE"]):$arEnum["VALUE"])?></option>
											<?
										}
									?>
							</select><br />
									<?
									break;

								endswitch;
							break;
						endswitch;?>
						</div>
						<?endif;?>	
			<?endforeach;?>
			<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
			
			
			<?php //if($USER->isAdmin()):?>	
						<div class="rev-val">
							<div class="g-recaptcha" data-sitekey="6LcxFosUAAAAAFQyJo0kt-KYJIFqQnenCj43aQ7t"></div>
							
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<input id="n_<?=$pref?>_captcha_word" type="hidden" name="captcha_word" value="12345"/>
							<div class="clr"></div>
							</div>
							<div class="rev-val" style="text-align:center;">
							<input type="submit" style="float:none;" name="<?=$pref?>iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"  />
								<div class="clr"></div>
							</div>
							
				<?php /*else:?>			
				<div class="rev-val">
						<label for="n_<?=$pref?>_captcha_word"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?>:<span>*</span></label>
						<div class="cod">
							<div class="cod-img">
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" />
							</div>
							<input type="hidden" name="<?=$pref?>captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<input id="n_<?=$pref?>_captcha_word" type="text" name="<?=$pref?>captcha_word" />
						</div>
						<input type="submit" name="<?=$pref?>iblock_submit"  value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>"  />
						
						<div class="clr"></div>
					</div>
					
					<?php endif;*/?>
			<?endif?>
		<?endif?>
		<script>
			<?/**if($_REQUEST["bxajaxid"]):?>
			bxAjaxid = $("#add_form_<?=$pref;?> input[name='bxajaxid']").val();
			if(bxAjaxid){
				$(".bx-core-waitwindow").remove();
				InitForm_<?=$pref;?>();
				<?if($errors):?>
					$("#add_form_<?=$pref;?>").valid();
				<?endif;?>
			}
			<?endif;*/?>
			
				<?if($errors):?>
				    InitForm_<?=$pref;?>();
					$("#add_form_<?=$pref;?>").valid();
					$(".bx-core-waitwindow").remove();
					
				<?endif;?>
			

        
	
		</script>
</form>



<?echo '

<script>
Recaptchafree.reset();
$( document ).ready(function() {
	InitForm_'.$pref.'();
});


function InitForm_'.$pref.'(){
   

$(function(){
	
			$("#add_form_'.$pref.'").validate({
				//onsubmit: false,
				invalidHandler: function() {
					$(".bx-core-waitwindow").remove();
					$("#add_form_'.$pref.' .ok_message").remove();
                    BX.closeWait();
					
				}
			});

			$("#n_'.$pref.'_NAME").rules("add", {required:true, minlength: 3,
				messages: {
			    required: "",
			    minlength: "",
			    
			  }
			});
			$("#n_'.$pref.'_tel1").rules("add", {required: true, minlength: 2, maxlength: 3,
				messages: {
				    required: "",
				    minlength: "",
				    maxlength: "",
				  }
			});
			$("#n_'.$pref.'_tel2").rules("add", {required: true, minlength: 3, maxlength: 3,
				messages: {
				    required: "",
				    minlength: "",
				    maxlength: "",
				  }
			});
			$("#n_'.$pref.'_tel3").rules("add", {required: true, minlength: 7, maxlength: 7,
					messages: {
				    required: "",
				    minlength: "",
				    maxlength: "",
				  }
			});
			$("#n_'.$pref.'_captcha_word").rules("add", {required: true, minlength: 5, maxlength: 5,
				messages: {
				    required: "",
				    minlength: "",
				    maxlength: "",
				  }
			});	
			$("#n_'.$pref.'_12").rules("add", {required: true, email: true,
					messages: {
				    required: "",
				    email: "",
				    
				  }
			});

	
	
});

    
$("#add_form_'.$pref.' #phones input[type!=\'hidden\']").bind({
  		keyup: function() {
    		setChenges("#add_form_'.$pref.'");
		}
 }); 
 
 }

    function setChenges(id){
    	var newal="";
		$(id + " #phones input[type!=\'hidden\']").each(function() {
  			newal+=$( this ).val();
			//console.log(newal);
  		});
  		
  		$(id + " #phones input[name=\'PROPERTY[13][0]\']").val(newal);
		
    }  
</script>';?>
<?endif?>