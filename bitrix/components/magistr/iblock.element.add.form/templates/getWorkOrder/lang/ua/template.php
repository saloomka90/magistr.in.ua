<?
$MESS["OK_ORDER_author2Order"] = "���� ���������� ������ �������� ������ ������. <br/>���������� ����� �� ������� � ���� ���������";
$MESS["OK_ORDER_agentOrder"] = "���� ���������� ������ ������! <br/>��� �������� ��'������� � ���� ���������� �����";

$MESS ['IBLOCK_FORM_SUBMIT'] = "³��������";
$MESS ['IBLOCK_FORM_APPLY'] = "�����������";
$MESS ['IBLOCK_FORM_RESET'] = "��������";
$MESS ['IBLOCK_FORM_BACK'] = "����� �� ������";

$MESS ['IBLOCK_FORM_DATE_FORMAT'] = "������: ";

$MESS ['IBLOCK_FORM_FILE_NAME'] = "����";
$MESS ['IBLOCK_FORM_FILE_SIZE'] = "�����";
$MESS ['IBLOCK_FORM_FILE_DOWNLOAD'] = "�������";
$MESS ['IBLOCK_FORM_FILE_DELETE'] = "�������� ����";
$MESS ['IBLOCK_FORM_FILE_ADD'] = "��������� ����� ����";

$MESS ['IBLOCK_FORM_CAPTCHA_TITLE'] = "������ �� ������������� ����������";
$MESS ['IBLOCK_FORM_CAPTCHA_PROMPT'] = "������ ����� � ��������";

$MESS ['IBLOCK_FIELD_NAME'] = "�.�.�.";
$MESS ['IBLOCK_FIELD2_NAME'] = "���";
$MESS ['CUSTOM_TITLE_email'] = "Email";
$MESS ['CUSTOM_TITLE_note'] = "����i���";
$MESS ["IBLOCK_FORM_WRONG_CAPTCHA"]= "������ ������� �����";
$MESS ['CUSTOM_TITLE_phone'] = "���������� �������";
$MESS ['CUSTOM_TITLE_phone_EXAMPLE'] = "<i>���������:</i> +38 (050) 1234567";
$MESS ['CUSTOM_TITLE_time_to_call'] = "��� ������";
$MESS ['CUSTOM_TITLE_city_region'] = "̳���, �������";
$MESS ['CUSTOM_TITLE_subject'] = "���������, �������";
$MESS ['CUSTOM_TITLE_topic'] = "���� ������";
$MESS ['CUSTOM_TITLE_type'] = "��� ������";
$MESS ['CUSTOM_TITLE_sphere'] = "������ ���������";
$MESS ['CUSTOM_TITLE_lang'] = "����";
$MESS ['CUSTOM_TITLE_pages'] = "ʳ������ �������";
$MESS ['CUSTOM_TITLE_period'] = "���� ����� ������";
$MESS ['CUSTOM_TITLE_pract'] = "��������� �������";
$MESS ['CUSTOM_TITLE_graf_tabl'] = "�������, �������";
$MESS ['CUSTOM_TITLE_organ'] = "���������� ������";
$MESS ['CUSTOM_TITLE_payment'] = "����� ����������";
//$MESS ['CUSTOM_TITLE_task'] = "�������� �� ������";
$MESS ['CUSTOM_TITLE_pref_price'] = '��������� ������� ������';

$MESS ['CUSTOM_TITLE_NAME_DESCR'] = "������ �������� ���� �������, ��'� �� ��-�������";
$MESS ['CUSTOM_TITLE_email_DESCR'] = "�-����, �� ���� �������� ��������� ������";
$MESS ['CUSTOM_TITLE_phone_DESCR'] = "������ ���� �������, � ��������� ����� � ���� ����� ��'������� ����������� ���������";
$MESS ['CUSTOM_TITLE_time_to_call_DESCR'] = "���, ���� �� ������ �� ��'����";
$MESS ['CUSTOM_TITLE_city_region_DESCR'] = "̳��� (��� ��������� ����� �� �������) ����������.";
$MESS ['CUSTOM_TITLE_subject_DESCR'] = "������ ����� ��������, � ����� ��� ��������� �������� ������";
$MESS ['CUSTOM_TITLE_topic_DESCR'] = "������ �������� ����� ������";
$MESS ['CUSTOM_TITLE_type_DESCR'] = "�������, ��������, �����������, ����";
$MESS ['CUSTOM_TITLE_lang_DESCR'] = "��������� ��� ��������";
$MESS ['CUSTOM_TITLE_pages_DESCR'] = "������ ��������� ������� �������";
$MESS ['CUSTOM_TITLE_period_DESCR'] = "ʳ����� ���� ��������� ������.<br />
�������� �����: ��� ������ ����� ��������� - ��� ������� ������. ��� ��������� �������� ������������� 1 - 2 �����, ��� ��������� 2 - 4 �����";
$MESS ['CUSTOM_TITLE_pract_DESCR'] = "����'������ / ������'������<br />��������� ������� - �� ��������� ������ �� ������� ������� ��'����. ���������, ���������� � ����� ��������� �� ������� ������� ����������.";
$MESS ['CUSTOM_TITLE_graf_tabl_DESCR'] = "����'������ / ������������";
$MESS ['CUSTOM_TITLE_organ_DESCR'] = "������ ����� ������ ����";
$MESS ['CUSTOM_TITLE_payment_DESCR'] = "WebMoney / ���������� / ������� � ���� (���)";
$MESS ['CUSTOM_TITLE_discount_DESCR'] = "���� � ��� �, ������ ��� ������";
$MESS ['CUSTOM_TITLE_file_DESCR'] = "��������� ����";
$MESS ['CUSTOM_TITLE_task'] = "�������� ��� ���� ������ (���� �) �� ������� ������ ������ ����. ��� ��������� ���� ������� ����'������ ���� ��������� �� ��������� ������.";
?>