<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();



?>
<? if(!$_REQUEST["strIMessage"]):?>
	<h1 id="stepTitle">���� 1 � 4</h1>
	<div class="progress progress-striped active">
	  <div class="progress-bar"  role="progressbar" aria-valuenow="<?=(100/count($arResult["PROPERTY_GROUPS"]));?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=(100/count($arResult["PROPERTY_GROUPS"]));?>%">
	    <span class="sr-only">0% Complete</span>
	  </div>
	</div>
<?endif;?>
<?if (count($arResult["ERRORS"])):?>
<? foreach ($arResult["ERRORS"] as $error): ?>
	<div class="alert alert-danger">
        <?=$error;?> 
      </div>
<?endforeach; ?>	
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	
		 <div class="alert alert-success">
		 	<?=ShowNote(str_replace("#ID#",$_SESSION["LAST_ORDER_ID"],$arResult["MESSAGE"]))?>
	      </div>

<?endif?>

<? if(!$_REQUEST["strIMessage"]):?>

<form class="form-signin" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input id="<?=$propertyID;?>" class="form-control" type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>


		<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
			<?foreach ($arResult["PROPERTY_GROUPS"] as $step=>$arGroups):?>
			<div id="step_<?=$step;?>" <? if( $step > 1) echo 'style="display:none;"';?> >
			<?foreach ($arGroups as $propertyID):?>
					<br />
					<label>
					<?if (intval($propertyID) > 0):?>
						<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
					<?else:?>
						<?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)?><?endif?><?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?><span class="starrequired">*</span>
					<?endif?>
					<?if($propertyID==24){echo "<p class=\"text-warning\">(�������, ��������, �����������, ����)</p>";}?>
					<?if($propertyID==34){echo "<p class=\"text-warning\">(��� �������� � ��������� ���� ����'������ ��������� ���������)</p>";}?>
					
						</label>
						<?
						//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
						if (intval($propertyID) > 0)
						{
							if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
							elseif (
								(
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
									||
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
						}
						elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
						{
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						}
						else
						{
							$inputNum = 1;
						}

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
							$INPUT_TYPE = "USER_TYPE";
						else
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

						switch ($INPUT_TYPE):
							case "USER_TYPE":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
										$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										$description = "";
									}
									else
									{
										$value = "";
										$description = "";
									}
									echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
										array(
											$arResult["PROPERTY_LIST_FULL"][$propertyID],
											array(
												"VALUE" => $value,
												"DESCRIPTION" => $description,
											),
											array(
												"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
												"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
												"FORM_NAME"=>"iblock_add",
											),
										));
								?><?
								}
							break;
							case "TAGS":
								$APPLICATION->IncludeComponent(
									"bitrix:search.tags.input",
									"",
									array(
										"VALUE" => $arResult["ELEMENT"][$propertyID],
										"NAME" => "PROPERTY[".$propertyID."][0]",
										"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
									), null, array("HIDE_ICONS"=>"Y")
								);
								break;
							case "HTML":
								$LHE = new CLightHTMLEditor;
								$LHE->Show(array(
									'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
									'width' => '100%',
									'height' => '200px',
									'inputName' => "PROPERTY[".$propertyID."][0]",
									'content' => $arResult["ELEMENT"][$propertyID],
									'bUseFileDialogs' => false,
									'bFloatingToolbar' => false,
									'bArisingToolbar' => false,
									'toolbarConfig' => array(
										'Bold', 'Italic', 'Underline', 'RemoveFormat',
										'CreateLink', 'DeleteLink', 'Image', 'Video',
										'BackColor', 'ForeColor',
										'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
										'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
										'StyleList', 'HeaderList',
										'FontList', 'FontSizeList',
									),
								));
								break;
							case "T":
								for ($i = 0; $i<$inputNum; $i++)
								{

									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									}
									else
									{
										$value = "";
									}
								?>
						<textarea id="<?=$propertyID;?>" class="form-control" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
								<?
								}
							break;

							case "S":
							case "N":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

									}
									else
									{
										$value = "";
									}
								?>
								<input  id="<?=$propertyID;?>" class="form-control" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
									$APPLICATION->IncludeComponent(
										'bitrix:main.calendar',
										'',
										array(
											'FORM_NAME' => 'iblock_add',
											'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
											'INPUT_VALUE' => $value,
										),
										null,
										array('HIDE_ICONS' => 'Y')
									);
									?><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
								endif
								?><?
								}
							break;

							case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?>
						<div id="multy_<?=$propertyID;?>">			
						<input id="<?=$propertyID;?>" class="form-control" type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
						<input id="<?=$propertyID;?>" class="form-control" type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" />
							<div><a href="javascript:;" onclick="addFileField(this,'multy_<?=$propertyID;?>','<?=$propertyID;?>');">��������� ����� ����</a></div>
						</div>
									<?

									if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
									{
										?>
					<input id="<?=$propertyID;?>" class="form-control" type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label>
										<?

										if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
										{
											?>
					<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" />
											<?
										}
										else
										{
											?>
					<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?>
					<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b
					[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]
											<?
										}
									}
								}

							break;
							case "L":

								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
								else
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

								switch ($type):
									case "checkbox":
									case "radio":

										//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
												{
													foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
													{
														if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}

											?>
							<input id="<?=$propertyID;?>" class="form-control" type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label>
											<?
										}
									break;

									case "dropdown":
									case "multiselect":
									?>
							<select id="<?=$propertyID;?>" class="form-control" name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
									<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}
											?>
								<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
											<?
										}
									?>
							</select>
									<?
									break;

								endswitch;
							break;
						endswitch;?>
					
				
			<?endforeach;?>
				</div>
			<?endforeach;?>
			<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
<?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>>
					
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					
				
				
					<?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:
					<input id="<?=$propertyID;?>" class="form-control" type="text" name="captcha_word" maxlength="50" value="">
				
			<?endif?>

		<?endif?>
					<br/>
					<div id="butSubmit" style="display:none;">
						<? 
						$userID = $USER->GetID();
						$rsUser = $USER->GetByID($userID);
							if($arUser=$rsUser->Fetch())
							{
								//print_r($arUser);
								$arUserCity = $arUser["WORK_CITY"];
							}
	
						?>
						
						<input type="hidden" value="<?=$userID;?>" name="PROPERTY[55][0]">
						<input type="text" value="<?=$arUserCity;?>" name="PROPERTY[21][0]">
						<input type="hidden" value="Y" name="iblock_submit">
						<button class="btn btn-lg btn-primary btn-block" name="iblock_submit" value="Y" type="submit">³��������</button>
					<!--	<button class="btn btn-danger" name="iblock_submit"  type="reset"><?=GetMessage("IBLOCK_FORM_RESET")?></button> -->
					</div>
					<?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>

	</form>
	
<ul class="pager">
		  <li id="prevS" style="font-size: 18px;"><a href="#" onclick="PrevStep()">�����</a></li>
		  <li id="nextS" style="font-size: 18px;"><a href="#" onclick="NextStep()">���</a></li>
</ul>
<?else:?>
<ul class="pager">
		  <li style="font-size: 18px;"><a href="/agent/" >������� ���� ����������</a></li>
</ul>
<?endif;?>
<script>

	var stepCount = <?=count($arResult["PROPERTY_GROUPS"]);?>;
	
	var arRequaredFields = [];
	
	arRequaredFields = [<?
	$i=0;
	foreach($arResult["PROPERTY_REQUIRED"] as $reqID){
		if($reqID){
			if($i>0){
			echo ",\"".$reqID."\"";
			}else{
				echo "\"".$reqID."\"";
			}
			$i++;
		}
	}?>
	
	];
	function NextStep(){
			//alert('next');
			
			if(NeedRequaredFields() == true){
				active_id = GetActiveStep();
				
				if(active_id < stepCount){
					HideStep(active_id);
					ShowStep(active_id+1);
				}
			}else{
				alert("��������� �� ����, �� ������� �������");
			}
	}
	
	function PrevStep(){
		
		active_id = GetActiveStep();
		

		
		if(active_id > 1){
			HideStep(active_id);
			ShowStep(active_id-1);
		}
	}
	
	function HideShowNavButton(){
		//$("#butSubmit").attr("style", "display: none");
		//$("#nextS").attr("style", "display: show; font-size: 18px;");
	}
		
	function ShowSubmitButton() {
	  //$("#butSubmit").attr("style", "display: show");
	  //$("#nextS").attr("style", "display: none");
	  
	  
	}
	
	function UpdateStatusBar(){
	  val = (GetActiveStep()*100)/stepCount;
	  $( ".progress-bar" ).attr( "aria-valuenow", val );
	  $( ".progress-bar" ).attr( "style", "width: " + val + "%" );
	  
	  $("#stepTitle").html('<h1 id="stepTitle">���� ' + GetActiveStep() + ' � ' + stepCount+ '</h1>');
	  
	}	
	
	
	function NeedRequaredFields(){
		var Res=true;
		control = "#step_" + GetActiveStep(); 
		res = $(control + ' > [id]');
		
		$(control + ' > [id]').each(function() {
   			 if(in_array(this.id, arRequaredFields) == true ){
   			 	if(!$(this).val()){
   			 		Res = false;
   			 		
   			 	}
   			 	
   			 }
   			 
		});
		
		return Res;
	
	}
	
	
	function ShowStep (step_id) {
	  $("#step_" + step_id).css("display", "block");
	  UpdateStatusBar();
	  
	  if(step_id == 4){
	  	$("#nextS").html('<li id="nextS" style="font-size: 18px;"><a href="#" onclick="SubmitForm()">³��������</a></li>');
	  }else{
	  	$("#nextS").html('<li id="nextS" style="font-size: 18px;"><a href="#" onclick="NextStep()">���</a></li>');
	  
	  }
	  
	  
	}
	
	function HideStep(step_id){
		$("#step_" + step_id).css("display", "none");
	}
	
	function GetActiveStep(){
		
		for (i=1; i <=stepCount; i++){
			stepItem  = '#step_' + i;
			//alert(stepItem);
			state = $(stepItem).css('display');
			
			if(state == 'block'){
				active = i;
			}
		}
		
		return active;
	
	
	}
	function SubmitForm(){
	  $(".form-signin").submit();
	}
	
	function in_array(needle, haystack, strict) {	// Checks if a value exists in an array

	var found = false, key, strict = !!strict;

	for (key in haystack) {
		if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
			found = true;
			break;
		}
	}

	return found;
}



	function addFileField(caller,p_id,n_id) {
		var cnt = $("#"+p_id+" input:file").size();
		$(caller).parent().before('<div><input type="hidden" value="" name="PROPERTY['+n_id+']['+(cnt)+']"><input type="file" name="PROPERTY_FILE_'+n_id+'_'+(cnt)+'" class="form-control"></div>');
	}
</script>