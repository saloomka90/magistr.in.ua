<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
function decode_params(&$src, $to_charset)
{
	foreach($src as $key => $val)
		if(!is_array($val)) $src[$key] = unicodeUrlDecode($val, $to_charset);
		else decode_params($src[$key], $to_charset);
}

function unicodeUrlDecode($url, $encoding = "")
{
    if ($encoding == '')
    {
	if (isset($_SERVER['HTTP_ACCEPT_CHARSET']))
	{
	    preg_match('/^\s*([-\w]+?)([,;\s]|$)/', $_SERVER['HTTP_ACCEPT_CHARSET'], $a);
	    $encoding = strtoupper($a[1]);
	}
	else
	    $encoding = 'CP1251'; // default
    }

    preg_match_all('/%u([[:xdigit:]]{4})/', $url, $a);
    foreach ($a[1] as $unicode)
    {
	$num = hexdec($unicode);
	$str = '';

	// UTF-16(32) number to UTF-8 string
	if ($num < 0x80)
	    $str = chr($num);
	else if ($num < 0x800)
	    $str = chr(0xc0 | (($num & 0x7c0) >> 6)) .
		chr(0x80 | ($num & 0x3f));
	else if ($num < 0x10000)
	    $str = chr(0xe0 | (($num & 0xf000) >> 12)) .
		chr(0x80 | (($num & 0xfc0) >> 6)) .
		chr(0x80 | ($num & 0x3f));
	else
	    $str = chr(0xf0 | (($num & 0x1c0000) >> 18)) .
		chr(0x80 | (($num & 0x3f000) >> 12)) .
		chr(0x80 | (($num & 0xfc0) >> 6)) .
		chr(0x80 | ($num & 0x3f));

	$str = iconv("UTF-8", "$encoding//IGNORE", $str);
	$url = str_replace('%u'.$unicode, $str, $url);
    }

    return urldecode($url);
}
function FileExistsRecursive($cur_dir, $file_name)
{
	if(strlen($cur_dir)<=0/* || strlen($file_name)<=0*/) return false;
	
	$dir = $cur_dir;
	while(strlen($dir)>0 && !file_exists($_SERVER["DOCUMENT_ROOT"].$dir.$file_name))
	{
		$p = explode("/", $dir);
		while(strlen(end($p))<=0) unset($p[end(array_keys($p))]);
		unset($p[end(array_keys($p))]);
		$dir = implode("/", $p);
		if(strlen($dir)>1) $dir .= "/";
	}
	if(strlen($dir)>0 && file_exists($_SERVER["DOCUMENT_ROOT"].$dir.$file_name)) return $dir.$file_name;
}
?>