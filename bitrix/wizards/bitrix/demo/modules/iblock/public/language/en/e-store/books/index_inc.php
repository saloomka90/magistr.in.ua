<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!--VOTE_FORM-->

<div class="information-block">
	<div class="information-block-head">How did you do it?</div>

	<div class="information-block-body">
	To provide a better user experience, the catalog is structured by creating a new information
	block type (<b>Book catalog</b>), for which the following information blocks were created:</p>
	<ul>
		<li>Authors;</li>
		<li>Books;</li>
		<li>Reviews.</li>
	</ul>

<a href="/e-store/">more...</a>
	</div>
</div>
