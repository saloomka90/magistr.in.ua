<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<a href="/" title="Home"><img width="12" height="11" src="<?=BX_PERSONAL_ROOT?>/templates/books/images/icons/home.gif" alt="Home" border="0" /></a>
<a href="/search/" title="Search"><img width="12" height="12" src="<?=BX_PERSONAL_ROOT?>/templates/books/images/icons/search.gif" alt="Search" border="0" /></a>
<a href="/search/map.php" title="Site map"><img width="14" height="9" src="<?=BX_PERSONAL_ROOT?>/templates/books/images/icons/map.gif" alt="Site map" border="0" /></a>
<a href="<?=htmlspecialchars($APPLICATION->GetCurUri("print=Y"));?>" title="Print Version" rel="nofollow"><img width="12" height="12" src="<?=BX_PERSONAL_ROOT?>/templates/books/images/icons/printer.gif" alt="Print Version" border="0" /></a>