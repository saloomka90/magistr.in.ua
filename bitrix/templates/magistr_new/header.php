<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(empty($_REQUEST["edit"])):?>
<!DOCTYPE HTML>
<html>
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="windows-1251">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />

	<link rel="stylesheet" href="<?=$APPLICATION->GetTemplatePath("css/style.css");?>" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/popup.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("js/checkbox/checkbox.css");?>" />
	<link rel="stylesheet" type="text/css" href="<?=$APPLICATION->GetTemplatePath("css/jquery-filestyle.css");?>" />
	
	<link rel="stylesheet" href="<?=$APPLICATION->GetTemplatePath("css/datepicker/jquery-ui.css");?>">
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-1.7.1.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/cufon-yui.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/tahoma_cufon.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/select/jquery.selectbox-0.2.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/popup.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/checkbox/jquery.checkbox.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-filestyle.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.validate.min.js");?>"></script>
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery-ui.js");?>"></script>
	
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.form-2.25.js");?>"></script>
	<link href="<?=$APPLICATION->GetTemplatePath("css/nyroModal.css");?>" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?=$APPLICATION->GetTemplatePath("js/jquery.nyroModal-1.6.2.pack.js");?>"></script>


    <?$APPLICATION->ShowHead()?>
</head>

<body>
<?$APPLICATION->ShowPanel();?>
	
	<div class="bg">
		<div class="overlay" id="overlay_order" style="display:none;"></div>
		<div class="header-bg">
			<div class="header">
				<div class="logo">
					<a href="#"><img src="<?=$APPLICATION->GetTemplatePath('images/logo.png');?>" /></a>
				</div>
				<div class="header-r">
					<? if($USER->IsAuthorized()):?>
					<div class="header_top">
						<div class="name overchack"><?= $USER->GetFullName()?>
							<?CModule::IncludeModule('iblock');?>
							<? 
							$opt = true;

							// if ($_GET['opt_head'] == 'Y'){
							// 	$opt = true;
							// }

							$filt_ids = array();
							$count_works1 = 0;

							if ($opt){
								$arSelect = Array("ID","PROPERTY_ORDER_ID");//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
								$arFilter = Array("IBLOCK_ID"=>IntVal(13), "PROPERTY_END"=>false,"!PROPERTY_STYPE"=>false,"ACTIVE"=>'Y','<DATE_ACTIVE_TO'=>ConvertTimeStamp(date(), 'FULL'));
								$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
								while($ob = $res->Fetch()){
									$arFields = $ob;
									if (!in_array($arFields["PROPERTY_ORDER_ID_VALUE"], $filt_ids)){
										if ($arFields["PROPERTY_ORDER_ID_VALUE"]>0)
											$filt_ids[] = $arFields["PROPERTY_ORDER_ID_VALUE"];
									}
								}
								// if ($USER->IsAdmin()){
								// 	var_dump($filt_ids);
								// }
								$count_works1 = 0;
								$count_works = 0;
								$count_works2 = 0;
								$count_works212 = 0;
								// ������� ������
								$obCache = new CPHPCache; 

								// ����� ����������� - 30 �����
								$life_time = 1*60; 

								$cache_id = 'header_cache_'. implode('_', $absent_ids); 

								// ���� ��� ���� � �� ��� �� ����� ��
								if($obCache->InitCache($life_time, $cache_id, "/")) {
								    $vars = $obCache->GetVars();
								    $count_works1 = $vars["count_works1"];
								    $count_works = $vars["count_works"];
								    $count_works2 = $vars["count_works2"];
								    $count_works212 = $vars["count_works212"];
								    $count_works212_chat = $vars["count_works212_chat"];//wtf
								}

								// �������� ��������������� ������
								if($obCache->StartDataCache()):
									if (!empty($filt_ids)):
										$arFilter = array(
												'IBLOCK_ID' => 13,
												'<DATE_ACTIVE_FROM' => ConvertTimeStamp(date(), 'FULL'),
												"PROPERTY_STYPE"=>false,
												"PROPERTY_END"=>false,
												"PROPERTY_TYPE"=>'��������',
												"PROPERTY_ORDER_ID"=>$filt_ids,
												"ACTIVE"=>'Y',
										);
										$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
										if ($el = $res->Fetch()) {
											$count_works1 = $el['CNT'];
										}
									endif;

									$filt_ids = array();
									$arSelect = Array("ID","PROPERTY_ORDER_ID");//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
									$arFilter = Array("IBLOCK_ID"=>IntVal(13),"PROPERTY_END"=>false,array('LOGIC'=>"OR",array("PROPERTY_STATUS"=>false),array("PROPERTY_STATUS_VALUE"=>'� ������� ��������')));
									$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
									while($ob = $res->Fetch()){
										$arFields = $ob;
										if (!in_array($arFields["PROPERTY_ORDER_ID_VALUE"], $filt_ids)){
											if ($arFields["PROPERTY_ORDER_ID_VALUE"]>0)
												$filt_ids[] = $arFields["PROPERTY_ORDER_ID_VALUE"];
										}
									}
									
									$count_works = 0;
									$arFilter = array(
											'IBLOCK_ID' => 6,
											'!ID' => $filt_ids,

											array("LOGIC" => "OR",
												array(		
													"PROPERTY" => array(											
															"state" => array(
																	"VALUE_ENUM_ID" =>23
															)
													)
												),
												array(
													"PROPERTY" => array(
															"state" => array(
																	"VALUE_ENUM_ID" =>21
															)
													)
												)
											),
											"ACTIVE"=>'Y',
									);
									$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
									if ($el = $res->Fetch()) {
										$count_works = $el['CNT'];
									}
									
									/////////////////////////////////////////////
									$count_works2 = 0;
									$arFilter = array(
											'IBLOCK_ID' => 13,									
											"PROPERTY_STATUS_VALUE"=>'��������� ���� ������������',
											"ACTIVE"=>'Y',
									);
									$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
									if ($el = $res->Fetch()) {
										$count_works2 = $el['CNT'];
									}
									
									
									////////////////////////////////////////////////////////////////////////
									$arFilter = array(
											'IBLOCK_ID' => 14,
											'PROPERTY_STATUS_VALUE' => '��� ������',
											 '!PROPERTY_USER_ID'=>false
											
									);
									$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
									if ($el = $res->Fetch()) {
										$count_works212 = $el['CNT'];
									}
									
									////////////////////////////////////////////////
									$count_works212_chat = 0;
									$arFilter = array(
									'IBLOCK_ID' => 19,
									'PROPERTY_STATUS_VALUE' => '��� ������',
									'!PROPERTY_USER_ID'=>false
									
									);
									$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
									if ($el = $res->Fetch()) {
									    $count_works212_chat = $el['CNT'];
									}
									

								    // ���������� �������������� ���������������� ����� � ���� ����
								    // ������ � �������������� ����������
									
								    $obCache->EndDataCache(array(
								        "count_works1"    => $count_works1,
								        "count_works"    => $count_works,
								        "count_works2"    => $count_works2,
								        "count_works212"    => $count_works212,
								        "count_works212_chat" => $count_works212_chat,
								        )); 
								endif;?>
									<?
									if ($count_works1>0){
										?>
									<a href="/overdue/">������: <?=$count_works1?></a>
										<?
									}
									?>
									<?
									if ($count_works>0){
										?>
									<a href="/notask/">��� ���: <?=$count_works?></a>
										<?
									}
									?>
									<?
									if ($count_works2>0){
										?>
									<a href="/not_confirmed/">�� ����: <?=$count_works2?></a>
										<?
									}
									?>
									<?
									if ($count_works212>0){
										?>
									<a class="commentcountthisajax" href="/comments_admin/">������: <span><?=$count_works212?></span></a>
										<?
									}
									?>
									<?
									if ($count_works212_chat>0){
										?>
									<a class="commentcountthisajax2"  href="/chats/">���: <span><?=$count_works212_chat?></span></a>
									<?
									}
									?>
									<a class="js-revisionclass"  href="/revision/">�������: <span><?=0?></span></a>

							<?
							}else{
								$count_works1 = 0;
							
							
								$arSelect = Array("ID","PROPERTY_ORDER_ID");//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
								$arFilter = Array("IBLOCK_ID"=>IntVal(13), "PROPERTY_END"=>false,"!PROPERTY_STYPE"=>false,"ACTIVE"=>'Y','<DATE_ACTIVE_TO'=>ConvertTimeStamp(date(), 'FULL'));
								$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
								while($ob = $res->Fetch()){
									$arFields = $ob;
									$filt_ids[] = $arFields["PROPERTY_ORDER_ID_VALUE"];
								}
								// if ($USER->IsAdmin()){
								// 	var_dump($filt_ids);
								// }

								if (!empty($filt_ids)):
								
								
								
								$arFilter = array(
										'IBLOCK_ID' => 13,
										'<DATE_ACTIVE_FROM' => ConvertTimeStamp(date(), 'FULL'),
										"PROPERTY_STYPE"=>false,
										"PROPERTY_END"=>false,
										"PROPERTY_TYPE"=>'��������',
										"PROPERTY_ORDER_ID"=>$filt_ids,
										"ACTIVE"=>'Y',
								);
								$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
								if ($el = $res->Fetch()) {
									$count_works1 = $el['CNT'];
									?>
									<a href="/overdue/">�����������: <?=$count_works1?></a>
									<? 
								}
								endif;
								///////////////////////////////////////////////////////
								?>
								<?
								$filt_ids = array();
								$arSelect = Array("ID","PROPERTY_ORDER_ID");//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
								$arFilter = Array("IBLOCK_ID"=>IntVal(13),"PROPERTY_END"=>false,array('LOGIC'=>"OR",array("PROPERTY_STATUS"=>false),array("PROPERTY_STATUS_VALUE"=>'� ������� ��������')));
								$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
								while($ob = $res->Fetch()){
									$arFields = $ob;
									$filt_ids[] = $arFields["PROPERTY_ORDER_ID_VALUE"];
								}
								
								$count_works = 0;
								$arFilter = array(
										'IBLOCK_ID' => 6,
										'!ID' => $filt_ids,

										array("LOGIC" => "OR",
											array(		
												"PROPERTY" => array(											
														"state" => array(
																"VALUE_ENUM_ID" =>23
														)
												)
											),
											array(
												"PROPERTY" => array(
														"state" => array(
																"VALUE_ENUM_ID" =>21
														)
												)
											)
										),
										"ACTIVE"=>'Y',
								);
								$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
								if ($el = $res->Fetch()) {
									$count_works = $el['CNT'];
									?>
																<a href="/notask/">��� ��������: <?=$count_works?></a>
																<? 
															}
								?>
								
								<? 
								/////////////////////////////////////////////
								$count_works2 = 0;
								$arFilter = array(
										'IBLOCK_ID' => 13,									
										"PROPERTY_STATUS_VALUE"=>'��������� ���� ������������',
										"ACTIVE"=>'Y',
								);
								$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
								if ($el = $res->Fetch()) {
									$count_works2 = $el['CNT'];
									?>
									<a href="/not_confirmed/">�� �����������: <?=$count_works2?></a>
									<? 
								}
								
								
								////////////////////////////////////////////////////////////////////////
								$arFilter = array(
										'IBLOCK_ID' => 14,
										'PROPERTY_STATUS_VALUE' => '��� ������',
										 '!PROPERTY_USER_ID'=>false
										
								);
								$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
								if ($el = $res->Fetch()) {
									$count_works212 = $el['CNT'];
									?>
																<a class="commentcountthisajax" href="/comments_admin/">������: <span><?=$count_works212?></span></a>
																<? 
															}
															
															
								////////////////////////////////////////////////////////////////////////
								$arFilter = array(
								'IBLOCK_ID' => 19,
								'PROPERTY_STATUS_VALUE' => '��� ������',
								'!PROPERTY_USER_ID'=>false
								
								);
								$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
								if ($el = $res->Fetch()) {
								    $count_works212 = $el['CNT'];
								    ?>
									<a class="commentcountthisajax2" href="/chat/">���: <span><?=$count_works212?></span></a>
									<? 
								}
							}

							?>							
				<script>
            		$( document ).ready(function(){
            			setInterval(function(){
                			
            					$.ajax({
            					  url: "/count_admin.php",
            					  cache: false,
            					  dataType: 'json',
            					  success: function(html){


                					  
									count = html.p1*1;
                					if(count>0){
                						if($(".commentcountthisajax").length>0) {
											nowcount = $('.commentcountthisajax span').html()*1;
											if(nowcount!=count){
												$('.commentcountthisajax span').html(count);
												//������
												if(count>nowcount){
													var audio = new Audio();
													audio.preload = 'auto';
													audio.src = '/sound1.mp3';
													audio.play();
												}
												
											}
                						}else{

                    						if($(".commentcountthisajax2").length>0){
            									$('.js-revisionclass').before('<a class="commentcountthisajax" href="/comments_admin/">������: <span>'+count+'</span></a>')
                        					}else{
                        						$('.commentcountthisajax2').before('<a class="commentcountthisajax" href="/comments_admin/">������: <span>'+count+'</span></a>')
                        					}
											
											//������
											var audio = new Audio();
											audio.preload = 'auto';
											audio.src = '/sound1.mp3';
											audio.play();
                						}
                					}else{
                						$('.commentcountthisajax').remove();
                					}


                						 count = html.p2*1;
                    					if(count>0){
                        					
                    						if($(".commentcountthisajax2").length>0) {
    											nowcount = $('.commentcountthisajax2 span').html()*1;


    											//console.log(nowcount,count);

    											
    											if(nowcount!=count){
    												$('.commentcountthisajax2 span').html(count);
    												//������
    												if(count>nowcount){
    													var audio = new Audio();
    													audio.preload = 'auto';
    													audio.src = '/definite.mp3';
    													audio.play();
    													
    												}
        												
        												
    													

    												
    												
    											}else{

    												if(html.new){
    													$('.commentcountthisajax2 span').html(0);
    													$('.commentcountthisajax2 span').html(count);
														var audio = new Audio();
    													audio.preload = 'auto';
    													audio.muted = false;
    													audio.src = '/definite.mp3';
    													audio.play();         													
													}
    											}
                    						}else{

                    							
    											$('.js-revisionclass').before('<a class="commentcountthisajax2" href="/chats/">���: <span>'+count+'</span></a>')
    											//������
    											var audio = new Audio();
    											audio.preload = 'auto';
    											audio.src = '/definite.mp3';
    											audio.play();
    											
                    						}


                    					}else{
                    						$('.commentcountthisajax2').remove();

                    						
                    					}
                					
            					    //alert(html);
            					  }
            					});

                		}, 5000)
						

                	});
            	</script>
						</div>
						<div class="panel_link">
							<a href="/authors/personal_data"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_account.png');?>" /> <span>����i��</span></a> |
							<a href="/?logout=yes"><img src="<?=$APPLICATION->GetTemplatePath('images/ico_exit.png');?>" /> <span>�����</span></a>
						</div>
					</div>
					<div class="clr"></div>

					<?php
						
						if ($opt){

						}else{
							$arFilter = array(
								'IBLOCK_ID' => 4,
								'CREATED_USER_ID'=>$USER->GetID(),
							);
							$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
							if ($el = $res->Fetch()) {
								$count_works = $el['CNT'];
							}

							$arFilter = array(
								"IBLOCK_ID" => 5,
								'CREATED_USER_ID'=>$USER->GetID(),
							);
							$res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
							if ($el = $res->Fetch()) {
								$count_quotes = $el['CNT'];
							}

	                        $arFilter = array(
	                            'IBLOCK_ID' => 6,//4
	                            "PROPERTY" => array(
	                                "performer"=> array(
	                                    "VALUE" => $USER->GetID()
	                                    ),
	                                "state" => array(
	                                	"VALUE_ENUM_ID" =>23
	                                	)
	                                )
	                            // 'CREATED_USER_ID'=>$USER->GetID(),
	                        );
	                        $res = CIBlockElement::GetList(false, $arFilter, array('IBLOCK_ID'));
	                        if ($el = $res->Fetch()) {
	                            $in_progress = $el['CNT'];
	                        }
						}

						

						
                        $url_segments = explode('/',$APPLICATION->GetCurPage());
                        $active_item = $url_segments[1];
					?>
					<!-- ��� ����! -->
					<div class="menu">
					<ul>
					
							
							<li class="parent <?if($active_item=="quotes_admin" or $active_item=="notifications"or $active_item=="overdue"or $active_item=="notask"or $active_item=="not_confirmed")echo 'active';?>">
								<a href="/quotes_admin/">���i<br> ������</a>
								<ul>
									<?/* ?><li <?if($active_item=="stolrabot")echo 'active';?>><a href="#">������� ��i�</a></li><? */?>
									<li <?if($active_item=="quotes_admin")echo 'active';?>><a href="/quotes_admin/">������ ���i�</a></li>
									<li class="<?if($active_item=="notifications")echo 'active';?>"><a href="/notifications/">�����������</a></li>
									<li class="<?if($active_item=="overdue")echo 'active';?>"><a href="/overdue/">�����������</a></li>
									<li class="<?if($active_item=="notask")echo 'active';?>"><a href="/notask/">��� ��������</a></li>
									<li class="<?if($active_item=="not_confirmed")echo 'active';?>"><a href="/not_confirmed/">�� �����������</a></li>
								</ul>
							</li>
							<li class="parent <?if($active_item=="quotes_admin_ready")echo 'active';?>">
								<a href="/quotes_admin_ready/">�����i<br> ������</a>
								<ul>
									<li class="<?if($active_item=="quotes_admin_ready")echo 'active';?>"><a href="/quotes_admin_ready/">������ �� ����� ������</a></li>
									
								</ul> 
							</li>
							<li class="parent <?if($active_item=="topay" or $active_item=="payments" or $active_item=="income")echo 'active';?>">
								<a href="/topay/">�i�����</a>
								<ul>
									<li class="<?if($active_item=="topay")echo 'active';?>"><a href="/topay/">�� ������</a></li>
									<li class="<?if($active_item=="payments")echo 'active';?>"><a href="/payments/">���i� ������</a></li>
									<li class="<?if($active_item=="income")echo 'active';?>"><a href="/income/">���i� ����������</a></li>
								</ul>
							</li>
							<li class="parent  <?if($active_item=="authors_admin")echo 'active';?>">
								<a href="/authors_admin/">������</a>
								<ul>									
									<li <?if($active_item=="authors_admin")echo 'class="active"';?>><a href="/authors_admin/">������� �����i�</a></li>
								</ul>
							</li>
							<li class="parent <?if($active_item=="users" or $active_item=="tips" or $active_item=="chats" or $active_item=="reviews_admin")echo 'active';?>">
								<a href="/users/">�볺���</a>
								<ul>
									<li <?if($active_item=="users")echo 'class="active"';?>><a href="/users/">������ �볺���</a></li>
									<li <?if($active_item=="tips")echo 'class="active"';?>><a href="/tips/">�����</a></li>
									<li <?if($active_item=="chats")echo 'class="active"';?>><a href="/chats/">���� ��� ������</a></li>
									<li <?if($active_item=="reviews_admin")echo 'class="active"';?>><a href="/reviews_admin/">³�����</a></li>
								</ul>							
							</li>
							
							<li class="parent <?if($active_item=="partners_admin" or $active_item=="quotes_partners_admin")echo 'active';?>">
								<a href="/partners_admin/">��������</a>
								<ul>
									<li <?if($active_item=="partners_admin")echo 'class="active"';?>><a href="/partners_admin/">������ ��������</a></li>
									<li <?if($active_item=="quotes_partners_admin")echo 'class="active"';?>><a href="/quotes_partners_admin/">������ ��������</a></li>
								</ul>
							</li>
							
							<li class="parent <?if($active_item=="comments_admin" or $active_item=="comments_list_admin")echo 'active';?>">
								<a href="/comments_admin/">��������</a>
								<ul>
									<li <?if($active_item=="comments_admin")echo 'class="active"';?>><a href="/comments_admin/">�������� ��� ������</a></li>
									<li <?if($active_item=="comments_list_admin")echo 'class="active"';?>><a href="/comments_list_admin/">����� ���������</a></li>
								</ul>
							</li>
							
						</ul>
						
					</div>
					<div class="clr"></div>
					<? endif;?>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<div class="wrap-bg">
			<div class="wrap">
			<h1>
			
				<?$APPLICATION->ShowTitle()?>
				<?if($a):?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="profile-edit">��i���� ������</a>
				<?endif;?>
			</h1>
<?endif;?>
