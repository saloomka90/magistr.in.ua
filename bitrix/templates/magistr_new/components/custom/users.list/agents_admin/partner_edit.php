<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID","ua");
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

//echo "<pre>".print_r($_GET, true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

$user_id = intval($_GET["u_id"]);
//echo "<pre>".print_r($_GET,true)."</pre>";
if(sizeof(array_intersect(array(1,6),$USER->GetUserGroupArray())) && CModule::IncludeModule("iblock"))
{
	$show_form = false;
	if($user_id>0)
	{
		$rsUser = CUser::GetByID($user_id);
		if($arUser=$rsUser->Fetch())
			$show_form = true;
		$edit = true;
	}
	else
	{
		$show_form = true;
	}
	
	if($show_form)
	{
		if($_REQUEST["save"]=="Y")
		{
			$user = new CUser;
			
			$arLoadArray = $_REQUEST["user"];
			if(isset($arLoadArray["EMAIL"]))
				$arLoadArray["LOGIN"] = ($arLoadArray["LOGIN"])? $arLoadArray["LOGIN"] : $arLoadArray["EMAIL"];
			if(isset($arLoadArray["PERSONAL_NOTES"]))
				$arLoadArray["PERSONAL_NOTES"] = floatval(str_replace(",",".",$arLoadArray["PERSONAL_NOTES"]));
			
			if($_FILES["WORK_LOGO"]){
				//$fid = CFile::SaveFile($_FILES["WORK_LOGO"], "agents");
				$arLoadArray["WORK_LOGO"] = $_FILES["WORK_LOGO"];
			}
			
			if($_REQUEST["WORK_LOGO_del"]){
				$arLoadArray["WORK_LOGO"] = array("del"=> "Y");
			}
			/*
			if($fid){
				$arLoadArray["WORK_LOGO"] = $fid;
			}*/
			//echo "<pre>".print_r($_REQUEST,true)."</pre>";
			//die;
			
			if($edit)
			{
				if(strlen($arLoadArray["PASSWORD"])<=0)
					unset($arLoadArray["PASSWORD"], $arLoadArray["CONFIRM_PASSWORD"]);
				
				$ID = $user->Update($arUser["ID"],$arLoadArray);
				if($ID)
				{
					echo '<table width="100%"><tr><td align="center" valign="middle" height="300">���������� ��� �������� ������ ������.</td></tr></table><script type="text/javascript">var reload_page = 1;</script>';
					$saved = true;
				}
				else echo "Error: ".$user->LAST_ERROR;
			}
			else
			{
				$arLoadArray["GROUP_ID"] = array(10);
				
				$ID = $user->Add($arLoadArray);
				if($ID)
				{
					echo '<table width="100%"><tr><td align="center" valign="middle" height="300">������� ������ �������.</td></tr></table><script type="text/javascript">var reload_page = 1;</script>';
					$saved = true;
				}
				else echo "Error: ".$user->LAST_ERROR;
			}
		}
		
		if(!$saved)
		{
			$res .= '
	<script type="text/javascript">
		function savePartner(user_id,params)
		{
			$.nyroModalManual({
				url: \''.$cur_dir.'/partner_edit.php?u_id=\'+user_id+\'&lang='.LANG_ID.'&save=Y&\'+params
			});
			return false;
		}
	</script>
	<form enctype="multipart/form-data" id="partner_edit_form" method="post" action="'.$cur_dir.'/partner_edit.php?u_id='.$user_id.'&lang='.LANG_ID.'&save=Y">
	<table class="rounded">
	<thead><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
	<tfoot><tr><td colspan="2"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
	<tbody>
	<tr class="head">
	<td style="width:25% !important;border-right:none;">&nbsp;</td>
	<td colspan="2" style="width:50% !important;border-left:none;border-right:none;"><strong>���������� ��� ��������</strong></td>
	<td style="width:25% !important;border-left:none;">&nbsp;</td>
	</tr>';
			$res .= '
			<tr>
				<td colspan="2" style="width:50% !important;">��������</td>
				<td colspan="2" style="width:50% !important;">
					<input type="hidden" name="user[ACTIVE]" value="N" />
					<input type="checkbox" name="user[ACTIVE]" value="Y"'.($arUser["ACTIVE"]=="Y" ? ' checked="checked"' : '').' />
				<input type="hidden" class="inputtext" name="user[LOGIN]" value="'.htmlspecialchars($arUser["LOGIN"]).'" />
				</td>
			</tr>
			<tr>
				<td colspan="2" style="width:50% !important;">'.GetMessage("FIO").'</td>
				<td colspan="2" style="width:50% !important;"><input type="text" class="inputtext" name="user[NAME]" value="'.htmlspecialchars($arUser["NAME"]).'" /></td>
			</tr>
			<tr>
				<td colspan="2">Email</td>
				<td colspan="2"><input type="text" class="inputtext" name="user[EMAIL]" value="'.htmlspecialchars($arUser["EMAIL"]).'" /></td>
			</tr>
			<tr>
				<td colspan="2">������ ������</td>
				<td colspan="2"><input type="password" class="inputtext" name="user[PASSWORD]" value="" /></td>
			</tr>
			<tr>
				<td colspan="2">ϳ��������� ������</td>
				<td colspan="2"><input type="password" class="inputtext" name="user[CONFIRM_PASSWORD]" value="" /></td>
			</tr>
			<tr>
				<td colspan="2">'.GetMessage("PHONE").'</td>
				<td colspan="2"><input type="text" class="inputtext" name="user[PERSONAL_PHONE]" value="'.htmlspecialchars($arUser["PERSONAL_PHONE"]).'" /></td>
			</tr>
			
			<tr>
				<td colspan="2">̳���</td>
				<td colspan="2"><input type="text" class="inputtext" name="user[WORK_CITY]" value="'.htmlspecialchars($arUser["WORK_CITY"]).'" /></td>
			</tr>
			';
			$res .= '<tr>
				<td colspan="2">����</td><td>'.CFile::InputFile("WORK_LOGO", 20, ($arUser["WORK_LOGO"])? $arUser["WORK_LOGO"] : false ).'</td></tr>';

			$res .= '
	</tbody>
	</table>
	<div class="clear">&nbsp;</div><br /><br />

	<input type="submit" value="��������" onclick="/*savePartner('.$user_id.', escape($(\'#partner_edit_form\').serialize()));*/" />
	<input type="button" value="³����" onclick="'.($user_id>0 ? 'showPartners('.$arUser["ID"].');' : '$.nyroModalRemove();').'" />
	</form>
<script type="text/javascript">
	setFormOptions();
</script>';
		}
	}
	echo $res;
}
?>