<?
$data = array();

//echo "<pre>".print_r($arResult["ITEMS"],true)."</pre>";
$rsUF = CUserTypeEntity::GetList( array($by=>$order), array("ENTITY_ID"=>"USER") );
while($arUF=$rsUF->Fetch())
{
	if($arUF["USER_TYPE_ID"]=="enumeration")
	{
		$data[$arUF["FIELD_NAME"]] = array();
		$rsEnum = call_user_func_array(
			array("CUserTypeEnum", "getlist"),
			array(
				$arParams['arUserField'],
			)
		);
		while($arEnum = $rsEnum->GetNext())
		{
			//echo "<pre>".print_r($arEnum,true)."</pre>";
			$k = strpos($arEnum["VALUE"]," (");
			if($k!==false) $data[$arUF["FIELD_NAME"]][] = substr($arEnum["VALUE"],0,$k);
			else $data[$arUF["FIELD_NAME"]][$arEnum["ID"]] = $arEnum["VALUE"];
		}
	}
}

foreach(array_keys($arResult["ITEMS"]) as $k)
{
	foreach($data as $dk => $dv)
	{
		if(is_array($dv))
		{
			foreach($dv as $dvk => $dvv)
			if(in_array($dvk, $arResult["ITEMS"][$k][$dk]))
				$arResult["ITEMS"][$k][$dk."_VALUE"] .= (strlen($arResult["ITEMS"][$k][$dk."_VALUE"])>0?" / ":"").$dvv;
		}
		else $arResult["ITEMS"][$k][$dk."_VALUE"] = implode(" / ",$dv);
	}
}
?>