<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
$iblock_id = intval($_GET["iblock_id"]);
CModule::IncludeModule("iblock");
if ($order_id){
	
	$el = new CIBlockElement;
	
	$PROP = array();
	$PROP['ORDER_ID'] = $order_id;  // �������� � ����� 12 ����������� �������� "�����"
	$PROP['ADD_PRICE'] = intval($_GET["price"]);        // �������� � ����� 3 ����������� �������� 38
	$PROP['USER_ID'] = $USER->GetID();
	
	if($iblock_id==12){
		$act ="Y";
	}else{
		$act ="N";
	}
	if($iblock_id==12){
	    
	    $date = $_GET["date"];
	    if(strtotime($date)>time()){
	        $date = date("d.m.Y");
	    }
	    
		$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
				"IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
				"IBLOCK_ID"      => $iblock_id,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "�������",
				"ACTIVE"         => $act,  
		    "DATE_ACTIVE_FROM" =>ConvertDateTime($date,CSite::GetDateFormat("FULL"))          // �������
				
		);
	}else{
		$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
				"IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
				"IBLOCK_ID"      => $iblock_id,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => "�������",
				"ACTIVE"         => $act,  				
		);
	}
	
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
		echo $PRODUCT_ID;
	else
		echo 0;
}
?>