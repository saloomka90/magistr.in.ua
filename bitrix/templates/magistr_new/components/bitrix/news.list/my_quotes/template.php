<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

unset($arParams["PROPERTY_CODE"][array_search("parent", $arParams["PROPERTY_CODE"])]);
?>
<?if(count($arResult["ITEMS"])>0):?>
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>
	<form class="zayavki" onsubmit="return confirm('<?=GetMessage("CONFIRM_DELETE")?>');" action="" method="POST">
	<div class="table_box">
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		<thead>
			<tr>
				<td></td>
				<td>����</td>
				<td>����� ������</td>
				<td>��������</td>
				<td>�������</td>
				<td>E-mail</td>
				<td>�������</td>
			</tr>
		</thead>
		<tbody>
		<?if (count($arResult["ELEMENTS"]) > 0):$cnt=(intval($_GET["PAGEN_1"])>1?intval($_GET["PAGEN_1"])-1:0)*$arParams["NAV_ON_PAGE"];?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<tr class="am1">
				<td>
					<input type="checkbox" onclick="checkSelection('my_quotes', 'top_chck')" name="delete_me[]" value="<?=$arItem["ID"]?>" />
				</td>
				<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
				<td class="id">
					<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
					<div><b><?=GetMessage("IBLOCK_FIELD_ID")?></b> <?=$arItem["ID"];?></div>
				</td>
				<?endif?>
				<?if($arItem["DISPLAY_PROPERTIES"]["parent"]):?>
				<td class="sub1">
					<?
						$rs = CIBlockElement::GetList(array("id"=>"asc"), array(
							"ID"=>$arItem["DISPLAY_PROPERTIES"]["parent"]["VALUE"]),
							false,
							array("nTopCount"=>1),
							array("NAME","DETAIL_PAGE_URL","PROPERTY_NOTE")
						);
						if($el=$rs->GetNext()) echo '<a href="'.$el["DETAIL_PAGE_URL"].'" target="_blank">'.$el["NAME"].'</a>';
					?>
					<br/><span><?=$el["PROPERTY_NOTE_VALUE"]?></span>
					<?unset($arItem["DISPLAY_PROPERTIES"]["parent"]);?>
				</td>
				<?endif?>
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<td>
					<?echo $arItem["NAME"]?>
				</td>
				<?endif;?>
				<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
				<td>
					<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
						<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
					<?else:?>
						<?=$arProperty["DISPLAY_VALUE"];?>
					<?endif?>
				</td>
				<?endforeach;?>
			</tr>
		<?endforeach;?>
		<?else:?>
			<tr>
				<td>
					<div class="alert"><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></div>
				</td>
			</tr>
		<?endif?>
		</tbody>
	</table>
	</div>
	<input type="submit" name="submit_delete_quotes" value="<?=GetMessage("IBLOCK_DELETE_QUOTES")?>" />
	</form>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>
