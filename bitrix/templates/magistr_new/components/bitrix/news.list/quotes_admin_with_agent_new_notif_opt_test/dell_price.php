<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/func.php");

decode_params($_POST, "windows-1251");
decode_params($_GET, "windows-1251");
decode_params($_REQUEST, "windows-1251");

global $USER, $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
if(!defined("LANG_ID") && strlen($_GET["lang"])>0) define("LANG_ID",$_GET["lang"]);
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$order_id = intval($_GET["o_id"]);
CModule::IncludeModule("iblock");
if ($order_id){
	CIBlockElement::Delete($order_id);
}
?>