<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
foreach($arResult["ITEMS"] as $arItem)
	if($arItem["PROPERTIES"]["agent_id"]["VALUE"]>0)
		$ids[] = $arItem["PROPERTIES"]["agent_id"]["VALUE"];

$arUsers = array();
$rsUser = CUser::GetList($by="",$order="",array("ID"=>implode("|",$ids)));
while($arUser=$rsUser->Fetch())
	$arUsers[$arUser["ID"]] = $arUser;

//echo "<pre>".print_r($arUsers,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]+1?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]+1?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<!-- <td style="width:0% !important;"><?=GetMessage("IBLOCK_PROP_date_to")?></td> -->
		<td style="width:80% !important;"><?=GetMessage("IBLOCK_PROP_topic")?></td>
		<!--<td><?=GetMessage("IBLOCK_PROP_type")?></td>-->
		
	
	<td>������</td>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{
		case 22:
			$style = ' style="background:#fa832b;"';
			break;
		case 23:
			$style = ' style="background:#99CCFF;"';
			break;
		case 24:
			if(is_array($arItem["PROPERTIES"]["partner_paid"]["VALUE_ENUM_ID"]) && reset($arItem["PROPERTIES"]["partner_paid"]["VALUE_ENUM_ID"])>0)
			{
				$arItem["DISPLAY_PROPERTIES"]["state"]["VALUE"] = $arItem["PROPERTIES"]["partner_paid"]["VALUE"];
				$style = ' style="background:#cccccc;"';
			}
			else
				$style = ' style="background:#b4e828;"';
			break;
		default:
			if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0) $style = ' style="background:#eeff61;"';
			else $style = '';
			break;
	}
	?>
	<tr <?=$style?>>

		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
			<br>
			ID: <b><?=$arItem["ID"]; ?></b>
		</td>
		<?endif?>
		<!-- <td style="width:0%;text-align:center;">
			<?echo $arItem["PROPERTIES"]["period"]["VALUE"]?>
		</td> -->
		<?//if($arItem["DISPLAY_PROPERTIES"]["topic"]):?>
		<td>
			<?
					$link_o = '<a href="javascript:void(0)" onclick="showOrderDetails('.$arItem["ID"].');">';
					$link_c = '</a>';
			echo $link_o.$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"].$link_c;?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["topic"]);?>
		</td>
		<?//endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["type"]):?>
		<!--<td>
			<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["type"]);?>
		</td>-->
		<?//endif?>
		
		<?/*if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<td>
			<?echo $arItem["NAME"]?>
		</td>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<td>
			<?echo $arItem["PREVIEW_TEXT"];?>
		</td>
		<?endif;*/?>

		<td>
			<?		$AutorSumm = false;
					if($arItem["PROPERTIES"]["performer"]["VALUE"]>0)
					{
						$rsUser = CUser::GetByID($arItem["PROPERTIES"]["performer"]["VALUE"]);
						if($arUser=$rsUser->Fetch())
						{
							$rsQuote = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"PROPERTY_order_id"=>$arItem["ID"],"ACTIVE"=>"Y","PROPERTY_author_id"=>$arUser["ID"]),false,array("nTopCount"=>1),array("ID","PROPERTY_SUMM"));
							$arQuote = $rsQuote->Fetch();
							$AutorSumm = $arQuote["PROPERTY_SUMM_VALUE"];
						}
					}
					
					
					if(!$AutorSumm){
						$AutorSumm = $arItem["PROPERTIES"]["author_sum_total"]["VALUE"];
					}
?>
			
			<?if( $arItem["DISPLAY_PROPERTIES"]["sum_paid"]["VALUE"] && $arItem["DISPLAY_PROPERTIES"]["sum_total"]["VALUE"]):?>
				�볺��: <?=$arItem["DISPLAY_PROPERTIES"]["sum_paid"]["VALUE"]?> � <?=$arItem["DISPLAY_PROPERTIES"]["sum_total"]["VALUE"]?> ���.<br/>
			<?endif; ?>
			
			<?if($AutorSumm):?>
				������: <?=$AutorSumm?> ���.<br/>
			<?endif; ?>
			
			<?if($arItem["DISPLAY_PROPERTIES"]["sum_total"]["VALUE"] &&  $AutorSumm ):?>
				������: <?= ($arItem["DISPLAY_PROPERTIES"]["sum_total"]["VALUE"] - $AutorSumm)/2 ?> ���.<br/>
			<?endif; ?>
		</td>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>

<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 650,
			processHandler: function(settings) {
				
			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id, other_params)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'+(other_params&&typeof(other_params)!==undefined?other_params:'')
		});
		return false;
	}

	//]]>
	</script>