<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$dir = '/bitrix/templates/magistr_new/components/bitrix/catalog.filter/quotes_admin';
?>
<script type="text/javascript" src="<?=$dir?>/jquery.autocomplete.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#<?=$arParams["FILTER_NAME"]?>_pf_author').autocomplete('<?=$dir?>/autocomplete.php', {
		delay: 10,
		minChars: 2,
		matchSubset: 1,
		autoFill: false,
		matchContains: 1,
		cacheLength: 10,
		selectFirst: false,
		formatItem: liFormat,
		maxItemsToShow: 10,
		onItemSelect: selectItem
	});
});
function liFormat (row, i, num) {
	return row[0];
}
function selectItem(li) {
	if( li == null ) var sValue = '';
	else var sValue = li.extra;
	$('#<?=$arParams["FILTER_NAME"]?>_pf_performer').val(sValue);
}
</script>
<link href="<?=$dir?>/styles.css" type="text/css" rel="stylesheet" />
<?
global $MESS;
//echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
$ar_new_values = array("(���)"=>GetMessage("CUSTOM_TITLE_#0"),"������������"=>GetMessage("CUSTOM_TITLE_#1"),"��������� ����������"=>GetMessage("CUSTOM_TITLE_#2"),"���������� ���������"=>GetMessage("CUSTOM_TITLE_#3"),"��������"=>GetMessage("CUSTOM_TITLE_#4"),"����"=>GetMessage("CUSTOM_TITLE_#5"),"³������ ���������� ������"=>GetMessage("CUSTOM_TITLE_#6"),"������� ������"=>GetMessage("CUSTOM_TITLE_#7"),"������"=>GetMessage("CUSTOM_TITLE_#8"));

$ar_replacements = array(
	'#<input type=\"text\" name=\"'.$arParams["FILTER_NAME"].'_ff\[ID\]\[LEFT\]\" size="5" value="[^\"]*" />&nbsp;��&nbsp;<input type="text" name="'.$arParams["FILTER_NAME"].'_ff\[ID\]\[RIGHT\]" size="5" value="[^\"]*" />#Usi' => '<input type="text" name="'.$arParams["FILTER_NAME"].'_ff[ID]" value="'.($_REQUEST[$arParams["FILTER_NAME"]."_ff"]["ID"]["LEFT"]).'" />',
);
?>

<div class="autor_filter">
					
					<form name="arrFilterQuotesAdmin_form" method="get">
						<input class="id" type="text" name="arrFilterQuotesAdmin_ff[ID]" placeholder="ID" 
						<?if(isset($_GET["arrFilterQuotesAdmin_ff"]["ID"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_ff"]["ID"].'"';?>
						/>
						<input class="fio" type="text" name="arrFilterQuotesAdmin_ff[NAME]" placeholder="�.�.�." 
						<?if(isset($_GET["arrFilterQuotesAdmin_ff"]["NAME"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_ff"]["NAME"].'"';?>
						/>
						<input class="email" type="text" name="arrFilterQuotesAdmin_pf[email]" placeholder="E-mail" 
						<?if(isset($_GET["arrFilterQuotesAdmin_pf"]["email"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_pf"]["email"].'"';?>
						/>
						<input class="tema" type="text" name="arrFilterQuotesAdmin_pf[topic]" placeholder="����" 
						<?if(isset($_GET["arrFilterQuotesAdmin_pf"]["topic"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_pf"]["topic"].'"';?>
						/>
						
						<input type="text" onblur="if(this.value.length==0) jQuery('#arrFilterQuotesAdmin_pf_performer').val('');" name="arrFilterQuotesAdmin_pf[author]" id="arrFilterQuotesAdmin_pf_author"  autocomplete="off" class="ac_input autor" placeholder="�����"
						<?if(isset($_GET["arrFilterQuotesAdmin_pf"]["author"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_pf"]["author"].'"';?>
						/>
						<input type="hidden" name="<?=$arParams["FILTER_NAME"]?>_pf[performer]" id="<?=$arParams["FILTER_NAME"]?>_pf_performer" value="<?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["performer"])?>" />
						<div class="dodate">
							<input class="date" type="text" name="arrFilterQuotesAdmin_DATE_ACTIVE_FROM_1" id="do_date"  placeholder="�"
							<?if(isset($_GET["arrFilterQuotesAdmin_DATE_ACTIVE_FROM_1"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_DATE_ACTIVE_FROM_1"].'"';?>
							/>
						</div>
						<div class="dodate">
							<input class="date" type="text" name="arrFilterQuotesAdmin_DATE_ACTIVE_FROM_2" id="do_date_2"   placeholder="��"
							<?if(isset($_GET["arrFilterQuotesAdmin_DATE_ACTIVE_FROM_2"]))echo 'value="'.$_GET["arrFilterQuotesAdmin_DATE_ACTIVE_FROM_2"].'"';?>
							/>
						</div>
						<select name="arrFilterQuotesAdmin_pf[state]">
							<option value="" <?if($_GET["arrFilterQuotesAdmin_pf"]["state"]=="")echo 'selected';?>>(�� �������)</option>
							<option value="21" 
							<?if($_GET["arrFilterQuotesAdmin_pf"]["state"]=="21")echo 'selected';?>
							>������������</option>
							<option value="22"
							<?if($_GET["arrFilterQuotesAdmin_pf"]["state"]=="22")echo 'selected';?>
							>��������� ����������</option>
							<option value="23"
							<?if($_GET["arrFilterQuotesAdmin_pf"]["state"]=="23")echo 'selected';?>
							>���������� ���������</option>
							<option value="24"
							<?if($_GET["arrFilterQuotesAdmin_pf"]["state"]=="24")echo 'selected';?>
							>��������</option>
						</select>
						<input type="submit" name="set_filter" class="af_button" value="��" />
						<a href='/quotes_admin/'class="au_remove" ></a>
						
						<input type="hidden" name="set_filter" value="Y">
					</form>
				</div>	