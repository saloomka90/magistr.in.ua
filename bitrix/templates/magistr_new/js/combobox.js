  $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
    	  //console.log(0);
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
    	  
    	//console.log(1);
    	
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
		var select = this.element.hide();
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .attr( "type", "text" )
          .attr( "class", "input-dropdown" )
		.attr("id", 'input_' + select.attr("id"))
		.attr("name", 'input_' + select.attr("name"))
		.attr("placeholder", select.attr("data-placeholder"))
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
    	  
    	  //console.log(2);
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
    	  
    	  console.log(3);
    	  
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          var stext = $( this ).text() + ' && '+$( this ).attr('data-ru');
          if ( this.value && ( !request.term || matcher.test(stext) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
    	  //console.log(4);
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }

      },
 
      _destroy: function() {
    	  //console.log(5);
        this.wrapper.remove();
        this.element.show();
      }
    });
  });