<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$dir = substr(dirname(__FILE__), strlen($_SERVER["DOCUMENT_ROOT"]));
?>
<script type="text/javascript" src="<?=$dir?>/jquery.autocomplete.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#<?=$arParams["FILTER_NAME"]?>_pf_author').autocomplete('<?=$dir?>/autocomplete.php', {
		delay: 10,
		minChars: 2,
		matchSubset: 1,
		autoFill: false,
		matchContains: 1,
		cacheLength: 10,
		selectFirst: false,
		formatItem: liFormat,
		maxItemsToShow: 10,
		onItemSelect: selectItem
	});
});
function liFormat (row, i, num) {
	return row[0];
}
function selectItem(li) {
	if( li == null ) var sValue = '';
	else var sValue = li.extra;
	$('#<?=$arParams["FILTER_NAME"]?>_pf_performer').val(sValue);
}
</script>
<link href="<?=$dir?>/styles.css" type="text/css" rel="stylesheet" />
<?
global $MESS;
//echo "<pre>".print_r($arResult, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));
$ar_new_values = array("(���)"=>GetMessage("CUSTOM_TITLE_#0"),"������������"=>GetMessage("CUSTOM_TITLE_#1"),"��������� ����������"=>GetMessage("CUSTOM_TITLE_#2"),"���������� ���������"=>GetMessage("CUSTOM_TITLE_#3"),"��������"=>GetMessage("CUSTOM_TITLE_#4"),"����"=>GetMessage("CUSTOM_TITLE_#5"),"³������ ���������� ������"=>GetMessage("CUSTOM_TITLE_#6"),"������� ������"=>GetMessage("CUSTOM_TITLE_#7"),"������"=>GetMessage("CUSTOM_TITLE_#8"));

$ar_replacements = array(
	'#<input type=\"text\" name=\"'.$arParams["FILTER_NAME"].'_ff\[ID\]\[LEFT\]\" size="5" value="[^\"]*" />&nbsp;��&nbsp;<input type="text" name="'.$arParams["FILTER_NAME"].'_ff\[ID\]\[RIGHT\]" size="5" value="[^\"]*" />#Usi' => '<input type="text" name="'.$arParams["FILTER_NAME"].'_ff[ID]" value="'.($_REQUEST[$arParams["FILTER_NAME"]."_ff"]["ID"]["LEFT"]).'" />',
);
?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?//echo $arResult["FORM_ACTION"]?>" method="get">
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
	<table class="data-table" cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td colspan="2" align="center"><?=GetMessage("IBLOCK_FILTER_TITLE")?></td>
		</tr>
		<tr><td colspan="2">

<table width="100%"><tr><td width="50%">
<table>
		<?
		$v_cnt = 0;
		foreach($arResult["ITEMS"] as $cnt => $arItem):?>
			<?
			if(!array_key_exists("HIDDEN", $arItem)):
				if($v_cnt>0 && $v_cnt%4==0) echo '</table></td><td><table>';
				
				$code = "";
				$cnt_in = 0;
				foreach($arResult["arrProp"] as $vals):
					if($cnt_in<$cnt):
						$cnt_in++;
						continue;
					else:
						$code = $vals["CODE"];
						break;
					endif;
				endforeach;
				$title = GetMessage("CUSTOM_TITLE_".$code);
				if(strlen($title)<=0) $title = GetMessage("CUSTOM_TITLE_".$arItem["NAME"]);
				if(strlen($title)<=0) $title = $arItem["NAME"];
				foreach($ar_new_values as $key=>$value):
					if(strlen($value)>0) $arItem["INPUT"] = str_replace($key,$value,$arItem["INPUT"]);
				endforeach;

				foreach($ar_replacements as $ar_rk => $ar_rv):
					$arItem["INPUT"] = preg_replace($ar_rk,$ar_rv,$arItem["INPUT"]);
				endforeach;
				?>
				<tr>
					<td valign="top"><?=$title?>:</td>
					<td valign="top"><?=$arItem["INPUT"]?></td>
				</tr>
				<?$v_cnt++;?>
			<?endif?>
		<?endforeach;?>
		<tr>
			<td valign="top">�����:</td>
			<td valign="top">
				<input type="text" onblur="if(this.value.length==0) jQuery('#<?=$arParams["FILTER_NAME"]?>_pf_performer').val('');" name="<?=$arParams["FILTER_NAME"]?>_pf[author]" id="<?=$arParams["FILTER_NAME"]?>_pf_author" value="<?=htmlspecialchars($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["author"])?>" />
				<input type="hidden" name="<?=$arParams["FILTER_NAME"]?>_pf[performer]" id="<?=$arParams["FILTER_NAME"]?>_pf_performer" value="<?=intval($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["performer"])?>" />
			</td>
		</tr>
</table>
</td></tr></table>

		</td></tr>
		<tr>
			<td colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
		</tr>
	</table>
</form>