<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link href="/bitrix/templates/magistr/css/nyroModal.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/bitrix/templates/magistr/js/jquery.nyroModal-1.6.2.pack.js"></script>
<?
global $MESS;
//echo "<pre>".print_r($arParams, true)."</pre>";
include(dirname(__FILE__)."/lang/".LANG_ID."/".basename(__FILE__));

$ids = array();
foreach($arResult["ITEMS"] as $arItem) $ids[] = $arItem["ID"];

$user_responses = array();
$rs_ur = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y","PROPERTY_author_id"=>$USER->GetID(),"@PROPERTY_order_id"=>$ids),false,false,array("ID","NAME","PREVIEW_TEXT","PROPERTY_author_id","PROPERTY_summ","PROPERTY_refused","PROPERTY_order_id"));
while($ar_ur=$rs_ur->GetNext()) $user_responses[$ar_ur["PROPERTY_ORDER_ID_VALUE"]] = $ar_ur;

//echo "<pre>".print_r($user_responses,true)."</pre>";

$cur_dir = substr(dirname(__FILE__),strlen($_SERVER["DOCUMENT_ROOT"]));

unset($arParams["PROPERTY_CODE"][array_search("topic", $arParams["PROPERTY_CODE"])]);
unset($arParams["PROPERTY_CODE"][array_search("type", $arParams["PROPERTY_CODE"])]);
?>	<script type="text/javascript">
	//<![CDATA[
	// Demo NyroModal
	$(function() {
		$.nyroModalSettings({
			//debug: true,
			width: 600,
			processHandler: function(settings) {

			},
			beforeHideContent: function(elts, settings, callback) {
				if(!!window['reload_page'] && window['reload_page']==1) {
					location.href = '<?=$_SERVER["REQUEST_URI"]?>';
				}
				else callback();
			}
		});
	});
	function showOrderDetails(order_id)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&lang=<?=LANG_ID?>'
		});
		return false;
	}
	function refuse_order(order_id,comment)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&refuse=Y'+'&lang=<?=LANG_ID?>'+(comment&&typeof(comment)!=undefined?'&comment='+escape(comment):'')
		});
		return false;
	}
	function accept_order(order_id,summ,comment)
	{
		$.nyroModalManual({
			url: '<?=$cur_dir?>/order_detail.php?o_id='+order_id+'&summ='+escape(summ)+'&accept=Y'+'&lang=<?=LANG_ID?>'+(comment&&typeof(comment)!=undefined?'&comment='+escape(comment):'')
		});
		return false;
	}
	function onlyDigits(e) {
		e = e || event;

		if (e.ctrlKey || e.altKey) return;

		var chr = getChar(e);
		if (!isNaN(chr) && chr == null) return;

		if (isNaN(chr) || chr < '0' || chr > '9') {
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	}
	function getChar(event) {
		if (event.which == null) {
			return String.fromCharCode(event.keyCode) // IE
		} else if (event.which!=0 && event.charCode!=0) {
			return String.fromCharCode(event.which)   // the rest
		} else {
			return null // special key
		}
	}


	//]]>
	</script>
<?if(count($arResult["ITEMS"])>0):?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<table class="rounded" id="my_quotes_new">
<thead><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></thead>
<tfoot><tr><td colspan="<?=$arResult["COLS_COUNT"]?>"><div class="l"><div></div></div><div class="r"><div></div></div></td></tr></tfoot>
<tbody>
	<tr class="head">
		<?if($arParams["DISPLAY_PICTURE"]!="N"):?><td></td><?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N"):?><td style="width:0%;"><?=GetMessage("IBLOCK_FIELD_DATE")?></td><?endif;?>
		<td style="width:100% !important;"><?=GetMessage("IBLOCK_PROP_topic")?></td>
		<td><?=GetMessage("IBLOCK_PROP_type")?></td>
		<td><?=GetMessage("IBLOCK_PROP_response")?></td>
		<?foreach($arParams["FIELD_CODE"] as $key):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_FIELD_".$key));?>
			</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $key):if(strlen($key)>0):?>
			<td>
				<?=str_replace(" ", "&nbsp;", GetMessage("IBLOCK_PROP_".$key));?>
			</td>
		<?endif;endforeach;?>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
// echo "<pre>".print_r($arItem['PROPERTIES']['state'],true)."</pre>";
		/**

	**/
	// if($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==22){
	// 	unset($arItem);
	// }elseif($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==23){
	// 	unset($arItem);
	// }elseif($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==25){
	// 	unset($arItem);
	// }elseif($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==24 && $arItem["PROPERTIES"]["performer"]["VALUE"]!=$USER->GetID()){
	// 	unset($arItem);
	// }

		/**

	**/
	if(/*(!is_array($user_responses[$arItem["ID"]]) || !sizeof($user_responses[$arItem["ID"]])) &&*/ $arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==21||($arItem["PROPERTIES"]["performer"]["VALUE"]==$USER->GetID()&&$arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"]==23)):
		$link_o = '<a href="javascript:void(0)" onclick="showOrderDetails('.$arItem["ID"].');">';
		$link_c = '</a>';
	else:
		$link_o = '';
		$link_c = '';
	endif;
//echo "<pre>".print_r($arItem["PROPERTIES"]["state"],true)."</pre>";
	switch($arItem["PROPERTIES"]["state"]["VALUE_ENUM_ID"])
	{
		case 22:
			$style = ' style="background:#fa832b;"';
			break;
		case 23:
			if($arItem["PROPERTIES"]["performer"]["VALUE"]==$USER->GetID()) $style = ' style="background:#99ccff;"';
			else $style = ' style="background:#b4e828;"';
			break;
		case 24:
			$style = ' style="background:#e6e6e6;"';
			break;
		default:
			if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0) $style = ' style="background:#eeff61;"';
			else $style = '';
			break;
	}
	?>
	<tr<?=$style?>>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<td><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></td>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		<td style="width:0%;text-align:center;">
			<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
			<div><b><?=GetMessage("IBLOCK_FIELD_ID")?></b> <?=$arItem["ID"];?><?//=sprintf("%07d",$arItem["ID"])?></div>
		</td>
		<?endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["topic"]):?>
		<td>
			<?=$link_o?><?=$arItem["DISPLAY_PROPERTIES"]["topic"]["DISPLAY_VALUE"]?><?=$link_c?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["topic"]);?>
		</td>
		<?//endif?>
		<?//if($arItem["DISPLAY_PROPERTIES"]["type"]):?>
		<td>
			<?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
			<?unset($arItem["DISPLAY_PROPERTIES"]["type"]);?>
		</td>
		<?//endif?>

		<td align="center">
			<?if(is_array($user_responses[$arItem["ID"]]) && sizeof($user_responses[$arItem["ID"]])):?>
				<?if($user_responses[$arItem["ID"]]["PROPERTY_REFUSED_ENUM_ID"]>0):?>
					<?=GetMessage("REFUSED")?>
				<?elseif(strlen($user_responses[$arItem["ID"]]["PROPERTY_SUMM_VALUE"])>0):?>
					<?=GetMessage("SUMM")?> <strong><?=$user_responses[$arItem["ID"]]["PROPERTY_SUMM_VALUE"]?></strong> ���.
				<?endif;?>
			<?endif;?>
		</td>

		<?/*if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<td>
			<?echo $arItem["NAME"]?>
		</td>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<td>
			<?echo $arItem["PREVIEW_TEXT"];?>
		</td>
		<?endif;*/?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
		<td align="center">
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
		</td>
		<?endforeach;?>
		<?foreach($arParams["PROPERTY_CODE"] as $pid):$arProperty = $arItem["DISPLAY_PROPERTIES"][$pid];?>
		<td align="center">
			<?
				if(is_array($arProperty["VALUE"]))
				{
					$value = implode("&nbsp;/&nbsp;", $arProperty["VALUE"]);
				}
				else
				{
					$value = $arProperty["VALUE"];
				}
				$value_text = GetMessage("IBLOCK_PROP_VALUES_".$value);
				if(strlen($value_text)<=0) $value_text = $value;
				echo $value_text;
			?>
		</td>
		<?endforeach;?>
	</tr>
<?endforeach;?>
</tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?else:?>
<?ShowError(GetMessage("NOT_FOUND"));?>
<?endif;?>