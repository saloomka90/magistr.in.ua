(function () {
    var originalAddClassMethod = jQuery.fn.addClass;
    var originalRemoveClassMethod = jQuery.fn.removeClass;
    jQuery.fn.addClass = function () {
        var result = originalAddClassMethod.apply(this, arguments);
        jQuery(this).trigger('classChanged');
        return result;
    }
    jQuery.fn.removeClass = function () {
        var result = originalRemoveClassMethod.apply(this, arguments);
        jQuery(this).trigger('classChanged');
        return result;
    }
})();

var data_count_load_file = 0;

function closePop() {
    $('.pop-background').removeClass('active');
    $('body').removeClass('pop-visible');
}

function showPop(pop) {
    $(pop).addClass('active');
    $('body').addClass('pop-visible');
}


let countpage;
let voriginality;


function order_calculator( infoap ) {

    if ($('[name="action"]').val() == 'order_add') {
        var calc_A = 0;
        var calc_B = 1;
        var calc_C = 1;
        var calc_D = 1;
        var calc_E = 1;
        var calc_F = 1;
        var calc_G = 1;
        var calc_H = 1;



        let form_t = $('#input_select-type').val();
        let form_s = $('#select-subject').val();
        let form_d = $('[name="datepicker"]').val();
        let form_c = $('[name="countPage"]').val();
        let form_p = $('[name="plagiarism"]').val();
        let form_o = $('[name="originality"]').val();
        let form_v = $('[name="institution"]').val();
        let form_z = $('#select-course').val();

        if (types_p[form_t]) {
        	var tftformat = JSON.parse(types_p[form_t]);
        	
        	//console.log(tftformat.PROPERTY_UMAX_VALUE, tftformat.PROPERTY_UMIN_VALUE, tftformat.PROPERTY_UBASE_VALUE);
        	
        	
        	
        	/*if(infoap){
        		
        		$('.i-originality').val(2 + '%');  
        		
        		//console.log(voriginality);
        		
        		voriginality.update({
           	     	min: 5,
                    max: 100,
                    from: 25, 
                });
        	}*/
        	
        	
        	
        	
        	
        	
            calc_A = JSON.parse(types_p[form_t]).PROPERTY_PRICE_VALUE;           
        } else {
            calc_A = JSON.parse(types_p['����']).PROPERTY_PRICE_VALUE;
            var tftformat = JSON.parse(types_p['����']);
        }


        if (predmet_p[form_s]) {
            calc_D = predmet_p[form_s];
        } else {
            calc_D = predmet_p['����'];
        }

        if (form_d) {
            let dts = form_d.split('/');
            dts = dts[1] + ' ' + dts[0] + ' ' + dts[2];
            let days = Math.floor((new Date(dts).setHours(0) - new Date) / 1000 / 60 / 60 / 24) + 1;



            

            if (tftformat) {

                if (Number(tftformat.PROPERTY_DTF0_95_VALUE) <= days && days <= Number(tftformat.PROPERTY_DTT0_95_VALUE)) {
                    calc_C = 0.95;
                } else if (Number(tftformat.PROPERTY_DTF1_VALUE) <= days && days <= Number(tftformat.PROPERTY_DTT1_VALUE)) {
                    calc_C = 1;
                } else if (Number(tftformat.PROPERTY_DTF1_05_VALUE) <= days && days <= Number(tftformat.PROPERTY_DTT1_05_VALUE)) {
                    calc_C = 1.05;
                } else if (Number(tftformat.PROPERTY_DTF1_1_VALUE) <= days && days <= Number(tftformat.PROPERTY_DTT1_1_VALUE)) {
                    calc_C = 1.1;
                } else if (Number(tftformat.PROPERTY_DTF1_2_VALUE) <= days && days <= Number(tftformat.PROPERTY_DTT1_2_VALUE)) {
                    calc_C = 1.2;
                } else {
                    calc_C = 1;
                }
            } else {
                calc_C = 1;
            }


        } else {
            calc_C = 1;
        }
        //TODO
        if (form_c) {

        	

            if (tftformat) {
            	
            	if((infoap != undefined && infoap.type == 'change' && $(infoap.target).attr('name') == 'input_type') || infoap == 'first'){
            		
            		
            		
	            	if (tftformat.PROPERTY_UMAX_VALUE != null && tftformat.PROPERTY_UMIN_VALUE != null && tftformat.PROPERTY_UBASE_VALUE != null  ) {
	            		//console.log(1111);
		            	 voriginality.update({
		            	     	min: tftformat.PROPERTY_UMIN_VALUE,
		            	     	max: tftformat.PROPERTY_UMAX_VALUE,
		            	     	from: tftformat.PROPERTY_UBASE_VALUE, 
		                 });
		            	 
		            	 
		            	 form_o = tftformat.PROPERTY_UBASE_VALUE;
	            	}else{
	            		//console.log(2222);
	            		voriginality.update({
	            	     	min: 30,
	            	     	max: 90,
	            	     	from: 30, 
	                 });
	            	}
            	}
            
                if (tftformat.PROPERTY_DPZT1_2_VALUE != null) {
                    if (countpage.options.max != tftformat.PROPERTY_DPZT1_2_VALUE) {
                       
                        countpage.update({
                            max: tftformat.PROPERTY_DPZT1_2_VALUE,
                            from: tftformat.PROPERTY_MID_COUNT_VALUE,
                        });
                        
                       
                    }
                } else {
                    if (countpage.options.max != 120) {
                        countpage.update({
                            max: 120,
                            from: 1,
                        });
                    }
                }
                form_c = $('[name="countPage"]').val();
                


                /*
                 * ERROOR
                 */
                /*
                $(".count-page").data("ionRangeSlider").update({
                    max: tftformat.PROPERTY_DPZT1_2_VALUE,
                    from: tftformat.PROPERTY_MID_COUNT_VALUE
                });
                */
                /*
                 * END_ERROR
                 */


                if (tftformat.PROPERTY_MID_COUNT_VALUE > 0) {
                    calc_H = form_c / tftformat.PROPERTY_MID_COUNT_VALUE;
                }

                
                

                if (Number(tftformat.PROPERTY_DPZF0_9_VALUE) <= form_c && form_c <= Number(tftformat.PROPERTY_DPZT0_9_VALUE)) {
                    calc_B = 1.2;
                } else if (Number(tftformat.PROPERTY_DPZF0_95_VALUE) <= form_c && form_c <= Number(tftformat.PROPERTY_DPZT0_95_VALUE)) {
                    calc_B = 1.1;
                } else if (Number(tftformat.PROPERTY_DPZF1_VALUE) <= form_c && form_c <= Number(tftformat.PROPERTY_DPZT1_VALUE)) {
                    calc_B = 1;
                } else if (Number(tftformat.PROPERTY_DPZF1_1_VALUE) <= form_c && form_c <= Number(tftformat.PROPERTY_DPZT1_1_VALUE)) {
                    calc_B = 0.9;
                } else if (Number(tftformat.PROPERTY_DPZF1_2_VALUE) <= form_c && form_c <= Number(tftformat.PROPERTY_DPZT1_2_VALUE)) {
                    calc_B = 0.8;
                } else {
                    calc_B = 1;
                }

                
            } else {
                calc_B = 1;
                if (countpage.options.max != 120) {
                    countpage.update({
                        max: 120,
                        from: 1,
                    });
                }
            }





        } else {
            calc_B = 1;
        }

        if (form_p == 'yes') {
            calc_E = prov_p['0-100'];
            form_o = form_o.substring(0, form_o.length - 1);            
            
           //* console.log(prov_p);            
            
            
            
            for(let i in prov_p){
            	 if (form_o >= i.split('-')[0] && form_o <= i.split('-')[1]) {            		 
            		 calc_E =  prov_p[i]
            	 }
            }
            
            /*if (form_o >= 30 && form_o <= 40) {
                calc_E = prov_p['30-40'];
            } else if (form_o > 40 && form_o <= 50) {
                calc_E = 1;
            } else if (form_o > 50 && form_o <= 60) {
                calc_E = 1.05;
            } else if (form_o > 60 && form_o <= 70) {
                calc_E = 1.1;
            } else if (form_o > 70 && form_o <= 80) {
                calc_E = 1.25;
            } else if (form_o > 80 && form_o <= 90) {
                calc_E = 1.35;
            }*/
            
            
            
        } else {
            calc_E = prov_p['0-100'];
        }

        
        if (vnz_p[form_v]) {
            calc_F = vnz_p[form_v];
        } else {
            calc_F = vnz_p['����'];
        }

        
        
        console.log(kurs_p);

        if (kurs_p[form_z]) {
            calc_G = kurs_p[form_z];
        } else {
            calc_G = 1;
        }



        console.log(calc_A, calc_B, calc_C, calc_D, calc_E, calc_F, calc_G, calc_H);


        let summ = calc_A * calc_B * calc_C * calc_D * calc_E * calc_F * calc_G * calc_H;
        if (summ > 0) {
            let min = Math.ceil(summ * 0.9 / 10) * 10;
            let max = Math.ceil(summ * 1.1 / 10) * 10;
            
            $('input[name="min_summ"]').val(min);
            $('input[name="max_summ"]').val(max);
            
            $('.approximate-cost__price').html(min + ' - ' + max + '<span>���</span>');
        } else {
            $('.approximate-cost__price').html(' - ');
        }
    }
}



jQuery(document).ready(function () {
	
	
	
	if($('.chat-form').length>0){
		
		let timerId = setInterval(function() 
		
		{
			
			
			
			
			$.ajax({
	            method: $('.chat-form').attr('method'),
	            url: '/client/chk_comment.php',
	            dataType: 'json',
	            data: $('.chat-form').serialize() + '&ajax=Y',
	        })
	        .done(function (msg) {
	        	
	        	
	        	$('input[name="time"]').val(msg.time);
	        	
	        	if(msg.update){
	        		location.reload();
	        	}
	        	
	        	if(msg.html){
	        		$('.chat__body').append(msg.html);
	        		$('.chat__body').scrollTop(9999999);
	        		
	        		/*var audio = new Audio();
					audio.preload = 'auto';
					audio.src = '/sound1.mp3';*/
	        		
	        		var audioElement = new Audio('/definite.mp3');
	    			audioElement.play();
					
	        	}
	           
	       	 //console.log('[��� ������]');
	             
	        });
			
			
			
		}, 5000);
		
		
		
	}


	
	$(".chat-form").submit(function(){
		
		
		 $.ajax({
             method: $(this).attr('method'),
             url: $(this).attr('action'),
             
             data: $(this).serialize() + '&ajax=Y',
         })
         .done(function (msg) {
            
        	// console.log(1111,$(".chat-form").find('textarea').val());
                 
            	 $(".chat-form").find('textarea').val('');
            	 $(".chat-form").find('.file-list').html('');
            	 
            	 $(".chat__body").append(msg);
            	 $('.chat__body').scrollTop(9999999);
              
         });
		 return false;
	})


    $("input[type='tel']").inputmask("+38 (999) 999-99-99");



    $(".form-item_phone-disabled").click(function () {
        $(this).addClass('show');
    });
    $(".single-order__show-detal").click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.detal').slideToggle(300);
        if ($(this).hasClass('active')) {
            $('span', this).text('������� ����� ����������');
        } else {
            $('span', this).text('�������� ����� ����������');
        }
    });
    $(".faq__question").click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next().slideUp(150);
        } else {
            $(this).addClass('active');
            $(this).next().slideDown(150);
        }
    });


    $('.section__tub-item').click(function () {
        var tub = $(this);
        if (!tub.hasClass('section__tub-item_disabled')) {
            if (!tub.hasClass('section__tub-item_download')) {
                if (!tub.hasClass('active')) {
                    $('.section').removeClass('current');
                    tub.closest('.section').addClass('current');
                    $('.current .section__tub-item, .current .section__tub-content').removeClass('active');
                    tub.addClass('active');
                    $('.section__tub-content[data-tub="' + tub.data('tub') + '"]').addClass('active');
                } else {
                    tub.removeClass('active');
                    $('.section').removeClass('current');
                    tub.closest('.section').addClass('current');
                    $('.current .section__tub-item, .current .section__tub-content').removeClass('active');
                }
            } else {
                alert('������������ �����!');
            }
        }
    });


    //POPAPS------------------------------------------------ 
    $('body').on('click', '.js-cancel-order', function (e) {
        e.preventDefault();
        var pop = '.pop-cancel-order';
        showPop(pop);
    });
    $('body').on('click', '.js-tip', function (e) {
        e.preventDefault();
        var pop = '.pop-success-tip';
        showPop(pop);
    });
    $('body').on('click', '.js-new-review', function (e) {
        e.preventDefault();
        var pop = '.pop-new-review';
        showPop(pop);
    });
    $('body').on('click', '.js-pop-1', function (e) {
        e.preventDefault();
        var pop = '.pop-pop-1';
        showPop(pop);
    });
    $('body').on('click', '.js-pop-2', function (e) {
        e.preventDefault();
        var pop = '.pop-pop-2';
        showPop(pop);
    });
    $('body').on('click', '.js-pop-3', function (e) {
        e.preventDefault();
        var pop = '.pop-pop-3';
        showPop(pop);
    });


    if ($('.pop-background.pop-success').length > 0) {

        var pop = '.pop-success';
        showPop(pop);
    }

    $('body').on('click', '.js-success', function (e) {
        e.preventDefault();
        var pop = '.pop-success';
        showPop(pop);
    });
    $('.close, .button_ok, .js-close').click(function (e) {
        e.preventDefault();
        closePop();
    });
    $(document).click(function (event) {
        if ($(event.target).closest(".pop-scroll, .button, .link-style, .menu-mayk li a, .js-tip").length) return;
        closePop();
        event.stopPropagation();
    });

    //RATING------------------------------------------------ 
    if ($('.rating-archive').length > 0) {
        $(".rating-archive").starRating({
            readOnly: true,
            emptyColor: '#DBDBDB',
            strokeColor: '#ffffff',
            strokeWidth: 0,
            starSize: 15,
            starGradient: {
                start: '#8E817D',
                end: '#8E817D'
            }
        });
    }
    if ($('.your-rating__stars').length > 0) {
        $(".your-rating__stars").starRating({
            readOnly: true,
            emptyColor: '#EBE2CA',
            strokeColor: '#FFFAEB',
            strokeWidth: 0,
            starSize: 22,
            starGradient: {
                start: '#F4D569',
                end: '#F4D569'
            }
        });
    }
    if ($('.rating__stars').length > 0) {
        $(".rating__stars").starRating({
            emptyColor: '#EBE2CA',
            strokeColor: '#FFFAEB',
            hoverColor: '#F4D569',
            useFullStars: true,
            ratedColor: '#F4D569',
            strokeWidth: 0,
            starSize: 33,
            disableAfterRate: false,
            starGradient: {
                start: '#F4D569',
                end: '#F4D569'
            },
            callback: function (currentRating, $el) {
                $('.button_send-rating').removeAttr('disabled');
            }
        });
    }


    $('.add-file').click(function (e) {
        e.preventDefault();
        //var id = $(this).attr('data-file');
        $('.file-input:last').click();
    });
    
    
    $(document).on('change', '.file-input.order', function () {
    	
    	
    	/*let cnt = $('[type="submit"]').attr('data-cnt');
    	if(cnt == undefined){
    		cnt = 0;
    	}*/
    	data_count_load_file += $(this)[0].files.length; 
    	//$('[type="submit"]').attr('data-cnt',data_count_load_file);
    	$('[type="submit"]').attr('disabled','disabled');
    	
    	
    	
        for (let i = 0; i < $(this)[0].files.length; i++) {

            // ������� ����� � ����������� ����������
            let form = new FormData();
            form.append("sessid", $('input[name="sessid"]').val());
            form.append("action", "ajax_file");
            form.append("file", $(this)[0].files[i]);
            // ���������� ����� xhr
            let xhr = new XMLHttpRequest();
            xhr.onload = function () {
            	
            	
                //console.log(xhr.responseText);
                var dtts = JSON.parse(xhr.responseText);
                $('.file-list__item[data-name="' + dtts.name + '"]').find('.file-list__status').html('<a href="#" class="remove-file"></a>');
                $('.file-list__item[data-name="' + dtts.name + '"]').find('input[name="file_src[]"]').val(dtts.id);
                
                //
                
                
                //let cnt = $('[type="submit"]').attr('data-cnt');
                data_count_load_file--;
                
                
                //console.log($('[type="submit"]').attr('data-cnt'));
                
               
                
                
               // $('[type="submit"]').attr('data-cnt',cnt);
                if(data_count_load_file == 0){
                	$('[type="submit"]').removeAttr('disabled');
                }
                
            };
            xhr.open("post", "/client/order/", true);
            xhr.send(form);
            $('.file-list__add-item').before('<div class="file-list__item" data-name="' + $(this)[0].files[i].name + '">\
	                <div class="file-list__name">' + $(this)[0].files[i].name + '</div>\
	                <div class="file-list__status"><div id="circle2"></div></div>\
	                <input type="hidden" name="file_src[]" value="">\
	            </div>');

        }
        $('.file-input').val('');
        
        
        

        /*
		$(this).parent().find('.file-list__name').text($(this)[0].files[0].name);
		$(this).parent().show();
		
		
		
		//'+$(this)[0].files[0].name+'
		
		$('.file-list__add-item').before('<div class="file-list__item" style="display:none;">\
                <div class="file-list__name"></div>\
                <div class="file-list__status"><a href="#" class="remove-file"></a></div>\
                <input type="file" name="file[]" class="file-input">\
            </div>');
		
		*/
        /*var id = $(this).attr('id');
		if ($(this)[0].files[0]) {
			$('.psevdo-file[data-file="#'+id+'"] span').text($(this)[0].files[0].name);
		} else {
			$('.psevdo-file[data-file="#'+id+'"] span').text('���������� �������:');
		}*/
    });
    $(document).on('click', '.remove-file', function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();

    });
 ///////////////   
 $(document).on('change', '.file-input.chat', function () {
    	
    	
    	/*let cnt = $('[type="submit"]').attr('data-cnt');
    	if(cnt == undefined){
    		cnt = 0;
    	}*/
    	data_count_load_file += $(this)[0].files.length; 
    	//$('[type="submit"]').attr('data-cnt',data_count_load_file);
    	$('[type="submit"]').attr('disabled','disabled');
    	
    	
    	
        for (let i = 0; i < $(this)[0].files.length; i++) {

            // ������� ����� � ����������� ����������
            let form = new FormData();
            form.append("sessid", $('input[name="sessid"]').val());
            form.append("action", "ajax_file");
            form.append("file", $(this)[0].files[i]);
            // ���������� ����� xhr
            let xhr = new XMLHttpRequest();
            xhr.onload = function () {
            	
            	
                //console.log(xhr.responseText);
                var dtts = JSON.parse(xhr.responseText);
                $('.file-list__item[data-name="' + dtts.name + '"]').find('.file-list__status').html('<a href="#" class="remove-file"></a>');
                $('.file-list__item[data-name="' + dtts.name + '"]').find('input[name="file_src[]"]').val(dtts.id);
                
                //
                
                
                //let cnt = $('[type="submit"]').attr('data-cnt');
                data_count_load_file--;
                
                
                //console.log($('[type="submit"]').attr('data-cnt'));
                
               
                
                
               // $('[type="submit"]').attr('data-cnt',cnt);
                if(data_count_load_file == 0){
                	$('[type="submit"]').removeAttr('disabled');
                }
                
            };
            xhr.open("post", "/client/order/", true);
            xhr.send(form);
            $('.file-list').append('<div class="file-list__item" data-name="' + $(this)[0].files[i].name + '">\
	                <div class="file-list__name">' + $(this)[0].files[i].name + '</div>\
	                <div class="file-list__status"><div id="circle2"></div></div>\
	                <input type="hidden" name="file_src[]" value="">\
	            </div>');

        }
        $('.file-input').val('');
        
        
        

        /*
		$(this).parent().find('.file-list__name').text($(this)[0].files[0].name);
		$(this).parent().show();
		
		
		
		//'+$(this)[0].files[0].name+'
		
		$('.file-list__add-item').before('<div class="file-list__item" style="display:none;">\
                <div class="file-list__name"></div>\
                <div class="file-list__status"><a href="#" class="remove-file"></a></div>\
                <input type="file" name="file[]" class="file-input">\
            </div>');
		
		*/
        /*var id = $(this).attr('id');
		if ($(this)[0].files[0]) {
			$('.psevdo-file[data-file="#'+id+'"] span').text($(this)[0].files[0].name);
		} else {
			$('.psevdo-file[data-file="#'+id+'"] span').text('���������� �������:');
		}*/
    });

    //HELP------------------------------------------------ 
    $('.info__icon').click(function () {
        $('.info').removeClass('show');
        $(this).parent().addClass('show');
    });
    $('.info__close').click(function () {
        setTimeout(function () {
            $('.info, .form-item_phone-disabled').removeClass('show');
        }, 10);
    });
    $(document).click(function (event) {
        if ($(event.target).closest(".info, .form-item_phone-disabled").length) return;
        $('.info, .form-item_phone-disabled').removeClass('show');
        event.stopPropagation();
    });



    //SLIDER------------------------------------------------ 
    $(".count-page").ionRangeSlider({
        min: 1,
        max: 120,
        skin: "round",
        grid: true,
        grid_num: 1,
        grid_snap: false,
        hide_min_max: true,
        step: 1,
        onChange: function (data) {
            $('.i-count-page').val(data.from);
        },
        onUpdate: function (data) {
            $('.i-count-page').val(data.from);
        }
    });
    countpage = $(".count-page").data("ionRangeSlider");




    $(".originality").ionRangeSlider({
        min: 30,
        max: 90,
        skin: "round",
        grid: true,
        grid_num: 2,
        grid_snap: false,
        hide_min_max: true,
        postfix: " %",
        step: 1,
        onChange: function (data) {
            $('.i-originality').val(data.from + '%');
            order_calculator();
        },
        onUpdate: function (data) {
            $('.i-originality').val(data.from + '%');
            order_calculator();
        }
    });
    
    
    voriginality = $(".originality").data("ionRangeSlider");


    $('.icon-menu, .navigation-bg').click(function () {
        $('.icon-menu, .navigation, .navigation-bg').toggleClass('active');
    });

    //SELECT------------------------------------------------
    $('#select-plagiarism, #select-course').niceSelect({});


    $("#select-subject").combobox();
    $("#select-type").combobox();
    $("#select-institution").combobox();


    $('#input_select-subject, #input_select-type, #input_select-institution').focus(function () {
        $(this).parent().addClass('input-active');
        $(this).autocomplete("search");
    });
    $('#input_select-subject, #input_select-type, #input_select-institution').focusout(function () {
        $(this).parent().removeClass('input-active');
        $(this).change();
    });

    $('#input_select-type').on('change', function () {
        var value = $(this).val();
        /*if (value == '����������') {
            countpage.update({
                max: 300
            });
        } else {
            countpage.update({
                max: 120
            });                
        }*/
        if ((value == '�����������') || (value == '��������') || (value == '������������') || (value == '�������')) {
            $('.need-tutorial').addClass('show');
        } else {
            $('.need-tutorial').removeClass('show');
        }
    });

    //DATEPICKER------------------------------------------------
    $.fn.datepicker.language['uk'] = {
        days: ['�����', '��������', '³������', '������', '�������', '�\'������', '������'],
        daysShort: ['���', '���', '³�', '���', '���', '�\'�', '���'],
        daysMin: ['��', '��', '��', '��', '��', '��', '��'],
        months: ['ѳ����', '�����', '��������', '������', '�������', '�������', '������', '�������', '��������', '�������', '��������', '�������'],
        monthsShort: ['ѳ�', '���', '���', '��', '���', '���', '���', '���', '���', '���', '���', '���'],
        today: '�������',
        clear: '³����',
        dateFormat: 'dd/mm/yyyy',
        timeFormat: 'hh:ii aa',
        firstDay: 1
    };
    if ($('.datepicker-i').length > 0) {
        $('.datepicker-i').datepicker({
            minDate: new Date(),
            language: 'uk',
            position: "bottom right",
            offset: 0,
            autoClose: true,
            selectOtherYears: false,
            showOtherMonths: false,
            onShow: function (dp, animationCompleted) {
                $('.datepicker-i').parent().addClass('open');
            },
            onHide: function (dp, animationCompleted) {
                $('.datepicker-i').parent().removeClass('open');
                order_calculator();
            }
        });
        //var myDatepicker = $('.datepicker-i').datepicker().data('datepicker');
        //myDatepicker.selectDate(new Date());
    }


    //SHOW-HIDE PASSWORD------------------------------------------------
    $('.show-hide-pass').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next().attr("type", "password");
        } else {
            $(this).addClass('active');
            $(this).next().attr("type", "text");
        }
    });


    jQuery.ui.autocomplete.prototype._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
    }


    jQuery.ui.autocomplete.prototype._renderMenu = function (ul, items) {
        var that = this;
        $.each(items, function (index, item) {
            that._renderItemData(ul, item);
        });
        $(ul).addClass('autocomplete-' + this.element.attr('id'));
    }

    //VALIDATOR------------------------------------------------

    $('.plagiarism-i, .course-i').bind('classChanged', function () {
        if ($(this).hasClass('error')) {
            $(this).prev().addClass('error');
        } else {
            $(this).prev().removeClass('error');
        }
    });

    $('#select-plagiarism').change(function () {
        $('.plagiarism-i').val($(this).val());
        $('.plagiarism-i').valid();
        if ($(this).val() == 'yes') {
            $('.form-item_originality').addClass('show');
        } else {
            $('.form-item_originality').removeClass('show');
        }
    });

    $('#select-course').change(function () {
        $('.course-i').val($(this).val());
        $('.course-i').valid();
    });


    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg !== value;
    }, "Value must not equal arg.");

    $.validator.addMethod("minlenghtphone", function (value, element, arg) {
        return value.replace(/\D+/g, '').length > arg;
    }, " �������� ������� �����!");


    $(".form-profile").validate({
        rules: {
            input_institution: {
                required: true,
                minlength: 5,
            },
            name: {
                required: true,
                minlength: 2,
            },
            tel: {
                required: true,
                minlenghtphone: 11,
            },
            email: {
                required: true,
                email: true
            },
            coursei: {
                required: true,
                valueNotEquals: "�������"
            },
        },
        messages: {
            coursei: {
                required: "���������� ����",
                valueNotEquals: "���������� ����",
            },
            input_institution: {
                required: "���������� ����",
                minlength: '����� ������� �����',
            },
            name: {
                required: "���������� ����",
                minlength: '�� ����� ����� ������� ���',
            },
            tel: {
                required: "���������� ����",
                minlenghtphone: '����� �������� �������',
            },
            email: {
                required: "���������� ����",
                email: "������ �������� �����!"
            },
        },
        submitHandler: function (form) {

        	
        	
        	
        	
        	

            /*$.ajax({
    		   method: $(form).attr('method'),
    		   url: $(form).attr('action'),
    		   dataType: 'json',
    		   data: $(form).serialize()+'&ajax=Y',
    		 })
    		   .done(function( msg ) {
    			   if(msg.error){
    				   alert('�������.');
    				   
    				   return false;
    			   }else{
    				   alert('I�������i� ��������!');
    				   return false;
    			   }
    		   });
    	   */

            //form.reset();
        	
        	
        	
            return true;
        }
    });

    if ($("input[name='input_type'").length > 0 && input_type != '') {
        $("input[name='input_type'").val(input_type);
        
        var value = $('#input_select-type').val();
        
        
        
        if ((value == '�����������') || (value == '��������') || (value == '������������') || (value == '�������')) {
            $('.need-tutorial').addClass('show');
        } else {
            $('.need-tutorial').removeClass('show');
        }
    }

    if ($("input[name='input_institution'").length > 0 && input_institution != '') {
        $("input[name='input_institution'").val(input_institution);
    }

    if ($("input[name='coursei'").length > 0 && coursei != '') {
        $("input[name='coursei'").val(coursei);
    }


    $(".application-form").validate({
        rules: {
            input_type: {
                required: true,
                minlength: 3,
            },
            input_subject: {
                required: true,
                minlength: 3,
            },
            input_institution: {
                required: true,
                minlength: 5,
            },
            themes: {
                required: true,
                minlength: 5,
                normalizer: function( value ) {
                    return $.trim( value );
                  },
            },
            name: {
                required: true,
                minlength: 2,
            },
            tel: {
                required: true,
                minlenghtphone: 11,
            },
            email: {
                required: true,
                email: true
            },
            plagiarismi: {
                required: true,
                valueNotEquals: "�������"
            },
            coursei: {
                required: true,
                valueNotEquals: "�������"
            },
            datepicker: {
                required: true,
            },
        },
        messages: {
            plagiarismi: {
                required: "���������� ����",
                valueNotEquals: "���������� ����",
            },
            coursei: {
                required: "���������� ����",
                valueNotEquals: "���������� ����",
            },
            input_type: {
                required: "���������� ����",
                minlength: '����� ������� �����',
            },
            input_subject: {
                required: "���������� ����",
                minlength: '����� ������� �����',
            },
            input_institution: {
                required: "���������� ����",
                minlength: '����� ������� �����',
            },
            themes: {
                required: "���������� ����",
                minlength: '�� ����� ����� ������� �����',
            },
            name: {
                required: "���������� ����",
                minlength: '�� ����� ����� ������� ���',
            },
            tel: {
                required: "���������� ����",
                minlenghtphone: '����� �������� �������',
            },
            email: {
                required: "���������� ����",
                email: "������ �������� �����!"
            },
            datepicker: {
                required: "���������� ����",
            },
        },
        submitHandler: function (form) {
            //alert('�� ���� ������� ��������!');
            //form.reset();
        	
        	

        	let value = $(form).find('[name="input_type"]').val();
        	
        	let files = $('[name="file_src[]"]').length
        	
        	
        	
        	if ((value == '�����������') || (value == '��������') || (value == '������������') || (value == '�������')) {
        		
        		if(files==0){
        			$('.pop-pop-2').addClass('active');
                    return false;
        		}
                
            } 
        	
        	
        	
            return true;
        }
    });
    
    $('.pop__back-ok>.js-exiter1').click(function(e){
    	e.preventDefault();
    	$('.pop-background').removeClass('active');
    });
    
    $('.pop__back-ok>.js-exiter2').click(function(e){
    	e.preventDefault();
    	$('.file-list__add-item').append('<input type="hidden" name="file_src[]" value="">');
    	$('.pop-background').removeClass('active');
    	$(".application-form").submit();
    })

    $(".change-password").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
            },
            passwordagain: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
        },
        messages: {
            password: {
                required: "���������� ����",
                minlength: '����� �������� ������',
            },
            passwordagain: {
                required: "���������� ����",
                minlength: '����� �������� ������',
                equalTo: '����� ������� ���������!',
            },
        },
        submitHandler: function (form) {


            $.ajax({
                    method: $(form).attr('method'),
                    url: $(form).attr('action'),
                    dataType: 'json',
                    data: $(form).serialize() + '&ajax=Y',
                })
                .done(function (msg) {
                    if (msg.error) {
                        alert('�������.');
                        form.reset();
                        return false;
                    } else {
                        alert('������ ��i����!');
                        form.reset();
                        return false;
                    }
                });

            return false;
        }
    });
    $(".rating").validate({
        rules: {
            reviewText: {
                required: true,
                minlength: 1,
            },
        },
        messages: {
            reviewText: {
                required: "���������� ����",
                minlength: '����� �������� �����',
            },
        },
        submitHandler: function (form) {
            var pop = '.pop-new-review';
            showPop(pop);
            //$('.rating__hidden').slideToggle(150);
            //form.reset();
            return false;
        }
    });
    $(".authorization__form").validate({
        rules: {
            password: {
                required: true,
            },
            email: {
                required: true,
                //email: true
            },
        },
        messages: {
            password: {
                required: "���������� ����",
            },
            email: {
                required: "���������� ����",
                email: "������ �������� �����!"
            },
        },
        submitHandler: function (form) {



            $.ajax({
                    method: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: $(form).serialize() + '&ajax=Y',
                })
                .done(function (msg) {




                    if (msg == 'true') {
                        alert('������� ���� ��� ������.');
                        return false;
                    } else if (msg == 'client') {
                        alert('������ ���� ��� �볺���.');
                        return false;
                    } else if (msg == 'active') {
                        alert('������ �����������.');
                        return false;
                    } else {
                        location.reload();
                        return true;
                    }
                });
        }
    });
    $(".recover-password").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            email: {
                required: "���������� ����",
                email: "������ �������� �����!"
            },
        },
        submitHandler: function (form) {



            $.ajax({
                    method: $(form).attr('method'),
                    url: $(form).attr('action'),
                    dataType: 'json',
                    data: $(form).serialize() + '&ajax=Y',
                })
                .done(function (msg) {
                    if (msg.error) {
                        alert('����������� � ����� ������ �� ��������.');
                        return false;
                    } else {
                        $('.authorization__success').show();
                        
                        $('.authorization__title').hide();
                        $('.form-item').hide();
                        $('.button.button_small').hide();
                        
                        return false;
                    }
                });




            return false;
        }
    });
    $(".finalize").validate({
        rules: {
            datepicker: {
                required: true,
            },
            message: {
                required: true,
            },
        },
        messages: {
            datepicker: {
                required: "���������� ����",
            },
            message: {
                required: "���������� ����",
            },
        },
        submitHandler: function (form) {
            alert('�� ���� ������� ��������!');
            return false;
        }
    });

    $('[name="addReview"]').change(function () {
        $('.rating__hidden').slideToggle(150);
    })



    $('.order__title, .archive__name').shave(40);
    $(window).resize(function () {
        $('.order__title, .archive__name').shave(40);
    });

    $('.chat__body').scrollTop(9999999);

    /*
    $('body').on('click', '.ui-menu-item-wrapper', function () {
        $('#input_select-subject, #input_select-institution, #input_select-type').valid();
        $('#input_select-subject, #input_select-institution, #input_select-type').change();
        alert('1');
    });
    */
    $('body').on('click', '.autocomplete-input_select-type .ui-menu-item-wrapper', function () {
        $('#input_select-type').valid();
        $('#input_select-type').change();
    });
    $('body').on('click', '.autocomplete-input_select-subject .ui-menu-item-wrapper', function () {
        $('#input_select-subject').valid();
        $('#input_select-subject').change();
    });
    $('body').on('click', '.autocomplete-input_select-institution .ui-menu-item-wrapper', function () {
        $('#input_select-institution').valid();
        $('#input_select-institution').change();
    });




    if ($('[name="action"]').val() == 'order_add') {
        order_calculator('first');

        $(document).on('change', 'input, select, textarea', order_calculator);
    }

});
