<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->SetTitle("���� � ������ �볺��� - ��������� ������");
$APPLICATION->SetPageProperty("description",  '���� � ������ �볺��� - ��������� ������');
?>
<?
//ShowMessage($arParams["~AUTH_RESULT"]);


//ShowMessage($arResult['ERROR_MESSAGE']);
//var_dump($_REQUEST);die();

if($_REQUEST['ajax'] == 'Y' && !empty($arParams["~AUTH_RESULT"]) && $arParams["~AUTH_RESULT"]["MESSAGE"]!=''){
    $APPLICATION->RestartBuffer();
    
    echo 'true';die();
    
}

if($_REQUEST['ajax'] == 'Y' && empty($arParams["~AUTH_RESULT"])){
    $APPLICATION->RestartBuffer();
    
    echo 'false';die();
}



?>
<div class="wrapper wrapper_not-header">
        <main class="main main_authorization">
            <div class="content">
                <a href="/" class="logo-full"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo-full.svg" alt=""></a>
                <div class="authorization">
               	<?if(!$USER->IsAuthorized() && !in_array(12, $USER->GetUserGroupArray())): ?>
                    <div class="authorization__title">�����������</div>
                    <form class="authorization__form" name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                    
                    
                    	<input type="hidden" name="AUTH_FORM" value="Y" />
                		<input type="hidden" name="TYPE" value="AUTH" />
                		<?if (strlen($arResult["BACKURL"]) > 0):?>
                		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                		<?endif?>
                		<?foreach ($arResult["POST"] as $key => $value):?>
                		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                		<?endforeach?>
                		
                		<input type="hidden"  id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />
                        <div class="form-item">
                            <div class="form-item__label">E-mail:</div>
                            <input type="email" name="USER_LOGIN" maxlength="255" value="<?=$_GET['logins']?$_GET['logins']:$arResult["LAST_LOGIN"]?>">
                        </div>
                        <div class="form-item">
                            <div class="form-item__label">������:</div>
                            <input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" >
                        </div>    
                        <?if($arResult["SECURE_AUTH"]):?>
            				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
            					<div class="bx-auth-secure-icon"></div>
            				</span>
            				<noscript>
            				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
            					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
            				</span>
            				</noscript>
                            <script type="text/javascript">
                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                            </script>
                        <?endif?>
                        <?if($arResult["CAPTCHA_CODE"]):?>
            					
        					<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
        					<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
        				
        					<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:
        					<input class="bx-auth-input form-control" type="text" name="captcha_word" maxlength="50" value="" size="15" />
            				
            			<?endif;?>
                        <div class="form-item-bottom">
                            <div class="form-item-bottom__button">
                                <button type="submit" class="button button_small" name="Login">����</button>
                            </div>
                            <div class="form-item-bottom__pass">
                                <a href="/client/password_recover/" class="link-style"><span>������ ������?</span></a>
                            </div>
                        </div>                    
                    </form>
                <?else: ?>   
                <div class="authorization__title">������ ���� ��� �볺���</div>
                <?endif; ?>    
                </div>
            </div>
        </main>
        <footer class="footer footer_static">
            <div class="content">
                <ul class="social">
                    <li>�� � ����������:</li>
                    <li><a href="https://www.instagram.com/magistr.in.ua/" target="_blank"><svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.54024 0H10.4598C11.3068 0 11.9999 0.627137 11.9999 1.54012V10.4599C11.9999 11.3729 11.3068 12 10.4598 12H1.54024C0.692931 12 0 11.3729 0 10.4599V1.54012C0 0.627137 0.692931 0 1.54024 0ZM8.74148 1.33325C8.44429 1.33325 8.20145 1.57622 8.20145 1.87353V3.16661C8.20145 3.46379 8.44429 3.70688 8.74148 3.70688H10.0978C10.3949 3.70688 10.638 3.46379 10.638 3.16661V1.87353C10.638 1.57622 10.3949 1.33325 10.0978 1.33325H8.74148ZM10.6437 5.07476H9.58756C9.68748 5.40102 9.74157 5.74662 9.74157 6.10417C9.74157 8.10017 8.07134 9.71816 6.0114 9.71816C3.95158 9.71816 2.2816 8.10017 2.2816 6.10417C2.2816 5.74637 2.33556 5.40089 2.43561 5.07476H1.3335V10.1437C1.3335 10.406 1.54813 10.6208 1.81057 10.6208H10.1669C10.4293 10.6208 10.644 10.4062 10.644 10.1437V5.07476H10.6437ZM6.01127 3.63875C4.68036 3.63875 3.60129 4.68418 3.60129 5.97394C3.60129 7.2637 4.68036 8.30913 6.01127 8.30913C7.34231 8.30913 8.4215 7.2637 8.4215 5.97394C8.4215 4.68418 7.34243 3.63875 6.01127 3.63875Z" fill="currentColor"/></svg></a></li>
                    <li><a href="https://www.facebook.com/magistr.in.ua/" target="_blank"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.875 4.8125V3.0625C7.875 2.5795 8.267 2.1875 8.75 2.1875H9.625V0H7.875C6.42513 0 5.25 1.17513 5.25 2.625V4.8125H3.5V7H5.25V14H7.875V7H9.625L10.5 4.8125H7.875Z" fill="currentColor"/></svg></a></li>
                </ul>
            </div>
        </footer>
    </div>
    
    
    


<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>


