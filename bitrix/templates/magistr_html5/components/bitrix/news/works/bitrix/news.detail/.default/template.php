<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$APPLICATION->AddHeadString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.js"></script>');
//if($USER->IsAdmin()) echo "<pre>".print_r($arResult, true)."</pre>";

//$APPLICATION->AddChainItem($arResult["NAME"]);

?>
<?
CJSCore::Init(array("ajax"));
?>
		<div class="work-list-bg">
			<div class="work-list work-prod">
				<h1><?=$arResult["NAME"]?> <?='(ID:'.$arResult["ID"].')' ?></h1>
				<div class="work_pdesc">
					<table width="100%" cellpadding="0" cellspacing="0">
		<?
		$arSection = $arResult["SECTION"]["PATH"][end(array_keys($arResult["SECTION"]["PATH"]))];
		if($arSection):
		?>
		<tr>
			<td ><strong><?=GetMessage("IBLOCK_FIELD_SECTION_ID")?></strong></td>
			<td><a href="<?=str_replace(array("#SECTION_ID#","#SECTION_CODE#"),array($arSection["ID"],$arSection["CODE"]),$arParams["SECTION_URL"])?>"><?=$arSection["NAME"]?></a></td>
		</tr>
		<?endif;?>
		<?//echo "<pre>"; print_r($arResult["DISPLAY_PROPERTIES"]);?>
		<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<? if($pid == 'work_prev'){
				$work_prev = $arResult["PROPERTIES"][$pid]["~VALUE"];
				continue;
				}//work_prev?>

			<tr>
				<td>
					<strong>
							<?if(strlen(GetMessage("IBLOCK_PROP_".$pid))>0):?>
								<?=GetMessage("IBLOCK_PROP_".$pid)?>
							<?else:?>
								<?=$arProperty["NAME"]?>
							<?endif;?>
					</strong>
				</td>

				<td <?=($pid=="zmist") ? 'class="work_pzmist"' : "";?> ><?=trim($arResult["PROPERTIES"][$pid]["~VALUE"]);?>

				</td>
			</tr>
		<?endforeach;?>


					</table>
				</div>
				<?
				$js_string = "
				<script>
					$(document).ready(function(){";
						
				if(LANGUAGE_ID == 'ru'){
				    $js_string .= "$.ajax({url: '/js-text/ru-js-html-box.html'}).done(function(msg){ $( '.work_pbuy' ).html(msg);});";
                }else{
				    $js_string .= "$.ajax({url: '/js-text/js-html-box.html'}).done(function(msg){ $( '.work_pbuy' ).html(msg);});";
                }
				$js_string .= "	});
				</script>
				";
				$APPLICATION->AddHeadString($js_string,true);
				?>
				
				<div class="work_pbuy" >
				
				<?/* ?><div class="work_pbuy_t1"><?=GetMessage("WORK_ORDER_TITLE_1")?></div>
					<div class="work_pbuy_t2"><?=GetMessage("WORK_ORDER_TITLE_2")?></div>
					<div class="work_pbuy_list">
						<div class="work_pbuy_i">
							<div class="image"><img src="/bitrix/templates/magistr_html5/images/work_pbuy1.png" alt="" /></div>
							<div class="name"><?=GetMessage("WORK_ORDER_TITLE_AUTOR")?> </div>
							<div class="desc"><?=GetMessage("WORK_ORDER_DESC_AUTOR")?></div>
							<a id="authorOrder" href=" javascript: void(0);" onclick="getAutorOrder();" class="buy"><?=GetMessage("WORK_ORDER_BUY")?></a>
						</div>
						<div class="work_pbuy_i">
							<div class="image"><img src="/bitrix/templates/magistr_html5/images/work_pbuy2.png" alt="" /></div>
							<div class="name"><?=GetMessage("WORK_ORDER_TITLE_AGENT")?></div>
							<div class="desc"><?=GetMessage("WORK_ORDER_DESC_AGENT")?></div>
							<a id="agentOrder" href="javascript: void(0);" onclick="getAgencyOrder();" class="buy"><?=GetMessage("WORK_ORDER_BUY")?></a>
						</div>
						<div class="work_pbuy_i">
							<div class="image"><img src="/bitrix/templates/magistr_html5/images/work_pbuy3.png" alt="" /></div>
							<div class="name"><?=GetMessage("WORK_ORDER_TITLE_NEW")?></div>
							<div class="desc"><?=GetMessage("WORK_ORDER_DESC_NEW")?></div>
							<a id="newOrder" href="javascript: void(0);" onclick="getNewOrder();" class="buy"><?=GetMessage("WORK_ORDER_ZERO")?></a>
						</div>
					</div><? */?>
				</div>
				<div class="work_line"></div>
				<?
			if(!empty($work_prev)){
				 ?>
				<div class="work_zrazok">
					<div class="work_zrazok_t">������ ������:</div><?=trim($work_prev);?>
				</div>
				<?}?>

<script>

        $('#box-close2').click(function(){
				$('#nonebox .rev-form').html("");
             });



        function getAutorOrder(){
             $('#overlay').fadeIn('fast',function(){
	             $('#nonebox').animate({'top':'40px'},500);
	             $("html, body").scrollTop(0);

	             $.ajax({
					url: '<?=LANG_ROOT_DIR?>/works/add_order_author.php',
					type: 'post',
					data: 'workId=<?=$arResult["ID"]?>',
					success: function(results){
						$('#nonebox .rev-form').html(results);
					}
				});

             });
              return false;
		};

		function getAgencyOrder(){
             $('#overlay').fadeIn('fast',function(){
	             $('#nonebox2').animate({'top':'40px'},500);
	             $("html, body").scrollTop(0);

	            $.ajax({
					url: '<?=LANG_ROOT_DIR?>/works/add_order_agency.php',
					type: 'post',
					data: 'workId=<?=$arResult["ID"]?>',
					success: function(results){
						$('#nonebox2 .rev-form').html(results);	
						Recaptchafree.reset();					
					}


				});
             });
              return false;
		};

		function getNewOrder(){
             window.location = "<?=LANG_ROOT_DIR?>/order/ref_718";
             return false;
        };



</script>

				<?if($arResult["~DETAIL_TEXT"]):?>
				<div class="work_line"></div>
				<div class="work_zrazok">
					<div class="work_zrazok_t"><?=GetMessage("ZRAZOK")?>:</div>
					<p><?=$arResult["DETAIL_TEXT"];?></p>
				</div>
				<?endif;?>

					<?
						$APPLICATION->IncludeComponent(
							"magistr:otherWorks2",
							"",
							Array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"NOT_SHOW_ID" => $arResult["ID"],
								"SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
								"FOLDER" => $arResult["SECTION_URL"],
							),
							$component
						);
					?>

			</div>
		</div>
<?php 


   


?>