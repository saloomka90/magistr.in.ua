<?
  if (strlen($_POST['ajax_key']) && $_POST['ajax_key']==md5('ajax_'.LICENSE_KEY)) {
	$APPLICATION->RestartBuffer();
	if (!defined('PUBLIC_AJAX_MODE')) {
		define('PUBLIC_AJAX_MODE', true);
	}
	
	
	header('Content-type: application/json');
 	if ($arResult['ERROR']) {
 	    $arResult["ERROR_MESSAGE"]["MESSAGE"] = /*mb_convert_encoding(*/strip_tags($arResult["ERROR_MESSAGE"]["MESSAGE"])/*, "utf-8", "windows-1251")*/;
 	    
 	    
 	    if($arResult["ERROR_MESSAGE"]["MESSAGE"])
		echo json_encode(array(
				'type' => 'error',
		        'message' => ($arResult["ERROR_MESSAGE"]["MESSAGE"] == "������ ���� ��� ������")?"CLIENT":"LOGIN", 
				'newCaptcha'=> $arResult["CAPTCHA_CODE"],
		));
	} else {
		echo json_encode(array('type' => 'ok'));
	} 
	
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
	die();
}  
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["FORM_TYPE"] == "login"):?>
<form id="log_form"  name="system_auth_form<?=$arResult["RND"]?>" method="post" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	
	<div class="rev-val">
		<label>E-mail:<span>*</span></label>
		<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" />
	</div>
	<div class="rev-val">
		<label><?=GetMessage("AUTH_PASSWORD")?>:<span>*</span></label>
		<input id="pass" type="password" name="USER_PASSWORD" />
	</div>	
	
	<div class="rev-val">
		<noindex>
			<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="wrong" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
			<a href="javascript:void(0);" onclick="$('#log-close').click(); $('.reg-bt').click();" class="reg" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
		</noindex>
		<div class="clr"></div>
	</div>

<?if ($arResult["CAPTCHA_CODE"]):?>
	<div class="rev-val">
		<label><?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<span>*</span></label>
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img id="capt" style="vertical-align: middle;" src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			<input style="width: 200px; margin-left: 60px;" type="text" name="captcha_word" maxlength="50" value="" />
		<div class="clr"></div>
	</div>
<?endif?>

<?if ($arResult["STORE_PASSWORD"] == "Y"):?>	
	<div class="rev-val">
		<i><input type="checkbox"  id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /><?echo GetMessage("AUTH_REMEMBER_SHORT")?></i>
		<div class="clr"></div>
	</div>
<?endif?>
	<div class="rev-val">
			<input type="submit" class="log-bt2" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>"/>
			<div class="clr"></div>
	</div>

</form>
<script>

$("#log_form").validate({
	submitHandler: function(form) {
		$("#err_txt").remove();
	    var $this = $(form);
	    var $form = {
	       action: $this.attr('action'),
	       post: {'ajax_key':'<?=md5('ajax_'.LICENSE_KEY)?>'}
	    };
	    
	    $.each($('input', $this), function(){
	       if ($(this).attr('name').length) {
	          $form.post[$(this).attr('name')] = $(this).val();
	       }
	    });
	    $.post($form.action, $form.post, function(data){
	       $('input', $this).removeAttr('disabled');

		     if(data.newCaptcha!= null){
		    	 $("input[name='captcha_sid']").val(data.newCaptcha);
		    	 $("input[name='captcha_word']").val("");
		    	 $("#capt").attr('src','/bitrix/tools/captcha.php?captcha_sid='+data.newCaptcha);
			 }
		      
	       if (data.type == 'error') {
		       
				

			  if( data.message == "CLIENT"){
				  $(form).before('<p id="err_txt" style="color:red;"><?=GetMessage('AUTH_ERR_CLIENT');?></p>' );
			  }else{
				  $(form).before('<p id="err_txt" style="color:red;"><?=GetMessage('AUTH_ERR_LOGIN');?></p>' );
			  }	
	    	  
	          $("input[name='USER_LOGIN']").addClass("error").val("");
	          $("input[name='USER_PASSWORD']").addClass("error").val("");
	          $("input[name='captcha_word']").addClass("error").val("");
	          
	       } else {
	    	   location.reload();
	          
	       }
	    }, 'json');
	    return false; 
	  }, 
	   rules:{
		   USER_LOGIN:{
				required: true,
				
			},
			USER_PASSWORD:{
				required: true,
				
			},
			captcha_word:{
				required: true,
			},
	   },
	   messages:{
		   USER_LOGIN:{
				required: "",
			},
			USER_PASSWORD:{
				required: "",
			},
			captcha_word:{
				required: "",
			},					
	   }
	});	
</script>
<?else:?>
 <!-- Already Auth -->
<?endif?>