<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

if(CSite::InDir('/bakalavrska-robota/') || CSite::InDir('/kursovye_zakaz/') || CSite::InDir('/mahisterska-robota/') || CSite::InDir('/kandydatska-dysertatsiya/')) {
    $newResult = array();
    foreach($arResult as $m=>$res){
        
        if($m == 1){
            $newResult[] = array("TITLE"=>'������� �� ����',"LINK"=>'/prices/');
        }
        $newResult[] = $res;
    }
    
    $arResult = $newResult;
}

if(CSite::InDir('/ru/bakalavrska-robota/') || CSite::InDir('/ru/kursovye_zakaz/') || CSite::InDir('/ru/mahisterska-robota/') || CSite::InDir('/ru/kandydatska-dysertatsiya/')) {
    $newResult = array();
    foreach($arResult as $m=>$res){
        
        if($m == 1){
            $newResult[] = array("TITLE"=>'������ � ����',"LINK"=>'/ru/prices/');
        }
        $newResult[] = $res;
    }
    
    $arResult = $newResult;
}

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			<div class="bx-breadcrumb-item"  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				'.$arrow.'
				<a itemtype="https://schema.org/Thing" itemprop="item" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" >
					<span itemprop="name">'.$title.'</span>
				</a>
				<meta itemprop="position" content="'.($index + 1).'" />
			</div>';
	}
	else
	{
		$strReturn .= '
			<div class="bx-breadcrumb-item" >
				'.$arrow.'
				<span >'.$title.'</span>
				
			</div>';
	}
}

$strReturn .= '<div style="clear:both"></div></div>';

return $strReturn;
