<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

//echo "<pre>"; print_r($arResult);echo "</pre>";

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

?>
<div class="cat-pag">

	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
		
		<?
			
			switch($arResult["nStartPage"]){
			    case 1:
			        $APPLICATION->AddHeadString('<link rel="next" href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["nStartPage"]+1).'" />', true);
			       
			        break;
			    case 2:
			        $APPLICATION->AddHeadString('<link rel="next" href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["nStartPage"]+1).'" />', true);
			        $APPLICATION->AddHeadString('<link rel="prev" href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'" />', true);
			        break;
			    case $arResult['NavPageCount']:
			        $APPLICATION->AddHeadString('<link rel="prev" href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["nStartPage"]-1).'" />', true);
			        break;
			    default:
			        
			        $APPLICATION->AddHeadString('<link rel="next" href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["nStartPage"]+1).'" />', true);
			        $APPLICATION->AddHeadString('<link rel="prev" href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["nStartPage"]-1).'" />', true);
			        
			}
			
			?>
			<span><?=$arResult["nStartPage"]?></span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
		<? /*
			if(($arResult['NavPageNomer']-$arResult["nStartPage"]) == 1){
			    $APPLICATION->AddHeadString('<link rel="prev" href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'">',true);
			}
			if(($arResult['NavPageNomer']-$arResult["nStartPage"]) == -1){
			    $APPLICATION->AddHeadString('<link rel="next" href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'">',true);
			}*/
			?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
		<?else:?>
		<? /*
			if(($arResult['NavPageNomer']-$arResult["nStartPage"]) == 1){
			    $APPLICATION->AddHeadString('<link rel="prev" href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'">',true);
			}
			if(($arResult['NavPageNomer']-$arResult["nStartPage"]) == -1){
			    $APPLICATION->AddHeadString('<link rel="next" href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'">',true);
			}*/
			?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
		
	<?endwhile?>
		<?if(($arResult["NavPageNomer"]<($arResult["NavPageCount"]-$arResult["nPageWindow"])) && $arResult["NavPageCount"] > $arResult["nPageWindow"] ):?>
			...
		<?endif?>
</div>