<?IncludeTemplateLangFile(__FILE__, LANGUAGE_ID);?>
<?php 
CJSCore::Init() ;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />	
	
	 <link rel="apple-touch-icon" href="/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/touch-icon-ipad-retina.png" />	
	<?php 	
	/*if($USER->isAdmin()){
	    echo "<pre>";
	    var_dump($_SERVER);
	}*/
	if(strpos($APPLICATION->GetCurPage(), '/ru/') !== false){
	    if( (strpos($APPLICATION->GetCurPage(), '/works/') !== false) || (strpos($APPLICATION->GetCurPage(), '/reviews/') !== false)){
	?>
<link rel="canonical" href="https://<?=SITE_SERVER_NAME.str_replace("/ru/","/",$APPLICATION->GetCurPage())?>"/>
<?$APPLICATION->ShowViewContent('link_p');?>
<?$APPLICATION->ShowViewContent('link_n');?>
	<?php
	    }
	}else{
	    if(empty($_REQUEST["PAGEN_1"])):
	    ?>
	    <link rel="canonical" href="https://<?=SITE_SERVER_NAME.str_replace("/ru/","/",$APPLICATION->GetCurPage())?>"/>
	    <?
	    endif;
	}
	?>
	<?
	/*
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-1.7.1.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/topSlider.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/kkcountdown.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.spritely-0.6.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.color.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.animateNumber.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/cufon-yui.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tahoma_cufon.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/select/jquery.selectbox.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/popup.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.validate.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.rating.min.js');
	$APPLICATION->AddHeadScript('https://apis.google.com/js/plusone.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/header1.js');
	$APPLICATION->AddHeadScript('/bitrix/templates/magistr/js/jquery.form-2.25.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/header2.js');
	*/
	
	
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.css?v4');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/layout.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/popup.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/select/selectbox.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/skeleton1200.css');
	
	
	?>
	
<?/* ?>	
	
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css?v4" type="text/css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/select/selectbox.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/skeleton1200.css" type="text/css" />	
<?*/ ?>	
	
   
    
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/topSlider.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/kkcountdown.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.spritely-0.6.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.color.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.animateNumber.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/cufon-yui.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/tahoma_cufon.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/select/jquery.selectbox.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/popup.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.rating.min.js"></script>
	<script type="text/javascript" async="" src="https://apis.google.com/js/plusone.js" gapi_processed="true"></script>
	<script type="text/javascript">
		Cufon.replace(".slider-info h3, .title, .title2, .f-title1, .action-txt");
	</script>
	
	<script type="text/javascript" src="/bitrix/templates/magistr/js/jquery.form-2.25.js"></script>

	<script type="text/javascript">
		$(function(){
			var currentYear = (new Date).getFullYear();
			$('.copy span').text(currentYear);			
		});
	    
	   $(document).ready(function() {
				$('.cont-bt > a').toggle(function(){
					$(this).parent().find('.cont-window').show();
					$(this).find('.carr').attr('src', 'images/arr-t.png');
					
					$(document).click(function(event) {
						if(event.target.getAttribute("class") == "next" || 
							event.target.getAttribute("class") == "prev" || 
							event.target.getAttribute("class") == "pag box active"
						){
							return;
							}
						
    					if ($(event.target).closest(".cont-window").length) return;
    						$('.cont-window').hide();
    						event.stopPropagation();
  					});
					

					
				}, function() {
					$(this).parent().find('.cont-window').hide();
					$(this).find('.carr').attr('src', 'images/arr.png');
				});
		});
	</script>
	<style>
	.clientss-bt{
		float: left;
		height: 24px;
		padding: 0 10px;
		line-height: 24px;
		color: #fff !important;
		font-size: 14px;
		background: linear-gradient(to bottom, #75605b 0%,#5c443f 100%);
		border-radius: 5px;
		margin-left: 10px;
		text-decoration: none;
	}
	.clientss-bt:hover{
		background: linear-gradient(to bottom, #5c443f 0%,#75605b 100%);
	}
	@media only screen and (max-width: 489px) {
		.clientss-bt{
			font-size: 12px;
			line-height: 17px;
			height: 17px;
		}
	}
	</style>
<? ?>
<?$APPLICATION->ShowHead();?>	
	<?//$APPLICATION->ShowHeadStrings()?>
	<?//$APPLICATION->ShowHeadScripts()?>

<script src="//code-ya.jivosite.com/widget/EBhT864cym" async></script>

</head>

<body>

	<div class="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="bg">
		<div class="header-bg">
			<div class="header">
				<div class="logo">
					<a href="<?=LANG_ROOT_DIR?>/"><?
						// Contacts
						$APPLICATION->IncludeFile("/include/logo.php", Array(), Array(
						    "MODE"      => "html",                                           
						    "NAME"      => "�������",     
						    "TEMPLATE"  => "" 
						    ));
						?>
					</a>
				</div>
				<div class="header-r">
					<a href="<?=LANG_ROOT_DIR?>/authors_register.php" class="authors-bt">�������</a>
					<a href="/client/" class="clientss-bt">������ �볺���</a>
					<div class="lang">
							
						<a href="<?if(LANGUAGE_ID!="ua"):?><?=str_replace("/".LANGUAGE_ID."/","/",$_SERVER["REQUEST_URI"])?><?endif;?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/uk.png" /></a>
						<?if(!CSite::InDir('/works/')):?>
						<a href="<?if(LANGUAGE_ID!="ru"):?><?="/ru".$_SERVER["REQUEST_URI"];?><?endif;?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/ru.png" /></a>
						<?endif; ?>
					</div>
					<?
						// Contacts
						$APPLICATION->IncludeFile("/include/headerPhones.php", Array(), Array(
						    "MODE"      => "text",                                           
						    "NAME"      => "��������",     
						    "TEMPLATE"  => "" 
						    ));
						?>
					<div class="clr"></div>
					<?$APPLICATION->IncludeComponent("magistr:menu", "horizontal", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<script>
			  
			$(document).ready(function() {
			                $('#near-clouds2').pan({fps: 30, speed: 1, dir: 'right', depth: 70});
                
                window.actions = {
                    speedyClouds: function(){
                        $('#near-clouds2').spSpeed(20);
                    },
                    runningClouds: function(){
                        $('#near-clouds2').spSpeed(12);
                    },
                    walkingClouds: function(){
                        $('#near-clouds2').spSpeed(5);
                    },
                    lazyClouds: function(){
                        $('#near-clouds2').spSpeed(1);
                    },
                    stop: function(){
                        $('#near-clouds2').spStop();
                    },
                    start: function(){
                        $('#near-clouds2').spStart();
                    },
                    toggle: function(){
                        $('#near-clouds2').spToggle();
                    },
                    left: function(){
                        $('#near-clouds2').spChangeDir('left');                    
                    },
                    right: function(){
                        $('#near-clouds2').spChangeDir('right');                    
                    }
                };
            }); 
		</script>
		
		
		<? 
		
		if(!$APPLICATION->GetPageProperty("NOTSHOWTITLE")):?>
			<div class="title-line-bg">
				<div id="near-clouds2" class="near-clouds2 stage"></div>
				<div class="title-line"><?$APPLICATION->ShowTitle(false);?></div>
			</div>
		<?endif;?>
		
		<?if(CSite::InDir('/bakalavrska-robota/') || CSite::InDir('/kursovye_zakaz/') || CSite::InDir('/zvit-z-praktyky/') || CSite::InDir('/mahisterska-robota/') || CSite::InDir('/kandydatska-dysertatsiya/')) :
		?>
		<div class="rev-page-bg" ><div class="rev-page" style="padding:0px;">
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","bca",Array(
                "START_FROM" => "0", 
                "PATH" => "", 
                "SITE_ID" => "s1" 
                )
            );?>
    </div></div>
    <?endif; ?>
    
    <?if(CSite::InDir('/ru/bakalavrska-robota/') || CSite::InDir('/ru/kursovye_zakaz/') || CSite::InDir('/ru/zvit-z-praktyky/') || CSite::InDir('/ru/mahisterska-robota/') || CSite::InDir('/ru/kandydatska-dysertatsiya/')) :
		?>
		<div class="rev-page-bg" ><div class="rev-page" style="padding:0px;">
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","bca",Array(
                "START_FROM" => "1", 
                "PATH" => "", 
                "SITE_ID" => "s1" 
                )
            );?>
    </div></div>
    <?endif; ?>
