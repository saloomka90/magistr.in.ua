<?
// ���� /bitrix/php_interface/init.php
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log_artteh.txt");
AddEventHandler("main", "OnSendUserInfo", "MyOnSendUserInfoHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("OrderActivate","OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("OrderActivate","OnAfterIBlockElementUpdateHandler"));
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");


// ������� ���������� ������� "OnBeforeUserRegister"
function OnBeforeUserRegisterHandler(&$arFields)
{
	foreach ($arFields as $key=>$val){
		if($val){
			$arFieldsEncoded[$key] = CUtil::ConvertToLangCharset($val);
		}
	}

	if(is_array($arFieldsEncoded) && count($arFieldsEncoded)>0){
		$arFields = $arFieldsEncoded;
	}

}



function MyOnSendUserInfoHandler(&$arParams)
{
	/*$ar_lang = array("ru");
	foreach($ar_lang as $lang)
		if(substr($_SERVER["PHP_SELF"], 0, strlen($lang)+2)=="/".$lang."/")
			$lang_id = ToUpper($lang);
			
	CEvent::Send("USER_INFO".(strlen($lang_id)>0?"_".$lang_id:""), $arParams["SITE_ID"], $arParams["FIELDS"]);
	//CEvent::CheckEvents();

	unset($arParams);*/
}

function OnBeforeIBlockElementAddHandler(&$arFields)
{
	global $APPLICATION;
	
	if($arFields["IBLOCK_ID"]==6)
	{
		if(strlen($arFields["PROPERTY_VALUES"][46])>0)
		{
			$rs = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>8,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","PROPERTY_discount_code"=>$arFields["PROPERTY_VALUES"][46]),false,array("nTopCOunt"=>1),array("ID"));
			if($ar=$rs->Fetch()) $arFields["PROPERTY_VALUES"][46] = $ar["ID"];
			else
			{
				switch(LANG_ID)
				{
					case "ru":
						$msg = "��� ������ ������ �� �����! ��������� ������������ ����� � ���������� ��� ���.";
						break;
					case "ua":
						$msg = "��� ������ �������� �� ����! �������� ������������ �������� �� ��������� �� ���.";
						break;
				}
				$APPLICATION->ThrowException($msg);
				return false;
			}
		}
		if(!defined("ADMIN_SECTION") || ADMIN_SECTION!==true)
		{
			if(isset($_COOKIE["referal_id"]) && time()-$_COOKIE["referal_click_time"]<=86400 && in_array(7, CUser::GetUserGroup($_COOKIE["referal_id"])))
			{
				$arFields["PROPERTY_VALUES"][47] = $_COOKIE["referal_id"];
			}
		}
	}
}

// ������� ���������� ������� "OnAfterIBlockElementAdd"
function OnAfterIBlockElementAddHandler(&$arFields)
{
	if($arFields["ID"]>0)
	{
		$ar_lang = array("ru");
		foreach($ar_lang as $lang)
			if(substr($_SERVER["PHP_SELF"], 0, strlen($lang)+2)=="/".$lang."/")
				$lang_id = ToUpper($lang);

		if($arFields["IBLOCK_ID"]==5)
		{
			$res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
			$arIBlock = $res->GetNext();

			$arFilter = array("IBLOCK_ID"=>$arFields["IBLOCK_ID"], "ID"=>$arFields["ID"], "!PROPERTY_order_of"=>false);
			$arSelect = array("ID", "IBLOCK_ID", "CREATED_BY", "NAME", "PROPERTY_phone", "PROPERTY_email", "PROPERTY_note", "PROPERTY_order_of", "PROPERTY_payment_type", "PROPERTY_parent");
			
			$rs_list = CIBlockElement::GetList(array("id"=>"asc"), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ar_elem=$rs_list->GetNext())
			{
				$rs_clist = CIBlockElement::GetList(array("id"=>"asc"), array(/*"IBLOCK_ID"=>$arFields["IBLOCK_ID"],*/ "ID"=>$ar_elem["PROPERTY_PARENT_VALUE"]), false, array("nTopCount"=>1), array("ID", "IBLOCK_ID", "NAME", "CREATED_BY"));
				$ar_celem=$rs_clist->GetNext();

				$rsUser = CUser::GetByID($ar_celem["CREATED_BY"]);
				$arUser = $rsUser->Fetch();

				$arEventFields = array(
				    "NAME"		=> $ar_elem["NAME"],
				    "PHONE"		=> $ar_elem["PROPERTY_PHONE_VALUE"],
				    "USER_EMAIL"		=> $ar_elem["PROPERTY_EMAIL_VALUE"],
				    "NOTE"		=> $ar_elem["PROPERTY_NOTE_VALUE"],
				    "ID"		=> $ar_elem["ID"],
				    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
				    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"],
				    "WORK_TITLE"	=> $ar_celem["NAME"],
				    "EMAIL"		=> $arUser["EMAIL"]
				    );

				$rsSites = CSite::GetList($by="sort", $order="asc", Array("ACTIVE" => "Y"));
				while ($arSite = $rsSites->Fetch()) $arrSITE[] = $arSite["ID"];
				CEvent::Send("NEW_ORDER_AUTHOR".(strlen($lang_id)>0?"_".$lang_id:""), $arrSITE, $arEventFields);
				CEvent::CheckEvents();
			}
			else
			{
				unset($arFilter["!PROPERTY_order_of"]);
				$rs_list = CIBlockElement::GetList(array("id"=>"asc"), $arFilter, false, array("nTopCount"=>1), $arSelect);
				if($ar_elem=$rs_list->GetNext())
				{
					$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"state"));
					while($ar_props = $db_props->Fetch())
					{
						if($ar_props["VALUE_ENUM_ID"]<=0)
						{
							$db_enum_list = CIBlockProperty::GetPropertyEnum("state", Array("sort"=>"asc"), Array("IBLOCK_ID"=>$ar_elem["IBLOCK_ID"]));
							if($ar_enum_list=$db_enum_list->GetNext())
							{
								CIBlockElement::SetPropertyValueCode($ar_elem["ID"], "state", array("VALUE"=>$ar_enum_list["ID"]));
							}
						}
					}

					$rs_clist = CIBlockElement::GetList(array("id"=>"asc"), array("ID"=>$ar_elem["PROPERTY_PARENT_VALUE"]), false, array("nTopCount"=>1), array("ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL", "CREATED_BY"));
					$ar_celem=$rs_clist->GetNext();

					$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"payment_type"));
					if($ar_props = $db_props->Fetch())
						$payment = $ar_props["VALUE_ENUM"];

					$arEventFields = array(
					    "NAME"			=> $ar_elem["NAME"],
					    "PHONE"			=> $ar_elem["PROPERTY_PHONE_VALUE"],
					    "EMAIL"			=> $ar_elem["PROPERTY_EMAIL_VALUE"],
					    "NOTE"			=> $ar_elem["PROPERTY_NOTE_VALUE"],
					    "PAYMENT"		=> $payment,
					    "ID"			=> $ar_elem["ID"],
					    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
					    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"],
					    "WORK_TITLE"	=> $ar_celem["NAME"],
					    "WORK_ID"	=> $ar_celem["ID"],
					    "WORK_LINK"	=> $ar_celem["DETAIL_PAGE_URL"]
					    );
					
					$rsUser = CUser::GetByID($ar_celem["CREATED_BY"]);
					if($arUser=$rsUser->GetNext())
					{
						foreach($arUser as $key => $val) $arEventFields["USER_".$key] = $val;
						$arGroups = CUser::GetUserGroup($arUser["ID"]);
						if(in_array(5,$arGroups)) $g_id = 5;
						else $g_id = 3;
						$rsGroup = CGroup::GetByID($g_id);
						$arGroup = $rsGroup->Fetch();
						$arEventFields["USER_GROUP_NAME"] = $arGroup["NAME"];
					}

					$rsSites = CSite::GetList($by="sort", $order="asc", Array("ACTIVE" => "Y"));
					while ($arSite = $rsSites->Fetch()) $arrSITE[] = $arSite["ID"];
					CEvent::Send("NEW_ORDER_AGENCY", $arrSITE, $arEventFields);
					CEvent::CheckEvents();
				}
			}
		}
		elseif($arFields["IBLOCK_ID"]==6)
		{
			$_SESSION["LAST_ORDER_ID"] = $arFields["ID"];
			
			$res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
			$arIBlock = $res->GetNext();

			$arFilter = array("IBLOCK_ID"=>$arFields["IBLOCK_ID"], "ID"=>$arFields["ID"]);
			$arSelect = array(
				"ID", "IBLOCK_ID", "NAME",
				"PROPERTY_email", "PROPERTY_phone", "PROPERTY_time_to_call",
				"PROPERTY_city_region", "PROPERTY_subject", "PROPERTY_topic",
				"PROPERTY_type", "PROPERTY_period", /*"PROPERTY_lang",*/
				"PROPERTY_pages", "PROPERTY_period", /*"PROPERTY_pract",*/
				/*"PROPERTY_graf_tabl",*/ "PROPERTY_organ", /*"PROPERTY_payment",*/
				"PROPERTY_format", "PROPERTY_task", "PROPERTY_file", "PROPERTY_pref_price"
			);
			
			$rs_list = CIBlockElement::GetList(array("id"=>"asc"), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ar_elem=$rs_list->GetNext())
			{
				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"payment"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"lang"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"pract"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"graf_tabl"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];
				
				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"sphere"));
				while($ar_props = $db_props->Fetch())
				{
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];
					$sphere_codes[] = $ar_props["VALUE_XML_ID"];
				}
					
				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"state"));
				while($ar_props = $db_props->Fetch())
				{
					if($ar_props["VALUE_ENUM_ID"]<=0)
					{
						$db_enum_list = CIBlockProperty::GetPropertyEnum("state", Array("sort"=>"asc"), Array("IBLOCK_ID"=>$ar_elem["IBLOCK_ID"]));
						if($ar_enum_list=$db_enum_list->GetNext())
						{
							CIBlockElement::SetPropertyValueCode($ar_elem["ID"], "state", array("VALUE"=>$ar_enum_list["ID"]));
						}
					}
				}
						
				$arEventFields = array(
				    "NAME"		=> $ar_elem["NAME"],
				    "EMAIL"		=> $ar_elem["PROPERTY_EMAIL_VALUE"],
				    "PHONE"		=> $ar_elem["PROPERTY_PHONE_VALUE"],
				    "TIME_TO_CALL"	=> $ar_elem["PROPERTY_TIME_TO_CALL_VALUE"],
				    "CITY_REGION"	=> $ar_elem["PROPERTY_CITY_REGION_VALUE"],
				    "SUBJECT"		=> $ar_elem["PROPERTY_SUBJECT_VALUE"],
				    "TOPIC"		=> $ar_elem["PROPERTY_TOPIC_VALUE"],
				    "SPHERE"	=> $_str_sphere,
				    "TYPE"		=> $ar_elem["PROPERTY_TYPE_VALUE"],
				    "PERIOD"		=> $ar_elem["PROPERTY_PERIOD_VALUE"],
				    "LANG"		=> $_str_lang,
				    "PAGES"		=> $ar_elem["PROPERTY_PAGES_VALUE"],
				    "PRACT"		=> $_str_pract,
				    "GRAF_TABL"		=> $_str_graf_tabl,
				    "ORGAN"		=> $ar_elem["PROPERTY_ORGAN_VALUE"],
				    "PAYMENT"		=> $_str_payment,
				    "FORMAT"		=> $ar_elem["PROPERTY_FORMAT_VALUE"],
				    "TASK"		=> $ar_elem["PROPERTY_TASK_VALUE"],
					"PRICE"		=> $ar_elem["PROPERTY_PREF_PRICE_VALUE"],
				    "FILE"		=> ($ar_elem["PROPERTY_FILE_VALUE"]?("http://".$_SERVER["HTTP_HOST"]."/zipfiles.php?oid=".$ar_elem["ID"]):""),
				    "ID"		=> $ar_elem["ID"],
				    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
				    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"]
				    );
					
				$rsUF = CUserTypeEntity::GetList( array($by=>$order), array("FIELD_NAME"=>"UF_SPHERE") );
				while($arUF=$rsUF->Fetch())
				{
					if($arUF["USER_TYPE_ID"]=="enumeration")
					{
						$rsEnum = call_user_func_array(
							array("CUserTypeEnum", "getlist"),
							array(
								$arParams['arUserField'],
							)
						);
						while($arEnum = $rsEnum->GetNext())
						{
							if(in_array($arEnum["XML_ID"],$sphere_codes)) $sphere_ids[] = $arEnum["ID"];
						}
					}
				}

				$rsSites = CSite::GetList($by="sort", $order="asc", Array("ACTIVE" => "Y"));
				while ($arSite = $rsSites->Fetch()) $arrSITE[] = $arSite["ID"];
				CEvent::Send("NEW_ORDER_AGENCY_NEW_WORK", $arrSITE, $arEventFields);
				CEvent::Send("NEW_WORK_MESSAGE_TO_INITIATOR".(strlen($lang_id)>0?"_".$lang_id:""), $arrSITE, $arEventFields);
				
                                /** ��� ���� ��� ��������� �� ������� ���������� �������� **/
                                
				$arFilter = Array(
						"ACTIVE"	=> "Y",
						"GROUPS_ID"	=> Array(5),
						"!ID"		=> Array(7),
						//"ID"		=> 2,
					);
				$emails = "";
				$rsUsers = CUser::GetList(($by="email"), ($order="asc"), $arFilter, array("SELECT"=>array("UF_*")));
				while($usr=$rsUsers->GetNext())
				{
					if(sizeof(array_intersect($sphere_ids,$usr["UF_SPHERE"])))
						$emails .= (strlen($emails)>0?", ":"").$usr["EMAIL"];
				}
				$arEventFields = array(
				    "TOPIC"		=> $ar_elem["PROPERTY_TOPIC_VALUE"],
				    "ID"		=> $ar_elem["ID"],
				    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
				    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"],
				    "EMAIL_TO"		=> $emails
				    );
				//echo "<pre>"; print_r($arEventFields); echo "</pre>";
				//exit();
				//if(strlen($emails)>0) CEvent::Send("NEW_ORDER_AGENCY_NEW_WORK_AUTHORS", $arrSITE, $arEventFields);
				
				CEvent::CheckEvents();
			}
		}
		elseif($arFields["IBLOCK_ID"]==3)
		{
			$res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
			$arIBlock = $res->GetNext();

			$arFilter = array("IBLOCK_ID"=>$arFields["IBLOCK_ID"], "ID"=>$arFields["ID"]);
			$arSelect = array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PROPERTY_email");
			
			$rs_list = CIBlockElement::GetList(array("id"=>"asc"), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ar_elem=$rs_list->GetNext())
			{
				$arEventFields = array(
				    "NAME"			=> $ar_elem["NAME"],
				    "EMAIL"			=> $ar_elem["PROPERTY_EMAIL_VALUE"],
				    "TEXT"			=> $ar_elem["PREVIEW_TEXT"],
				    "ID"			=> $ar_elem["ID"],
				    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
				    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"]
				    );

				$rsSites = CSite::GetList($by="sort", $order="asc", Array("ACTIVE" => "Y"));
				while ($arSite = $rsSites->Fetch()) $arrSITE[] = $arSite["ID"];
				CEvent::Send("NEW_REVIEW", $arrSITE, $arEventFields);
				CEvent::CheckEvents();
			}
		}
	}
}

if(isset($_REQUEST["PARTNER_REFERAL_ID"]) && $_REQUEST["PARTNER_REFERAL_ID"]>0)
{
	if(!isset($_COOKIE["referal_id"]))
	{
		setcookie("referal_id", (int)$_REQUEST["PARTNER_REFERAL_ID"], time()+86400*365, "/", $_SERVER["HTTP_HOST"]);
		setcookie("referal_click_time", time(), time()+86400*365, "/", $_SERVER["HTTP_HOST"]);
	}
}
//AddMessage2Log("TEST");

class OrderActivate{
    // ������� ���������� ������� "OnAfterIBlockPropertyUpdate"
   
 function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
      
      if($arFields["IBLOCK_ID"]==6)
	{
          
          $res = CIBlockElement::GetByID($arFields["ID"]);
          if($ar_res = $res->GetNext()){
           $ActiveOld = $ar_res["ACTIVE"];
           $GLOBALS['ACTIVE_OLD'][$arFields["ID"]] = $ActiveOld; // ��������� �������� ���������� �� ���������.
          
         }
          

          //AddMessage2Log("Old mame: ".$oldName);
           
        }
           
    }


 function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
      global $ACTIVE_OLD;  
      
      if($arFields["IBLOCK_ID"]==6)
	{
          
          if ($ACTIVE_OLD[$arFields["ID"]] == "N" && $arFields["ACTIVE"] == "Y"){
             //SendEmails

			 
           $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
			$arIBlock = $res->GetNext();

			$arFilter = array("IBLOCK_ID"=>$arFields["IBLOCK_ID"], "ID"=>$arFields["ID"]);
			$arSelect = array(
				"ID", "IBLOCK_ID", "NAME",
				"PROPERTY_email", "PROPERTY_phone", "PROPERTY_time_to_call",
				"PROPERTY_city_region", "PROPERTY_subject", "PROPERTY_topic",
				"PROPERTY_type", "PROPERTY_period", /*"PROPERTY_lang",*/
				"PROPERTY_pages", "PROPERTY_period", /*"PROPERTY_pract",*/
				/*"PROPERTY_graf_tabl",*/ "PROPERTY_organ", /*"PROPERTY_payment",*/
				"PROPERTY_format", "PROPERTY_task", "PROPERTY_file", "PROPERTY_pref_price"
			);
			
			$rs_list = CIBlockElement::GetList(array("id"=>"asc"), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ar_elem=$rs_list->GetNext())
			{
				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"payment"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"lang"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"pract"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];

				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"graf_tabl"));
				while($ar_props = $db_props->Fetch())
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];
				
				$db_props = CIBlockElement::GetProperty($ar_elem["IBLOCK_ID"], $ar_elem["ID"], "sort", "asc", Array("CODE"=>"sphere"));
				while($ar_props = $db_props->Fetch())
				{
					${"_str_".$ar_props["CODE"]} = $ar_props["VALUE_ENUM"];
					$sphere_codes[] = $ar_props["VALUE_XML_ID"];
				}
			}		
				
						
				
					
				$rsUF = CUserTypeEntity::GetList( array($by=>$order), array("FIELD_NAME"=>"UF_SPHERE") );
				while($arUF=$rsUF->Fetch())
				{
					if($arUF["USER_TYPE_ID"]=="enumeration")
					{
						$rsEnum = call_user_func_array(
							array("CUserTypeEnum", "getlist"),
							array(
								$arParams['arUserField'],
							)
						);
						while($arEnum = $rsEnum->GetNext())
						{
							if(in_array($arEnum["XML_ID"],$sphere_codes)) $sphere_ids[] = $arEnum["ID"];
						}
					}
				}

				$rsSites = CSite::GetList($by="sort", $order="asc", Array("ACTIVE" => "Y"));
				while ($arSite = $rsSites->Fetch()) $arrSITE[] = $arSite["ID"];
				
				$arFilter = Array(
						"ACTIVE"	=> "Y",
						"GROUPS_ID"	=> Array(5),
						"!ID"		=> Array(7),
						//"ID"		=> 2,
					);
				$emails = "";
				$rsUsers = CUser::GetList(($by="email"), ($order="asc"), $arFilter, array("SELECT"=>array("UF_*")));
				while($usr=$rsUsers->GetNext())
				{
					if(sizeof(array_intersect($sphere_ids,$usr["UF_SPHERE"]))){
						$emails .= (strlen($emails)>0?", ":"").$usr["EMAIL"];
						$arMails[] = $usr["EMAIL"];
					}	
				}
				$arEventFields = array(
				    "TOPIC"		=> $ar_elem["PROPERTY_TOPIC_VALUE"],
				    "ID"		=> $ar_elem["ID"],
				    "IBLOCK_ID"		=> $ar_elem["IBLOCK_ID"],
				    "IBLOCK_TYPE"	=> $arIBlock["IBLOCK_TYPE_ID"],
				    //"EMAIL_TO"		=> $emails
				);
					
				/* if(strlen($emails)>0){
					CEvent::Send("NEW_ORDER_AGENCY_NEW_WORK_AUTHORS", $arrSITE, $arEventFields);
					CEvent::CheckEvents();
					//AddMessage2Log($arFields["ID"]."-".$arFields["NAME"]);
				} */
				if(isset($arMails) && count($arMails)>0){
					foreach ($arMails as $mail){
						$arEventFields["EMAIL_TO"] = $mail; 
						CEvent::Send("NEW_ORDER_AGENCY_NEW_WORK_AUTHORS", $arrSITE, $arEventFields);
						
					}
					CEvent::CheckEvents();
					
				}
				
          }
        }    
    }

}



// Included Classed
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/ext/extTools.php");?>